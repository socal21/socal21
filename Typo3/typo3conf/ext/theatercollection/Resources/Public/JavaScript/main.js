$(function() {
	$.toaster({ settings : {'toaster': {'css': {'top': '50%', 'right': '50%', 'height': '50%', 'width': '50%', 'transform': 'translate(50%, -50%)'}}, 'timeout': 6000} });
	$loader = $('#joAjaxloader');

	var $save_div = $('.saved-message'); 
	if ($save_div.length) {
		$.toaster($save_div.text(), 'Achtung', 'success');
	}
	
	$('.joMainWrapper').on('click', '.build_container .item, .action_container .edit', function() {
		var $that = $(this);
		var url = $that.data('url');

		$loader.show();

		if(typeof url != 'undefined' && url != '') {
			$.get(url, function(data) {
				$loader.hide();
				$('.build_container > h4').fadeOut(400);
				$('.select-container').fadeOut(400);
				$('.output-container').fadeOut(400, function() {
					$('.output-container').html(data).promise().done(function() {
						$('.output-container').fadeIn();

						newEventActions();
					});
				});
			});
		} else {
			$loader.hide();
		}
	});

	$('.joMainWrapper').on('click', '.action_container .copy', function() {
		var $that = $(this);
		var url = $that.data('url');

		$loader.show();

		if(typeof url != 'undefined' && url != '') {
			$.get(url, function(data) {
				var newUrl = $(data).find('.alert-success');

				if(newUrl.length) {
					newUrl = newUrl.text();
					$.get(newUrl, function(data2) {
						$loader.hide();
						$('.build_container > h4').fadeOut(400);
						$('.select-container').fadeOut(400);
						$('.output-container').fadeOut(400, function() {
							$('.output-container').html(data2).promise().done(function() {
								$('.output-container').fadeIn();

								newEventActions();
							});
						});
					});
				} else {
					$loader.hide();
				}

			});
		} else {
			$loader.hide();
		}
	});

	$('.joMainWrapper').on('click', '.action_container .delete', function() {
		var $that = $(this);
		var url = $that.data('url');
		var search = $that.data('search');
		
		var $modal = $('#formModal');
		$modal.find('.modal-footer .btn-primary').show();

		if(typeof url != 'undefined' && url != '' && typeof search != 'undefined' && search != '') {
			$loader.show();
			$.get(search, function(data) {
				$loader.hide();
				$modal.find('.modal-title').html('Löschen?');
				$modal.find('.modal-body').html(data);

				var cancelled = data.indexOf('Cancelled') != -1;

				if (cancelled) {
					$modal.find('.modal-body').append('<div><p>Ausfallgrund:</p><textarea class="delete-reason"></textarea></div>');					
				}

				$btn = $modal.find('.modal-footer .btn-primary');
				$btn.off('click');
				$btn.on('click', function() {
					$loader.show();
					if (cancelled) {
						// @all vllt irgendwie sauberer machen, den cHash entfernen
						url = url.split('&cHash=')[0];
						url += '&tx_theatercollection_editor%5Bcancelled%5D=true';
					}
					var $text = $modal.find('.delete-reason');
					if ($text.length) {
						url += '&tx_theatercollection_editor%5Breason%5D=' + $text.val();
					}
					$.get(url, function(data) {
						var cancelled = data.indexOf('Cancelled') != -1;
						console.log(data);
						console.log(cancelled);

						$loader.hide();
						$modal.find('.modal-body').html(data);
						$btn.hide();
						$btn.off();
						
						if (cancelled) {
							// $that.closest('.list-events-item').fadeOut(400, function() { /* do something */});
							$that.closest('.list-events-item').addClass('item-hidden-ausfall');
						} else {
							$that.closest('.list-events-item').fadeOut(400, function() {$that.closest('.list-events-item').remove()});
						}
					});
				});

				$modal.modal('show');
			});
		}
	});

	$pageUrl = $('.currentPage');
	if($pageUrl.length) {
		var url = $pageUrl.attr('href');
		window.history.pushState(url, null, url);
	}

	$(window).on('popstate', function() {
  		location.reload(true);
   	});

	function newEventActions() {
		$('[data-toggle="popover"]').popover();

		var $lockItem = $('.lockupdate'); 
		if ($lockItem.length) {
			var lockurl = $lockItem.attr('href');
			if (typeof lockurl != 'undefined' && lockurl != '') {
				var lockInterval = setInterval(function() {
		    		$.get(lockurl, function() {});
		    	}, 60000);

		    	$.get(lockurl, function() {});
			}
		}

		$('input[maxlength]').on('keyup', function(e) {
			var val = $(this).val();
			var maxlength = $(this).attr('maxlength');

			if(val.length >= maxlength) {
				$.toaster('Maximale länge der Eingabe erreicht.', 'Achtung', 'danger');
				//$(this).val(val.substr(0, maxlength));
			}
		});

		$('.vali-url').on('keyup', function(e) {
			var val = $(this).val();

			if(typeof val != 'undefined' && val != '' && !val.startsWith('http://') && !val.startsWith('https://')) {
				$(this).val('https://' + val);
			}
		});

		$('.vali-url2').on('keyup', function(e) {
			var val = $(this).val();

			if(typeof val != 'undefined' && !val.startsWith('https://')) {
				$(this).val('https://' + val);
			}
		});

		/*
		$('.datepicker_2 .datepicker').on('changeDate', function(event) {
			$( ".master_datepicker_2" ).clone().appendTo(".clonestimeline").promise().done(function() {
				updateContainerHeight();
			});

			console.log(event);
			//console.log(event.date);
			//console.log(event.date.format('MM/DD/YYYY'));
		});
		*/

		var $date_range = $('[data-target="daterange"]');

		if ($date_range.length) {
			$date_range.each(function() {
				var $that = $(this);
				var $target_start = $($that.data('start'));
				var $target_end = $($that.data('end'));

				var $target_start_dp = $target_start.find('.form_datepicker');
				var $target_end_dp = $target_end.find('.form_datepicker');

				var $sideInputs = $target_start.find('.date-input-container input');

				var $prototyp = $that.find('.prototyp');
				var $container = $that.find('.timeline-clones');

				var $output_con = $that.find('.output .output-con');

				if ($target_start.length && $target_end.length && $prototyp.length && $container.length && $target_start_dp.length && $target_end_dp.length) {
					function calcDate(event) {
						var start_date = $target_start_dp.data('datepicker').getDate();
						var end_date = $target_end_dp.data('datepicker').getDate();
						
						var dates = [];

						if (start_date != null && start_date != '' && (end_date == null || end_date == '')) {
							dates = [start_date];
						}

						if (start_date != null && start_date != '' && end_date != null && end_date != '') {
							if(start_date > end_date) {
								// $container.empty();
								$container.find('.cloned').not('.no-del').remove();
								updateContainerHeight();
								$.toaster('Das Startdatum ist größer als das Enddatum.', 'Achtung', 'danger');

								return false;
							}
							
							while (start_date < end_date) {
								dates = [...dates, new Date(start_date)];
								// dates.push(new Date(start_date));
								start_date.setDate(new Date(start_date.getDate() + 1));
							}
							dates = [...dates, new Date(end_date)];
							// dates.push(end_date);
						}

						if (dates.length) {
							//dates.reverse();
							var $old_dates = $container.find('.cloned');
							var $old_dates_arr = [];

							$old_dates.each(function(i, v) {
								var id = $(v).data('id');
								$old_dates_arr[id] = v;							
							});

							// $container.empty();

							var $start_time = $target_start.find('#' + $that.data('prefix') + '-start-date-time-beginn');
							var $end_time = $target_start.find('#' + $that.data('prefix') + '-start-date-time-end');
							var $einlass = $target_start.find('#' + $that.data('prefix') + '-start-date-einlass');
							var $dauer = $target_start.find('#' + $that.data('prefix') + '-start-date-dauer');

							var div = '';

							// var ort = $prototyp.find('.ort').val();
							var ort = $prototyp.find('.ort option:selected').text();

							for (var i = 0; i < dates.length; i++) {
								var tmp_date = dates[i].getDate() + '.' + (dates[i].getMonth()+1) + '.' + dates[i].getFullYear();

								if(tmp_date in $old_dates_arr) continue; 

								var $clone = $prototyp.clone();

								$clone.removeClass('prototyp').addClass('cloned col-md-12 d-flex');
								$clone.find('input.datum').val(tmp_date);
								$clone.find('input.datum_out').val(dates[i].toLocaleDateString('de-DE', {weekday: 'short', year: 'numeric', month: 'numeric', day: 'numeric'}));

								$clone.find('input.von').val($start_time.val());
								$clone.find('input.bis').val($end_time.val());
								$clone.find('input.einlass').val($einlass.val());
								$clone.find('input.dauer').val($dauer.val());

								$clone.find('input, select').each(function() {
									if(typeof $(this).attr('name') != 'undefined') {
										$(this).attr('name', $(this).attr('name').replace('[0]', '[' + i + ']'));
									}
								});

								$clone.data('id', tmp_date);

								$clone.appendTo($container).promise().done(function() {
									$clone.fadeIn(400, function() {
										if(i == dates.length-1) {
											updateContainerHeight();
										}
									});
								});

								div += '<div class="row">' +
									'<div class="form-control col datum_out">' + dates[i].toLocaleDateString('de-DE', {weekday: 'short', year: 'numeric', month: 'numeric', day: 'numeric'}) + '</div>' +
									'<div class="form-control col von">' + $start_time.val() + '</div>' +
									'<div class="form-control col bis">' + $end_time.val() + '</div>' +
									'<div class="form-control col einlass">' + $einlass.val() + '</div>' +
									'<div class="form-control col dauer">' + $dauer.val() + '</div>' +
									'<div class="form-control col ort" title="' + ort + '">' + ort + '</div>' +
								'</div>';
							}

							$output_con.html(div);

						}
					}

					// $target_start_dp.add($target_end_dp).on('changeDate', calcDate);
					$target_end_dp.on('changeDate', calcDate);

					$target_start_dp.on('changeDate', function() {
						$('.datepicker_x').addClass('active');
					});

					$('.down-day').click(function() {
						$('.datepicker_2').addClass('active');
					});

					$('.crt-day').click(calcDate);

					// $sideInputs.keyup(calcDate);

					$container.on('keyup change', 'input, select', function() {
						var $items = $container.find('.cloned');

						var div = '';

						$items.each(function(i,v) {
							$tmp = $(v);
							if($tmp.find('.check').prop('checked')) {
								div += '<div class="row">' +
									'<div class="form-control col datum_out">' + $tmp.find('.datum_out').val() + '</div>' +
									'<div class="form-control col von">' + $tmp.find('.von').val() + '</div>' +
									'<div class="form-control col bis">' + $tmp.find('.bis').val() + '</div>' +
									'<div class="form-control col einlass">' + $tmp.find('.einlass').val() + '</div>' +
									'<div class="form-control col dauer">' + $tmp.find('.dauer').val() + '</div>' +
									'<div class="form-control col ort" title="' + $tmp.find('.calender-ort-nummer').attr('title') + '">' + $tmp.find('.ort option:selected').text() + '</div>' +
								'</div>';
							}
						});

						$output_con.html(div);
					});

					var prevSDate = null;
					$($target_start_dp).on('changeDate', function(event) {
						var date = event.date;

						if(prevSDate != null && prevSDate.getTime() == date.getTime()) {
							$(this).data('datepicker').update('');
							// $container.empty();
							$container.find('.cloned').not('.no-del').remove();
							updateContainerHeight();
							date = null;
						}

						prevSDate = date;
					});

					var prevEDate = null;
					$($target_end_dp).on('changeDate', function(event) {
						var date = event.date;

						if(prevEDate != null && prevEDate.getTime() == date.getTime()) {
							$(this).data('datepicker').update('');
							// $container.empty();
							$container.find('.cloned').not('.no-del').remove();
							updateContainerHeight();
							date = null;
						}

						prevEDate = date;
					});
					/*
					$($target_start_dp).add($target_end_dp).on('onSelect', function(event) {
						console.log('click');
					});
					*/
				}
			});
		}

		$('.form_datepicker').datepicker({
	    	language: 'de'
	    	//clearBtn: true
		});

		$('input.datum_out').each(function(i, v) {
			var val = $(this).val();
			if(val) {
				var date = new Date($(this).val());
				$(this).val(date.toLocaleDateString('de-DE', {weekday: 'short', year: 'numeric', month: 'numeric', day: 'numeric'}));
			}
		});

		/*
		$('body').on('click', '.day.active', function() {
			console.log('drinnen');
			var $that = $(this);
			$that.closest('.form_datepicker').data('datepicker').setDate(null);
		});
		*/

		/*
		$popover.on('click', function (e) {
		    $popover.not(this).popover('hide');
		});
		*/

		function updateMultiple() {
	    	var $multilpe = $('.multiple-type');

	    	if($multilpe.length == 0) return false;

	    	var $outputs = $multilpe.find('.output.d-none').not('.output-multilpe').addClass('p_multiple');
	    	$multilpe.find('.output-multilpe').html($outputs.clone().removeClass('d-none'));
	    }

		function doSummary() {
			var $output_container = $('.summary-type .summary-content');

			if($output_container.length) {
				var $form = $output_container.closest('form');

		    	var $requiredItems = $form.find('.req_it');
		    	$requiredItems.each(function(i,v) {
		    		var $that = $(this);
		    		var val = $that.val();
		    		if (typeof val == 'undefined' || val == '' || ($.isArray(val) && val.length == 0)) {
		    			$that.closest('.form-group').find('.output.row').addClass('form-out-missing');
		    		} else {
		    			$that.closest('.form-group').find('.output.row').removeClass('form-out-missing');
		    		}
		    	});
				
		    	var $required2Items = $form.find('.req_it_time');
		    	$required2Items.each(function(i,v) {
		    		var $items = $(this).find('.timeline-clones .cloned');

		    		if ($items.length) {
		    			$(this).closest('.form-group').find('.output.row').removeClass('form-out-missing');
		    		} else {
		    			$(this).closest('.form-group').find('.output.row').addClass('form-out-missing');
		    		}
		    	});

		    	var $required3Items = $form.find('.fileupload.warning_it');
		    	$required3Items.each(function(i,v) {
		    		var $itm = $(this).closest('.file-item');
		    		var fl_val = $(this).val();

		    		if (!$itm.hasClass('.prototyp') && fl_val != '') {
		    			var ftg = $itm.find('.ftg');
		    			var ftg_ck = $itm.find('.ftg-ck');

		    			if (typeof ftg.val() != 'undefined' && ftg.val() != '' && ftg_ck.is(':checked')) {
		    				$itm.closest('.form-group').find('.output.row').removeClass('file-out-missing');
		    			} else {
		    				$itm.closest('.form-group').find('.output.row').addClass('file-out-missing');
		    			}
		    		} else {
		    			$itm.closest('.form-group').find('.output.row').removeClass('file-out-missing');
		    		}
		    	});
		    	
		    	var $warnedItems = $form.find('.warning_it');
		    	$warnedItems.each(function(i,v) {
		    		var val = $(this).val();

		    		if (typeof val == 'undefined' || val == '' || ($.isArray(val) && val.length == 0)) {
		    			$(this).closest('.form-group').find('.output.row').addClass('form-out-warning');	
		    		} else {
		    			$(this).closest('.form-group').find('.output.row').removeClass('form-out-warning');
		    		}
		    	});


				updateMultiple();

				var $output_items = $('form .output.d-none').not('.p_multiple');
				if($output_items.length) {
					$output_container.html($output_items.clone().removeClass('d-none')).promise().done(function() {
						updateContainerHeight();
					});
				}
			}
		}

		function updateContainerHeight() {
			$('.form-content').css('min-height', ($('.form-page.active').outerHeight() + 10) + 'px');
		}

		updateContainerHeight();

		var working = false;

		$('.button-next, .continue_button').click(function() {

			if($(this).hasClass('continue_button')) {
				$('html, body').animate({
			        scrollTop: $('.joMainWrapper').offset().top
			    }, 1000);
			}

			$('.kill_transition').removeClass('kill_transition');

			if (working) return false;

			working = true;

			var $pages = $('.form-page');
			var index;

			for (var i = 0; i < $pages.length; i++) {
				if ($pages[i].classList.contains("active")) {
					index = i + 2;
					break;
				}
			}

			if (index <= $pages.length) {
				$('.form-page.active').addClass('slideout').removeClass('active');
			}

			if (index == $pages.length) {
				$('.continue_button').css('display', 'none');
				//$('.send_it_button').show();
				$('.button-next').hide();
			} else {
				$('.continue_button').css('display', 'flex');
				//$('.send_it_button').hide();
				$('.button-next').show();
			}

			$('.button-prev').show();
			$('.back_button').css('display', 'flex');

			var $cur_item = $('.form-page.form-page-' + index);
			$cur_item.addClass('active');
			updateContainerHeight();

			if ($('.menu-steps .menu-step.active').index() < $('.menu-steps .menu-step').length - 1) {
			    $('.menu-steps .menu-step.active').removeClass('active').next().addClass('active');
			}

			$('.cur-index').val(index);

			if($cur_item.find('.summary-type').length) {
				doSummary();
			}

			setTimeout(function() {working = false;}, 750);

		});

		$('.button-prev, .back_button').click(function() {

			if($(this).hasClass('back_button')) {
				$('html, body').animate({
			        scrollTop: $('.joMainWrapper').offset().top
			    }, 1000);
			}

			$('.kill_transition').removeClass('kill_transition');

			if (working) return false;

			working = true;

			var $pages = $('.form-page');
			var index;

			for (var i = 0; i < $pages.length; i++) {
				if ($pages[i].classList.contains("active")) {
					index = i;
					break;
				}
			}

			if (index >= 1) {
				$('.form-page.active').removeClass('slideout active');
			}

			if (index == 1) {
				$('.button-prev').hide();
				$('.back_button').css('display', 'none');
			} else {
				$('.continue_button').css('display', 'flex');
				//$('.send_it_button').hide();
			}

			$('.button-next').show();

			var $cur_item = $('.form-page.form-page-' + index);
			$cur_item.addClass('active');
			updateContainerHeight();


			if ($('.menu-steps .menu-step.active').index() > 0) {
			    $('.menu-steps .menu-step.active').removeClass('active').prev().addClass('active');
			}


			$('.cur-index').val(index);

			if($cur_item.find('.summary-type').length) {
				doSummary();
			}

			setTimeout(function() {working = false;}, 750);

		});

		$('#close_modal .btn-primary').click(function() {
			var url = $(this).data('url');
			if(typeof url != 'undefined' && url != '') {
				window.location.href = url;
			}
		});

		$('.menu-step').click(function() {

			$that = $(this);

			$('.kill_transition').removeClass('kill_transition');

			if (working) return false;

			working = true;

			var $active = $('.menu-step.active');

		  	var activeclass = $active.data('num');
		  	var clicked_index = $that.data('num');

		  	if (activeclass != clicked_index) {
				var pages = $('.form-page');
		  		pages.each(function(i, obj) {
		  			if (parseInt($(this).data('num')) < parseInt(clicked_index)) {
		  				$(this).addClass('slideout');

		  				if (i != parseInt(activeclass) - 1) {
		  					$(this).addClass('kill_transition');
		  				}
		  			}
		  			if (parseInt($(this).data('num')) > parseInt(clicked_index)) {
						$(this).removeClass('slideout');

						if (i == parseInt(activeclass)) {	
							$(this).addClass('kill_transition');	
		  				}
		  			}
		  		});
			  	
				$('.form-page.active').removeClass('active');
				
				var $cur_item = $('.form-page.form-page-' + clicked_index);
				$cur_item.removeClass('kill_transition').addClass('active');
				// $cur_item.parent().css('min-height', $cur_item.css('height'));
				updateContainerHeight();

				$('.menu-step.active').removeClass('active');
				$that.addClass('active');

				if(clicked_index == 1) {
					$('.button-prev').hide();
					$('.back_button').css('display', 'none');
				}

				if(clicked_index == pages.length) {
					$('.continue_button').css('display', 'none');
					//$('.send_it_button').show();
					$('.button-next').hide();
				}

				if(clicked_index > 1 && clicked_index < pages.length) {
					$('.continue_button').css('display', 'flex');
					//$('.send_it_button').hide();
					$('.button-next').show();
					$('.button-prev').show();
					$('.back_button').css('display', 'flex');
				}

				$('.cur-index').val(clicked_index);

				if($cur_item.find('.summary-type').length) {
					doSummary();
				}
			}

			setTimeout(function() {working = false;}, 750);
		});

		$('.field-plus').click(function() {
			var $that = $(this);

			var $target = $($that.data('target'));
			// var $container = $($that.data('save-container'));
			var $container = $that.prev('.container-plus');

			if($target.length && $container.length) {
				var $clone = $target.clone();
				var rnd = Math.floor((Math.random() * (1000 - 1)) + 1000);

				var $input = $clone.find('input, textarea, select');
				if($input.length) {
					$input.attr('id', $input.attr('id') + '-' + rnd);
					
					var $label = $clone.find('label');

					if($label.length) {
						$label.attr('for', $label.attr('for') + '-' + rnd);
					}
				}

				$clone.find('[data-toggle="popover"]').popover();

				$clone.appendTo($container);
				updateContainerHeight();
			}
		});

		$('.field-create').click(function() {
			var $that = $(this);

			// var $container = $($that.data('save-container'));
			var $container = $that.prev('.container-creat');
			var url = $that.data('url');

			if($container.length && typeof url != 'undefined' && url != '') {
				$.get(url, function(data) {
					$container.html(data);
					$that.fadeOut();
					updateContainerHeight();
				});
			}
		});

		// fixt Text area resize event -> container will get new height on resize
		var resizeInterval = null;
		$('.form-content').on('mousedown', '.form-page textarea', function(e) {
			$('.form-content').addClass('kill_transition');
				
	    	resizeInterval = setInterval(function() {
	    		updateContainerHeight();
	    	}, 1000/15);
	    });

		$(window).on('mouseup', function(e) {
	        if (resizeInterval !== null) {
	            clearInterval(resizeInterval);
		        
		        updateContainerHeight();
		        
		        $('.form-content').removeClass('kill_transition');
	        }

	    });

		$('.fileupload-type').on('click', '.fileupload-btn', function() {
	    	// $target = $('#' + $(this).attr('for'));
	    	$(this).parent().find('.fileupload').trigger('click');
	    });

	    $('form').on('change', '.fileupload', function(e) {
	    	$that = $(this);
	    	if ($that.val() != '') {
		    	var file = this.files[0];
		    	$that.prev('.file-name').html(file.name);

		    	var img = '';
		    	if (file) {
			        var img = new Image();
			        var objectUrl = URL.createObjectURL(file);

			        img.onload = function () {
			        	if(1000 > this.width) {
			        		var div = file.name + '<div class="fileupload-info alert alert-danger">Weite: ' + this.width + 'px, Höhe: ' + this.height + 'px. Das Bild sollte mindestens 1000 Pixel Weite haben.</div>';
			            	$that.prev('.file-name').html(div).promise().done(function() {
			            		updateContainerHeight();
			            	});
			            }
			            //URL.revokeObjectURL(objectUrl);
			        };

			        img.src = objectUrl;
			        img.classList.add('fileupload-img');
			    }

			    var $img_con = $that.closest('.file-item').find('.tmp-img-container');
			    // $that.closest('.file-item').find('.tmp-img-container').html(img);
			    $img_con.html(img);
			    $img_con.append('<figcaption>' + $that.closest('.file-item').find('.ftt').val() + '</figcaption>');
			    // $that.closest('.form-group').find('.output .output-text').html(img);
			    $parent = $that.closest('.form-group');
			    $all_images = $parent.find('.tmp-img-container img');
			    $parent.find('.output .output-text').html($all_images.clone());
	    	} else {
	    		$that.prev('.file-name').html('').promise().done(function() {
            		updateContainerHeight();
            	});
	    	}
	    });

	    $('.fileupload-type').on('keyup', '.ftt', function(e) {
	    	var $parent = $(this).closest('.fileupload-type');
	    	var $fileItems = $parent.find('.file-item').not('.prototyp');
	    	if ($fileItems.length) {
	    		var $output = $parent.find('.output .output-text');
	    		$output.html('');

	    		$fileItems.each(function() {
	    			var $that = $(this);
	    			$output.append($that.find('.tmp-img-container img').clone());
	    			$output.append('<figcaption>' + $that.find('.ftt').val() + '</figcaption');
	    		});
	    	}
	    });

	    /*
	    $('form .fileupload-type').ready(function() {
	    	var $inputImg = $(this).find('.fileupload');

	    	$inputImg.change(function() {
	    		$that = $(this);
	    		var file = this.files[0];
	    		var img = '';
	    		if (file) {
	    			img = new Image();
	    			var objectUrl = URL.createObjectURL(file);

		    		img.onload = function () {
		    			$that.closest('.form-group').find('.fileupload-img-info').remove();
			            if(1000 > this.width) {
			            	var div = '<div class="alert alert-danger fileupload-img-info">Das Bild sollte mindestens 1000 Pixel Weite haben.</div>';
			            	$that.closest('.form-group').find('.fileupload-img').after(div).promise().done(function() {
			            		updateContainerHeight();
			            	});
			            }
			        };

			        img.src = objectUrl;
			        img.classList.add('fileupload-img');

		    	}

		    	$that.closest('.form-group').find('.output .output-text').html(img);
	    	});
	    });
	    */

	    $('.btn-tosave').click(function() {
	    	var $that = $(this);
	    	
	    	$('#savetype').val($that.data('value'));

	    	$that.closest('form').submit();

	    	// $that.closest('form').submit();
	    });

	    $('#newEventForm').submit(function(e) {
	    	e.preventDefault();

	    	$loader.show();

	    	var $form = $(this);

	    	var $requiredItems = $form.find('.req_it');
	    	var found = [];

	    	$requiredItems.each(function(i,v) {
	    		var $that = $(this);
	    		var val = $that.val();
	    		if (typeof val == 'undefined' || val == '' || ($.isArray(val) && val.length == 0)) {
	    			$that.closest('.form-group').addClass('form-missing');
	    			found.push($that);
	    		} else {
	    			$that.closest('.form-group').removeClass('form-missing');
	    		}
	    	});

	    	var $required2Items = $form.find('.req_it_time');

	    	$required2Items.each(function(i,v) {
	    		var $items = $(this).find('.timeline-clones .cloned');

	    		if ($items.length) {
	    			$(this).removeClass('form-missing');
	    		} else {
	    			$(this).addClass('form-missing');
	    			found.push($(this));
	    		}
	    	});

	    	var $required3Items = $form.find('.fileupload.warning_it');

	    	$required3Items.each(function(i,v) {
	    		var $itm = $(this).closest('.file-item');
	    		var fl_val = $(this).val();

	    		if (!$itm.hasClass('.prototyp') && fl_val != '') {
	    			var ftg = $itm.find('.ftg');
	    			var ftg_ck = $itm.find('.ftg-ck');

	    			if (typeof ftg.val() != 'undefined' && ftg.val() != '' && ftg_ck.is(':checked')) {
	    				$itm.removeClass('file-missing');
	    			} else {
	    				$itm.addClass('file-missing');
	    				found.push($(this));
	    			}
	    		} else {
	    			$itm.removeClass('file-missing');
	    		}
	    	});

	    	var $requiredOrItems = $form.find('.ort_req');

	    	if ($requiredOrItems.length == 2) {
	    		var val0 = $($requiredOrItems[0]).val();
	    		var val1 = $($requiredOrItems[1]).val();
	    		if ((typeof val1 == 'undefined' || val1 == '') && ($.isArray(val0) && val0[0] == '')) {
	    			$($requiredOrItems[0]).closest('.form-group').addClass('form-missing');
	    			$($requiredOrItems[1]).closest('.form-group').addClass('form-missing');
	    			found.push($($requiredOrItems[0]));
	    			found.push($($requiredOrItems[1]));
	    		} else {
	    			$($requiredOrItems[0]).closest('.form-group').removeClass('form-missing');
	    			$($requiredOrItems[1]).closest('.form-group').removeClass('form-missing');
	    		}
	    	}


	    	var $warnedItems = $form.find('.warning_it');

	    	$warnedItems.each(function(i,v) {
	    		var val = $(this).val();

	    		if (typeof val == 'undefined' || val == '' || ($.isArray(val) && val.length == 0)) {
	    			$(this).closest('.form-group').addClass('form-warning');	
	    		} else {
	    			$(this).closest('.form-group').removeClass('form-warning');
	    		}
	    	});

	    	if (found.length) {
	    		var num = found[0].closest('.form-page').data('num');
	    		$('.menu-step[data-num="' + num + '"').trigger('click');

	    		$loader.hide();

	    		$('html,body').animate({
				    scrollTop: found[0].offset().top - ($(window).height() - found[0].outerHeight(true)) / 2
				}, 500);

	    		return false;
	    	}

	    	var url = $form.attr('action');

	    	var formdata = new FormData(this);

	    	$.ajax({
	    		/*
	    		xhr: function() {
				    var xhr = new window.XMLHttpRequest();

				    xhr.upload.addEventListener('progress', function(evt) {
	      				console.log(evt.lengthComputable);
				    	
			      		if (evt.lengthComputable) {
					        var percentComplete = evt.loaded / evt.total;
					        percentComplete = parseInt(percentComplete * 100);
					        console.log(percentComplete);

					        if (percentComplete === 100) {

					        }

				      	}
				      	
				    }, false);

				    return xhr;
			  	},
			  	*/
	            type: "POST",
	           	url: url,
	           	data: formdata,
	           	contentType: false,
                processData:false,
	           	success: function(data)
	           	{
	           		$loader.hide();
	               	var $target = $(data).find('.targetUrl');

	               	if($target.length) {
	               		var newurl = $target.data('url');

		               	if(typeof newurl != 'undefined' && newurl != '') {
		               		$.toaster('Die Veranstaltung wurde erfolgreich gespeichert aber noch nicht veröffentlicht! Du kannst sie unter "Veranstaltungen ändern/anzeigen" weiter bearbeiten, verschieben oder absagen', '', 'success');
							$.get(newurl, function(data) {
								$('.build_container > h4').fadeOut(400);
								$('.select-container').fadeOut(400);
								$('.output-container').fadeOut(400, function() {
									$('.output-container').html(data).promise().done(function() {
										$('.output-container').fadeIn();

										newEventActions();
									});
								});
							});
						}
	                } else {

	                	var $target = $(data).find('.targetRedirectUrl');
	                	//$.toaster('Die Veranstaltung wurde erfolgreich gespeichert und veröffentlicht! Du kannst sie unter "Veranstaltungen ändern/anzeigen" weiter bearbeiten, verschieben oder absagen', '', 'success');
						
	                	if($target.length) {
	                		var newurl = $target.data('url');
	                		window.location.href = newurl;
	                	}
	                }
	           	}
         	}).fail(function(e) {
         		$loader.hide();
			    console.log(e);

			    var $modal = $('#formModal');

               	$modal.find('.modal-title').html('Achtung! Leider ist beim Speichern was schiefgegangen.');
				$modal.find('.modal-body').html(e.responseText);
				$modal.find('.btn-primary').hide();

				$btn = $modal.find('.modal-footer .btn-secondary');
				$btn.off('click');

           		$btn.on('click', function() {
           			$modal.modal('hide');
				});

               	$modal.modal('show');
		  	});
	    });

	    /*
	    function checkMultiple(item) {
	    	var $that = $(item);
	    	var $multilpe = $that.closest('.multiple-type');

	    	if($multilpe.length == 0) return false;

	    	var $outputs = $multilpe.find('.output').not('.output-multilpe');
	    	$multilpe.find('.output-multilpe').html($outputs.clone());
	    }
	    */

	    $('form .input-type, form .textarea-type').ready(function() {
	    	$('form .input-type, form .textarea-type').each(function(i,v) {
		    	var $input = $(this).find('input, textarea');
		    	
		    	$input.change(function() {
		    		var $that = $(this);
		    		if(!$that.parent().hasClass('cloned') && !$that.hasClass('no-track')) {
		    			var val = $that.val();
		    			$that.closest('.form-group').find('.output .output-text').html(val);
		    		}
		    	});
	    	});
	    });

	    $('form .select-type').ready(function() {
	    	$('form .select-type').each(function(i,v) {
		    	var $select = $(this).find('select');
		    	
		    	$select.change(function() {
		    		var val = '';
		    		var $items = $(this).find('option:selected');
		    		
		    		$items.each(function(i,v) {
		    			val += $(this).text();
		    			if(i != $items.length - 1) {
		    				val += '<br/>';
		    			}
		    		});

		    		if ($(this).hasClass('prm-select')) {
		    			$(this).closest('.primery-select').find('.output .output-text').html(val);
		    		} else {
		    			$(this).closest('.select-type').children('.output').find('.output-text').html(val);
		    		}
		    	});
	    	});
	    });

	    var imgsrc_counter = 800;

	    $('form select[multiple="multiple"]').change(function() {
	    	var $that = $(this);
	    	var arr = $that.val();
	    	var $selected = $that.find('option:selected');

	    	var $con = $that.closest('.select-type').find('.primery-select');
	    	if ($con.length == 0) return false;
	    	var $select = $con.find('select');
	    	if ($select.length == 0) return false;

	    	if (arr.length > 1) {
	    		$con.slideDown(400, function() {
	    			updateContainerHeight();
	    		});

	    		$select.html($selected.clone());
	    		$select.val(arr[0]).change();
	    	} else {
	    		$con.slideUp(400, function() {
	    			updateContainerHeight();
	    		});

	    		$select.html('');
	    	}

	    	var $form = $that.closest('form');
			var $img_typ = $form.find('.defaultImg-type');
			var $img_out = $form.find('.defaultImg-disp');

			if ($img_typ.length && $selected.length) {
	    		var srcList = [];

				$img_typ.removeClass('d-none');
	    		$img_out.empty();

		    	$selected.each(function(i, v) {
		    		var img = $(this).data('imagesrc');
		    		var img_title = $(this).data('imagetitle');

		    		if (typeof img != 'undefined' && img != '') {
		    			if (!srcList.includes(img)) {
		    				srcList.push(img);

		    				var $prototyp = $img_typ.find('.prototyp').clone();
		    				$prototyp.removeClass('prototyp');

		    				$prototyp.find('input[type="hidden"]').each(function() {
		    					$(this).attr('name', $(this).attr('name').replace('[0]', '[' + imgsrc_counter + ']'));
		    				});

		    				var $img_input = $prototyp.find('.defaultImg-url');
		    				$img_input.val(img);

		    				var $img_tag = $('<img class="defaultImg-preview" src="' + img + '"/>');

		    				var $img_label = $prototyp.find('label');
		    				$img_label.attr('for', $img_label.attr('for').replace('-0', '-' + imgsrc_counter));
				    		$img_label.append($img_tag);

				    		if (typeof img_title != 'undefined' && img_title != '') {
				    			var $title_input = $prototyp.find('.defaultImg-title');
			    				$title_input.val(img_title);
			    				$img_tag.attr('title', img_title);
			    				$img_label.append('<figcaption>' + img_title + '</figcaption>');
				    		}

				    		var $img_save = $prototyp.find('.defaultImg-save');
		    				$img_save.attr('name', $img_save.attr('name').replace('[0]', '[' + imgsrc_counter + ']'));
		    				$img_save.attr('id', $img_save.attr('id').replace('-0', '-' + imgsrc_counter));

				    		$img_out.append($prototyp);
				    		imgsrc_counter++;
		    			}
		    		}
		    	});
		    }

		    if ($selected.length == 0) {
		    	$img_typ.addClass('d-none');
	    		$img_out.empty();
		    }
	    });

	    var $select_mult = $('form select[multiple="multiple"]');

	    if($select_mult.length) {
		    $select_mult.multiselect({
		    	maxHeight: 400,
	            // enableFiltering: true,
	            enableCaseInsensitiveFiltering: true,
	            templates: {
			        filter: '<div class="multiselect-filter"><div class="input-group input-group-sm p-1"><div class="input-group-prepend"><i class="input-group-text fas fa-search"></i></div><input class="form-control multiselect-search" type="text" /><div class="input-group-append"><button class="multiselect-clear-filter input-group-text" type="button"><i class="fas fa-times"></i></button></div></div></div>'
			    },
			    nonSelectedText: 'Nicht ausgewählt',
			    filterPlaceholder: 'Suche',
			    nSelectedText: ' ausgewählt',
			    allSelectedText: 'Alles ausgewählt',
			    resetButtonText: 'Zurücksetzen',
	            // enableClickableOptGroups: true,
	            enableCollapsibleOptGroups: true,
	            collapseOptGroupsByDefault: true,
	            enableResetButton: true,
	            optionClass: function(element) {
					if ($(element).parent().prop('tagName') == 'SELECT' && $(element).prop('tagName') != 'OPTGROUP') { return 'multiselect-filter-hidden'; }
					if ($(element).parent().prop('tagName') == 'OPTGROUP') { return 'nested'; }
				},
				onChange: function(option, checked) {
					var $opt = $(option);
					var $that = $opt.closest('select');
					var id = $that.attr('id');

					if ('new_event-select-spielort' == id || 'edit_performance-select-spielort' == id) {
						var $box = $that.next();

						var $selectedOptions = $that.find('option:selected').not(option);

						$selectedOptions.each(function() {
	                        var $input = $box.find('input[value="' + $(this).val() + '"]');
	                        //input.prop('disabled', true);
	                        $input.prop('checked', false);
	                        $input.parent().parent('.multiselect-option').removeClass('active');
	                        $(this).prop('selected', false);
	                    });
					}
				}
	        });

	        $select_mult.parent().find('.multiselect-group.dropdown-item-text').click(function(e) {
	        	e.preventDefault();
	        	e.stopPropagation();
	        	$(this).find('.caret-container.dropdown-toggle').trigger('click');
	        });

	        $select_mult.parent().find('.caret-container.dropdown-toggle').click(function(e) {
	        	e.stopPropagation();
	        });
			
			$select_mult.parent().find('.multiselect-container .caret-container').addClass('open');

			$select_mult.parent().find('.multiselect-container .caret-container').click(function() {
				$(this).toggleClass('open close');
			});
	    }

	    var $select_ort = $('#new_event-select-spielort, #edit_performance-select-spielort');

	    if($select_ort.length && false) {
	    	$select_ort.multiselect({
		    	maxHeight: 400,
	            // enableFiltering: true,
	            enableCaseInsensitiveFiltering: true,
	            templates: {
			        filter: '<div class="multiselect-filter"><div class="input-group input-group-sm p-1"><div class="input-group-prepend"><i class="input-group-text fas fa-search"></i></div><input class="form-control multiselect-search" type="text" /><div class="input-group-append"><button class="multiselect-clear-filter input-group-text" type="button"><i class="fas fa-times"></i></button></div></div></div>'
			    },
			    nonSelectedText: 'Nicht ausgewählt',
			    filterPlaceholder: 'Suche',
			    nSelectedText: ' ausgewählt',
			    allSelectedText: 'Alles ausgewählt',
			    resetButtonText: 'Zurücksetzen',
	            //enableClickableOptGroups: true,
	            enableCollapsibleOptGroups: true,
	            //collapseOptGroupsByDefault: true,
	            optionClass: function(element) {
					if ($(element).parent().prop('tagName') == 'SELECT' && $(element).prop('tagName') != 'OPTGROUP') { return 'multiselect-filter-hidden'; }
					if ($(element).parent().prop('tagName') == 'OPTGROUP') { return 'nested'; }
				},
				onChange: function(option, checked) {

					var $that = $(this);
	                
	                /*
	                var $selectedOptions = $that.find('option:selected').not(checked);


	 
	                if ($selectedOptions.length >= 2) {
	                    // Disable all other checkboxes.
	                    var nonSelectedOptions = $('#example-limit option').filter(function() {
	                        return !$(this).is(':selected');
	                    });
	 
	                    nonSelectedOptions.each(function() {
	                        var input = $('input[value="' + $(this).val() + '"]');
	                        input.prop('disabled', true);
	                        input.parent('.multiselect-option').addClass('disabled');
	                    });
	                } else {
	                    // Enable all checkboxes.
	                    $('#example-limit option').each(function() {
	                        var input = $('input[value="' + $(this).val() + '"]');
	                        input.prop('disabled', false);
	                        input.parent('.multiselect-option').addClass('disabled');
	                    });
	                }
	                */
	            }
	        });

	        $select_ort.parent().find('.multiselect-group.dropdown-item-text').click(function(e) {
	        	e.preventDefault();
	        	e.stopPropagation();
	        	$(this).find('.caret-container.dropdown-toggle').trigger('click');
	        });

	        $select_ort.parent().find('.caret-container.dropdown-toggle').click(function(e) {
	        	e.stopPropagation();
	        });
			
			$select_ort.parent().find('.multiselect-container .caret-container').addClass('open');

			$select_ort.parent().find('.multiselect-container .caret-container').click(function() {
				$(this).toggleClass('open close');
			});
	    }

	    var $cal_ort = $('.calender-ort-selector');

	    if($cal_ort.length) {
	    	var $ort_modal = $('#ort-modal');
	    	$('body').append($ort_modal);

			$('.joMainWrapper').on('click', '.calender-ort-selector', function() {
				$that = $(this);

				$ort_modal.find('.list-group-item.active').removeClass('active');
				$ort_modal.modal('show');

				$('#ort-modal .btn-primary').off('click')
				$('#ort-modal .btn-primary').on('click', function (e) {
					var $ort = $ort_modal.find('.list-group-item.active');

					if($ort.length) {
						var id = $ort.data('id');
						var title = $ort.text();

						$that.prev('.calender-ort-nummer').attr('title', title).text(id);
						$that.next('input').val(id).trigger('keyup');
					}

					$ort_modal.modal('hide');
				});
			});

			$('#ort-modal .list-group-item').click(function() {
				$(this).toggleClass('active').parent().find('.active').not(this).removeClass('active');
			});
	    }

	    var $spielort_select = $('#new_event-select-spielort, #edit_performance-select-spielort');

	    if($spielort_select.length) {
			$spielort_select.change(function() {
				var id = $(this).val();
				// var title = $(this).find('option:selected').text();

				// $('.master_datepicker_2 .prototyp .calender-ort-nummer').attr('title', title).text(id);
				if (typeof id != 'undefined' && id.length && id[0] != '') {
					$('.master_datepicker_2 .prototyp .ort').val(id[0]);
					$('.master_datepicker_2 .prototyp .ort option').removeAttr('selected');
					$('.master_datepicker_2 .prototyp .ort option[value=' + id[0] + ']').attr('selected', 'selected');
				}
			});

			$spielort_select.trigger('change');
	    }


	    var file_index = 1;
	    // var file_counter = 1;

	    $('.fileupload-type').on('click', '.file-plus', function() {
	    	var $parent = $(this).closest('.fileupload-type');
	    	var $prototyp = $parent.find('.file-item.prototyp');

	    	if($prototyp.length) {
	    		var $clone = $prototyp.clone();
	    		$clone.removeClass('prototyp');

	    		$clone.find('input, textarea').each(function() {
					if(typeof $(this).attr('name') != 'undefined') {
						$(this).attr('name', $(this).attr('name').replace('[0]', '[' + file_index + ']'));
					}
				});

				file_index++;
				// file_counter++;

				// $parent.append($clone);
				$(this).closest('.file-item').after($clone);

				updateContainerHeight();
	    	}
	    });

	    $('.fileupload-type').on('click', '.file-minus', function() {
	    	var $parent = $(this).closest('.fileupload-type');
	    	var $items = $parent.find('.file-item').not('.prototyp');
	    	var file_counter = $items.length;

	    	if(file_counter > 1) {
	    		$(this).closest('.file-item').remove();
	    		updateContainerHeight();
	    		file_counter--;
	    	} else if(file_counter == 1) {
	    		$fl_item = $(this).closest('.file-item');
	    		$fl_item.find('input[type="text"]').val('');
	    		$fl_item.find('input[type="file"]').val('');
	    		$fl_item.find('.olgimg').val('');
	    		$fl_item.find('.file-name').text('');
	    		$fl_item.find('.tmp-img-container').html('');
	    		$fl_item.find('input[type="checkbox"]').prop('checked', false);
	    		$fl_item.find('textarea').val('');
	    	}
	    });

	    $('.defaultImg-type').on('click', '.defaultImg-delete', function() {
	    	var $parent = $(this).closest('.defaultImg-item');
	    	var $gparent = $(this).closest('.defaultImg-disp');
	    	var $ggp = $(this).closest('.defaultImg-type');

	    	$parent.remove();
			if ($gparent.children().length == 0) $ggp.addClass('d-none');
	    });

	    var tml_counter = 800;

	    $('.timeline-type').on('click', '.timeline-plus', function() {
	    	var $parent = $(this).closest('.timeline-type');
	    	var $prototyp = $parent.find('.prototyp');

	    	if($prototyp.length) {
	    		var $clone = $prototyp.clone();
	    		$clone.removeClass('prototyp').addClass('cloned col-md-12');

	    		$clone.find('input, select, textarea').each(function() {
					$el = $(this);
					if(typeof $el.attr('name') != 'undefined') {
						$el.attr('name', $el.attr('name').replace('[0]', '[' + tml_counter + ']'));
					}

					if(typeof $el.attr('readonly') != 'undefined') {
						$el.removeAttr('readonly disabled');

						$el.datepicker({
					    	language: 'de',
					    	autoclose: true
					    });

					    $el.on('hide', function() {
					    	var the_date = $(this).data('datepicker').getDate();
					    	$(this).prev('input').val($(this).val());
					    	$(this).val(the_date.toLocaleDateString('de-DE', {weekday: 'short', year: 'numeric', month: 'numeric', day: 'numeric'}));
					    });
					}
				});

	    		tml_counter++;

	    		$(this).closest('.cloned').after($clone);

	    		updateContainerHeight();
	    	}
	    });

	    $('.timeline-type').on('click', '.timeline-minus', function() {
    		$(this).closest('.cloned').remove();
    		updateContainerHeight();
	    });


	    $('.timeline-clones').on('change', '.status', function() {
	    	$that = $(this);
			$parent = $that.closest('.cloned-row');
	    	
	    	switch($(this).val()) {
	    		case 'EventScheduled':
	    			$parent.find('.ev-rescheduled, .ev-postponed, .ev-cancelled').hide();
	    			updateContainerHeight();
	    			break;
	    		case 'EventPostponed':
	    			$parent.find('.ev-rescheduled, .ev-cancelled').hide();
	    			// $parent.find('.ev-postponed').show();
	    			updateContainerHeight();
	    			break;
	    		case 'EventCancelled':
	    			$parent.find('.ev-rescheduled, .ev-postponed').hide();
	    			$parent.find('.ev-cancelled').show();
	    			updateContainerHeight();
	    			break;
	    	}
	    });

	    $('.timeline-clones').on('change', '.check', function() {
	    	var $that = $(this);
	    	var $p = $that.closest('.cloned');
	    	if ($that.is(':checked')) {
	    		$p.removeClass('deactive');
	    	} else {
	    		$p.addClass('deactive');
	    	}
	    });

	    $('#kontakt-form').submit(function(e) {
	    	e.preventDefault();

	    	$loader.show();

	    	var $form = $(this);
	    	var url = $form.attr('action');
	    	var formdata = new FormData(this);

	    	$.ajax({
	            type: 'POST',
	           	url: url,
	           	data: formdata,
	           	contentType: false,
                processData:false,
	           	success: function(data)
	           	{
	           		$loader.hide();
	               	var $target = $(data).find('.alert-success');
	               	var $modal = $('#formModal');

	               	$modal.find('.modal-title').html('Kontakt');
					$modal.find('.modal-body').html(data);
					$modal.find('.btn-primary').hide();

					$btn = $modal.find('.modal-footer .btn-secondary');
					$btn.off('click');

	               	if ($target.length) {
	               		$btn.on('click', function() {
	               			location.reload(true);
						});
	               	} else {
	               		$btn.on('click', function() {
	               			$modal.modal('hide');
						});
	               	}

	               	$modal.modal('show');

	           	}
         	});
	    });

	    $('.tsl_icon').click(function() {
	    	$that = $(this);

	    	$item = $that.parent().parent().find('.lng_trsl');
	    	$item.collapse('toggle');
	    });

	    $('.lng_trsl').on('shown.bs.collapse hidden.bs.collapse', function () {
	    	updateContainerHeight();
	    });
	}
});