<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(function() {

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_theatercollection_domain_model_performance', 'EXT:theatercollection/Resources/Private/Language/locallang_csh_tx_theatercollection_domain_model_performance.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_theatercollection_domain_model_performance');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_theatercollection_domain_model_spielort', 'EXT:theatercollection/Resources/Private/Language/locallang_csh_tx_theatercollection_domain_model_spielort.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_theatercollection_domain_model_spielort');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_theatercollection_domain_model_spielzeit', 'EXT:theatercollection/Resources/Private/Language/locallang_csh_tx_theatercollection_domain_model_spielzeit.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_theatercollection_domain_model_spielzeit');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_theatercollection_domain_model_veranstalter', 'EXT:theatercollection/Resources/Private/Language/locallang_csh_tx_theatercollection_domain_model_veranstalter.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_theatercollection_domain_model_veranstalter');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_theatercollection_domain_model_theater', 'EXT:theatercollection/Resources/Private/Language/locallang_csh_tx_theatercollection_domain_model_theater.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_theatercollection_domain_model_theater');

});
