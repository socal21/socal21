<?php
namespace JO\Theatercollection\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;

/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * The repository for Lock
 */
class LockRepository extends Repository
{
    public function searchIt($id, $user, $action, $id_list = null)
    {
        $query = $this->createQuery();

        $conditions = [];

        if (null == $id_list) {
            $conditions[] = $query->equals('objid', $id);
        } else {
            $conditions[] = $query->in('objid', $id_list);
        }

        if (null != $user) {
            $conditions[] = $query->equals('user', $user);
        }
        $conditions[] = $query->equals('objidtyp', $action);

        return $query->matching($query->logicalAnd($conditions))->execute();
    }
}
