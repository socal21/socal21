<?php
namespace JO\Theatercollection\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * The repository for Spielzeits
 */
class SpielzeitRepository extends Repository
{
    protected $defaultOrderings = [
        'datum' => QueryInterface::ORDER_ASCENDING
    ];

    public function searchMine($demand = [])
    {
        $query = $this->createQuery();
        $conditions = [];

        if ($demand['setIgnoreEnableFields'] || ($demand['show_hidden'] && true == $demand['show_hidden'])) {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
        }

        if ($demand['by_veranstalter_ids'] && is_array($demand['by_veranstalter_ids']) && !empty($demand['by_veranstalter_ids'])) {
            $conditions[] = $query->in('veranstalter.uid', $demand['by_veranstalter_ids']);
        }

        if ($demand['by_veranstalterxtree_ids'] && is_array($demand['by_veranstalterxtree_ids']) && !empty($demand['by_veranstalterxtree_ids'])) {
            $conditions[] = $query->in('veranstalterxtree', $demand['by_veranstalterxtree_ids']);
        }

        if ($demand['by_performance_ids'] && is_array($demand['by_performance_ids']) && !empty($demand['by_performance_ids'])) {
            $conditions[] = $query->in('performance.uid', $demand['by_performance_ids']);
        }

        if ($demand['by_spielzeit_ids'] && is_array($demand['by_spielzeit_ids']) && !empty($demand['by_spielzeit_ids'])) {
            $conditions[] = $query->in('uid', $demand['by_spielzeit_ids']);
        }

        if ($demand['by_spielort_ids'] && is_array($demand['by_spielort_ids']) && !empty($demand['by_spielort_ids'])) {
            $conditions[] = $query->in('spielort.uid', $demand['by_spielort_ids']);
        }

        if ($demand['start_date']) {
            $conditions[] = $query->greaterThanOrEqual('datum', $demand['start_date']->format('Y-m-d'));
        }

        if ($demand['end_date']) {
            $conditions[] = $query->lessThanOrEqual('datum', $demand['end_date']->format('Y-m-d'));
        }

        if ($demand['start_ch_date']) {
            $conditions[] = $query->greaterThanOrEqual('tstamp', $demand['start_ch_date']->getTimestamp());
        }

        if ($demand['end_ch_date']) {
            $conditions[] = $query->lessThanOrEqual('tstamp', $demand['end_ch_date']->getTimestamp());
        }

        if ($demand['limit'] && is_int($demand['limit']) && $demand['limit'] > 0) {
            $query->setLimit($demand['limit']);
        }
        
        if ($demand['offset'] && is_int($demand['offset'])) {
            $query->setOffset($demand['offset']);
        }


        if (!empty($conditions)) {
            return $query->matching(
                $query->logicalAnd(
                    $conditions
                ))->execute();
        } else {
            return $query->execute();
        }
    }
}
