<?php
namespace JO\Theatercollection\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * The repository for Performances
 */
class PerformanceRepository extends Repository
{
    protected $defaultOrderings = [
        'tstamp' => QueryInterface::ORDER_DESCENDING,
    ];

    /**
     * Finds an object matching the given unique id.
     *
     * @param int $uid The unique id of the object to find
     * @return object The matching object if found, otherwise NULL
     * @api
     */
    /*
    public function getByUid($uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $conditions = [];
        if ($uid) {
            $conditions[] = $query->equals('uid', $uid);
            $result = $query->matching(
                $query->logicalAnd(
                    $conditions
                ))->execute()->getFirst();
            return $result;
        }
    }
    */

    public function searchByJODemand($demand = [])
    {
        $query = $this->createQuery();

        if ($demand['show_hidden'] && true == $demand['show_hidden']) {
            $query->getQuerySettings()->setIgnoreEnableFields(true);
        }

        $conditions = [];

        if ($demand['qry'] && '' != $demand['qry']) {
            $conditions[] = $query->like('name', '%' . $demand['qry'] . '%');
        }

        if ($demand['by_ids'] && is_array($demand['by_ids']) && !empty($demand['by_ids'])) {
            $conditions[] = $query->in('uid', $demand['by_ids']);
        }

        if ($demand['by_veranstalter_ids'] && is_array($demand['by_veranstalter_ids']) && !empty($demand['by_veranstalter_ids'])) {
            $conditions[] = $query->in('veranstalter.uid', $demand['by_veranstalter_ids']);
        }

        if ($demand['by_veranstalterxtree_ids'] && is_array($demand['by_veranstalterxtree_ids']) && !empty($demand['by_veranstalterxtree_ids'])) {
            $conditions[] = $query->in('veranstalterxtree', $demand['by_veranstalterxtree_ids']);
        }

        if ($demand['by_veranstaltungsart_ids'] && is_array($demand['by_veranstaltungsart_ids']) && !empty($demand['by_veranstaltungsart_ids'])) {
            $conditions[] = $query->in('art.uid', $demand['by_veranstaltungsart_ids']);
        }

        if ($demand['by_veranstaltungstype_ids'] && is_array($demand['by_veranstaltungstype_ids']) && !empty($demand['by_veranstaltungstype_ids'])) {
            $conditions[] = $query->in('typ.uid', $demand['by_veranstaltungstype_ids']);
        }

        if ($demand['start_date']) {
            $conditions[] = $query->greaterThanOrEqual('saisonstart', $demand['start_date']->format('Y-m-d'));
        }

        if ($demand['end_date']) {
            $conditions[] = $query->lessThanOrEqual('saisonend', $demand['end_date']->format('Y-m-d'));
        }

        if ($demand['limit'] && is_int($demand['limit']) && $demand['limit'] > 0) {
            $query->setLimit($demand['limit']);
        }
        if ($demand['offset']) {
            $query->setOffset($demand['offset']);
        }

        if (!empty($conditions)) {
            return $query->matching(
                $query->logicalAnd(
                    $conditions
                ))->execute();
        } else {
            return $query->execute();
        }
    }

    public function searchByLang($parentUid, $lang = 1)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        // $query->getQuerySettings()->setLanguageUid($lang);

        $conditions = [];

        $conditions[] = $query->equals('sys_language_uid', $lang);
        $conditions[] = $query->equals('l10n_parent', $parentUid);

        if (!empty($conditions)) {
            return $query->matching(
                $query->logicalAnd(
                    $conditions
                ))->execute();
        } else {
            return $query->execute();
        }
    }
}
