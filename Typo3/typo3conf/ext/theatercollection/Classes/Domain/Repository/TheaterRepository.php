<?php
namespace JO\Theatercollection\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;
/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * The repository for Theaters
 */
class TheaterRepository extends Repository
{

    public function searchByJODemand($demand = [])
    {
	   	$query = $this->createQuery();
	   	$conditions = [];

        if($demand['qry'] && $demand['qry'] != '') {
            $conditions[] = $query->like('name', '%' . $demand['qry'] . '%');
        }

	   	if($demand['by_ids'] && is_array($demand['by_ids']) && !empty($demand['by_ids'])) {
    		$conditions[] = $query->in('uid', $demand['by_ids']);
        }

        if($demand['start_date']) {
            $conditions[] = $query->greaterThanOrEqual('spielzeit.datum', $demand['start_date']->format('Y-m-d'));
        }

        if($demand['by_spielort_ids'] && is_array($demand['by_spielort_ids']) && !empty($demand['by_spielort_ids'])) {
    		$conditions[] = $query->in('spielzeit.spielort.uid', $demand['by_spielort_ids']);
        }

        if($demand['plz'] && $demand['plz'] != '') {
            $conditions[] = $query->equals('spielzeit.spielort.plz', $demand['plz']);
        }

        if($demand['by_veranstalter_ids'] && is_array($demand['by_veranstalter_ids']) && !empty($demand['by_veranstalter_ids'])) {
    		$conditions[] = $query->in('spielzeit.veranstalter.uid', $demand['by_veranstalter_ids']);
        }

        if($demand['by_performance_ids'] && is_array($demand['by_performance_ids']) && !empty($demand['by_performance_ids'])) {
    		$conditions[] = $query->in('spielzeit.performance.uid', $demand['by_performance_ids']);
        }

        if($demand['limit'] && is_int($demand['limit']) && $demand['limit'] > 0) {
        	$query->setLimit($demand['limit']);
        }
        if($demand['offset'] && is_int($demand['offset'])) {
        	$query->setOffset($demand['offset']);
        }
		
		if(!empty($conditions)) {
	        return $query->matching(
	        	$query->logicalAnd(
	                $conditions
	            ))->execute();
		} else {
			return $query->execute();
		}
    }
    
}
