<?php
namespace JO\Theatercollection\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * The repository for Spielorts
 */
class SpielortRepository extends Repository
{
    protected $defaultOrderings = [
        'name' => QueryInterface::ORDER_ASCENDING
    ];

	public function searchByJODemand($demand = [])
    {
	   	$query = $this->createQuery();
	   	$conditions = [];

	   	if($demand['qry'] && $demand['qry'] != '') {
            $conditions[] = $query->like('name', '%' . $demand['qry'] . '%');
        }

	   	if($demand['by_ids'] && is_array($demand['by_ids']) && !empty($demand['by_ids'])) {
    		$conditions[] = $query->in('uid', $demand['by_ids']);
        }

        if($demand['plz'] && $demand['plz'] != '') {
    		$conditions[] = $query->equals('plz', $demand['plz']);
        }

        if($demand['limit'] && is_int($demand['limit']) && $demand['limit'] > 0) {
        	$query->setLimit($demand['limit']);
        }
        if($demand['offset']) {
        	$query->setOffset($demand['offset']);
        }
		
		if(!empty($conditions)) {
	        return $query->matching(
	        	$query->logicalAnd(
	                $conditions
	            ))->execute();
		} else {
			return $query->execute();
		}
    }
}
