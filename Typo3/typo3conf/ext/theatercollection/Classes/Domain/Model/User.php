<?php
namespace JO\Theatercollection\Domain\Model;

use In2code\Femanager\Domain\Model\User as FE_User;

class User extends FE_User
{
    /**
     * veranstalter
     *
     * @var int
     */
    protected $veranstalter;

    /**
     * veranstalterxtree
     *
     * @var int
     */
    protected $veranstalterxtree;

    /**
     * Returns the veranstalter
     *
     * @return int $veranstalter
     */
    public function getVeranstalter()
    {
        return $this->veranstalter;
    }

    /**
     * Sets the veranstalter
     *
     * @param int $veranstalter
     * @return void
     */
    public function setVeranstalter($veranstalter)
    {
        $this->veranstalter = $veranstalter;
    }

    /**
     * Returns the veranstalterxtree
     *
     * @return string $veranstalterxtree
     */
    public function getVeranstalterxtree()
    {
        return $this->veranstalterxtree;
    }

    /**
     * Sets the veranstalterxtree
     *
     * @param string $veranstalterxtree
     * @return void
     */
    public function setVeranstalterxtree($veranstalterxtree)
    {
        $this->veranstalterxtree = $veranstalterxtree;
    }
}
