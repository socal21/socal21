<?php
namespace JO\Theatercollection\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * Url
 */
class Url extends AbstractEntity
{
    /**
     * fieldid
     *
     * @var string
     * 
     */
    protected $fieldid;

    /**
     * title
     *
     * @var string
     * 
     */
    protected $title;

    /**
     * url
     *
     * @var string
     * 
     */
    protected $url;

    /**
     * Returns the fieldid
     *
     * @return string $fieldid
     */
    public function getFieldid()
    {
        return $this->fieldid;
    }

    /**
     * Sets the fieldid
     *
     * @param string $fieldid
     * @return void
     */
    public function setFieldid($fieldid)
    {
        $this->fieldid = $fieldid;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the url
     *
     * @return string $url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Sets the url
     *
     * @param string $url
     * @return void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}
