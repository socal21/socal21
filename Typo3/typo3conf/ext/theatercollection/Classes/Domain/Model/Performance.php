<?php
namespace JO\Theatercollection\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * Performance
 */
class Performance extends AbstractEntity
{
    /**
     * @var \DateTime
     */
    protected $tstamp;

    /**
     * @var \DateTime
     */
    protected $crdate;

    /**
     * @var int
     */
    protected $sysLanguageUid = 0;

    /**
     * @var int
     */
    protected $l10nParent = 0;

    /**
     * saisonstart
     *
     * @var \DateTime
     */
    protected $saisonstart = null;

    /**
     * saisonend
     *
     * @var \DateTime
     */
    protected $saisonend = null;

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * booking
     *
     * @var string
     */
    protected $booking = '';

    /**
     * bookingstart
     *
     * @var string
     */
    protected $bookingstart = '';

    /**
     * info
     *
     * @var string
     */
    protected $info = '';

    /**
     * beschreibung
     *
     * @var string
     */
    protected $beschreibung = '';

    /**
     * editoranmerkung
     *
     * @var string
     */
    protected $editoranmerkung = '';

    /**
     * imgdefault
     *
     * @var string
     */
    protected $imgdefault = '';

    /**
     * spielort
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Spielort>
     *
     */
    protected $spielort = null;

    /**
     * spielort url
     *
     * @var string
     */
    protected $spielorturl = '';

    /**
     * veranstalter
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstalter>
     */
    protected $veranstalter = null;

    /**
     * veranstalterxtree
     *
     * @var string
     */
    protected $veranstalterxtree = '';

    /**
     * veranstalterxtreejson
     *
     * @var string
     */
    protected $veranstalterxtreejson = '';

    /**
     * infouri
     *
     * @var string
     */
    protected $infouri = '';

    /**
     * langbeschreibung
     *
     * @var string
     */
    protected $langbeschreibung = '';

    /**
     * autor
     *
     * @var string
     */
    protected $autor = '';

    /**
     * altersfreigabe
     *
     * @var string
     */
    protected $altersfreigabe = '';

    /**
     * maxattendee
     *
     * @var string
     */
    protected $maxattendee = '';

    /**
     * maxattendeephy
     *
     * @var string
     */
    protected $maxattendeephy = '';

    /**
     * maxattendeevirt
     *
     * @var string
     */
    protected $maxattendeevirt = '';

    /**
     * art
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstaltungsart>
     */
    protected $art = '';

    /**
     * artxtree
     *
     * @var string
     */
    protected $artxtree = '';

    /**
     * artprimary
     *
     * @var string
     */
    protected $artprimary = '';

    /**
     * typ
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstaltungstype>
     */
    protected $typ = '';

    /**
     * typxtree
     *
     * @var string
     */
    protected $typxtree = '';

    /**
     * eventtyp
     *
     * @var string
     */
    protected $eventtyp = '';

    /**
     * sozartxtree
     *
     * @var string
     */
    protected $sozartxtree = '';

    /**
     * suchkat
     *
     * @var string
     */
    protected $suchkat = '';

    /**
     * mappedkat
     *
     * @var string
     */
    protected $mappedkat = '';

    /**
     * typprimary
     *
     * @var string
     */
    protected $typprimary = '';

    /**
     * ausfallenvon
     *
     * @var \DateTime
     */
    protected $ausfallenvon = null;

    /**
     * ausfallenbis
     *
     * @var \DateTime
     */
    protected $ausfallenbis = null;

    /**
     * ausfallinfo
     *
     * @var string
     */
    protected $ausfallinfo = '';

    /**
     * vorschausmall
     *
     * @var ObjectStorage<FileReference>
     */
    protected $vorschausmall;

    /**
     * vorschaubig
     *
     * @var ObjectStorage<FileReference>
     */
    protected $vorschaubig;

    /**
     * pdf
     *
     * @var ObjectStorage<FileReference>
     */
    protected $pdf;

    /**
     * urls
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Url>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $urls;

    /**
     * metas
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Data>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $metas;

    /**
     * @var bool
     */
    protected $hidden;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->art = new ObjectStorage();
        $this->typ = new ObjectStorage();
        $this->vorschausmall = new ObjectStorage();
        $this->vorschaubig = new ObjectStorage();
        $this->pdf = new ObjectStorage();
        $this->urls = new ObjectStorage();
        $this->metas = new ObjectStorage();
        $this->veranstalter = new ObjectStorage();
        $this->spielort = new ObjectStorage();
    }

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the booking
     *
     * @return string $booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * Sets the booking
     *
     * @param string $booking
     * @return void
     */
    public function setBooking($booking)
    {
        $this->booking = $booking;
    }

    /**
     * Returns the bookingstart
     *
     * @return string $bookingstart
     */
    public function getBookingstart()
    {
        return $this->bookingstart;
    }

    /**
     * Sets the bookingstart
     *
     * @param string $bookingstart
     * @return void
     */
    public function setBookingstart($bookingstart)
    {
        $this->bookingstart = $bookingstart;
    }

    /**
     * Returns the info
     *
     * @return string $info
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Sets the info
     *
     * @param string $info
     * @return void
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * Returns the infouri
     *
     * @return string $infouri
     */
    public function getInfouri()
    {
        return $this->infouri;
    }

    /**
     * Sets the infouri
     *
     * @param string $infouri
     * @return void
     */
    public function setInfouri($infouri)
    {
        $this->infouri = $infouri;
    }

    /**
     * Returns the beschreibung
     *
     * @return string $beschreibung
     */
    public function getBeschreibung()
    {
        return $this->beschreibung;
    }

    /**
     * Sets the beschreibung
     *
     * @param string $beschreibung
     * @return void
     */
    public function setBeschreibung($beschreibung)
    {
        $this->beschreibung = $beschreibung;
    }

    /**
     * Returns the editoranmerkung
     *
     * @return string $editoranmerkung
     */
    public function getEditoranmerkung()
    {
        return $this->editoranmerkung;
    }

    /**
     * Sets the editoranmerkung
     *
     * @param string $editoranmerkung
     * @return void
     */
    public function setEditoranmerkung($editoranmerkung)
    {
        $this->editoranmerkung = $editoranmerkung;
    }

    /**
     * Returns the imgdefault
     *
     * @return string $imgdefault
     */
    public function getImgdefault()
    {
        return $this->imgdefault;
    }

    /**
     * Sets the imgdefault
     *
     * @param string $imgdefault
     * @return void
     */
    public function setImgdefault($imgdefault)
    {
        $this->imgdefault = $imgdefault;
    }

    /**
     * Returns the langbeschreibung
     *
     * @return string $langbeschreibung
     */
    public function getLangbeschreibung()
    {
        return $this->langbeschreibung;
    }

    /**
     * Sets the langbeschreibung
     *
     * @param string $langbeschreibung
     * @return void
     */
    public function setLangbeschreibung($langbeschreibung)
    {
        $this->langbeschreibung = $langbeschreibung;
    }

    /**
     * Adds a Veranstalter
     *
     * @param \JO\Theatercollection\Domain\Model\Veranstalter $veranstalter
     * @return void
     */
    public function addVeranstalter(\JO\Theatercollection\Domain\Model\Veranstalter $veranstalter)
    {
        $this->veranstalter->attach($veranstalter);
    }

    /**
     * Removes a Veranstalter
     *
     * @param \JO\Theatercollection\Domain\Model\Veranstalter $veranstalterToRemove The Veranstalter to be removed
     * @return void
     */
    public function removeVeranstalter(\JO\Theatercollection\Domain\Model\Veranstalter $veranstalterToRemove)
    {
        $this->veranstalter->detach($veranstalterToRemove);
    }

    /**
     * Returns the veranstalter
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstalter> $veranstalter
     */
    public function getVeranstalter()
    {
        return $this->veranstalter;
    }

    /**
     * Sets the veranstalter
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstalter> $veranstalter
     * @return void
     */
    public function setVeranstalter(ObjectStorage $veranstalter)
    {
        $this->veranstalter = $veranstalter;
    }

    /**
     * Returns the veranstalterxtree
     *
     * @return string $veranstalterxtree
     */
    public function getVeranstalterxtree()
    {
        return $this->veranstalterxtree;
    }

    /**
     * Sets the veranstalterxtree
     *
     * @param string $veranstalterxtree
     * @return void
     */
    public function setVeranstalterxtree($veranstalterxtree)
    {
        $this->veranstalterxtree = $veranstalterxtree;
    }

    /**
     * Returns the veranstalterxtreejson
     *
     * @return string $veranstalterxtreejson
     */
    public function getVeranstalterxtreejson()
    {
        return $this->veranstalterxtreejson;
    }

    /**
     * Sets the veranstalterxtreejson
     *
     * @param string $veranstalterxtreejson
     * @return void
     */
    public function setVeranstalterxtreejson($veranstalterxtreejson)
    {
        $this->veranstalterxtreejson = $veranstalterxtreejson;
    }

    /**
     * Returns the autor
     *
     * @return string $autor
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Sets the autor
     *
     * @param string $autor
     * @return void
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;
    }

    /**
     * Returns the altersfreigabe
     *
     * @return string $altersfreigabe
     */
    public function getAltersfreigabe()
    {
        return $this->altersfreigabe;
    }

    /**
     * Sets the altersfreigabe
     *
     * @param string $altersfreigabe
     * @return void
     */
    public function setAltersfreigabe($altersfreigabe)
    {
        $this->altersfreigabe = $altersfreigabe;
    }

    /**
     * Returns the maxattendee
     *
     * @return string $maxattendee
     */
    public function getMaxattendee()
    {
        return $this->maxattendee;
    }

    /**
     * Sets the maxattendee
     *
     * @param string $maxattendee
     * @return void
     */
    public function setMaxattendee($maxattendee)
    {
        $this->maxattendee = $maxattendee;
    }

    /**
     * Returns the maxattendeephy
     *
     * @return string $maxattendeephy
     */
    public function getMaxattendeephy()
    {
        return $this->maxattendeephy;
    }

    /**
     * Sets the maxattendeephy
     *
     * @param string $maxattendeephy
     * @return void
     */
    public function setMaxattendeephy($maxattendeephy)
    {
        $this->maxattendeephy = $maxattendeephy;
    }

    /**
     * Returns the maxattendeevirt
     *
     * @return string $maxattendeevirt
     */
    public function getMaxattendeevirt()
    {
        return $this->maxattendeevirt;
    }

    /**
     * Sets the maxattendeevirt
     *
     * @param string $maxattendeevirt
     * @return void
     */
    public function setMaxattendeevirt($maxattendeevirt)
    {
        $this->maxattendeevirt = $maxattendeevirt;
    }

    /**
     * Returns the typprimary
     *
     * @return string $typprimary
     */
    public function getTypprimary()
    {
        return $this->typprimary;
    }

    /**
     * Sets the typprimary
     *
     * @param string $typprimary
     * @return void
     */
    public function setTypprimary($typprimary)
    {
        $this->typprimary = $typprimary;
    }

    /**
     * Returns the artprimary
     *
     * @return string $artprimary
     */
    public function getArtprimary()
    {
        return $this->artprimary;
    }

    /**
     * Sets the artprimary
     *
     * @param string $artprimary
     * @return void
     */
    public function setArtprimary($artprimary)
    {
        $this->artprimary = $artprimary;
    }

    /**
     * Returns the artxtree
     *
     * @return string $artxtree
     */
    public function getArtxtree()
    {
        return $this->artxtree;
    }

    /**
     * Sets the artxtree
     *
     * @param string $artxtree
     * @return void
     */
    public function setArtxtree($artxtree)
    {
        $this->artxtree = $artxtree;
    }

    /**
     * Returns the typxtree
     *
     * @return string $typxtree
     */
    public function getTypxtree()
    {
        return $this->typxtree;
    }

    /**
     * Sets the typxtree
     *
     * @param string $typxtree
     * @return void
     */
    public function setTypxtree($typxtree)
    {
        $this->typxtree = $typxtree;
    }

    /**
     * Returns the eventtyp
     *
     * @return string $eventtyp
     */
    public function getEventtyp()
    {
        return $this->eventtyp;
    }

    /**
     * Sets the eventtyp
     *
     * @param string $eventtyp
     * @return void
     */
    public function setEventtyp($eventtyp)
    {
        $this->eventtyp = $eventtyp;
    }

    /**
     * Returns the sozartxtree
     *
     * @return string $sozartxtree
     */
    public function getSozartxtree()
    {
        return $this->sozartxtree;
    }

    /**
     * Sets the sozartxtree
     *
     * @param string $sozartxtree
     * @return void
     */
    public function setSozartxtree($sozartxtree)
    {
        $this->sozartxtree = $sozartxtree;
    }

    /**
     * Returns the suchkat
     *
     * @return string $suchkat
     */
    public function getSuchkat()
    {
        return $this->suchkat;
    }

    /**
     * Sets the suchkat
     *
     * @param string $suchkat
     * @return void
     */
    public function setSuchkat($suchkat)
    {
        $this->suchkat = $suchkat;
    }

    /**
     * Returns the mappedkat
     *
     * @return string $mappedkat
     */
    public function getMappedkat()
    {
        return $this->mappedkat;
    }

    /**
     * Sets the mappedkat
     *
     * @param string $mappedkat
     * @return void
     */
    public function setMappedkat($mappedkat)
    {
        $this->mappedkat = $mappedkat;
    }

    /**
     * Adds a Typ
     *
     * @param \JO\Theatercollection\Domain\Model\Veranstaltungstype $typ
     * @return void
     */
    public function addTyp(\JO\Theatercollection\Domain\Model\Veranstaltungstype $typ)
    {
        $this->typ->attach($typ);
    }

    /**
     * Removes a Typ
     *
     * @param \JO\Theatercollection\Domain\Model\Veranstaltungstype $typToRemove The Typ to be removed
     * @return void
     */
    public function removeTyp(\JO\Theatercollection\Domain\Model\Veranstaltungstype $typToRemove)
    {
        $this->typ->detach($typToRemove);
    }

    /**
     * Returns the Typ
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstaltungstype> $typ
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * Sets the Typ
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstaltungstype> $typ
     * @return void
     */
    public function setTyp(ObjectStorage $typ)
    {
        $this->typ = $typ;
    }

    /**
     * Adds a Art
     *
     * @param \JO\Theatercollection\Domain\Model\Veranstaltungsart $art
     * @return void
     */
    public function addArt(\JO\Theatercollection\Domain\Model\Veranstaltungsart $art)
    {
        $this->art->attach($art);
    }

    /**
     * Removes a Art
     *
     * @param \JO\Theatercollection\Domain\Model\Veranstaltungsart $artToRemove The Art to be removed
     * @return void
     */
    public function removeArt(\JO\Theatercollection\Domain\Model\Veranstaltungsart $artToRemove)
    {
        $this->art->detach($artToRemove);
    }

    /**
     * Returns the Art
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstaltungsart> $art
     */
    public function getArt()
    {
        return $this->art;
    }

    /**
     * Sets the Art
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstaltungsart> $art
     * @return void
     */
    public function setArt(ObjectStorage $art)
    {
        $this->art = $art;
    }

    /**
     * Returns the ausfallenvon
     *
     * @return \DateTime $ausfallenvon
     */
    public function getAusfallenvon()
    {
        return $this->ausfallenvon;
    }

    /**
     * Sets the ausfallenvon
     *
     * @param \DateTime $ausfallenvon
     * @return void
     */
    public function setAusfallenvon(\DateTime $ausfallenvon)
    {
        $this->ausfallenvon = $ausfallenvon;
    }

    /**
     * Returns the ausfallenbis
     *
     * @return \DateTime $ausfallenbis
     */
    public function getAusfallenbis()
    {
        return $this->ausfallenbis;
    }

    /**
     * Sets the ausfallenbis
     *
     * @param \DateTime $ausfallenbis
     * @return void
     */
    public function setAusfallenbis(\DateTime $ausfallenbis)
    {
        $this->ausfallenbis = $ausfallenbis;
    }

    /**
     * Returns the ausfallinfo
     *
     * @return string $ausfallinfo
     */
    public function getAusfallinfo()
    {
        return $this->ausfallinfo;
    }

    /**
     * Sets the ausfallinfo
     *
     * @param string $ausfallinfo
     * @return void
     */
    public function setAusfallinfo($ausfallinfo)
    {
        $this->ausfallinfo = $ausfallinfo;
    }

    /**
     * Returns vorschausmall
     *
     * @return ObjectStorage<FileReference> $vorschausmall
     */
    public function getVorschausmall()
    {
        return $this->vorschausmall;
    }

    /**
     * Set vorschausmall
     *
     * @param ObjectStorage $vorschausmall
     */
    public function setVorschausmall(ObjectStorage $vorschausmall)
    {
        $this->vorschausmall = $vorschausmall;
    }

    /**
     * Adds a Vorschausmall
     *
     * @param FileReference $vorschausmall
     * @return void
     */
    public function addVorschausmall(FileReference $vorschausmall)
    {
        $this->vorschausmall->attach($vorschausmall);
    }

    /**
     * Removes a Vorschausmall
     *
     * @param FileReference $vorschausmallToRemove The Vorschausmall to be removed
     * @return void
     */
    public function removeVorschausmall(FileReference $vorschausmall)
    {
        $this->vorschausmall->detach($vorschausmall);
    }

    /**
     * Returns vorschaubig
     *
     * @return ObjectStorage<FileReference> $vorschaubig
     */
    public function getVorschaubig()
    {
        return $this->vorschaubig;
    }

    /**
     * Set vorschaubig
     *
     * @param ObjectStorage $vorschaubig
     */
    public function setVorschaubig(ObjectStorage $vorschaubig)
    {
        $this->vorschaubig = $vorschaubig;
    }

    /**
     * Adds a Vorschaubig
     *
     * @param FileReference $vorschaubig
     * @return void
     */
    public function addVorschaubig(FileReference $vorschaubig)
    {
        $this->vorschaubig->attach($vorschaubig);
    }

    /**
     * Removes a Vorschaubig
     *
     * @param FileReference $vorschaubigteToRemove The Vorschaubig to be removed
     * @return void
     */
    public function removeVorschaubig(FileReference $vorschaubig)
    {
        $this->vorschaubig->detach($vorschaubig);
    }

    /**
     * Returns pdf
     *
     * @return ObjectStorage<FileReference> $pdf
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set pdf
     *
     * @param ObjectStorage $pdf
     */
    public function setPdf(ObjectStorage $pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * Adds a pdf
     *
     * @param FileReference $pdf
     * @return void
     */
    public function addPdf(FileReference $pdf)
    {
        $this->pdf->attach($pdf);
    }

    /**
     * Removes a pdf
     *
     * @param FileReference $pdfteToRemove The pdf to be removed
     * @return void
     */
    public function removePdf(FileReference $pdf)
    {
        $this->pdf->detach($pdf);
    }

    /**
     * Returns the saisonstart
     *
     * @return \DateTime $saisonstart
     */
    public function getSaisonstart()
    {
        return $this->saisonstart;
    }

    /**
     * Sets the saisonstart
     *
     * @param \DateTime $saisonstart
     * @return void
     */
    public function setSaisonstart(\DateTime $saisonstart)
    {
        $this->saisonstart = $saisonstart;
    }

    /**
     * Returns the saisonend
     *
     * @return \DateTime $saisonend
     */
    public function getSaisonend()
    {
        return $this->saisonend;
    }

    /**
     * Sets the saisonend
     *
     * @param \DateTime $saisonend
     * @return void
     */
    public function setSaisonend(\DateTime $saisonend)
    {
        $this->saisonend = $saisonend;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set time stamp
     *
     * @param \DateTime $tstamp time stamp
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns the urls
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Url> $urls
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * Sets the urls
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Url> $urls
     * @return void
     */
    public function setUrls($urls)
    {
        $this->urls = $urls;
    }

    /**
     * Adds a urls
     *
     * @param \JO\Theatercollection\Domain\Model\Url $urls
     * @return void
     */
    public function addUrls(\JO\Theatercollection\Domain\Model\Url $urls)
    {
        $this->urls->attach($urls);
    }

    /**
     * Removes a urls
     *
     * @param \JO\Theatercollection\Domain\Model\Url $urlsToRemove The Urls to be removed
     * @return void
     */
    public function removeUrls(\JO\Theatercollection\Domain\Model\Url $urls)
    {
        $this->urls->detach($urls);
    }

    /**
     * Returns the metas
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Data> $metas
     */
    public function getMetas()
    {
        return $this->metas;
    }

    /**
     * Sets the metas
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Data> $metas
     * @return void
     */
    public function setMetas($metas)
    {
        $this->metas = $metas;
    }

    /**
     * Adds a metas
     *
     * @param \JO\Theatercollection\Domain\Model\Data $metas
     * @return void
     */
    public function addMetas(\JO\Theatercollection\Domain\Model\Data $metas)
    {
        $this->metas->attach($metas);
    }

    /**
     * Removes a metas
     *
     * @param \JO\Theatercollection\Domain\Model\Url $metasToRemove The Metas to be removed
     * @return void
     */
    public function removeMetas(\JO\Theatercollection\Domain\Model\Data $metas)
    {
        $this->metas->detach($metas);
    }

    /**
     * Get hidden flag
     *
     * @return int
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set hidden flag
     *
     * @param int $hidden hidden flag
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * Returns the crdate
     *
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * Adds a Spielort
     *
     * @param \JO\Theatercollection\Domain\Model\Spielort $spielort
     * @return void
     */
    public function addSpielort(\JO\Theatercollection\Domain\Model\Spielort $spielort)
    {
        $this->spielort->attach($spielort);
    }

    /**
     * Removes a Spielort
     *
     * @param \JO\Theatercollection\Domain\Model\Spielort $spielortToRemove The Spielort to be removed
     * @return void
     */
    public function removeSpielort(\JO\Theatercollection\Domain\Model\Spielort $spielortToRemove)
    {
        $this->spielort->detach($spielortToRemove);
    }

    /**
     * Returns the spielort
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Spielort> $spielort
     */
    public function getSpielort()
    {
        return $this->spielort;
    }

    /**
     * Sets the spielort
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Spielort> $spielort
     * @return void
     */
    public function setSpielort(ObjectStorage $spielort)
    {
        $this->spielort = $spielort;
    }

    /**
     * Returns the Spielort Url
     *
     * @return string $spielorturl
     */
    public function getSpielorturl()
    {
        return $this->spielorturl;
    }

    /**
     * Sets the Spielort Url
     *
     * @param string $spielorturl
     * @return void
     */
    public function setSpielorturl($spielorturl)
    {
        $this->spielorturl = $spielorturl;
    }

    /**
     * Set sys language
     *
     * @param int $sysLanguageUid
     *
     * @return void
     */
    public function setSysLanguageUid($sysLanguageUid): void
    {
        $this->_languageUid = $sysLanguageUid;
    }

    /**
     * Get sys language
     *
     * @return int
     */
    public function getSysLanguageUid(): int
    {
        return $this->_languageUid;
    }

    /**
     * Set l10n parent
     *
     * @param int $l10nParent
     *
     * @return void
     */
    public function setL10nParent($l10nParent): void
    {
        $this->l10nParent = $l10nParent;
    }

    /**
     * Get l10n parent
     *
     * @return int
     */
    public function getL10nParent(): int
    {
        return $this->l10nParent;
    }
}
