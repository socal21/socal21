<?php
namespace JO\Theatercollection\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * Spielort
 */
class Spielort extends AbstractEntity
{

    /**
     * @var \DateTime
     */
    protected $tstamp;

    /**
     * @var \DateTime
     */
    protected $crdate;

    /**
     * @var int
     */
    protected $sysLanguageUid = 0;

    /**
     * @var int
     */
    protected $l10nParent = 0;

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * raum
     *
     * @var string
     */
    protected $raum = '';

    /**
     * namefull
     *
     * @var string
     */
    protected $namefull = '';

    /**
     * bundesland
     *
     * @var string
     */
    protected $bundesland = '';

    /**
     * landkreis
     *
     * @var string
     */
    protected $landkreis = '';

    /**
     * legalname
     *
     * @var string
     */
    protected $legalname = '';

    /**
     * alternativname
     *
     * @var string
     */
    protected $alternativname = '';

    /**
     * digicultid
     *
     * @var string
     */
    protected $digicultid = '';

    /**
     * ort
     *
     * @var string
     */
    protected $ort = '';

    /**
     * plz
     *
     * @var string
     */
    protected $plz = '';

    /**
     * strasse
     *
     * @var string
     */
    protected $strasse = '';

    /**
     * opnv
     *
     * @var string
     */
    protected $opnv = '';

    /**
     * lat
     *
     * @var float
     */
    protected $lat = 0;

    /**
     * lon
     *
     * @var float
     */
    protected $lon = 0;

    /**
     * ansprechperson
     *
     * @var string
     */
    protected $ansprechperson = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * telefon
     *
     * @var string
     */
    protected $telefon = '';

    /**
     * kultur
     *
     * @var string
     */
    protected $kultur = '';

    /**
     * mobil
     *
     * @var string
     */
    protected $mobil = '';

    /**
     * website
     *
     * @var string
     */
    protected $website = '';

    /**
     * editoranmerkung
     *
     * @var string
     */
    protected $editoranmerkung = '';

    /**
     * vorschausmall
     *
     * @var ObjectStorage<FileReference>
     */
    protected $vorschausmall;

    /**
     * vorschaubig
     *
     * @var ObjectStorage<FileReference>
     */
    protected $vorschaubig;

    /**
     * urls
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Url>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $urls;

    /**
     * metas
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Data>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $metas;

    /**
     * mrhid
     *
     * @var string
     */
    protected $mrhid = '';

    /**
     * geonames
     *
     * @var string
     */
    protected $geonames = '';

    /**
     * __construct
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->vorschausmall = new ObjectStorage();
        $this->vorschaubig = new ObjectStorage();
        $this->urls = new ObjectStorage();
        $this->metas = new ObjectStorage();
    }

    /**
     * Returns the kultur
     *
     * @return string $kultur
     */
    public function getKultur()
    {
        return $this->kultur;
    }

    /**
     * Sets the kultur
     *
     * @param string $kultur
     * @return void
     */
    public function setKultur($kultur)
    {
        $this->kultur = $kultur;
    }

    /**
     * Returns the landkreis
     *
     * @return string $landkreis
     */
    public function getLandkreis()
    {
        return $this->landkreis;
    }

    /**
     * Sets the landkreis
     *
     * @param string $landkreis
     * @return void
     */
    public function setLandkreis($landkreis)
    {
        $this->landkreis = $landkreis;
    }

    /**
     * Returns the bundesland
     *
     * @return string $bundesland
     */
    public function getBundesland()
    {
        return $this->bundesland;
    }

    /**
     * Sets the bundesland
     *
     * @param string $bundesland
     * @return void
     */
    public function setBundesland($bundesland)
    {
        $this->bundesland = $bundesland;
    }

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the raum
     *
     * @return string $raum
     */
    public function getRaum()
    {
        return $this->raum;
    }

    /**
     * Sets the raum
     *
     * @param string $raum
     * @return void
     */
    public function setRaum($raum)
    {
        $this->raum = $raum;
    }

    /**
     * Returns the namefull
     *
     * @return string $namefull
     */
    public function getNamefull()
    {
        return $this->namefull;
    }

    /**
     * Sets the namefull
     *
     * @param string $namefull
     * @return void
     */
    public function setNamefull($namefull)
    {
        $this->namefull = $namefull;
    }

    /**
     * Returns the legalname
     *
     * @return string $legalname
     */
    public function getLegalname()
    {
        return $this->legalname;
    }

    /**
     * Sets the legalname
     *
     * @param string $legalname
     * @return void
     */
    public function setLegalname($legalname)
    {
        $this->legalname = $legalname;
    }

    /**
     * Returns the alternativname
     *
     * @return string $alternativname
     */
    public function getAlternativname()
    {
        return $this->alternativname;
    }

    /**
     * Sets the alternativname
     *
     * @param string $alternativname
     * @return void
     */
    public function setAlternativname($alternativname)
    {
        $this->alternativname = $alternativname;
    }

    /**
     * Returns the lat
     *
     * @return string $lat
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Sets the lat
     *
     * @param string $lat
     * @return void
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * Returns the lon
     *
     * @return string $lon
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Sets the lon
     *
     * @param string $lon
     * @return void
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
    }

    /**
     * Returns the digicultid
     *
     * @return string $digicultid
     */
    public function getDigicultid()
    {
        return $this->digicultid;
    }

    /**
     * Sets the digicultid
     *
     * @param string $digicultid
     * @return void
     */
    public function setDigicultid($digicultid)
    {
        $this->digicultid = $digicultid;
    }

    /**
     * Returns the ort
     *
     * @return string $ort
     */
    public function getOrt()
    {
        return $this->ort;
    }

    /**
     * Sets the ort
     *
     * @param string $ort
     * @return void
     */
    public function setOrt($ort)
    {
        $this->ort = $ort;
    }

    /**
     * Returns the plz
     *
     * @return string $plz
     */
    public function getPlz()
    {
        return $this->plz;
    }

    /**
     * Sets the plz
     *
     * @param string $plz
     * @return void
     */
    public function setPlz($plz)
    {
        $this->plz = $plz;
    }

    /**
     * Returns the strasse
     *
     * @return string $strasse
     */
    public function getStrasse()
    {
        return $this->strasse;
    }

    /**
     * Sets the strasse
     *
     * @param string $strasse
     * @return void
     */
    public function setStrasse($strasse)
    {
        $this->strasse = $strasse;
    }

    /**
     * Returns the opnv
     *
     * @return string $opnv
     */
    public function getOpnv()
    {
        return $this->opnv;
    }

    /**
     * Sets the opnv
     *
     * @param string $opnv
     * @return void
     */
    public function setOpnv($opnv)
    {
        $this->opnv = $opnv;
    }

    /**
     * Returns the ansprechperson
     *
     * @return string $ansprechperson
     */
    public function getAnsprechperson()
    {
        return $this->ansprechperson;
    }

    /**
     * Sets the ansprechperson
     *
     * @param string $ansprechperson
     * @return void
     */
    public function setAnsprechperson($ansprechperson)
    {
        $this->ansprechperson = $ansprechperson;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the telefon
     *
     * @return string $telefon
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Sets the telefon
     *
     * @param string $telefon
     * @return void
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;
    }

    /**
     * Returns the mobil
     *
     * @return string $mobil
     */
    public function getMobil()
    {
        return $this->mobil;
    }

    /**
     * Sets the mobil
     *
     * @param string $mobil
     * @return void
     */
    public function setMobil($mobil)
    {
        $this->mobil = $mobil;
    }

    /**
     * Returns the website
     *
     * @return string $website
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Sets the website
     *
     * @param string $website
     * @return void
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }

    /**
     * Returns the editoranmerkung
     *
     * @return string $editoranmerkung
     */
    public function getEditoranmerkung()
    {
        return $this->editoranmerkung;
    }

    /**
     * Sets the editoranmerkung
     *
     * @param string $editoranmerkung
     * @return void
     */
    public function setEditoranmerkung($editoranmerkung)
    {
        $this->editoranmerkung = $editoranmerkung;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set time stamp
     *
     * @param \DateTime $tstamp time stamp
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns vorschausmall
     *
     * @return ObjectStorage<FileReference> $vorschausmall
     */
    public function getVorschausmall()
    {
        return $this->vorschausmall;
    }

    /**
     * Set vorschausmall
     *
     * @param ObjectStorage $vorschausmall
     */
    public function setVorschausmall(ObjectStorage $vorschausmall)
    {
        $this->vorschausmall = $vorschausmall;
    }

    /**
     * Adds a Vorschausmall
     *
     * @param FileReference $vorschausmall
     * @return void
     */
    public function addVorschausmall(FileReference $vorschausmall)
    {
        $this->vorschausmall->attach($vorschausmall);
    }

    /**
     * Removes a Vorschausmall
     *
     * @param FileReference $vorschausmallToRemove The Vorschausmall to be removed
     * @return void
     */
    public function removeVorschausmall(FileReference $vorschausmall)
    {
        $this->vorschausmall->detach($vorschausmall);
    }

    /**
     * Returns vorschaubig
     *
     * @return ObjectStorage<FileReference> $vorschaubig
     */
    public function getVorschaubig()
    {
        return $this->vorschaubig;
    }

    /**
     * Set vorschaubig
     *
     * @param ObjectStorage $vorschaubig
     */
    public function setVorschaubig(ObjectStorage $vorschaubig)
    {
        $this->vorschaubig = $vorschaubig;
    }

    /**
     * Adds a Vorschaubig
     *
     * @param FileReference $vorschaubig
     * @return void
     */
    public function addVorschaubig(FileReference $vorschaubig)
    {
        $this->vorschaubig->attach($vorschaubig);
    }

    /**
     * Removes a Vorschaubig
     *
     * @param FileReference $vorschaubigteToRemove The Vorschaubig to be removed
     * @return void
     */
    public function removeVorschaubig(FileReference $vorschaubig)
    {
        $this->vorschaubig->detach($vorschaubig);
    }

    /**
     * Returns the urls
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Url> $urls
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * Sets the urls
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Url> $urls
     * @return void
     */
    public function setUrls($urls)
    {
        $this->urls = $urls;
    }

    /**
     * Adds a urls
     *
     * @param \JO\Theatercollection\Domain\Model\Url $urls
     * @return void
     */
    public function addUrls(\JO\Theatercollection\Domain\Model\Url $urls)
    {
        $this->urls->attach($urls);
    }

    /**
     * Removes a urls
     *
     * @param \JO\Theatercollection\Domain\Model\Url $urlsToRemove The Urls to be removed
     * @return void
     */
    public function removeUrls(\JO\Theatercollection\Domain\Model\Url $urls)
    {
        $this->urls->detach($urls);
    }

    /**
     * Returns the metas
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Data> $metas
     */
    public function getMetas()
    {
        return $this->metas;
    }

    /**
     * Sets the metas
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Data> $metas
     * @return void
     */
    public function setMetas($metas)
    {
        $this->metas = $metas;
    }

    /**
     * Adds a metas
     *
     * @param \JO\Theatercollection\Domain\Model\Data $metas
     * @return void
     */
    public function addMetas(\JO\Theatercollection\Domain\Model\Data $metas)
    {
        $this->metas->attach($metas);
    }

    /**
     * Removes a metas
     *
     * @param \JO\Theatercollection\Domain\Model\Url $metasToRemove The Metas to be removed
     * @return void
     */
    public function removeMetas(\JO\Theatercollection\Domain\Model\Data $metas)
    {
        $this->metas->detach($metas);
    }

    /**
     * Returns the mrhid
     *
     * @return string $mrhid
     */
    public function getMrhid()
    {
        return $this->mrhid;
    }

    /**
     * Sets the mrhid
     *
     * @param string $mrhid
     * @return void
     */
    public function setMrhid($mrhid)
    {
        $this->mrhid = $mrhid;
    }

    /**
     * Returns the geonames
     *
     * @return string $geonames
     */
    public function getGeonames()
    {
        return $this->geonames;
    }

    /**
     * Sets the geonames
     *
     * @param string $geonames
     * @return void
     */
    public function setGeonames($geonames)
    {
        $this->geonames = $geonames;
    }

    public function resetUid()
    {
        $this->uid = null;
        $this->_setClone(false);
    }

    /**
     * Returns the crdate
     *
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * Set sys language
     *
     * @param int $sysLanguageUid
     *
     * @return void
     */
    public function setSysLanguageUid($sysLanguageUid): void
    {
        $this->_languageUid = $sysLanguageUid;
    }

    /**
     * Get sys language
     *
     * @return int
     */
    public function getSysLanguageUid(): int
    {
        return $this->_languageUid;
    }

    /**
     * Set l10n parent
     *
     * @param int $l10nParent
     *
     * @return void
     */
    public function setL10nParent($l10nParent): void
    {
        $this->l10nParent = $l10nParent;
    }

    /**
     * Get l10n parent
     *
     * @return int
     */
    public function getL10nParent(): int
    {
        return $this->l10nParent;
    }
}
