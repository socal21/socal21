<?php
namespace JO\Theatercollection\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/

/**
 * Veranstaltungstype
 */
class Veranstaltungstype extends AbstractEntity
{
    /**
     * @var \DateTime
     */
    protected $tstamp;

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * jsonld
     *
     * @var string
     */
    protected $jsonld = '';

    /**
     * mrhid
     *
     * @var string
     */
    protected $mrhid = '';

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set time stamp
     *
     * @param \DateTime $tstamp time stamp
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the jsonld
     *
     * @return string $jsonld
     */
    public function getJsonld()
    {
        return $this->jsonld;
    }

    /**
     * Sets the jsonld
     *
     * @param string $jsonld
     * @return void
     */
    public function setJsonld($jsonld)
    {
        $this->jsonld = $jsonld;
    }

    /**
     * Returns the mrhid
     *
     * @return string $mrhid
     */
    public function getMrhid()
    {
        return $this->mrhid;
    }

    /**
     * Sets the mrhid
     *
     * @param string $mrhid
     * @return void
     */
    public function setMrhid($mrhid)
    {
        $this->mrhid = $mrhid;
    }
}
