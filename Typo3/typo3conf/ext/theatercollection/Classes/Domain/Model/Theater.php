<?php
namespace JO\Theatercollection\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * Theater
 */
class Theater extends AbstractEntity
{

    /**
     * @var \DateTime
     */
    protected $tstamp;
    
    /**
     * name
     * 
     * @var string
     */
    protected $name = '';

    /**
     * telefon
     * 
     * @var string
     */
    protected $telefon = '';

    /**
     * mobil
     * 
     * @var string
     */
    protected $mobil = '';

    /**
     * fax
     * 
     * @var string
     */
    protected $fax = '';

    /**
     * email
     * 
     * @var string
     */
    protected $email = '';

    /**
     * ansprechperson
     * 
     * @var string
     */
    protected $ansprechperson = '';

    /**
     * beschreibung
     * 
     * @var string
     */
    protected $beschreibung = '';

    /**
     * langbeschreibung
     * 
     * @var string
     */
    protected $langbeschreibung = '';

    /**
     * spielzeit
     * 
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Spielzeit>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $spielzeit = null;

    /**
     * vorschausmall
     *
     * @var ObjectStorage<FileReference>
     */
    protected $vorschausmall;

    /**
     * vorschaubig
     *
     * @var ObjectStorage<FileReference>
     */
    protected $vorschaubig;

    /**
     * urls
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Url>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $urls;

    /**
     * metas
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Data>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $metas;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->spielzeit = new ObjectStorage();
        $this->vorschausmall = new ObjectStorage();
        $this->vorschaubig = new ObjectStorage();
        $this->urls = new ObjectStorage();
        $this->metas = new ObjectStorage();
    }

    /**
     * Returns the name
     * 
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the telefon
     * 
     * @return string $telefon
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Sets the telefon
     * 
     * @param string $telefon
     * @return void
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;
    }

    /**
     * Returns the mobil
     * 
     * @return string $mobil
     */
    public function getMobil()
    {
        return $this->mobil;
    }

    /**
     * Sets the mobil
     * 
     * @param string $mobil
     * @return void
     */
    public function setMobil($mobil)
    {
        $this->mobil = $mobil;
    }

    /**
     * Returns the fax
     * 
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax
     * 
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Returns the email
     * 
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     * 
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the ansprechperson
     * 
     * @return string $ansprechperson
     */
    public function getAnsprechperson()
    {
        return $this->ansprechperson;
    }

    /**
     * Sets the ansprechperson
     * 
     * @param string $ansprechperson
     * @return void
     */
    public function setAnsprechperson($ansprechperson)
    {
        $this->ansprechperson = $ansprechperson;
    }

    /**
     * Returns the beschreibung
     * 
     * @return string $beschreibung
     */
    public function getBeschreibung()
    {
        return $this->beschreibung;
    }

    /**
     * Sets the beschreibung
     * 
     * @param string $beschreibung
     * @return void
     */
    public function setBeschreibung($beschreibung)
    {
        $this->beschreibung = $beschreibung;
    }

    /**
     * Returns the langbeschreibung
     * 
     * @return string $langbeschreibung
     */
    public function getLangbeschreibung()
    {
        return $this->langbeschreibung;
    }

    /**
     * Sets the langbeschreibung
     * 
     * @param string $langbeschreibung
     * @return void
     */
    public function setLangbeschreibung($langbeschreibung)
    {
        $this->langbeschreibung = $langbeschreibung;
    }

    /**
     * Adds a Spielzeit
     * 
     * @param \JO\Theatercollection\Domain\Model\Spielzeit $spielzeit
     * @return void
     */
    public function addSpielzeit(\JO\Theatercollection\Domain\Model\Spielzeit $spielzeit)
    {
        $this->spielzeit->attach($spielzeit);
    }

    /**
     * Removes a Spielzeit
     * 
     * @param \JO\Theatercollection\Domain\Model\Spielzeit $spielzeitToRemove The Spielzeit to be removed
     * @return void
     */
    public function removeSpielzeit(\JO\Theatercollection\Domain\Model\Spielzeit $spielzeitToRemove)
    {
        $this->spielzeit->detach($spielzeitToRemove);
    }

    /**
     * Returns the spielzeit
     * 
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Spielzeit> $spielzeit
     */
    public function getSpielzeit()
    {
        return $this->spielzeit;
    }

    /**
     * Sets the spielzeit
     * 
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Spielzeit> $spielzeit
     * @return void
     */
    public function setSpielzeit(ObjectStorage $spielzeit)
    {
        $this->spielzeit = $spielzeit;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set time stamp
     *
     * @param \DateTime $tstamp time stamp
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns vorschausmall
     *
     * @return ObjectStorage<FileReference> $vorschausmall
     */
    public function getVorschausmall()
    {
        return $this->vorschausmall;
    }

    /**
     * Set vorschausmall
     *
     * @param ObjectStorage $vorschausmall
     */
    public function setVorschausmall(ObjectStorage $vorschausmall)
    {
        $this->vorschausmall = $vorschausmall;
    }

    /**
     * Adds a Vorschausmall
     *
     * @param FileReference $vorschausmall
     * @return void
     */
    public function addVorschausmall(FileReference $vorschausmall)
    {
        $this->vorschausmall->attach($vorschausmall);
    }

    /**
     * Removes a Vorschausmall
     *
     * @param FileReference $vorschausmallToRemove The Vorschausmall to be removed
     * @return void
     */
    public function removeVorschausmall(FileReference $vorschausmall)
    {
        $this->vorschausmall->detach($vorschausmall);
    }

    /**
     * Returns vorschaubig
     *
     * @return ObjectStorage<FileReference> $vorschaubig
     */
    public function getVorschaubig()
    {
        return $this->vorschaubig;
    }

    /**
     * Set vorschaubig
     *
     * @param ObjectStorage $vorschaubig
     */
    public function setVorschaubig(ObjectStorage $vorschaubig)
    {
        $this->vorschaubig = $vorschaubig;
    }

    /**
     * Adds a Vorschaubig
     *
     * @param FileReference $vorschaubig
     * @return void
     */
    public function addVorschaubig(FileReference $vorschaubig)
    {
        $this->vorschaubig->attach($vorschaubig);
    }

    /**
     * Removes a Vorschaubig
     *
     * @param FileReference $vorschaubigteToRemove The Vorschaubig to be removed
     * @return void
     */
    public function removeVorschaubig(FileReference $vorschaubig)
    {
        $this->vorschaubig->detach($vorschaubig);
    }

    /**
     * Returns the urls
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Url> $urls
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * Sets the urls
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Url> $urls
     * @return void
     */
    public function setUrls($urls)
    {
        $this->urls = $urls;
    }

    /**
     * Adds a urls
     *
     * @param \JO\Theatercollection\Domain\Model\Url $urls
     * @return void
     */
    public function addUrls(\JO\Theatercollection\Domain\Model\Url $urls)
    {
        $this->urls->attach($urls);
    }

    /**
     * Removes a urls
     *
     * @param \JO\Theatercollection\Domain\Model\Url $urlsToRemove The Urls to be removed
     * @return void
     */
    public function removeUrls(\JO\Theatercollection\Domain\Model\Url $urls)
    {
        $this->urls->detach($urls);
    }

    /**
     * Returns the metas
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Data> $metas
     */
    public function getMetas()
    {
        return $this->metas;
    }

    /**
     * Sets the metas
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Data> $metas
     * @return void
     */
    public function setMetas($metas)
    {
        $this->metas = $metas;
    }

    /**
     * Adds a metas
     *
     * @param \JO\Theatercollection\Domain\Model\Data $metas
     * @return void
     */
    public function addMetas(\JO\Theatercollection\Domain\Model\Data $metas)
    {
        $this->metas->attach($metas);
    }

    /**
     * Removes a metas
     *
     * @param \JO\Theatercollection\Domain\Model\Url $metasToRemove The Metas to be removed
     * @return void
     */
    public function removeMetas(\JO\Theatercollection\Domain\Model\Data $metas)
    {
        $this->metas->detach($metas);
    }
}
