<?php
namespace JO\Theatercollection\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * Data
 */
class Data extends AbstractEntity
{
    /**
     * fieldid
     *
     * @var string
     * 
     */
    protected $fieldid;

    /**
     * title
     *
     * @var string
     * 
     */
    protected $title;

    /**
     * value
     *
     * @var string
     * 
     */
    protected $value;

    /**
     * Returns the fieldid
     *
     * @return string $fieldid
     */
    public function getFieldid()
    {
        return $this->fieldid;
    }

    /**
     * Sets the fieldid
     *
     * @param string $fieldid
     * @return void
     */
    public function setFieldid($fieldid)
    {
        $this->fieldid = $fieldid;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the value
     *
     * @param string $value
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}
