<?php
namespace JO\Theatercollection\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * Spielzeit
 */
class Spielzeit extends AbstractEntity
{
    /**
     * @var \DateTime
     */
    protected $tstamp;

    /**
     * @var int
     */
    protected $sysLanguageUid = 0;

    /**
     * @var int
     */
    protected $l10nParent = 0;

    /**
     * datum
     *
     * @var \DateTime
     */
    protected $datum = null;

    /**
     * newdatum
     *
     * @var \DateTime
     */
    protected $newdatum = null;

    /**
     * uhrzeit
     *
     * @var int
     */
    protected $uhrzeit = 0;

    /**
     * einlass
     *
     * @var int
     */
    protected $einlass = 0;

    /**
     * dauer
     *
     * @var string
     */
    protected $dauer = '';

    /**
     * uhrzeit bis
     *
     * @var int
     */
    protected $bis = 0;

    /**
     * booking
     *
     * @var string
     */
    protected $booking = '';

    /**
     * status
     *
     * @var string
     */
    protected $status = '';

    /**
     * spielort
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Spielort>
     *
     */
    protected $spielort = null;

    /**
     * spielort url
     *
     * @var string
     */
    protected $spielorturl = '';

    /**
     * veranstalter
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstalter>
     */
    protected $veranstalter = null;

    /**
     * veranstalterxtree
     *
     * @var string
     */
    protected $veranstalterxtree = '';

    /**
     * veranstalterxtreejson
     *
     * @var string
     */
    protected $veranstalterxtreejson = '';

    /**
     * performance
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Performance>
     */
    protected $performance = null;

    /**
     * ausfall
     *
     * @var bool
     */
    protected $ausfall = '';

    /**
     * ausfallinfo
     *
     * @var string
     */
    protected $ausfallinfo = '';

    /**
     * vorschausmall
     *
     * @var ObjectStorage<FileReference>
     */
    protected $vorschausmall;

    /**
     * vorschaubig
     *
     * @var ObjectStorage<FileReference>
     */
    protected $vorschaubig;

    /**
     * urls
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Url>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $urls;

    /**
     * metas
     *
     * @var ObjectStorage<\JO\Theatercollection\Domain\Model\Data>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $metas;

    /**
     * @var bool
     */
    protected $hidden;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->spielort = new ObjectStorage();
        $this->veranstalter = new ObjectStorage();
        $this->performance = new ObjectStorage();
        $this->vorschausmall = new ObjectStorage();
        $this->vorschaubig = new ObjectStorage();
        $this->urls = new ObjectStorage();
        $this->metas = new ObjectStorage();
    }

    /**
     * Returns the booking
     *
     * @return string $booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * Sets the booking
     *
     * @param string $booking
     * @return void
     */
    public function setBooking($booking)
    {
        $this->booking = $booking;
    }

    /**
     * Returns the status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status
     *
     * @param string $status
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Returns the datum
     *
     * @return \DateTime $datum
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * Sets the datum
     *
     * @param \DateTime $datum
     * @return void
     */
    public function setDatum(\DateTime $datum)
    {
        $this->datum = $datum;
    }

    /**
     * Returns the newdatum
     *
     * @return \DateTime $newdatum
     */
    public function getNewdatum()
    {
        return $this->newdatum;
    }

    /**
     * Sets the newdatum
     *
     * @param \DateTime $newdatum
     * @return void
     */
    public function setNewdatum(\DateTime $newdatum)
    {
        $this->newdatum = $newdatum;
    }

    /**
     * Returns the uhrzeit
     *
     * @return int $uhrzeit
     */
    public function getUhrzeit()
    {
        return $this->uhrzeit;
    }

    /**
     * Sets the uhrzeit
     *
     * @param int $uhrzeit
     * @return void
     */
    public function setUhrzeit(int $uhrzeit)
    {
        $this->uhrzeit = $uhrzeit;
    }

    /**
     * Returns the einlass
     *
     * @return int $einlass
     */
    public function getEinlass()
    {
        return $this->einlass;
    }

    /**
     * Sets the einlass
     *
     * @param int $einlass
     * @return void
     */
    public function setEinlass(int $einlass)
    {
        $this->einlass = $einlass;
    }

    /**
     * Returns the uhrzeit bis
     *
     * @return int $bis
     */
    public function getBis()
    {
        return $this->bis;
    }

    /**
     * Sets the uhrzeit bis
     *
     * @param int $bis
     * @return void
     */
    public function setBis(int $bis)
    {
        $this->bis = $bis;
    }

    /**
     * Adds a Spielort
     *
     * @param \JO\Theatercollection\Domain\Model\Spielort $spielort
     * @return void
     */
    public function addSpielort(\JO\Theatercollection\Domain\Model\Spielort $spielort)
    {
        $this->spielort->attach($spielort);
    }

    /**
     * Removes a Spielort
     *
     * @param \JO\Theatercollection\Domain\Model\Spielort $spielortToRemove The Spielort to be removed
     * @return void
     */
    public function removeSpielort(\JO\Theatercollection\Domain\Model\Spielort $spielortToRemove)
    {
        $this->spielort->detach($spielortToRemove);
    }

    /**
     * Returns the spielort
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Spielort> $spielort
     */
    public function getSpielort()
    {
        return $this->spielort;
    }

    /**
     * Sets the spielort
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Spielort> $spielort
     * @return void
     */
    public function setSpielort(ObjectStorage $spielort)
    {
        $this->spielort = $spielort;
    }

    /**
     * Returns the Spielort Url
     *
     * @return string $spielorturl
     */
    public function getSpielorturl()
    {
        return $this->spielorturl;
    }

    /**
     * Sets the Spielort Url
     *
     * @param string $spielorturl
     * @return void
     */
    public function setSpielorturl($spielorturl)
    {
        $this->spielorturl = $spielorturl;
    }

    /**
     * Returns the dauer
     *
     * @return string $dauer
     */
    public function getDauer()
    {
        return $this->dauer;
    }

    /**
     * Sets the dauer
     *
     * @param string $dauer
     * @return void
     */
    public function setDauer($dauer)
    {
        $this->dauer = $dauer;
    }

    /**
     * Adds a Veranstalter
     *
     * @param \JO\Theatercollection\Domain\Model\Veranstalter $veranstalter
     * @return void
     */
    public function addVeranstalter(\JO\Theatercollection\Domain\Model\Veranstalter $veranstalter)
    {
        $this->veranstalter->attach($veranstalter);
    }

    /**
     * Removes a Veranstalter
     *
     * @param \JO\Theatercollection\Domain\Model\Veranstalter $veranstalterToRemove The Veranstalter to be removed
     * @return void
     */
    public function removeVeranstalter(\JO\Theatercollection\Domain\Model\Veranstalter $veranstalterToRemove)
    {
        $this->veranstalter->detach($veranstalterToRemove);
    }

    /**
     * Returns the veranstalter
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstalter> $veranstalter
     */
    public function getVeranstalter()
    {
        return $this->veranstalter;
    }

    /**
     * Sets the veranstalter
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Veranstalter> $veranstalter
     * @return void
     */
    public function setVeranstalter(ObjectStorage $veranstalter)
    {
        $this->veranstalter = $veranstalter;
    }

    /**
     * Returns the veranstalterxtree
     *
     * @return string $veranstalterxtree
     */
    public function getVeranstalterxtree()
    {
        return $this->veranstalterxtree;
    }

    /**
     * Sets the veranstalterxtree
     *
     * @param string $veranstalterxtree
     * @return void
     */
    public function setVeranstalterxtree($veranstalterxtree)
    {
        $this->veranstalterxtree = $veranstalterxtree;
    }

    /**
     * Returns the veranstalterxtreejson
     *
     * @return string $veranstalterxtreejson
     */
    public function getVeranstalterxtreejson()
    {
        return $this->veranstalterxtreejson;
    }

    /**
     * Sets the veranstalterxtreejson
     *
     * @param string $veranstalterxtreejson
     * @return void
     */
    public function setVeranstalterxtreejson($veranstalterxtreejson)
    {
        $this->veranstalterxtreejson = $veranstalterxtreejson;
    }

    /**
     * Adds a Performance
     *
     * @param \JO\Theatercollection\Domain\Model\Performance $performance
     * @return void
     */
    public function addPerformance(\JO\Theatercollection\Domain\Model\Performance $performance)
    {
        $this->performance->attach($performance);
    }

    /**
     * Removes a Performance
     *
     * @param \JO\Theatercollection\Domain\Model\Performance $performanceToRemove The Performance to be removed
     * @return void
     */
    public function removePerformance(\JO\Theatercollection\Domain\Model\Performance $performanceToRemove)
    {
        $this->performance->detach($performanceToRemove);
    }

    /**
     * Returns the performance
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Performance> $performance
     */
    public function getPerformance()
    {
        return $this->performance;
    }

    /**
     * Sets the performance
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Performance> $performance
     * @return void
     */
    public function setPerformance(ObjectStorage $performance)
    {
        $this->performance = $performance;
    }

    /**
     * Returns the ausfall
     *
     * @return bool $ausfall
     */
    public function getAusfall()
    {
        return $this->ausfall;
    }

    /**
     * Sets the ausfall
     *
     * @param bool $ausfall
     * @return void
     */
    public function setAusfall($ausfall)
    {
        $this->ausfall = $ausfall;
    }

    /**
     * Returns the ausfallinfo
     *
     * @return string $ausfallinfo
     */
    public function getAusfallinfo()
    {
        return $this->ausfallinfo;
    }

    /**
     * Sets the ausfallinfo
     *
     * @param string $ausfallinfo
     * @return void
     */
    public function setAusfallinfo($ausfallinfo)
    {
        $this->ausfallinfo = $ausfallinfo;
    }

    /**
     * Get hidden flag
     *
     * @return int
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set hidden flag
     *
     * @param int $hidden hidden flag
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * Returns vorschausmall
     *
     * @return ObjectStorage<FileReference> $vorschausmall
     */
    public function getVorschausmall()
    {
        return $this->vorschausmall;
    }

    /**
     * Set vorschausmall
     *
     * @param ObjectStorage $vorschausmall
     */
    public function setVorschausmall(ObjectStorage $vorschausmall)
    {
        $this->vorschausmall = $vorschausmall;
    }

    /**
     * Adds a Vorschausmall
     *
     * @param FileReference $vorschausmall
     * @return void
     */
    public function addVorschausmall(FileReference $vorschausmall)
    {
        $this->vorschausmall->attach($vorschausmall);
    }

    /**
     * Removes a Vorschausmall
     *
     * @param FileReference $vorschausmallToRemove The Vorschausmall to be removed
     * @return void
     */
    public function removeVorschausmall(FileReference $vorschausmall)
    {
        $this->vorschausmall->detach($vorschausmall);
    }

    /**
     * Returns vorschaubig
     *
     * @return ObjectStorage<FileReference> $vorschaubig
     */
    public function getVorschaubig()
    {
        return $this->vorschaubig;
    }

    /**
     * Set vorschaubig
     *
     * @param ObjectStorage $vorschaubig
     */
    public function setVorschaubig(ObjectStorage $vorschaubig)
    {
        $this->vorschaubig = $vorschaubig;
    }

    /**
     * Adds a Vorschaubig
     *
     * @param FileReference $vorschaubig
     * @return void
     */
    public function addVorschaubig(FileReference $vorschaubig)
    {
        $this->vorschaubig->attach($vorschaubig);
    }

    /**
     * Removes a Vorschaubig
     *
     * @param FileReference $vorschaubigteToRemove The Vorschaubig to be removed
     * @return void
     */
    public function removeVorschaubig(FileReference $vorschaubig)
    {
        $this->vorschaubig->detach($vorschaubig);
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set time stamp
     *
     * @param \DateTime $tstamp time stamp
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns the urls
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Url> $urls
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * Sets the urls
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Url> $urls
     * @return void
     */
    public function setUrls($urls)
    {
        $this->urls = $urls;
    }

    /**
     * Adds a urls
     *
     * @param \JO\Theatercollection\Domain\Model\Url $urls
     * @return void
     */
    public function addUrls(\JO\Theatercollection\Domain\Model\Url $urls)
    {
        $this->urls->attach($urls);
    }

    /**
     * Removes a urls
     *
     * @param \JO\Theatercollection\Domain\Model\Url $urlsToRemove The Urls to be removed
     * @return void
     */
    public function removeUrls(\JO\Theatercollection\Domain\Model\Url $urls)
    {
        $this->urls->detach($urls);
    }

    /**
     * Returns the metas
     *
     * @return ObjectStorage<\JO\Theatercollection\Domain\Model\Data> $metas
     */
    public function getMetas()
    {
        return $this->metas;
    }

    /**
     * Sets the metas
     *
     * @param ObjectStorage<\JO\Theatercollection\Domain\Model\Data> $metas
     * @return void
     */
    public function setMetas($metas)
    {
        $this->metas = $metas;
    }

    /**
     * Adds a metas
     *
     * @param \JO\Theatercollection\Domain\Model\Data $metas
     * @return void
     */
    public function addMetas(\JO\Theatercollection\Domain\Model\Data $metas)
    {
        $this->metas->attach($metas);
    }

    /**
     * Removes a metas
     *
     * @param \JO\Theatercollection\Domain\Model\Url $metasToRemove The Metas to be removed
     * @return void
     */
    public function removeMetas(\JO\Theatercollection\Domain\Model\Data $metas)
    {
        $this->metas->detach($metas);
    }

    /**
     * Set sys language
     *
     * @param int $sysLanguageUid
     *
     * @return void
     */
    public function setSysLanguageUid($sysLanguageUid): void
    {
        $this->_languageUid = $sysLanguageUid;
    }

    /**
     * Get sys language
     *
     * @return int
     */
    public function getSysLanguageUid(): int
    {
        return $this->_languageUid;
    }

    /**
     * Set l10n parent
     *
     * @param int $l10nParent
     *
     * @return void
     */
    public function setL10nParent($l10nParent): void
    {
        $this->l10nParent = $l10nParent;
    }

    /**
     * Get l10n parent
     *
     * @return int
     */
    public function getL10nParent(): int
    {
        return $this->l10nParent;
    }
}
