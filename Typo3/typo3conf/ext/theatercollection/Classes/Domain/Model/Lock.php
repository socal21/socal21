<?php
namespace JO\Theatercollection\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/
/**
 * Lock
 */
class Lock extends AbstractEntity
{
    /**
     * objid
     *
     * @var string
     * 
     */
    protected $objid;

    /**
     * objidtyp
     *
     * @var string
     * 
     */
    protected $objidtyp;

    /**
     * user
     *
     * @var string
     * 
     */
    protected $user;

    /**
     * starttime
     *
     * @var string
     * 
     */
    protected $starttime;

    /**
     * lastupdate
     *
     * @var string
     * 
     */
    protected $lastupdate;

    /**
     * Returns the objid
     *
     * @return string $objid
     */
    public function getObjid()
    {
        return $this->objid;
    }

    /**
     * Sets the objid
     *
     * @param string $objid
     * @return void
     */
    public function setObjid($objid)
    {
        $this->objid = $objid;
    }

    /**
     * Returns the objidtyp
     *
     * @return string $objidtyp
     */
    public function getObjidtyp()
    {
        return $this->objidtyp;
    }

    /**
     * Sets the objidtyp
     *
     * @param string $objidtyp
     * @return void
     */
    public function setObjidtyp($objidtyp)
    {
        $this->objidtyp = $objidtyp;
    }

    /**
     * Returns the user
     *
     * @return string $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the user
     *
     * @param string $user
     * @return void
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Returns the starttime
     *
     * @return string $starttime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Sets the starttime
     *
     * @param string $starttime
     * @return void
     */
    public function setStarttime($starttime)
    {
        $this->starttime = $starttime;
    }

    /**
     * Returns the lastupdate
     *
     * @return string $lastupdate
     */
    public function getLastupdate()
    {
        return $this->lastupdate;
    }

    /**
     * Sets the lastupdate
     *
     * @param string $lastupdate
     * @return void
     */
    public function setLastupdate($lastupdate)
    {
        $this->lastupdate = $lastupdate;
    }
}
