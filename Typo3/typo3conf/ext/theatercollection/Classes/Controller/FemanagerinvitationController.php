<?php
declare (strict_types = 1);
namespace JO\Theatercollection\Controller;

use In2code\Femanager\Controller\InvitationController as FE_InvatationController;
use In2code\Femanager\Domain\Model\User;

/**
 * Class InvitationController
 */
class FemanagerinvitationController extends FE_InvatationController
{
    public function getJSON($url = null)
    {
        if (null != $url) {
            $ch = curl_init();
            $headers = [
                'Accept: application/json',
                'Content-type: application/json',
            ];
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            $response = curl_exec($ch);
            return $response;
        }
    }

    /**
     * action new
     */
    public function newAction()
    {
        $uri = 'http://xtree-actor-api.digicult-verbund.de/getRepositoryList?portalURI=http://digicult.vocnet.org/portal/p0314&count=100';
        $veranstalter = json_decode($this->getJSON($uri));
        $ver_arr = [];

        if (isset($veranstalter->Actor)) {
            foreach ($veranstalter->Actor as $value) {
                if (isset($value->name)) {
                    $ver_arr[$value->id] = $value->name;
                }
            }
        }

        $this->view->assign('veranstalterxtree', $ver_arr);

        parent::newAction();
    }

    /**
     * action create
     *
     * @param JO\Theatercollection\Domain\Model\User $user
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\ServersideValidator", param="user")
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\PasswordValidator", param="user")
     */
    public function createAction($user): void
    {
        if ($this->request->hasArgument('veranstalterxtree')) {
            $user->setVeranstalterxtree(filter_var($this->request->getArgument('veranstalterxtree'), FILTER_SANITIZE_STRING));
        }

        parent::createAction($user);
    }
 
    /**
     * action edit
     *
     * @param int $user User UID
     * @param string $hash
     */
    public function editAction($user, $hash = null)
    {
        parent::editAction($user, $hash);
    }

    /**
     * action update
     *
     * @param \In2code\Femanager\Domain\Model\User $user
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\ServersideValidator", param="user")
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\PasswordValidator", param="user")
     */
    public function updateAction($user)
    {
        parent::updateAction($user);
    }
}
