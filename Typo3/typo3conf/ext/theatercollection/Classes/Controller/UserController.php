<?php
namespace JO\Theatercollection\Controller;

/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/

use In2code\Femanager\Domain\Model\User;
use In2code\Femanager\Domain\Repository\UserRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * EditorController
 */
class UserController extends ActionController
{
    /**
     * @var \In2code\Femanager\Domain\Repository\UserRepository
     */
    protected $userRepository;

    /**
     * AbstractController constructor.
     * @param \In2code\Femanager\Domain\Repository\UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * action loadform
     *
     * @return void
     */
    public function editAction()
    {
        if ($GLOBALS['TSFE']->fe_user->user) {
            $this->view->assign('user', $GLOBALS['TSFE']->fe_user->user['uid']);
        }
    }

    /**
     * action termedit
     *
     * @return void
     */
    public function termupdateAction()
    {
        if ($this->request->hasArgument('user')) {
            $args = filter_var_array($this->request->getArgument('user'), FILTER_SANITIZE_STRING);

            if ('' != $args['id'] && '' != $args['terms']) {
                $user = $this->userRepository->findByUid($args['id']);
                $user->setTerms(true);
                $this->userRepository->update($user);

                $persistenceManager = $this->objectManager->get(PersistenceManager::class);
                $persistenceManager->persistAll();
            }
        }

        $uri = $this->uriBuilder->reset()->setTargetPageUid(5)->build();
        $this->redirectToURI($uri);
    }
}
