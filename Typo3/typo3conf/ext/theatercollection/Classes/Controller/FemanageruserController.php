<?php
namespace JO\Theatercollection\Controller;

use In2code\Femanager\Controller\UserController as FE_UserController;
use JO\Theatercollection\Domain\Model\User;
use JO\Theatercollection\Domain\Model\Veranstalter;
use JO\Theatercollection\Domain\Repository\VeranstalterRepository;

class FemanageruserController extends FE_UserController
{

    /**
     * veranstalterRepository
     *
     * @var VeranstalterRepository
     */
    protected $veranstalterRepository = null;

    /**
     * @param VeranstalterRepository $veranstalterRepository
     */
    public function injectVeranstalterRepository(VeranstalterRepository $veranstalterRepository)
    {
        $this->veranstalterRepository = $veranstalterRepository;
    }

    /**
     * @param User $user
     */
    public function showAction(User $user = null)
    {
        $user = $this->getUser($user);

        // kann aus user nicht veranstalter hohlen, deswegen GLOBALS user benutzen
        $user2 = $GLOBALS['TSFE']->fe_user->user;

        if ($user->getVeranstalter()) {
            $veranstalter = $this->veranstalterRepository->findByUid($user->getVeranstalter());
            $this->view->assign('veranstalter', $veranstalter);
        }

        if ($user2['veranstalterxtree']) {
            // \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($this->settings);
            $uri = 'http://xtree-actor-api.digicult-verbund.de/getRepositoryItem?id=' . $user2['veranstalterxtree'];
            $veranstalter = json_decode($this->getJSON($uri));
            $veranstalter_out = '';
            if (isset($veranstalter->Actor)) {
                $act = $veranstalter->Actor;
                if (isset($act->name)) {
                    foreach ($act->name as $k => $v) {
                        if (isset($v->label) && isset($v->role)) {
                            if ('preferred' == $v->role) {
                                $veranstalter_out = $v->label;
                                break;
                            } else if ('' == $veranstalter_out) {
                                $veranstalter_out = $v->label;
                            }
                        }
                    }
                }
            }

            if ('' != $veranstalter_out) {
                $this->view->assign('veranstalter', $veranstalter);
            }
        }

        $this->view->assign('user', $user);
        $this->assignForAll();
    }

    public function getJSON($url = null)
    {
        if (null != $url) {
            $ch = curl_init();
            $headers = [
                'Accept: application/json',
                'Content-type: application/json',
            ];
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            $response = curl_exec($ch);
            return $response;
        }
    }
}
