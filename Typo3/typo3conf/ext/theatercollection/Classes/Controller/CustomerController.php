<?php
namespace JO\Theatercollection\Controller;

/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 *
 *  (c) 2020 Carsten Resch <info@justorange.org>, JUSTORANGE
 *
 ***/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * CustomerController
 */
class CustomerController extends ActionController
{
    public $server = null;

    public $token = null;

    public $facettes = [];

    public $allowedarguments = [
        'addorganizer' => 'int',
        'removeorganizer' => 'int',
        'addspielort' => 'int',
        'removespielort' => 'int',
        'addveranstalter' => 'int',
        'removeveranstalter' => 'int',
    ];

    /**
     * @var frontendUserRepository
     */
    private $frontendUserRepository;

    /**
     * Inject the tbox repository
     *
     * @param FrontendUserRepository $frontendUserRepository
     */
    public function injectFrontendUserRepository(FrontendUserRepository $frontendUserRepository)
    {
        $this->frontendUserRepository = $frontendUserRepository;
    }

    public function buildRequest($url = null)
    {
        if (null != $url) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }
    }

    /**
     *  Entfernt einen spezifischen Wert aus einem gegebenen Array
     *
     *  @var array $joArray -> Array aus dem der entsprechenden Wert entfernt werden soll
     *  @var string $joElementToDelete -> Element, das entfernt werden soll
     *
     *  @return Array -> bereinigtes Array
     */
    public function joEliminateArrayValueAndKey($joArray = [], $joElementToDelete = "")
    {
        if (!empty($joArray) && !empty($joElementToDelete)) {
            $joKeyToDelete = array_search($joElementToDelete, $joArray);
            if (false !== $joKeyToDelete) {
                unset($joArray[$joKeyToDelete]);
                $joArray = array_values($joArray);
            }
        }
        return $joArray;
    }

    /**
     *  Fügt einem gegebenen Array einen Wert hinzu und macht es unique
     *
     *  @var array $joArray -> Array aus dem der entsprechenden Wert hinzugefügt werden soll
     *  @var string $joElementToAdd -> Element, das hinzugefügt werden soll
     *
     *  @return Array -> bereinigtes Array
     */
    public function joAddToArrayAndMakeUnique($joArray = [], $joElementToAdd = "")
    {
        if (empty($joArray)) {
            $joArray = [];
        }
        if (!empty($joElementToAdd)) {
            array_push($joArray, $joElementToAdd);
            $joArray = array_unique($joArray);
            $joArray = array_values($joArray);
        }
        return $joArray;
    }

    /**
     *  Löscht aus einem Array alle Keys außer den übergebenen
     *
     *  @var array $joArray -> Array das geleert werden soll
     *  @var string $joKeysToKeep -> Elemente, die beibehalten werden sollen
     *
     *  @return Array -> bereinigtes Array
     */
    public function joDeleteArrayExcept($joArray = [], $joKeysToKeep = [])
    {
        if (!empty($joArray)) {
            if (!empty($joKeysToKeep)) {
                $joTempArray = [];
                foreach ($joKeysToKeep as $value) {
                    $joTempArray[$value] = $joArray[$value];
                }
                $joArray = [];
                $joArray = $joTempArray;
            } else {
                $joArray = [];
            }
        }
        return $joArray;
    }

    public function addValue($session_name = null, $session_value = null)
    {
        $session_items = [];
        if (null != $session_name && null != $session_value) {
            $session_items = $this->getSessionValues($session_name);
            $session_items = array_filter($this->joAddToArrayAndMakeUnique($session_items, $session_value));
            $_SESSION[$session_name] = $session_items;
        }
        return $session_items;
    }

    public function removeValue($session_name = null, $session_value = null)
    {
        $session_items = [];
        if (null != $session_name && null != $session_value) {
            $session_items = $this->getSessionValues($session_name);
            $session_items = array_filter($this->joEliminateArrayValueAndKey($session_items, $session_value));
            if (count($session_items) > 0) {
                $_SESSION[$session_name] = $session_items;
            } else {
                $this->emptySession($session_name);
            }
        }
        return $session_items;
    }

    public function getSessionValues($session_name = null)
    {
        $session_items = [];
        if (null != $session_name) {
            if (!session_id()) {
                @session_start();
            }

            if ($_SESSION[$session_name]) {
                $session_items = $_SESSION[$session_name];
            }
        }
        return $session_items;
    }

    public function emptySession($session_name = null)
    {
        if (null != $session_name) {
            if (!session_id()) {
                @session_start();
            }

            unset($_SESSION[$session_name]);
        }
    }

    public function replaceAllValues($session_name = null, $session_value)
    {
        if (null != $session_name) {
            if (!session_id()) {
                @session_start();
            }

            $_SESSION[$session_name] = $session_value;
        }
    }

    /**
     * Initialize the view
     *
     * @param ViewInterface $view The view
     * @return void
     */
    public function initializeView(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface $view) {}

    /**
     *    Anzahl der Treffer an das Template geben
     *    Pagination ermitteln
     */
    public function makePagination($objectsfound, $limit, $numberofpaginatepages, $page)
    {
        $paginatedata = [];
        if ($limit > 0) {
            $offset = ($page - 1) * $limit;
            $numberpages = ceil($objectsfound / $limit);
            $paginatestart = 1; //    An welcher Stelle beginnt die Pagination initial
            $paginateend = $numberpages; //    Letzte Paginatorseite
            if ($numberofpaginatepages < $numberpages) {
                $halfnumberofpages = $numberofpaginatepages / 2; //    Die Hälfte der anzuzeigenden Paginationsdaten
                $paginatestart = $page - $halfnumberofpages;
                $paginateend = $page + $halfnumberofpages;
                if ($paginatestart <= 0) {
                    $offsetstart = abs($paginatestart);
                    $paginatestart = 1;
                }
                if ($paginateend > $numberpages) {
                    $offsetend = abs($numberpages - $paginateend);
                    $paginateend = $numberpages;
                }
                $paginatestart = $paginatestart - $offsetend;
                $paginateend = $paginateend + $offsetstart;
            }
            $paginaterange = range($paginatestart, $paginateend);
            $paginatedata = [
                "range" => $paginaterange,
                "aktiv" => $page,
                "links" => $page - 1,
                "rechts" => $page + 1,
            ];
            if ($paginatedata["links"] < 1) {
                $paginatedata["links"] = 1;
            }

            if ($paginatedata["rechts"] > $paginateend) {
                $paginatedata["rechts"] = $paginateend;
            }

            if (1 != $paginaterange[0]) {
                $paginatedata["first"] = 1;
            }

            if ($paginaterange[(count($paginaterange) - 1)] != $numberpages) {
                $paginatedata["last"] = $numberpages;
            }
        }
        return $paginatedata;
    }

    /**
     * Auswahl einzelner Segmente
     *
     * @return void
     */
    public function readAction()
    {
        $limit = 20;
        $pagecount = 20;
        $session_var_name = "eventsearch_" . $GLOBALS['TSFE']->id; //    Name der Session, in der die Suchparameter gespeichert werden
        if ($this->request->hasArgument('removefilters')) {
            $this->emptySession($session_var_name);
        }
        $searcharray = $this->getSessionValues($session_var_name);
        $search = false;
        $arguments = $this->request->getArguments();
        if (!empty($arguments)) {
            foreach ($arguments as $key => $a) {
                if (array_key_exists($key, $this->allowedarguments)) {
                    if (strpos($key, 'add') !== false) {
                        if (is_array($searcharray['content'][$key])) {
                            $searcharray['content'][$key][] = intval($a);
                        } else {
                            $searcharray['content'][$key] = [intval($a)];
                        }
                    } else {
                        $key = str_replace('remove', 'add', $key);
                        $array_to_remove = [intval($a)];
                        $searcharray['content'][$key] = array_diff($searcharray['content'][$key], $array_to_remove);
                    }
                    $searcharray['content'][$key] = array_values(array_unique($searcharray['content'][$key]));
                }
            }
            if (!empty($searcharray['content'])) {
                $searcharray['content'] = array_filter($searcharray['content']);
            }

            if (!empty($searcharray)) {
                $searcharray = array_filter($searcharray);
            }

            $this->replaceAllValues($session_var_name, $searcharray);
        }
        //  \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($searcharray);

        // Feuser auslesen
        //   $feusers = $this->frontendUserRepository->findAll();
        // Veranstalter
        $uri = 'https://web60.server1.justorange.org/api/?type=2328&search=veranstalter&apiKey=4d6c276ac285ac0ed5f73320a3a16f0b';
        $veranstalter = file_get_contents($uri);
        $veranstalterarray = [];
        if ($veranstalter) {
            $veranstalterarray = json_decode($veranstalter, true);
        }
        // Spielorte
        $uri = 'https://web60.server1.justorange.org/api/?type=2328&search=spielort&apiKey=4d6c276ac285ac0ed5f73320a3a16f0b';
        $spielorte = file_get_contents($uri);
        $spielortearray = [];
        if ($spielorte) {
            $spielortearray = json_decode($spielorte, true);
        }
        // Filter setzen
        $filter_string = '';
        if (!empty($searcharray)) {
            foreach ($searcharray['content'] as $k => $v) {
                switch ($k) {
                    case 'addveranstalter':
                        $filter_string .= implode('', substr_replace($v, '&hide=1&by_veranstalter_ids[]=', 0, 0));
                        break;
                    case 'addspielort':
                        $filter_string .= implode('', substr_replace($v, '&hide=1&by_spielort_ids[]=', 0, 0));
                        break;
                }
            }
        }
        // Events
        $page = 1;
        if (GeneralUtility::_GP("page")) {
            $page = intval(GeneralUtility::_GP("page"));
            $offset = (intval(GeneralUtility::_GP("page")) - 1) * $limit;
        }
        $uri = 'https://web60.server1.justorange.org/api/?type=2328&search=theater&apiKey=4d6c276ac285ac0ed5f73320a3a16f0b&start_date=10-01-2020' . $filter_string;
        $events = file_get_contents($uri);
        $eventsarray = [];
        if ($events) {
            $eventsarray = json_decode($events, true);
            // Pagination der Resultate
            /*
        $paginationdata = $this->makePagination(
        count($eventsarray['theater'][0]['spielzeit']),
        $limit,
        $pagecount,
        $page
        );
         */
        }

        //   \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($eventsarray);

        $result = [
            'feusers' => $feusers,
            'searcharray' => $searcharray,
            'veranstalterarray' => $veranstalterarray,
            'spielortearray' => $spielortearray,
            'eventsarray' => $eventsarray,
            'paginationdata' => $paginationdata,
        ];
        //
        $this->view->assignMultiple($result);
        // \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($feusers);
    }
}
