<?php
namespace JO\Theatercollection\Controller;

/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/

use JO\Theatercollection\Domain\Repository\PerformanceRepository;
use JO\Theatercollection\Domain\Repository\SpielortRepository;
use JO\Theatercollection\Domain\Repository\SpielzeitRepository;
use JO\Theatercollection\Domain\Repository\TheaterRepository;
use JO\Theatercollection\Domain\Repository\VeranstalterRepository;
use JO\Theatercollection\Domain\Repository\VeranstaltungsartRepository;
use JO\Theatercollection\Domain\Repository\VeranstaltungstypeRepository;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\IpAnonymizationUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;

/**
 * TheaterController
 */
class TheaterController extends ActionController
{

    /**
     * performanceRepository
     *
     * @var PerformanceRepository
     */
    protected $performanceRepository = null;

    /**
     * veranstalterRepository
     *
     * @var VeranstalterRepository
     */
    protected $veranstalterRepository = null;

    /**
     * spielortRepository
     *
     * @var SpielortRepository
     */
    protected $spielortRepository = null;

    /**
     * spielzeitRepository
     *
     * @var SpielzeitRepository
     */
    protected $spielzeitRepository = null;

    /**
     * theaterRepository
     *
     * @var TheaterRepository
     */
    protected $theaterRepository = null;

    /**
     * veranstaltungsartRepository
     *
     * @var VeranstaltungsartRepository
     */
    protected $veranstaltungsartRepository = null;

    /**
     * veranstaltungstypeRepository
     *
     * @var VeranstaltungstypeRepository
     */
    protected $veranstaltungstypeRepository = null;

    /**
     * Logger
     *
     * @var Logger
     */
    protected $logger = null;

    /**
     * AbstractController constructor.
     * @param PerformanceRepository $performanceRepository
     * @param VeranstalterRepository $veranstalterRepository
     * @param SpielortRepository $spielortRepository
     * @param SpielzeitRepository $spielzeitRepository
     * @param TheaterRepository $theaterRepository
     * @param VeranstaltungsartRepository $veranstaltungsartRepository
     * @param VeranstaltungstypeRepository $veranstaltungstypeRepository
     */
    public function __construct(
        PerformanceRepository $performanceRepository,
        VeranstalterRepository $veranstalterRepository,
        VeranstaltungstypeRepository $veranstaltungstypeRepository,
        VeranstaltungsartRepository $veranstaltungsartRepository,
        TheaterRepository $theaterRepository,
        SpielzeitRepository $spielzeitRepository,
        SpielortRepository $spielortRepository)
    {
        $this->performanceRepository = $performanceRepository;
        $this->veranstalterRepository = $veranstalterRepository;
        $this->spielortRepository = $spielortRepository;
        $this->spielzeitRepository = $spielzeitRepository;
        $this->theaterRepository = $theaterRepository;
        $this->veranstaltungsartRepository = $veranstaltungsartRepository;
        $this->veranstaltungstypeRepository = $veranstaltungstypeRepository;
    }

    public function initializeView(ViewInterface $view)
    {
        $this->logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);

        // Offset und Limit
        $this->offset = 0;
        $this->limit = 20;

        if (!is_null(GeneralUtility::_GP('offset'))) {
            $this->offset = intval(GeneralUtility::_GP('offset'));
        }

        if (!is_null(GeneralUtility::_GP('limit')) && intval(GeneralUtility::_GP('limit') <= 1000)) {
            $this->limit = intval(GeneralUtility::_GP('limit'));
        }

        if (0 == $this->limit) {
            $this->limit = 'unset';
        }
    }

    public function validateAPIKey()
    {
        $return = false;
        if (GeneralUtility::_GP('apiKey') && !empty($this->settings['apikeys'])) {
            $key = filter_var(GeneralUtility::_GP('apiKey'), FILTER_SANITIZE_STRING);
            foreach ($this->settings['apikeys'] as $ak) {
                if ($ak['key'] == $key) {
                    $return = $ak;
                }
            }
        }
        return $return;
    }

    /**
     * action api
     *
     * @return void
     */
    public function apiAction()
    {
        $apiuser = $this->validateAPIKey();
        if ($apiuser && !empty($apiuser) && isset($apiuser['key'])) {
            if (GeneralUtility::_GP('search')) {
                $type = filter_var(GeneralUtility::_GP('search'), FILTER_SANITIZE_STRING);
                $style = filter_var(GeneralUtility::_GP('style'), FILTER_SANITIZE_STRING);

                $output = [];

                switch ($style) {
                    case 'json-ld':
                        switch ($type) {
                            case 'performance':
                                $output = $this->performanceLDResponse();
                                break;
                            case 'mitglieder':
                                $output = $this->veranstalterLDResponse();
                                break;
                            default:
                                $output = ['output' => 'nothing found'];
                        }
                        break;
                    case 'xml':
                    case 'xmltojson':
                        switch ($type) {
                            case 'veranstaltungsart':
                                $output = $this->veranstaltungsartXMLResponse();
                                break;
                            case 'performance':
                                $output = $this->performanceXMLResponse($style);
                                break;
                            case 'veranstaltungsorte':
                                $output = $this->veranstaltungsartXMLResponse();
                                break;
                            default:
                                $output = ['output' => 'nothing found'];
                        }
                        break;
                    default:
                        switch ($type) {
                            case 'performance':
                                $output = $this->performanceResponse();
                                break;
                            case 'veranstalter':
                                $output = $this->veranstalterResponse();
                                break;
                            case 'spielort':
                                $output = $this->spielortResponse();
                                break;
                            case 'organisation':
                                $output = $this->theaterResponse();
                                break;
                            case 'veranstaltungsart':
                                $output = $this->veranstaltungsartResponse();
                                break;
                            case 'veranstaltungstype':
                                $output = $this->veranstaltungstypeResponse();
                                break;
                            default:
                                $output = ['output' => 'nothing found'];
                        }
                }

                $anonymIp = IpAnonymizationUtility::anonymizeIp(GeneralUtility::getIndpEnv('REMOTE_ADDR'), 2);
                $this->logger->notice('API zugriff. Style: ' . $style . '. Typ: ' . $type . '. Key: ' . $apiuser['key'] . '. IP (anonym): ' . $anonymIp);

                if (GeneralUtility::_GP('jodebug')) {
                    \TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($output);
                }

                if ('xml' == $style) {
                    if ($output instanceof \DOMDocument) {
                        $this->view->assign('output', $output->saveXML());
                    }
                } else if ('xmltojson' == $style) {
                    $outjson = json_encode(simplexml_import_dom($output->documentElement), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

                    $this->view->assign('output', $outjson);
                } else {
                    $this->view->assign('output', json_encode($output, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
                }
            } else {
                $this->view->assign('output', json_encode(['output' => 'No Input']));
            }
        } else {
            $this->view->assign('output', json_encode(['output' => 'No or wrong API-Key']));
        }
    }

    public function performanceResponse()
    {
        if (GeneralUtility::_GP('qry')) {
            $demand['qry'] = filter_var(GeneralUtility::_GP('qry'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('by_ids')) {
            $demand['by_ids'] = filter_var_array(GeneralUtility::_GP('by_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('by_organisation_ids')) {
            $demand['by_organisation_ids'] = filter_var_array(GeneralUtility::_GP('by_organisation_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('by_veranstaltungsart_ids')) {
            $demand['by_veranstaltungsart_ids'] = filter_var_array(GeneralUtility::_GP('by_veranstaltungsart_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('by_veranstaltungstype_ids')) {
            $demand['by_veranstaltungstype_ids'] = filter_var_array(GeneralUtility::_GP('by_veranstaltungstype_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('start_date')) {
            $demand['start_date'] = new \Datetime(filter_var(GeneralUtility::_GP('start_date'), FILTER_SANITIZE_STRING));
        }

        if (GeneralUtility::_GP('end_date')) {
            $demand['end_date'] = new \Datetime(filter_var(GeneralUtility::_GP('end_date'), FILTER_SANITIZE_STRING));
        }

        $demand['fullInfo'] = GeneralUtility::_GP('fullInfo') ? true : false;

        $demand['limit'] = $this->limit;
        $demand['offset'] = $this->offset;

        $performance = [];
        if ($demand['by_organisation_ids'] && '' != $demand['by_organisation_ids']) {
            $th_demand['by_ids'] = $demand['by_organisation_ids'];
            $theater = $this->theaterRepository->searchByJODemand($th_demand);

            $performance = [];
            foreach ($theater as $key => $value) {
                if ($value->getSpielzeit()) {
                    foreach ($value->getSpielzeit() as $s_key => $s_value) {
                        if ($s_value->getPerformance()) {
                            foreach ($s_value->getPerformance() as $p_key => $p_value) {
                                $performance[$p_key] = $p_value;
                            }
                        }
                    }
                }
            }
        } else {
            $performance = $this->performanceRepository->searchByJODemand($demand);
        }

        $output_perf = $this->buildPerformance($performance, $demand['fullInfo']);

        $meta = [
            'numFound' => count($output_perf),
            'limit' => $this->limit,
            'offset' => $this->offset,
        ];

        return ['performance' => $output_perf, 'meta' => $meta];
    }

    public function buildPerformance($performance, $full = true)
    {
        $output_perf = [];
        foreach ($performance as $key => $value) {
            $cur_perf = [];

            $cur_perf['id'] = $this->maskId($value->getUid());

            if ($value->getName()) {
                $cur_perf['name'] = $value->getName();
            }
            if ($value->getVorschausmall() && $value->getVorschausmall()[0]) {
                $url = $value->getVorschausmall()[0]->getOriginalResource()->getOriginalFile()->getPublicUrl();
                $cur_perf['vorschausmall']['url'] = $this->request->getBaseUri() . $url;
                if ($value->getVorschausmall()[0]->getOriginalResource()->getTitle()) {
                    $cur_perf['vorschausmall']['title'] = $value->getVorschausmall()[0]->getOriginalResource()->getTitle();
                }
                if ($value->getVorschausmall()[0]->getOriginalResource()->getDescription()) {
                    $cur_perf['vorschausmall']['desc'] = $value->getVorschausmall()[0]->getOriginalResource()->getDescription();
                }
            }
            if ($value->getVorschaubig() && $value->getVorschaubig()[0]) {
                $url = $value->getVorschaubig()[0]->getOriginalResource()->getOriginalFile()->getPublicUrl();
                $cur_perf['vorschaubig']['url'] = $this->request->getBaseUri() . $url;
                if ($value->getVorschaubig()[0]->getOriginalResource()->getTitle()) {
                    $cur_perf['vorschaubig']['title'] = $value->getVorschaubig()[0]->getOriginalResource()->getTitle();
                }
                if ($value->getVorschaubig()[0]->getOriginalResource()->getDescription()) {
                    $cur_perf['vorschaubig']['desc'] = $value->getVorschaubig()[0]->getOriginalResource()->getDescription();
                }
            }
            if ($value->getAusfallenvon()) {
                $cur_perf['ausgefallen']['yes'] = 1;
                $cur_perf['ausgefallen']['von'] = $value->getAusfallenvon()->format('Y-m-d');
            }
            if ($value->getAusfallenbis()) {
                $cur_perf['ausgefallen']['yes'] = 1;
                $cur_perf['ausgefallen']['bis'] = $value->getAusfallenbis()->format('Y-m-d');
            }
            if (($value->getAusfallenvon() || $value->getAusfallenbis()) && $value->getAusfallinfo()) {
                $cur_perf['ausgefallen']['ausgefallenInfo'] = $value->getAusfallinfo();
            }
            if ($value->getInfo()) {
                $cur_perf['info'] = $value->getInfo();
            }
            if ($value->getInfouri()) {
                $cur_perf['infouri'] = $value->getInfouri();
            }
            if ($value->getVeranstalter() && sizeof($value->getVeranstalter()) > 0) {
                $cur_perf['veranstalter'] = [];
                foreach ($value->getVeranstalter() as $k => $v) {
                    $tmp_ver = [];
                    $tmp_ver['id'] = $this->maskId($v->getUid());
                    $tmp_ver['name'] = $v->getName();

                    $cur_perf['veranstalter'][] = $tmp_ver;
                }
            }
            if ($full) {
                if ($value->getBeschreibung()) {
                    $cur_perf['beschreibung'] = $value->getBeschreibung();
                }
                if ($value->getLangbeschreibung()) {
                    $cur_perf['langbeschreibung'] = $value->getLangbeschreibung();
                }
                if ($value->getAutor()) {
                    $cur_perf['autor'] = $value->getAutor();
                }
                if ($value->getAltersfreigabe()) {
                    $cur_perf['altersfreigabe'] = $value->getAltersfreigabe();
                }
                if ($value->getArt() && sizeof($value->getArt()) > 0) {
                    $cur_perf['art'] = [];
                    foreach ($value->getArt() as $k => $v) {
                        $cur_perf['art'][] = $v->getName();
                    }
                }
                if ($value->getTyp() && sizeof($value->getTyp()) > 0) {
                    $cur_perf['typ'] = [];
                    foreach ($value->getTyp() as $k => $v) {
                        $cur_perf['typ'][] = $v->getName();
                    }
                }
                if ($value->getUrls()) {
                    foreach ($value->getUrls() as $url_key => $url_val) {
                        if ($url_val->getUrl()) {
                            $json_key = '';
                            if ($url_val->getFieldid()) {
                                $json_key = $url_val->getFieldid();
                            } else {
                                $json_key = strtolower(str_replace(' ', '', $url_val->getTitle()));
                            }

                            $cur_perf['urls'][$json_key]['title'] = $url_val->getTitle();
                            $cur_perf['urls'][$json_key]['url'] = $url_val->getUrl();
                        }
                    }
                }
                if ($value->getMetas()) {
                    foreach ($value->getMetas() as $meta_key => $meta_val) {
                        if ($meta_val->getValue()) {
                            $json_key = '';
                            if ($meta_val->getFieldid()) {
                                $json_key = $meta_val->getFieldid();
                            } else {
                                $json_key = strtolower(str_replace(' ', '', $meta_val->getTitle()));
                            }

                            $cur_perf['metas'][$json_key]['title'] = $meta_val->getTitle();
                            $cur_perf['metas'][$json_key]['value'] = $meta_val->getValue();
                        }
                    }
                }
                if ($value->getSaisonstart()) {
                    $cur_perf['saisonstart'] = $value->getSaisonstart()->format('Y-m-d');
                }
                if ($value->getSaisonend()) {
                    $cur_perf['saisonend'] = $value->getSaisonend()->format('Y-m-d');
                }
                if ($value->getBooking()) {
                    $cur_perf['booking'] = $value->getBooking();
                }

                $cur_perf['lastChanged'] = $value->getTstamp()->format('Y-m-d');
            }

            $output_perf[] = $cur_perf;
        }

        return $output_perf;
    }

    public function veranstalterResponse()
    {
        if (GeneralUtility::_GP('qry')) {
            $demand['qry'] = filter_var(GeneralUtility::_GP('qry'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('by_ids')) {
            $demand['by_ids'] = filter_var_array(GeneralUtility::_GP('by_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('plz')) {
            $demand['plz'] = filter_var(GeneralUtility::_GP('plz'), FILTER_SANITIZE_STRING);
        }

        $demand['fullInfo'] = GeneralUtility::_GP('fullInfo') ? true : false;

        $demand['limit'] = $this->limit;
        $demand['offset'] = $this->offset;

        $veranstalter = $this->veranstalterRepository->searchByJODemand($demand);

        $output_ver = $this->buildVeranstalter($veranstalter, $demand['fullInfo']);

        $meta = [
            'numFound' => count($veranstalter),
            'limit' => $this->limit,
            'offset' => $this->offset,
        ];

        return ['veranstalter' => $output_ver, 'meta' => $meta];
    }

    public function buildVeranstalter($veranstalter, $full = true)
    {
        $output_ver = [];

        foreach ($veranstalter as $key => $value) {
            $cur_ver = [];

            $cur_ver['id'] = $this->maskId($value->getUid());

            if ($value->getDigicultid()) {
                $cur_ver['digicultid'] = $value->getDigicultid();
            }
            if ($value->getName()) {
                $cur_ver['name'] = $value->getName();
            }
            if ($value->getVorschausmall() && $value->getVorschausmall()[0]) {
                $url = $value->getVorschausmall()[0]->getOriginalResource()->getOriginalFile()->getPublicUrl();
                $cur_ver['vorschausmall']['url'] = $this->request->getBaseUri() . $url;
                if ($value->getVorschausmall()[0]->getOriginalResource()->getTitle()) {
                    $cur_ver['vorschausmall']['title'] = $value->getVorschausmall()[0]->getOriginalResource()->getTitle();
                }
                if ($value->getVorschausmall()[0]->getOriginalResource()->getDescription()) {
                    $cur_ver['vorschausmall']['desc'] = $value->getVorschausmall()[0]->getOriginalResource()->getDescription();
                }
            }
            if ($value->getVorschaubig() && $value->getVorschaubig()[0]) {
                $url = $value->getVorschaubig()[0]->getOriginalResource()->getOriginalFile()->getPublicUrl();
                $cur_ver['vorschaubig']['url'] = $this->request->getBaseUri() . $url;
                if ($value->getVorschaubig()[0]->getOriginalResource()->getTitle()) {
                    $cur_ver['vorschaubig']['title'] = $value->getVorschaubig()[0]->getOriginalResource()->getTitle();
                }
                if ($value->getVorschaubig()[0]->getOriginalResource()->getDescription()) {
                    $cur_ver['vorschaubig']['desc'] = $value->getVorschaubig()[0]->getOriginalResource()->getDescription();
                }
            }
            if ($full) {
                if ($value->getOrt()) {
                    $cur_ver['ort'] = $value->getOrt();
                }
                if ($value->getPlz()) {
                    $cur_ver['plz'] = $value->getPlz();
                }
                if ($value->getStrasse()) {
                    $cur_ver['strasse'] = $value->getStrasse();
                }
                if ($value->getAnsprechperson()) {
                    $cur_ver['ansprechperson'] = $value->getAnsprechperson();
                }
                if ($value->getEmail()) {
                    $cur_ver['email'] = $value->getEmail();
                }
                if ($value->getTelefon()) {
                    $cur_ver['telefon'] = $value->getTelefon();
                }
                if ($value->getMobil()) {
                    $cur_ver['mobil'] = $value->getMobil();
                }
                if ($value->getFax()) {
                    $cur_ver['fax'] = $value->getFax();
                }
                if ($value->getVorbestellungtel()) {
                    $cur_ver['vorbestellungtel'] = $value->getVorbestellungtel();
                }
                if ($value->getVorbestellungemail()) {
                    $cur_ver['vorbestellungemail'] = $value->getVorbestellungemail();
                }
                if ($value->getUrls()) {
                    foreach ($value->getUrls() as $url_key => $url_val) {
                        if ($url_val->getUrl()) {
                            $json_key = '';
                            if ($url_val->getFieldid()) {
                                $json_key = $url_val->getFieldid();
                            } else {
                                $json_key = strtolower(str_replace(' ', '', $url_val->getTitle()));
                            }

                            $cur_ver['urls'][$json_key]['title'] = $url_val->getTitle();
                            $cur_ver['urls'][$json_key]['url'] = $url_val->getUrl();
                        }
                    }
                }
                if ($value->getMetas()) {
                    foreach ($value->getMetas() as $meta_key => $meta_val) {
                        if ($meta_val->getValue()) {
                            $json_key = '';
                            if ($meta_val->getFieldid()) {
                                $json_key = $meta_val->getFieldid();
                            } else {
                                $json_key = strtolower(str_replace(' ', '', $meta_val->getTitle()));
                            }

                            $cur_ver['metas'][$json_key]['title'] = $meta_val->getTitle();
                            $cur_ver['metas'][$json_key]['value'] = $meta_val->getValue();
                        }
                    }
                }

                $cur_ver['lastChanged'] = $value->getTstamp()->format('Y-m-d');
            }

            $output_ver[] = $cur_ver;
        }

        return $output_ver;
    }

    public function spielortResponse()
    {
        if (GeneralUtility::_GP('qry')) {
            $demand['qry'] = filter_var(GeneralUtility::_GP('qry'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('by_ids')) {
            $demand['by_ids'] = filter_var_array(GeneralUtility::_GP('by_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('plz')) {
            $demand['plz'] = filter_var(GeneralUtility::_GP('plz'), FILTER_SANITIZE_STRING);
        }

        $demand['fullInfo'] = GeneralUtility::_GP('fullInfo') ? true : false;

        $demand['limit'] = $this->limit;
        $demand['offset'] = $this->offset;

        $spielort = $this->spielortRepository->searchByJODemand($demand);

        $output_sp = $this->buildSpielort($spielort, $demand['fullInfo']);

        $meta = [
            'numFound' => count($spielort),
            'limit' => $this->limit,
            'offset' => $this->offset,
        ];

        return ['spielort' => $output_sp, 'meta' => $meta];
    }

    public function buildSpielort($spielort, $full = true)
    {
        $output_sp = [];

        foreach ($spielort as $key => $value) {
            $cur_sp = [];

            $cur_sp['id'] = $this->maskId($value->getUid());

            if ($value->getDigicultid()) {
                $cur_sp['digicultid'] = $value->getDigicultid();
            }
            if ($value->getName()) {
                $cur_sp['name'] = $value->getName();
            }
            if ($value->getVorschausmall() && $value->getVorschausmall()[0]) {
                $url = $value->getVorschausmall()[0]->getOriginalResource()->getOriginalFile()->getPublicUrl();
                $cur_sp['vorschausmall']['url'] = $this->request->getBaseUri() . $url;
                if ($value->getVorschausmall()[0]->getOriginalResource()->getTitle()) {
                    $cur_sp['vorschausmall']['title'] = $value->getVorschausmall()[0]->getOriginalResource()->getTitle();
                }
                if ($value->getVorschausmall()[0]->getOriginalResource()->getDescription()) {
                    $cur_sp['vorschausmall']['desc'] = $value->getVorschausmall()[0]->getOriginalResource()->getDescription();
                }
            }
            if ($value->getVorschaubig() && $value->getVorschaubig()[0]) {
                $url = $value->getVorschaubig()[0]->getOriginalResource()->getOriginalFile()->getPublicUrl();
                $cur_sp['vorschaubig']['url'] = $this->request->getBaseUri() . $url;
                if ($value->getVorschaubig()[0]->getOriginalResource()->getTitle()) {
                    $cur_sp['vorschaubig']['title'] = $value->getVorschaubig()[0]->getOriginalResource()->getTitle();
                }
                if ($value->getVorschaubig()[0]->getOriginalResource()->getDescription()) {
                    $cur_sp['vorschaubig']['desc'] = $value->getVorschaubig()[0]->getOriginalResource()->getDescription();
                }
            }
            if ($value->getKultur()) {
                $cur_sp['kultur'] = $value->getKultur();
            }
            if ($full) {
                if ($value->getOrt()) {
                    $cur_sp['ort'] = $value->getOrt();
                }
                if ($value->getLon()) {
                    $cur_sp['lon'] = $value->getLon();
                }
                if ($value->getLat()) {
                    $cur_sp['lat'] = $value->getLat();
                }
                if ($value->getPlz()) {
                    $cur_sp['plz'] = $value->getPlz();
                }
                if ($value->getStrasse()) {
                    $cur_sp['strasse'] = $value->getStrasse();
                }
                if ($value->getAnsprechperson()) {
                    $cur_sp['ansprechperson'] = $value->getAnsprechperson();
                }
                if ($value->getEmail()) {
                    $cur_sp['email'] = $value->getEmail();
                }
                if ($value->getTelefon()) {
                    $cur_sp['telefon'] = $value->getTelefon();
                }
                if ($value->getMobil()) {
                    $cur_sp['mobil'] = $value->getMobil();
                }
                if ($value->getUrls()) {
                    foreach ($value->getUrls() as $url_key => $url_val) {
                        if ($url_val->getUrl()) {
                            $json_key = '';
                            if ($url_val->getFieldid()) {
                                $json_key = $url_val->getFieldid();
                            } else {
                                $json_key = strtolower(str_replace(' ', '', $url_val->getTitle()));
                            }

                            $cur_sp['urls'][$json_key]['title'] = $url_val->getTitle();
                            $cur_sp['urls'][$json_key]['url'] = $url_val->getUrl();
                        }
                    }
                }

                if ($value->getMetas()) {
                    foreach ($value->getMetas() as $meta_key => $meta_val) {
                        if ($meta_val->getValue()) {
                            $json_key = '';
                            if ($meta_val->getFieldid()) {
                                $json_key = $meta_val->getFieldid();
                            } else {
                                $json_key = strtolower(str_replace(' ', '', $meta_val->getTitle()));
                            }

                            $cur_sp['metas'][$json_key]['title'] = $meta_val->getTitle();
                            $cur_sp['metas'][$json_key]['value'] = $meta_val->getValue();
                        }
                    }
                }

                $cur_sp['lastChanged'] = $value->getTstamp()->format('Y-m-d');
            }

            $output_sp[] = $cur_sp;
        }

        return $output_sp;
    }

    public function theaterResponse()
    {
        $demand = [];
        if (GeneralUtility::_GP('qry')) {
            $demand['qry'] = filter_var(GeneralUtility::_GP('qry'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('by_ids')) {
            $demand['by_ids'] = filter_var_array(GeneralUtility::_GP('by_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('by_spielort_ids')) {
            $demand['by_spielort_ids'] = filter_var_array(GeneralUtility::_GP('by_spielort_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('plz')) {
            $demand['plz'] = filter_var(GeneralUtility::_GP('plz'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('by_veranstalter_ids')) {
            $demand['by_veranstalter_ids'] = array_map('intval', filter_var_array(GeneralUtility::_GP('by_veranstalter_ids'), FILTER_SANITIZE_NUMBER_INT));
        }

        if (GeneralUtility::_GP('by_performance_ids')) {
            $demand['by_performance_ids'] = filter_var_array(GeneralUtility::_GP('by_performance_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('hide')) {
            $demand['hide'] = filter_var(GeneralUtility::_GP('hide'), FILTER_SANITIZE_NUMBER_INT);
        }

        $demand['start_date'] = GeneralUtility::_GP('start_date') ? new \Datetime(filter_var(GeneralUtility::_GP('start_date'), FILTER_SANITIZE_STRING)) : new \Datetime();

        if (GeneralUtility::_GP('end_date')) {
            $demand['end_date'] = new \Datetime(filter_var(GeneralUtility::_GP('end_date'), FILTER_SANITIZE_STRING));
        }

        if (GeneralUtility::_GP('start_time')) {
            $demand['start_time'] = filter_var(GeneralUtility::_GP('start_time'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('end_time')) {
            $demand['end_time'] = filter_var(GeneralUtility::_GP('end_time'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('limitSpielzeit')) {
            $demand['limitSpielzeit'] = filter_var(GeneralUtility::_GP('limitSpielzeit'), FILTER_SANITIZE_NUMBER_INT);
        }

        $demand['fullInfo'] = GeneralUtility::_GP('fullInfo') ? true : false;

        $demand['limit'] = $this->limit;
        $demand['offset'] = $this->offset;

        $theater = $this->theaterRepository->searchByJODemand($demand);

        $output_th = $this->buildTheater($theater, $demand);

        if ($demand['limitSpielzeit'] && !empty($demand['limitSpielzeit'])) {
            /* Hole benötigte Datums eingaben, sortiere und begrenze diese */
            $tmp_sp = [];
            foreach ($output_th as $key => $value) {
                foreach ($value['spielzeit'] as $k => $val) {
                    $tmp_sp[] = $val['datum'];
                }
            }

            $tmp_sp = array_unique($tmp_sp);

            usort($tmp_sp, function ($val1, $val2) {
                return strtotime($val1) - strtotime($val2);
            });

            $tmp_sp = array_slice($tmp_sp, 0, $demand['limitSpielzeit']);

            /* erstelle neues array, gehe limit durch spielzeiten durch und  */
            $tmp_output_th = [];
            $l_arr = [];
            /* gehe limit durch da es max ist, bei jedem durchgang gehe alle spielzeiten durch und vergleiche datum */
            for ($i = $demand['limitSpielzeit']; $i > 0; $i--) {
                foreach ($output_th as $key => $value) {
                    foreach ($value['spielzeit'] as $k => $val) {
                        if (in_array($val['datum'], $tmp_sp) && !in_array($key . '$' . $k, $l_arr)) {
                            if (array_key_exists($key, $tmp_output_th)) {
                                $tmp_output_th[$key]['spielzeit'][$k] = $val;
                            } else {
                                $tmp_output_th[$key] = $value;
                                $tmp_output_th[$key]['spielzeit'] = [];
                                $tmp_output_th[$key]['spielzeit'][$k] = $val;
                            }
                            $l_arr[] = $key . '$' . $k;
                        }
                    }
                }
            }

            $output_th = $tmp_output_th;
        }

        $meta = [
            'numFound' => count($theater),
            'limit' => $this->limit,
            'offset' => $this->offset,
        ];

        return ['organisation' => $output_th, 'meta' => $meta];
    }

    public function buildTheater($theater, $demand = [])
    {
        date_default_timezone_set('UTC');
        $output_th = [];

        foreach ($theater as $key => $value) {
            $cur_th = [];

            $cur_th['id'] = $this->maskId($value->getUid());

            if ($value->getName()) {
                $cur_th['name'] = $value->getName();
            }
            if ($value->getTelefon()) {
                $cur_th['telefon'] = $value->getTelefon();
            }
            if ($value->getMobil()) {
                $cur_th['mobil'] = $value->getMobil();
            }
            if ($value->getFax()) {
                $cur_th['fax'] = $value->getFax();
            }
            if ($value->getEmail()) {
                $cur_th['email'] = $value->getEmail();
            }
            if ($value->getAnsprechperson()) {
                $cur_th['ansprechperson'] = $value->getAnsprechperson();
            }
            if ($value->getBeschreibung()) {
                $cur_th['beschreibung'] = $value->getBeschreibung();
            }
            if ($value->getLangbeschreibung()) {
                $cur_th['langbeschreibung'] = $value->getLangbeschreibung();
            }
            if ($value->getUrls()) {
                foreach ($value->getUrls() as $url_key => $url_val) {
                    if ($url_val->getUrl()) {
                        $json_key = '';
                        if ($url_val->getFieldid()) {
                            $json_key = $url_val->getFieldid();
                        } else {
                            $json_key = strtolower(str_replace(' ', '', $url_val->getTitle()));
                        }

                        $cur_th['urls'][$json_key]['title'] = $url_val->getTitle();
                        $cur_th['urls'][$json_key]['url'] = $url_val->getUrl();
                    }
                }
            }
            if ($value->getMetas()) {
                foreach ($value->getMetas() as $meta_key => $meta_val) {
                    if ($meta_val->getValue()) {
                        $json_key = '';
                        if ($meta_val->getFieldid()) {
                            $json_key = $meta_val->getFieldid();
                        } else {
                            $json_key = strtolower(str_replace(' ', '', $meta_val->getTitle()));
                        }

                        $cur_th['metas'][$json_key]['title'] = $meta_val->getTitle();
                        $cur_th['metas'][$json_key]['value'] = $meta_val->getValue();
                    }
                }
            }
            if ($value->getVorschausmall() && $value->getVorschausmall()[0]) {
                $url = $value->getVorschausmall()[0]->getOriginalResource()->getOriginalFile()->getPublicUrl();
                $cur_th['vorschausmall']['url'] = $this->request->getBaseUri() . $url;
                if ($value->getVorschausmall()[0]->getOriginalResource()->getTitle()) {
                    $cur_th['vorschausmall']['title'] = $value->getVorschausmall()[0]->getOriginalResource()->getTitle();
                }
                if ($value->getVorschausmall()[0]->getOriginalResource()->getDescription()) {
                    $cur_th['vorschausmall']['desc'] = $value->getVorschausmall()[0]->getOriginalResource()->getDescription();
                }
            }
            if ($value->getVorschaubig() && $value->getVorschaubig()[0]) {
                $url = $value->getVorschaubig()[0]->getOriginalResource()->getOriginalFile()->getPublicUrl();
                $cur_th['vorschaubig']['url'] = $this->request->getBaseUri() . $url;
                if ($value->getVorschaubig()[0]->getOriginalResource()->getTitle()) {
                    $cur_th['vorschaubig']['title'] = $value->getVorschaubig()[0]->getOriginalResource()->getTitle();
                }
                if ($value->getVorschaubig()[0]->getOriginalResource()->getDescription()) {
                    $cur_th['vorschaubig']['desc'] = $value->getVorschaubig()[0]->getOriginalResource()->getDescription();
                }
            }
            if ($value->getSpielzeit()) {
                $cur_th['spielzeit'] = $this->buildSpielzeit($value->getSpielzeit(), $demand);
            }

            $cur_th['lastChanged'] = $value->getTstamp()->format('Y-m-d');

            if (!empty($cur_th['spielzeit']) || ($demand['by_ids'] && is_array($demand['by_ids']) && !empty($demand['by_ids'])) || ($demand['qry'] && '' != $demand['qry'])) {
                $output_th[] = $cur_th;
            }
        }

        return $output_th;
    }

    public function buildSpielzeit($spielzeit, $demand = [])
    {
        $output_spz = [];

        foreach ($spielzeit as $key => $value) {
            $run_check_ids = true;
            $countdemands = 0;
            if ($demand['hide'] && '' != $demand['hide']) {
                if ($demand['by_spielort_ids'] && is_array($demand['by_spielort_ids']) && !empty($demand['by_spielort_ids']) && $value->getSpielort()) {
                    $countdemands++;
                    foreach ($value->getSpielort() as $k => $v) {
                        if ($run_check_ids && in_array($v->getUid(), $demand['by_spielort_ids'])) {
                            $run_check_ids = false;
                            $countdemands--;
                        }
                    }
                }

                if ($demand['by_veranstalter_ids'] && is_array($demand['by_veranstalter_ids']) && !empty($demand['by_veranstalter_ids']) && $value->getVeranstalter()) {
                    $countdemands++;
                    foreach ($value->getVeranstalter() as $k => $v) {
                        if (in_array($v->getUid(), $demand['by_veranstalter_ids'])) {
                            $run_check_ids = false;
                            $countdemands--;
                        }
                    }
                }

                if ($demand['by_performance_ids'] && is_array($demand['by_performance_ids']) && !empty($demand['by_performance_ids']) && $value->getPerformance()) {
                    $countdemands++;
                    foreach ($value->getPerformance() as $k => $v) {
                        if (in_array($v->getUid(), $demand['by_performance_ids'])) {
                            $countdemands--;
                            $run_check_ids = false;
                        }
                    }
                }

                if ($demand['plz'] && '' != $demand['plz']) {
                    $countdemands++;
                    foreach ($value->getSpielort() as $k => $v) {
                        if ($v->getPlz() == $demand['plz']) {
                            $run_check_ids = false;
                            $countdemands--;
                        }
                    }
                }
            } else {
                $run_check_ids = false;
            }

            if ($countdemands > 0) {
                $run_check_ids = true;
            }

            $run_check_time = true;
            if ($demand['start_date'] && '' != $demand['start_date']) {
                if ($value->getDatum() >= $demand['start_date'] && !$run_check_ids) {
                    $run_check_time = false;
                }
            }

            if ($demand['end_date'] && '' != $demand['end_date']) {
                $run_check_time = true;
                if ($value->getDatum() < $demand['end_date'] && !$run_check_ids) {
                    $run_check_time = false;
                }
            }

            if ($demand['start_time'] && '' != $demand['start_time'] && $demand['end_time'] && '' != $demand['end_time']) {
                $run_check_time = true;
                $s_time = explode(':', $demand['start_time']);
                $s_time = (intval($s_time[0]) * 60 * 60) + (intval($s_time[1]) * 60);
                $e_time = explode(':', $demand['end_time']);
                $e_time = (intval($e_time[0]) * 60 * 60) + (intval($e_time[1]) * 60);
                if ($value->getUhrzeit() >= $s_time && $value->getUhrzeit() < $e_time && !$run_check_ids) {
                    $run_check_time = false;
                }
            } else {
                if ($demand['start_time'] && '' != $demand['start_time']) {
                    $run_check_time = true;
                    $time = explode(':', $demand['start_time']);
                    $time = (intval($time[0]) * 60 * 60) + (intval($time[1]) * 60);
                    if ($value->getUhrzeit() >= $time && !$run_check_ids) {
                        $run_check_time = false;
                    }
                }

                if ($demand['end_time'] && '' != $demand['end_time']) {
                    $time = explode(':', $demand['end_time']);
                    $time = (intval($time[0]) * 60 * 60) + (intval($time[1]) * 60);
                    if ($value->getUhrzeit() < $time && !$run_check_ids) {
                        $run_check_time = false;
                    }
                }
            }

            if (!$run_check_time) {
                $cur_spz = [];

                if ($value->getDatum()) {
                    $cur_spz['datum'] = $value->getDatum()->format('Y-m-d');
                }
                if ($value->getUhrzeit()) {
                    $cur_spz['uhrzeit'] = date('H:i', $value->getUhrzeit());
                }
                if ($value->getBis()) {
                    $cur_spz['bis'] = date('H:i', $value->getBis());
                }
                if ($value->getEinlass()) {
                    $cur_spz['einlass'] = date('H:i', $value->getEinlass());
                }
                if ($value->getDauer()) {
                    $cur_spz['dauer'] = $value->getDauer();
                }
                if ($value->getSpielort()) {
                    $cur_spz['spielort'] = $this->buildSpielort($value->getSpielort(), $demand['fullInfo']);
                }
                if ($value->getSpielorturl()) {
                    $cur_spz['spielorturl'] = $value->getSpielorturl();
                }
                if ($value->getBooking()) {
                    $cur_spz['booking'] = $value->getBooking();
                }
                if ($value->getVeranstalter()) {
                    $cur_spz['veranstalter'] = $this->buildVeranstalter($value->getVeranstalter(), $demand['fullInfo']);
                }
                if ($value->getPerformance()) {
                    $cur_spz['performance'] = $this->buildPerformance($value->getPerformance(), $demand['fullInfo']);
                }
                if ($value->getVorschausmall() && $value->getVorschausmall()[0]) {
                    $url = $value->getVorschausmall()[0]->getOriginalResource()->getOriginalFile()->getPublicUrl();
                    $cur_spz['vorschausmall']['url'] = $this->request->getBaseUri() . $url;
                    if ($value->getVorschausmall()[0]->getOriginalResource()->getTitle()) {
                        $cur_spz['vorschausmall']['title'] = $value->getVorschausmall()[0]->getOriginalResource()->getTitle();
                    }
                    if ($value->getVorschausmall()[0]->getOriginalResource()->getDescription()) {
                        $cur_spz['vorschausmall']['desc'] = $value->getVorschausmall()[0]->getOriginalResource()->getDescription();
                    }
                }
                if ($value->getVorschaubig() && $value->getVorschaubig()[0]) {
                    $url = $value->getVorschaubig()[0]->getOriginalResource()->getOriginalFile()->getPublicUrl();
                    $cur_spz['vorschaubig']['url'] = $this->request->getBaseUri() . $url;
                    if ($value->getVorschaubig()[0]->getOriginalResource()->getTitle()) {
                        $cur_spz['vorschaubig']['title'] = $value->getVorschaubig()[0]->getOriginalResource()->getTitle();
                    }
                    if ($value->getVorschaubig()[0]->getOriginalResource()->getDescription()) {
                        $cur_spz['vorschaubig']['desc'] = $value->getVorschaubig()[0]->getOriginalResource()->getDescription();
                    }
                }

                if ($value->getAusfall()) {
                    $cur_spz['ausgefallen']['yes'] = 1;
                    if ($value->getAusfallinfo()) {
                        $cur_spz['ausgefallen']['ausgefallenInfo'] = $value->getAusfallinfo();
                    }
                }

                if ($value->getPerformance()[0]) {
                    $tmp_per = $value->getPerformance()[0];
                    if ($tmp_per->getAusfallenvon() || $tmp_per->getAusfallenbis()) {
                        if ($tmp_per->getAusfallenvon() && $tmp_per->getAusfallenbis()) {
                            if (($tmp_per->getAusfallenvon() <= $value->getDatum()) && ($tmp_per->getAusfallenbis() > $value->getDatum())) {
                                $cur_spz['ausgefallen']['yes'] = 1;
                                $cur_spz['ausgefallen']['von'] = $tmp_per->getAusfallenvon()->format('Y-m-d');
                                $cur_spz['ausgefallen']['bis'] = $tmp_per->getAusfallenbis()->format('Y-m-d');
                                if ($tmp_per->getAusfallinfo()) {
                                    $cur_spz['ausgefallen']['ausgefallenInfo'] = $tmp_per->getAusfallinfo();
                                }
                            }
                        } elseif ($tmp_per->getAusfallenvon() && ($tmp_per->getAusfallenvon() <= $value->getDatum())) {
                            $cur_spz['ausgefallen']['yes'] = 1;
                            $cur_spz['ausgefallen']['von'] = $tmp_per->getAusfallenvon()->format('Y-m-d');
                            if ($tmp_per->getAusfallinfo()) {
                                $cur_spz['ausgefallen']['ausgefallenInfo'] = $tmp_per->getAusfallinfo();
                            }
                        } elseif ($tmp_per->getAusfallenbis() && ($tmp_per->getAusfallenbis() > $value->getDatum())) {
                            $cur_spz['ausgefallen']['yes'] = 1;
                            $cur_spz['ausgefallen']['bis'] = $tmp_per->getAusfallenbis()->format('Y-m-d');
                            if ($tmp_per->getAusfallinfo()) {
                                $cur_spz['ausgefallen']['ausgefallenInfo'] = $tmp_per->getAusfallinfo();
                            }
                        }
                    }
                }

                if ($value->getUrls()) {
                    foreach ($value->getUrls() as $url_key => $url_val) {
                        if ($url_val->getUrl()) {
                            $json_key = '';
                            if ($url_val->getFieldid()) {
                                $json_key = $url_val->getFieldid();
                            } else {
                                $json_key = strtolower(str_replace(' ', '', $url_val->getTitle()));
                            }

                            $cur_spz['urls'][$json_key]['title'] = $url_val->getTitle();
                            $cur_spz['urls'][$json_key]['url'] = $url_val->getUrl();
                        }
                    }
                }

                if ($value->getMetas()) {
                    foreach ($value->getMetas() as $meta_key => $meta_val) {
                        if ($meta_val->getValue()) {
                            $json_key = '';
                            if ($meta_val->getFieldid()) {
                                $json_key = $meta_val->getFieldid();
                            } else {
                                $json_key = strtolower(str_replace(' ', '', $meta_val->getTitle()));
                            }

                            $cur_spz['metas'][$json_key]['title'] = $meta_val->getTitle();
                            $cur_spz['metas'][$json_key]['value'] = $meta_val->getValue();
                        }
                    }
                }

                $cur_spz['lastChanged'] = $value->getTstamp()->format('Y-m-d');

                $output_spz[] = $cur_spz;
            }
        }

        return $output_spz;
    }

    public function veranstaltungsartResponse()
    {
        if (GeneralUtility::_GP('qry')) {
            $demand['qry'] = filter_var(GeneralUtility::_GP('qry'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('by_ids')) {
            $demand['by_ids'] = filter_var_array(GeneralUtility::_GP('by_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        $demand['limit'] = $this->limit;
        $demand['offset'] = $this->offset;

        $ver_art = $this->veranstaltungsartRepository->searchByJODemand($demand);

        $output_ver_art = $this->buildVeranstaltungsart($ver_art);

        $meta = [
            'numFound' => count($output_ver_art),
            'limit' => $this->limit,
            'offset' => $this->offset,
        ];

        return ['veranstaltungsart' => $output_ver_art, 'meta' => $meta];
    }

    public function buildVeranstaltungsart($ver_art)
    {
        $output_ver_art = [];

        foreach ($ver_art as $key => $value) {
            $cur_ver_art = [];

            $cur_ver_art['id'] = $this->maskId($value->getUid());

            if ($value->getName()) {
                $cur_ver_art['name'] = $value->getName();
            }

            $cur_ver_art['lastChanged'] = $value->getTstamp()->format('Y-m-d');

            $output_ver_art[] = $cur_ver_art;
        }

        return $output_ver_art;
    }

    public function veranstaltungstypeResponse()
    {
        if (GeneralUtility::_GP('qry')) {
            $demand['qry'] = filter_var(GeneralUtility::_GP('qry'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('by_ids')) {
            $demand['by_ids'] = filter_var_array(GeneralUtility::_GP('by_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        $demand['limit'] = $this->limit;
        $demand['offset'] = $this->offset;

        $ver_type = $this->veranstaltungstypeRepository->searchByJODemand($demand);

        $output_ver_type = $this->buildVeranstaltungstype($ver_type);

        $meta = [
            'numFound' => count($output_ver_type),
            'limit' => $this->limit,
            'offset' => $this->offset,
        ];

        return ['veranstaltungstype' => $output_ver_type, 'meta' => $meta];
    }

    public function buildVeranstaltungstype($ver_type)
    {
        $output_ver_type = [];

        foreach ($ver_type as $key => $value) {
            $cur_ver_type = [];

            $cur_ver_type['id'] = $this->maskId($value->getUid());

            if ($value->getName()) {
                $cur_ver_type['name'] = $value->getName();
            }

            $cur_ver_type['lastChanged'] = $value->getTstamp()->format('Y-m-d');

            $output_ver_type[] = $cur_ver_type;
        }

        return $output_ver_type;
    }

    public function performanceLDResponse()
    {
        $output_perf = [];

        if (GeneralUtility::_GP('by_ids')) {
            $demand['by_performance_ids'] = array_map([$this, 'deMaskId'], filter_var_array(GeneralUtility::_GP('by_ids'), FILTER_SANITIZE_NUMBER_INT));
        }

        if (GeneralUtility::_GP('by_intern_ids')) {
            $demand['by_performance_ids'] = filter_var_array(GeneralUtility::_GP('by_intern_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('by_veranstalter_ids')) {
            $demand['by_veranstalter_ids'] = filter_var_array(GeneralUtility::_GP('by_veranstalter_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('by_digiID_ids')) {
            $demand['by_veranstalterxtree_ids'] = filter_var_array(GeneralUtility::_GP('by_digiID_ids'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('by_spielzeit_ids')) {
            $demand['by_spielzeit_ids'] = filter_var_array(GeneralUtility::_GP('by_spielzeit_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        $demand['start_date'] = GeneralUtility::_GP('start_date') ? new \Datetime(filter_var(GeneralUtility::_GP('start_date'), FILTER_SANITIZE_STRING)) : new \Datetime('yesterday');

        if (GeneralUtility::_GP('end_date')) {
            $demand['end_date'] = new \Datetime(filter_var(GeneralUtility::_GP('end_date'), FILTER_SANITIZE_STRING));
        }

        if (GeneralUtility::_GP('start_ch_date')) {
            $demand['start_ch_date'] = new \Datetime(filter_var(GeneralUtility::_GP('start_ch_date'), FILTER_SANITIZE_STRING));
        }

        if (GeneralUtility::_GP('end_ch_date')) {
            $demand['end_ch_date'] = new \Datetime(filter_var(GeneralUtility::_GP('end_ch_date'), FILTER_SANITIZE_STRING));
        }

        $spielzeiten = $this->spielzeitRepository->searchMine($demand);

        if (count($spielzeiten) > 0) {
            $match = [];

            foreach ($spielzeiten as $key => $value) {
                $tmp_perf = $value->getPerformance()[0];

                if (!$tmp_perf) {
                    continue;
                }

                if (!array_key_exists($tmp_perf->getUid(), $match)) {
                    $match[$tmp_perf->getUid()]['perf'] = $tmp_perf;
                }

                $match[$tmp_perf->getUid()]['times'][] = $value;
            }

            if (count($match) > 0) {
                $output_perf = $this->buildPerformanceLD($match);
            }
        }

        return $output_perf;
    }

    public function buildPerformanceLD($match)
    {
        $out = [];
        foreach ($match as $key => $value) {
            $output = [];
            $arr = [];

            $performance = $value['perf'];
            $spielzeiten = $value['times'];

            $output['@context'] = 'https://schema.org/';
            $output['@language'] = 'DE';

            $ev_type = 'Event';

            $types = $performance->getTyp();
            if ($types && sizeof($types) > 0) {
                $tmp_type = $types[0]->getJsonld();
                if ($tmp_type && '' != $tmp_type) {
                    $ev_type = $tmp_type;
                }
            }

            $output['@type'] = $ev_type;

            if ($performance->getEventtyp()) {
                $output['@type'] = explode('$', $performance->getEventtyp());
            }

            if ($performance->getName()) {
                $output['name'] = $performance->getName();
            }

            if ($performance->getLangbeschreibung()) {
                $output['about'] = $performance->getLangbeschreibung();
            }

            if ($performance->getBeschreibung()) {
                $output['description'] = $performance->getBeschreibung();
            }

            if ($performance->getAltersfreigabe()) {
                $output['typicalAgeRange'] = $performance->getAltersfreigabe();
            }

            $output['inLanguage'] = 'DE';

            if ($performance->getBooking()) {
                $tmp_off = [];
                $tmp_off['@type'] = 'Offer';

                $tmp_off['url'] = $performance->getBooking();

                if ($performance->getBookingstart()) {
                    $tmp_off['availabilityStarts'] = $performance->getBookingstart();
                }

                $output['offers'] = $tmp_off;
            }

            if ($performance->getInfo()) {
                if ($output['description']) {
                    $tmp = $output['description'];
                    $output['description'] = [];
                    $output['description'][] = $tmp;
                    $output['description'][] = $performance->getInfo();
                } else {
                    $output['description'] = $performance->getInfo();
                }
            }

            if ($performance->getInfouri()) {
                $output['url'] = $performance->getInfouri();
            }

            if (count($performance->getVorschaubig()) >= 1) {
                $output['image'] = [];
                $tmp_img = $performance->getVorschaubig();
                foreach ($tmp_img as $key => $value) {
                    $origFile = $value->getOriginalResource();
                    $url = $origFile->getOriginalFile()->getPublicUrl();
                    $desc = $origFile->getDescription();
                    $desc_arr = explode('$', $desc);

                    if ($desc) {
                        $tmp_img = [];
                        $tmp_img['@type'] = 'ImageObject';
                        $tmp_img['contentUrl'] = $this->request->getBaseUri() . $url;

                        $title = $origFile->getTitle();

                        if ($title) {
                            $tmp_img['caption'] = $title;
                        }

                        if ($desc_arr[0]) {
                            // $tmp_img['copyrightHolder'] = $desc_arr[0];
                            $tmp_img['creator'] = $desc_arr[0];
                        }

                        if ($desc_arr[1] && '1' == $desc_arr[1]) {
                            $tmp_img['copyrightNotice'] = 'Alle Rechte liegen bei der/dem Fotografen/Fotografin. Eine Zustimmung zur Verbreitung liegt vor.';
                        }

                        if ($desc_arr[2]) {
                            $tmp_img['license'] = $desc_arr[2];
                        }

                        if ($desc_arr[3]) {
                            $tmp_img['keywords'] = [];

                            $tag_item = explode(',', $desc_arr[3]);

                            foreach ($tag_item as $kk => $vv) {
                                $tmp_img['keywords'][] = trim($vv);
                            }
                        }

                        if ($desc_arr[4]) {
                            $tmp_img['description'] = $desc_arr[4];
                        }

                        if ($tmp_img['copyrightNotice']) {
                            $output['image'][] = $tmp_img;
                        }
                    }
                }
            } elseif ($performance->getImgdefault()) {
                $imgDefault = json_decode($performance->getImgdefault());

                foreach ($imgDefault as $key => $value) {
                    $tmp_img = [];
                    $tmp_img['@type'] = 'ImageObject';
                    $tmp_img['contentUrl'] = $value->url;

                    if ($value->title) {
                        $tmp_img['caption'] = $value->title;
                    }

                    $output['image'][] = $tmp_img;
                }
            }

            $output['identifier'] = [];

            if (count($performance->getPdf()) >= 1) {
                $tmp_img = $performance->getPdf();
                foreach ($tmp_img as $key => $value) {
                    $origFile = $value->getOriginalResource();
                    $url = $origFile->getOriginalFile()->getPublicUrl();
                    $desc = $origFile->getDescription();
                    $desc_arr = explode('$', $desc);

                    $tmp_img = [];
                    $tmp_img['@type'] = 'PropertyValue';
                    $tmp_img['name'] = 'PDF';
                    $tmp_img['url'] = $this->request->getBaseUri() . $url;

                    $title = $origFile->getTitle();

                    if ($title) {
                        $tmp_img['value'] = $title;
                    }

                    if ($desc_arr[3]) {
                        $tmp_img['valueReference'] = $desc_arr[3];
                    }

                    if ($desc_arr[4]) {
                        $tmp_img['description'] = $desc_arr[4];
                    }

                    $output['identifier'][] = $tmp_img;
                }
            }

            $tmp = [];
            $tmp['@type'] = 'PropertyValue';
            $tmp['name'] = 'Lizenz';
            $tmp['value'] = ["https://creativecommons.org/publicdomain/zero/1.0/deed.de"];

            $output['identifier'][] = $tmp;

            $tmp = [];
            $tmp['@type'] = 'PropertyValue';
            $tmp['name'] = 'dateModified';
            $tmp['value'] = $performance->getTstamp()->format('Y-m-d') . 'T' . $performance->getTstamp()->format('H:i:s') . '+01:00';

            $output['identifier'][] = $tmp;

            $tmp = [];
            $tmp['@type'] = 'PropertyValue';
            $tmp['name'] = 'Parent-ID';
            $tmp['value'] = $this->maskId($performance->getUid());

            $output['identifier'][] = $tmp;

            if ($performance->getTyp()) {
                $typ = $performance->getTyp();

                foreach ($typ as $key => $value) {
                    $tmp = [];
                    $tmp['@type'] = 'PropertyValue';
                    $tmp['name'] = 'Kategorie';
                    $tmp['value'] = $value->getName();

                    $output['identifier'][] = $tmp;
                }
            }

            if ($performance->getTypxtree()) {
                $typ = json_decode($performance->getTypxtree());

                foreach ($typ as $key => $value) {
                    $tmp_arr = explode('$', $value);

                    $tmp = [];
                    $tmp['@type'] = 'PropertyValue';
                    $tmp['name'] = 'Kategorie';
                    $tmp['identifier'] = $tmp_arr[0];
                    $tmp['value'] = $tmp_arr[1];

                    $output['identifier'][] = $tmp;
                }
            }

            if ($performance->getTypprimary()) {
                $typ = $performance->getTypprimary();

                $tmp_arr = explode('$', $typ);

                $tmp = [];
                $tmp['@type'] = 'PropertyValue';
                $tmp['name'] = 'Kategorie Primary';
                $tmp['identifier'] = $tmp_arr[0];
                $tmp['value'] = $tmp_arr[1];

                $output['identifier'][] = $tmp;
            }

            if ($performance->getArt()) {
                $art = $performance->getArt();

                foreach ($art as $key => $value) {
                    $tmp = [];
                    $tmp['@type'] = 'PropertyValue';
                    $tmp['name'] = 'Merkmale';
                    $tmp['value'] = $value->getName();

                    $output['identifier'][] = $tmp;
                }
            }

            if ($performance->getArtxtree()) {
                $art = json_decode($performance->getArtxtree());

                foreach ($art as $key => $value) {
                    $tmp_arr = explode('$', $value);

                    $tmp = [];
                    $tmp['@type'] = 'PropertyValue';
                    $tmp['name'] = 'Merkmale';
                    $tmp['identifier'] = $tmp_arr[0];
                    $tmp['value'] = $tmp_arr[1];

                    $output['identifier'][] = $tmp;

                    if ('Kostenfrei' == $tmp_arr[1]) {
                        $output['isAccessibleForFree'] = true;
                    }
                }
            }

            if ($performance->getArtprimary()) {
                $art = $performance->getArtprimary();

                $tmp_arr = explode('$', $art);

                $tmp = [];
                $tmp['@type'] = 'PropertyValue';
                $tmp['name'] = 'Merkmale Primary';
                $tmp['identifier'] = $tmp_arr[0];
                $tmp['value'] = $tmp_arr[1];

                $output['identifier'][] = $tmp;
            }

            if ($performance->getSozartxtree()) {
                $art = json_decode($performance->getSozartxtree());

                foreach ($art as $key => $value) {
                    $tmp_arr = explode('$', $value);

                    $tmp = [];
                    $tmp['@type'] = 'PropertyValue';
                    $tmp['name'] = 'Suchkriterien';
                    $tmp['identifier'] = $tmp_arr[0];
                    $tmp['value'] = $tmp_arr[1];

                    $output['identifier'][] = $tmp;
                }
            }

            if ($performance->getSuchkat()) {
                $art = json_decode($performance->getSuchkat());

                foreach ($art as $key => $value) {
                    $tmp_arr = explode('$', $value);
                    $tmp = [];
                    $tmp['@type'] = 'PropertyValue';
                    $tmp['name'] = 'Suchkriterien';
                    $tmp['identifier'] = $tmp_arr[0];
                    $tmp['value'] = $tmp_arr[1];

                    $output['identifier'][] = $tmp;
                }
            }

            if ($performance->getMappedkat() && false) {
                $art = json_decode($performance->getMappedkat());

                foreach ($art as $key => $value) {
                    $tmp_arr = explode('$', $value);
                    $tmp = [];
                    $tmp['@type'] = 'PropertyValue';
                    $tmp['name'] = 'Kategorie gemappt';
                    $tmp['identifier'] = $tmp_arr[0];
                    $tmp['value'] = $tmp_arr[1];

                    $output['identifier'][] = $tmp;
                }
            }

            if ($performance->getMaxattendee()) {
                $output['maximumAttendeeCapacity'] = $performance->getMaxattendee();
            }

            if ($performance->getMaxattendeephy()) {
                $output['maximumPhysicalAttendeeCapacity'] = $performance->getMaxattendeephy();
            }

            if ($performance->getMaxattendeevirt()) {
                $output['maximumVirtualAttendeeCapacity'] = $performance->getMaxattendeevirt();
            }

            if (count($spielzeiten) > 0) {
                foreach ($spielzeiten as $sp_key => $sp_val) {
                    $output2 = [];
                    $output2 = $output;
                    $loc = null;
                    if (count($sp_val->getSpielort()) > 0) {
                        $loc = $sp_val->getSpielort()[0];
                    } else if (count($performance->getSpielort()) > 0) {
                        $loc = $performance->getSpielort()[0];
                    }

                    $output2['location'] = [];
                    $loc_arr = [];
                    if ($loc) {
                        $loc_arr = [];

                        $loc_arr['@type'] = 'Place';

                        if ($loc->getName()) {
                            $loc_arr['name'] = $loc->getName();
                        }

                        if ($loc->getRaum()) {
                            $loc_arr['name'] = $loc_arr['name'] . ' - ' . $loc->getRaum();
                        }

                        if ($loc->getAlternativname() || $loc->getNamefull()) {
                            $loc_arr['alternateName'] = [];
                        }

                        if ($loc->getAlternativname()) {
                            // $loc_arr['alternateName'][] = $loc->getAlternativname();
                        }

                        if ($loc->getNamefull()) {
                            $loc_arr['alternateName'][] = $loc->getNamefull();
                        }

                        if ($loc->getWebsite()) {
                            $loc_arr['url'] = $loc->getWebsite();
                        }

                        if ($loc->getLat() && $loc->getLon()) {
                            $tmp_coord = [];
                            $tmp_coord['@type'] = 'GeoCoordinates';
                            $tmp_coord['latitude'] = $loc->getLat();
                            $tmp_coord['longitude'] = $loc->getLon();

                            $loc_arr['geo'] = $tmp_coord;
                        }

                        $loc_arr['address'] = [];
                        $loc_arr['address']['@type'] = 'PostalAddress';

                        if ($loc->getStrasse()) {
                            $loc_arr['address']['streetAddress'] = $loc->getStrasse();
                        }

                        if ($loc->getOrt()) {
                            $loc_arr['address']['addressLocality'] = $loc->getOrt();
                        }

                        if ($loc->getPlz()) {
                            $loc_arr['address']['postalCode'] = $loc->getPlz();
                        }

                        if ($loc->getBundesland()) {
                            $loc_arr['address']['addressRegion'] = $loc->getBundesland();
                        }

                        $loc_arr['address']['areaServed'] = 'West';
                        $loc_arr['address']['addressCountry'] = 'DE';

                        if ($loc->getLandkreis()) {
                            $loc_arr['address']['identifier'] = [];

                            $tmp = [];
                            $tmp['@type'] = 'PropertyValue';
                            $tmp['name'] = 'Landkreis';
                            $tmp['value'] = $loc->getLandkreis();

                            $loc_arr['address']['identifier'][] = $tmp;
                        }

                        if ($loc->getGeonames()) {
                            $loc_arr['sameAs'] = [$loc->getGeonames()];
                        }
                    }

                    $loc_arr2 = [];
                    if ($sp_val->getSpielorturl()) {
                        $loc_arr2['@type'] = 'VirtualLocation';

                        $loc_arr2['url'] = $sp_val->getSpielorturl();
                    }

                    if (!empty($loc_arr) || !empty($loc_arr2)) {
                        if (!empty($loc_arr) && !empty($loc_arr2)) {
                            $output2['eventAttendanceMode'] = 'https://schema.org/MixedEventAttendanceMode';
                            $output2['location'][] = $loc_arr;
                            $output2['location'][] = $loc_arr2;
                        }

                        if (!empty($loc_arr) && empty($loc_arr2)) {
                            $output2['eventAttendanceMode'] = 'https://schema.org/OfflineEventAttendanceMode';
                            $output2['location'][] = $loc_arr;
                        }

                        if (empty($loc_arr) && !empty($loc_arr2)) {
                            $output2['eventAttendanceMode'] = 'https://schema.org/OnlineEventAttendanceMode';
                            $output2['location'][] = $loc_arr2;
                        }
                    }

                    $veranstalter = $sp_val->getVeranstalter()[0];
                    if ($veranstalter) {
                        $output2['organizer'] = [];
                        $output2['organizer']['@type'] = 'Organization';

                        if ($veranstalter->getName()) {
                            $output2['organizer']['name'] = $veranstalter->getName();
                        }

                        if ($veranstalter->getTelefon()) {
                            $output2['organizer']['telephone'] = $veranstalter->getTelefon();
                        }

                        if ($veranstalter->getFax()) {
                            $output2['organizer']['faxNumber'] = $veranstalter->getFax();
                        }

                        if ($veranstalter->getEmail()) {
                            $output2['organizer']['email'] = $veranstalter->getEmail();
                        }

                        if ($veranstalter->getWebsite()) {
                            $output2['organizer']['url'] = $veranstalter->getWebsite();
                        }

                        if ($veranstalter->getStrasse() || $veranstalter->getPlz() || $veranstalter->getOrt()) {
                            $output2['organizer']['address'] = $veranstalter->getStrasse() . ' ' . $veranstalter->getPlz() . ' ' . $veranstalter->getOrt();
                        }

                        $output2['organizer']['identifier'] = [];

                        $tmp = [];
                        $tmp['@type'] = 'PropertyValue';
                        $tmp['name'] = 'Interne-ID';
                        $tmp['value'] = $this->maskId($veranstalter->getUid());

                        $output2['organizer']['identifier'][] = $tmp;
                    }

                    if ($sp_val->getVeranstalterxtreejson()) {
                        $output2['organizer'] = [];
                        $output2['organizer']['@type'] = 'Organization';

                        $v = json_decode($sp_val->getVeranstalterxtreejson());

                        if ($v->name) {
                            foreach ($v->name as $kk => $vv) {
                                if ('preferred' == $vv->role) {
                                    $output2['organizer']['name'] = $vv->label;
                                }
                            }
                        }

                        if ($v->communication) {
                            foreach ($v->communication as $kk => $vv) {
                                if ('com0022' == $vv->id) {
                                    $output2['organizer']['telephone'] = $vv->item;
                                }

                                if ('com0024' == $vv->id) {
                                    $output2['organizer']['email'] = $vv->item;
                                }
                            }
                        }

                        if ($v->webResource) {
                            foreach ($v->webResource as $kk => $vv) {
                                if ('webpage' == $vv->source_id) {
                                    $output2['organizer']['url'] = $vv->identifier;
                                }
                            }
                        }

                        if ($v->id) {
                            $tmp = [];
                            $tmp['@type'] = 'PropertyValue';
                            $tmp['name'] = 'ID';
                            $tmp['value'] = $v->id;

                            $output2['organizer']['identifier'][] = $tmp;
                        }

                        if ($v->address) {
                            $output2['organizer']['address'] = [];

                            $visit_id = -1;
                            $post_id = -1;
                            foreach ($v->address as $kk => $vv) {
                                if ('visitor' == $vv->type || 'post' == $vv->type) {
                                    $tmp_addr = [];
                                    $tmp_addr['@type'] = 'PostalAddress';

                                    if ($v->street) {
                                        $tmp_addr['streetAddress'] = $vv->street;
                                    }

                                    if ($vv->place) {
                                        $tmp_addr['addressLocality'] = $vv->place;
                                    }

                                    if ($vv->zip) {
                                        $tmp_addr['postalCode'] = $vv->zip;
                                    }

                                    $tmp_addr['addressCountry'] = 'DE';

                                    $tmp_addr['identifier'] = [];

                                    $tmp = [];
                                    $tmp['@type'] = 'PropertyValue';
                                    $tmp['name'] = 'Typ';
                                    $tmp['value'] = $vv->type;

                                    $tmp_addr['identifier'][] = $tmp;

                                    if ('visitor' == $vv->type) {
                                        $tmp = [];
                                        $tmp['@type'] = 'PropertyValue';
                                        $tmp['name'] = 'Preferred';
                                        $tmp['value'] = true;
                                        $tmp_addr['identifier'][] = $tmp;

                                        $visit_id = $kk;
                                    }

                                    if ('post' == $vv->type) {
                                        $post_id = $kk;
                                    }

                                    $output2['organizer']['address'][$kk] = $tmp_addr;
                                }
                            }
                        }
                    }

                    if (empty($loc_arr) && empty($loc_arr2) && $veranstalter) {
                        $output2['eventAttendanceMode'] = 'https://schema.org/OfflineEventAttendanceMode';

                        $tmpLoc = [];

                        $tmpLoc['@type'] = 'Place';

                        if ($veranstalter->getName()) {
                            $tmpLoc['name'] = $veranstalter->getName();
                        }

                        $tmpLoc['address'] = [];
                        $tmpLoc['address']['@type'] = 'PostalAddress';

                        if ($veranstalter->getStrasse()) {
                            $tmpLoc['address']['streetAddress'] = $veranstalter->getStrasse();
                        }

                        if ($veranstalter->getOrt()) {
                            $tmpLoc['address']['addressLocality'] = $veranstalter->getOrt();
                        }

                        if ($veranstalter->getPlz()) {
                            $tmpLoc['address']['postalCode'] = $veranstalter->getPlz();
                        }

                        $tmpLoc['address']['addressCountry'] = 'DE';

                        $output2['location'][] = $tmpLoc;
                    }

                    if ($loc) {
                        if ($loc->getOpnv()) {
                            $tmpLoc = [];
                            $tmpLoc['@type'] = 'BusStation';
                            $tmpLoc['name'] = 'ÖPNV';
                            $tmpLoc['description'] = $loc->getOpnv();

                            $output2['location'][] = $tmpLoc;
                        }
                    }

                    $stAppend = 'T00:00:00+01:00';

                    if ($sp_val->getUhrzeit()) {
                        $stAppend = 'T' . date('H:i:s', $sp_val->getUhrzeit()) . '+01:00';
                    }

                    if ($sp_val->getDatum()) {
                        $output2['startDate'] = $sp_val->getDatum()->format('Y-m-d') . $stAppend;

                        if ($sp_val->getEinlass()) {
                            $stAppend2 = 'T' . date('H:i:s', $sp_val->getEinlass()) . '+01:00';
                            $output2['doorTime'] = $sp_val->getDatum()->format('Y-m-d') . $stAppend2;
                        }
                    }

                    $endAppend = 'T00:00:00+01:00';

                    if ($sp_val->getBis()) {
                        $endAppend = 'T' . date('H:i:s', $sp_val->getBis()) . '+01:00';
                    }

                    if ($sp_val->getDatum()) {
                        $output2['endDate'] = $sp_val->getDatum()->format('Y-m-d') . $endAppend;
                    }

                    if ($sp_val->getDauer()) {
                        $output2['duration'] = $sp_val->getDauer();
                    }

                    if ($sp_val->getStatus()) {
                        $output2['eventStatus'] = 'https://schema.org/' . $sp_val->getStatus();
                        switch ($sp_val->getStatus()) {
                            case 'EventScheduled':
                                break;
                            case 'EventRescheduled':
                                if ($sp_val->getNewdatum()) {
                                    $output2['previousStartDate'] = $output2['startDate'];
                                    $output2['startDate'] = $sp_val->getNewdatum()->format('Y-m-d') . $stAppend;
                                }
                                break;
                            case 'EventPostponed':
                                if ($sp_val->getNewdatum()) {
                                    $output2['previousStartDate'] = $output2['startDate'];
                                    $output2['startDate'] = $sp_val->getNewdatum()->format('Y-m-d') . $stAppend;
                                }
                                break;
                            case 'EventCancelled':
                                // $output2['name'] = 'Cancelled - ' . $output2['name'];
                                // $output2['name'] = 'Abgesagt: ' . $output2['name'];
                                if ($sp_val->getAusfallinfo()) {
                                    if ($output2['description']) {
                                        if (is_array($output2['description'])) {
                                            $output2['description'][] = $sp_val->getAusfallinfo();
                                        } else {
                                            $tmp = $output2['description'];
                                            $output2['description'] = [];
                                            $output2['description'][] = $tmp;
                                            $output2['description'][] = $sp_val->getAusfallinfo();
                                        }
                                    } else {
                                        $output2['description'] = $sp_val->getAusfallinfo();
                                    }
                                }
                                break;
                        }
                    }

                    $tmp = [];
                    $tmp['@type'] = 'PropertyValue';
                    $tmp['name'] = 'SP-ID';
                    $tmp['value'] = $this->maskId($sp_val->getUid());

                    $output2['identifier'][] = $tmp;

                    if ($loc || $sp_val->getSpielorturl()) {
                        $out[] = $output2;
                    }
                }
            }
        }

        return $out;
    }

    public function getJSON($url = null)
    {
        if (null != $url) {
            $ch = curl_init();
            $headers = [
                'Accept: application/json',
                'Content-type: application/json',
            ];
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            $response = curl_exec($ch);
            return $response;
        }
    }

    public function getVeranstalterXtree(&$fConfig, $format = 'full', $id = null)
    {
        //@all -> ID wird noch nicht berücksichtigt - das können wir noch verbessern und auch in die veranstalterLDResponse einbauen
        if (null != $id) {
            $uri = 'http://xtree-actor-api.digicult-verbund.de/getRepositoryItem?id=' . $id;
        } else {
            $uri = 'http://xtree-actor-api.digicult-verbund.de/getRepositoryList?portalURI=http://digicult.vocnet.org/portal/p0314&count=100';
        }
        $veranstalter = json_decode($this->getJSON($uri));
        if (isset($fConfig['config']['itemsProcConfig']['format']) && 'tca' == $fConfig['config']['itemsProcConfig']['format']) {
            if (isset($veranstalter->Actor)) {
                foreach ($veranstalter->Actor as $value) {
                    if (isset($value->name)) {
                        array_push($fConfig['items'], [
                            $value->name,
                            $value->id,
                        ]);
                    }
                }
            }
        }
        return $veranstalter;
    }

    public function veranstalterLDResponse()
    {
        $id = '';
        if (GeneralUtility::_GP('id')) {
            $id = filter_var(GeneralUtility::_GP('id'), FILTER_SANITIZE_STRING);
        }

        $veranstalter = json_decode($this->getJSON('http://xtree-actor-api.digicult-verbund.de/getRepositoryList?portalURI=http://digicult.vocnet.org/portal/p0314&count=100'));

        if ('' != $id) {
            $veranstalter = json_decode($this->getJSON('http://xtree-actor-api.digicult-verbund.de/getRepositoryItem?id=' . $id));
        }

        if ($veranstalter->Actor) {
            if (!$veranstalter->header->actorCount) {
                $veranstalter = [$veranstalter->Actor];
            } else {
                $veranstalter = $veranstalter->Actor;
            }

            if (!empty($veranstalter)) {
                $veranstalterfull = [];
                foreach ($veranstalter as $v) {
                    $uri_self = $v->links->self;
                    if ($uri_self) {
                        $veranstalterfull[] = json_decode($this->getJSON($uri_self))->Actor;
                    } else {
                        $veranstalterfull[] = $v;
                    }
                }
                $output_ver = $this->buildVeranstalterLD($veranstalterfull);
            }
        }

        return $output_ver;
    }

    public function buildVeranstalterLD($veranstalter)
    {
        $output = [];

        foreach ($veranstalter as $key => $value) {
            $out = [];

            $out['@context'] = 'https://schema.org/';
            $out['@type'] = 'LocalBusiness';

            if ($value->name) {
                if (is_array($value->name)) {
                    foreach ($value->name as $k => $v) {
                        if ($v->role && 'preferred' == $v->role) {
                            $out['name'] = $v->label;
                        } else {
                            if ($out['alternateName'] && !is_array($out['alternateName'])) {
                                $tmpName = $out['alternateName'];
                                $out['alternateName'] = [];
                                $out['alternateName'][] = $tmpName;
                            }

                            if (is_array($out['alternateName'])) {
                                $out['alternateName'][] = $v->label;
                            } else {
                                $out['alternateName'] = $v->label;
                            }
                        }
                    }
                } else {
                    $out['name'] = $value->name;
                }
            }

            $out['identifier'] = [];

            if ($value->id) {
                $tmp = [];
                $tmp['@type'] = 'PropertyValue';
                $tmp['name'] = 'ID';
                $tmp['value'] = $value->id;

                $out['identifier'][] = $tmp;
            }

            if ($value->uri) {
                $tmp = [];
                $tmp['@type'] = 'PropertyValue';
                $tmp['name'] = 'URI';
                $tmp['value'] = $value->uri;

                $out['identifier'][] = $tmp;
            }
            if ($value->address) {
                $out['address'] = [];

                $visit_id = -1;
                $post_id = -1;

                foreach ($value->address as $k => $v) {
                    if ('visitor' == $v->type || 'post' == $v->type) {
                        $tmp_addr = [];
                        $tmp_addr['@type'] = 'PostalAddress';

                        if ($v->street) {
                            $tmp_addr['streetAddress'] = $v->street;
                        }

                        if ($v->place) {
                            $tmp_addr['addressLocality'] = $v->place;
                        }

                        if ($v->zip) {
                            $tmp_addr['postalCode'] = $v->zip;
                        }

                        $tmp_addr['addressCountry'] = 'DE';

                        $tmp_addr['identifier'] = [];

                        $tmp = [];
                        $tmp['@type'] = 'PropertyValue';
                        $tmp['name'] = 'Typ';
                        $tmp['value'] = $v->type;

                        $tmp_addr['identifier'][] = $tmp;

                        if ('visitor' == $v->type) {
                            $tmp = [];
                            $tmp['@type'] = 'PropertyValue';
                            $tmp['name'] = 'Preferred';
                            $tmp['value'] = true;
                            $tmp_addr['identifier'][] = $tmp;

                            $visit_id = $k;
                        }

                        if ('post' == $v->type) {
                            $post_id = $k;
                        }

                        $out['address'][$k] = $tmp_addr;
                    }
                }
            }

            if ($value->place || $value->street) {
                if (!$out['address']) {
                    $out['address'] = [];
                }

                $tmp_addr = [];
                $tmp_addr['@type'] = 'PostalAddress';

                if ($value->street) {
                    $tmp_addr['streetAddress'] = $value->street;
                }

                if ($value->place) {
                    $tmp_addr['addressLocality'] = $value->place;
                }

                if ($value->zip) {
                    $tmp_addr['postalCode'] = $value->zip;
                }

                $tmp_addr['addressCountry'] = 'DE';
                $out['address'][] = $tmp_addr;
            }

            if ($value->pos && trim($value->pos) != '') {
                $tmp_coords = explode(' ', $value->pos);

                if (is_array($tmp_coords) && sizeof($tmp_coords) == 2) {
                    $tmpLoc = [];
                    $tmpLoc['@type'] = 'Place';

                    $tmp_coord = [];
                    $tmp_coord['@type'] = 'GeoCoordinates';
                    $tmp_coord['longitude'] = $tmp_coords[0];
                    $tmp_coord['latitude'] = $tmp_coords[1];

                    $tmpLoc['geo'] = $tmp_coord;

                    $out['location'] = [];
                    $out['location'][] = $tmpLoc;
                }
            }

            if ($value->classification) {
                foreach ($value->classification as $k => $v) {
                    if ($v->type && $v->uri) {
                        $tmp = [];
                        $tmp['@type'] = 'PropertyValue';
                        $tmp['name'] = $v->type;
                        $tmp['identifier'] = $v->uri;
                    }

                    $out['identifier'][] = $tmp;
                }
            }

            if ($value->communication) {
                foreach ($value->communication as $k => $v) {
                    if ('com0022' == $v->id) {
                        $out['telephone'] = $v->item;
                    }

                    if ('com0024' == $v->id) {
                        $out['email'] = $v->item;
                    }
                }
            }

            if ($value->webResource) {
                foreach ($value->webResource as $k => $v) {
                    if ('webpage' == $v->source_id) {
                        $out['url'] = $v->identifier;
                    } else if ('socialMedia' == $v->source_id) {
                        $tmp = [];
                        $tmp['@type'] = 'PropertyValue';
                        $tmp['name'] = $v->label;
                        $tmp['identifier'] = $v->identifier;
                        $tmp['value'] = $v->identifier;
                        $out['identifier'][] = $tmp;
                    } else {
                        if (!$out['sameAs']) {
                            $out['sameAs'] = [];
                        }

                        $out['sameAs'][] = $v->identifier;
                    }
                }
            }

            if ($value->description) {
                if (is_array($value->description)) {
                    $out['description'] = [];
                    foreach ($value->description as $k => $v) {
                        if ('info001' == $v->id) {
                            $out['description'] = $v->noteValue[0];
                            continue;
                        }
                        if ('info005' == $v->id) {
                            $tmpLoc = [];
                            $tmpLoc['@type'] = 'BusStation';
                            $tmpLoc['name'] = 'ÖPNV';
                            $tmpLoc['description'] = $v->noteValue[0];

                            if (!$out['location']) {
                                $out['location'] = [];
                            }
                            $out['location'][] = $tmpLoc;

                            continue;
                        }
                        if ('info019' == $v->id) {
                            $out['openingHours'] = $v->noteValue[0];
                            continue;
                        }
                        if ('info021' == $v->id) {
                            $out['foundingDate'] = $v->noteValue[0];
                            continue;
                        }
                        if ('info027' == $v->id) {
                            $tmp = [];

                            $tag_item = explode(',', $v->noteValue[0]);

                            foreach ($tag_item as $kk => $vv) {
                                $tmp['@type'] = 'PropertyValue';
                                $tmp['name'] = 'Tag';
                                $tmp['value'] = trim($vv);

                                $out['identifier'][] = $tmp;
                            }

                            continue;
                        }
                        if ('info031' == $v->id) {
                            if (!$out['disambiguatingDescription']) {
                                $out['disambiguatingDescription'] = [];
                            }

                            $out['disambiguatingDescription'][] = $v->noteValue[0];
                            continue;
                        }
                        if ('info029' == $v->id) {
                            if (!$out['disambiguatingDescription']) {
                                $out['disambiguatingDescription'] = [];
                            }

                            $out['disambiguatingDescription'][] = $v->noteValue[0];
                            continue;
                        }
                    }
                } else {
                    $out['description'] = $v->description;
                }
            }

            if ($value->teaser) {
                if (!$out['disambiguatingDescription']) {
                    $out['disambiguatingDescription'] = [];
                }

                $out['disambiguatingDescription'][] = $value->teaser;
            }

            if ($value->icon) {
                foreach ($value->icon as $k => $v) {
                    $the_val = '';

                    switch ($v->id) {
                        case 'opt004':
                            $the_val = 'Parkplatz';
                            break;
                        case 'opt022':
                            $the_val = 'Gastronomie vorhanden';
                            break;
                        case 'opt019':
                            $the_val = 'nicht Rollstuhlzugänglich';
                            break;
                        case 'opt037':
                            $the_val = 'Open Air';
                            break;
                        case 'opt018':
                            $the_val = 'Rollstuhlgerechter Eingang';
                            break;
                        case 'opt024':
                            $the_val = 'Rollstuhlgerechtes WC';
                            break;
                        case 'opt017':
                            $the_val = 'Rollstuhlgerechte Sitzgelegenheiten';
                            break;
                        case 'opt020':
                            $the_val = 'Rollstuhlgerechter Parkplatz';
                            break;
                    }

                    if ('' != $the_val) {
                        $tmp = [];
                        $tmp['@type'] = 'PropertyValue';
                        $tmp['name'] = 'Merkmale';
                        $tmp['value'] = $the_val;

                        $out['identifier'][] = $tmp;
                    }
                }
            }

            if ($value->resource) {
                $out['image'] = [];
                foreach ($value->resource as $k => $v) {
                    if ('image' == $v->type) {
                        $tmp_img = [];
                        $tmp_img['@type'] = 'ImageObject';

                        foreach ($v->resourceRepresentation as $kk => $vv) {
                            if ('provided_image' == $vv->type) {
                                $tmp_img['contentUrl'] = $vv->link;
                                break;
                            }
                        }

                        if ($v->rightsHolder) {
                            $tmp_img['license'] = $v->rightsHolder;
                        }

                        if ($v->photographer) {
                            $tmp_img['creator'] = $v->photographer;
                        }

                        if ($v->title) {
                            $tmp_img['caption'] = $v->title;
                        }

                        if ($v->description) {
                            $tmp_img['description'] = $v->description;
                        }

                        if ($v->tag) {
                            $tmp_img['keywords'] = [];

                            $tag_item = explode(',', $v->tag);

                            foreach ($tag_item as $kk => $vv) {
                                $tmp_img['keywords'][] = trim($vv);
                            }
                        }

                        $out['image'][] = $tmp_img;
                    }
                }
            }

            $output[] = $out;
        }

        return $output;
    }

    public function veranstaltungsartXMLResponse()
    {
        if (GeneralUtility::_GP('qry')) {
            $demand['qry'] = filter_var(GeneralUtility::_GP('qry'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('by_ids')) {
            $demand['by_ids'] = filter_var_array(GeneralUtility::_GP('by_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        $ver_art = $this->veranstaltungsartRepository->searchByJODemand($demand);

        $output_ver_art = $this->buildVeranstaltungsartXML($ver_art);

        return $output_ver_art;
    }

    public function buildVeranstaltungsartXML($ver_art)
    {
        $output_ver_art = [];

        $xml = new \DOMDocument('1.0', 'UTF-8');

        $data = $xml->createElement('data');
        $categories = $xml->createElement('categories');

        foreach ($ver_art as $key => $value) {
            $categorie = $xml->createElement('Category');
            $i18nName = $xml->createElement('i18nName');
            $i18n = $xml->createElement('I18n');
            $de = $xml->createElement('de');

            if ($value->getName()) {
                $de->nodeValue = $value->getName();
            }

            $i18n->appendChild($de);
            $i18nName->appendChild($i18n);
            $categorie->appendChild($i18nName);

            $categories->appendChild($categorie);
        }

        $data->appendChild($categories);
        $xml->appendChild($data);

        return $xml;
    }

    public function performanceXMLResponse($style)
    {
        $output_perf = [];

        if (GeneralUtility::_GP('by_ids')) {
            $demand['by_performance_ids'] = array_map([$this, 'deMaskId'], filter_var_array(GeneralUtility::_GP('by_ids'), FILTER_SANITIZE_NUMBER_INT));
        }

        if (GeneralUtility::_GP('by_intern_ids')) {
            $demand['by_performance_ids'] = filter_var_array(GeneralUtility::_GP('by_intern_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('by_veranstalter_ids')) {
            $demand['by_veranstalter_ids'] = filter_var_array(GeneralUtility::_GP('by_veranstalter_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        if (GeneralUtility::_GP('by_digiID_ids')) {
            $demand['by_veranstalterxtree_ids'] = filter_var_array(GeneralUtility::_GP('by_digiID_ids'), FILTER_SANITIZE_STRING);
        }

        if (GeneralUtility::_GP('by_spielzeit_ids')) {
            $demand['by_spielzeit_ids'] = filter_var_array(GeneralUtility::_GP('by_spielzeit_ids'), FILTER_SANITIZE_NUMBER_INT);
        }

        $demand['start_date'] = GeneralUtility::_GP('start_date') ? new \Datetime(filter_var(GeneralUtility::_GP('start_date'), FILTER_SANITIZE_STRING)) : new \Datetime('yesterday');

        if (GeneralUtility::_GP('end_date')) {
            $demand['end_date'] = new \Datetime(filter_var(GeneralUtility::_GP('end_date'), FILTER_SANITIZE_STRING));
        }

        if (GeneralUtility::_GP('start_ch_date')) {
            $demand['start_ch_date'] = new \Datetime(filter_var(GeneralUtility::_GP('start_ch_date'), FILTER_SANITIZE_STRING));
        }

        if (GeneralUtility::_GP('end_ch_date')) {
            $demand['end_ch_date'] = new \Datetime(filter_var(GeneralUtility::_GP('end_ch_date'), FILTER_SANITIZE_STRING));
        }

        $spielzeiten = $this->spielzeitRepository->searchMine($demand);

        if (count($spielzeiten) > 0) {
            $match = [];

            foreach ($spielzeiten as $key => $value) {
                $tmp_perf = $value->getPerformance()[0];

                if (!$tmp_perf) {
                    continue;
                }

                $perf_uid = $tmp_perf->getUid();
                if (!array_key_exists($perf_uid, $match)) {
                    $match[$perf_uid]['perf'] = $tmp_perf;

                    $performance_en = $this->performanceRepository->searchByLang($perf_uid);
                    if (count($performance_en) == 1) {
                        $match[$perf_uid]['perf_en'] = $performance_en[0];
                    }
                }

                $match[$tmp_perf->getUid()]['times'][] = $value;
            }

            if (count($match) > 0) {
                $output_perf = $this->buildPerformanceXML($match, $style);
            }
        }

        return $output_perf;
    }

    public function buildPerformanceXML($ver, $style)
    {
        $xml = new \DOMDocument('1.0', 'UTF-8');

        $CommentString = '
This work is CC0 http://creativecommons.org/publicdomain/zero/1.0/
To the extent possible under law, Landesarbeitsgemeinschaft Soziokultur Schleswig-Holstein e.V. 
has waived all copyright and related or neighboring rights to SozioKulturKalender (https://api.kulturadressen.de/) This work is published from: 
Deutschland/Germany
';

        $CommentNode = $xml->createComment($CommentString);
        $xml->appendChild($CommentNode);

        if (is_array($ver) && !empty($ver)) {
            $location_arr = [];

            $eventsParent = $xml->createElement('events');

            foreach ($ver as $key => $value) {
                $tmpClient = null;
                $tmpCreationTime = null;
                $tmpLastChangeTime = null;
                $tmpLanguages = null;
                $tmpTitle = null;
                $tmpShortDescription = null;
                $tmpLink = null;
                $tmpBookingLink = null;
                $tmpLocation = null;
                $tmpMedia = null;
                $tmpCategories = null;
                $tmpCriteria = null;
                $tmpPricing = null;
                $tmpEventDates = null;
                $tmpCancelled = null;

                $performance = $value['perf'];
                $spielzeiten = $value['times'];

                $translated = false;
                if ($value['perf_en']) {
                    $performance_en = $value['perf_en'];
                    $translated = true;
                }

                $event_xml = $xml->createElement('Event');

                $eventsAttr = $xml->createAttribute('id');
                $eventsAttr->value = $this->maskId($performance->getUid());
                $event_xml->appendChild($eventsAttr);

                // add creation Time
                $tmpCreationTime = $xml->createElement('creationTime');
                $tmpCreationTime->nodeValue = $performance->getCrdate()->format('Y-m-d H:i:s');
                // creation Time end

                // add Change Time
                $tmpLastChangeTime = $xml->createElement('lastChangeTime');
                $tmpLastChangeTime->nodeValue = $performance->getTstamp()->format('Y-m-d H:i:s');
                // change Time end

                // add language
                $tmpLanguages = $xml->createElement('languages');
                $tmpI18n = $xml->createElement('Language');
                $tmpAttr = $xml->createAttribute('id');
                $tmpAttr->value = '1';
                $tmpI18n->appendChild($tmpAttr);
                $tmpLanguages->appendChild($tmpI18n);

                if ($translated) {
                    $tmpI18n2 = $xml->createElement('Language');
                    $tmpAttr2 = $xml->createAttribute('id');
                    $tmpAttr2->value = '2';
                    $tmpI18n2->appendChild($tmpAttr2);
                    $tmpLanguages->appendChild($tmpI18n2);
                }
                // language - end

                $spl_url = false;
                if ($performance->getBeschreibung() || $performance->getInfo() || $performance->getSpielorturl()) {
                    $tmpShortDescription = $xml->createElement('shortDescription');
                    $tmpI18n = $xml->createElement('I18n');
                    $de = $xml->createElement('de');

                    $tmpText = $performance->getBeschreibung();

                    if ($performance->getInfo()) {
                        if ($performance->getBeschreibung()) {
                            $tmpText .= '


';
                        } else {
                            $tmpText = '';
                        }

                        $tmpText .= $performance->getInfo();
                    }

                    if ($performance->getSpielorturl()) {
                        $spl_url = true;

                        if (null != $tmpText && '' != $tmpText) {
                            $tmpText .= '


';
                        } else {
                            $tmpText = '';
                        }

                        $tmpText .= 'Streaming-URL: ' . $performance->getSpielorturl();
                    }

                    if ('xmltojson' == $style) {
                        $de->nodeValue = $tmpText;
                    } else {
                        $t = $xml->createCDATASection($tmpText);
                        $de->appendChild($t);
                    }

                    $tmpI18n->appendChild($de);
                    
                    if ($translated && $performance_en->getBeschreibung()) {
                        $en = $xml->createElement('en');

                        $tmpText_en = $performance_en->getBeschreibung();

                        if ($performance_en->getInfo()) {
                            if ($performance_en->getBeschreibung()) {
                                $tmpText .= '


';
                            } else {
                                $tmpText = '';
                            }

                            $tmpText .= $performance_en->getInfo();
                        }

                        if ('xmltojson' == $style) {
                            $en->nodeValue = $tmpText_en;
                        } else {
                            $t = $xml->createCDATASection($tmpText_en);
                            $en->appendChild($t);
                        }

                        $tmpI18n->appendChild($en);
                    }
                    
                    $tmpShortDescription->appendChild($tmpI18n);
                }

                if ($performance->getInfouri()) {
                    $tmpLink = $xml->createElement('link');

                    $tmpI18n = $xml->createElement('I18n');
                    $de = $xml->createElement('de');

                    if ('xmltojson' == $style) {
                        $de->nodeValue = $performance->getInfouri();
                    } else {
                        $t = $xml->createCDATASection($performance->getInfouri());
                        $de->appendChild($t);
                    }

                    $tmpI18n->appendChild($de);
                    
                    if ($translated && $performance_en->getInfouri()) {
                        $en = $xml->createElement('en');

                        if ('xmltojson' == $style) {
                            $en->nodeValue = $performance_en->getInfouri();
                        } else {
                            $t = $xml->createCDATASection($performance_en->getInfouri());
                            $en->appendChild($t);
                        }

                        $tmpI18n->appendChild($en);
                    }

                    $tmpLink->appendChild($tmpI18n);
                }

                if ($performance->getBooking()) {
                    $tmpBookingLink = $xml->createElement('bookingLink');
                    $tmpI18n = $xml->createElement('I18n');
                    $de = $xml->createElement('de');

                    if ('xmltojson' == $style) {
                        $de->nodeValue = $performance->getBooking();
                    } else {
                        $t = $xml->createCDATASection($performance->getBooking());
                        $de->appendChild($t);
                    }

                    $tmpI18n->appendChild($de);

                    if ($translated && $performance_en->getBooking()) {
                        $en = $xml->createElement('en');

                        if ('xmltojson' == $style) {
                            $en->nodeValue = $performance_en->getBooking();
                        } else {
                            $t = $xml->createCDATASection($performance_en->getBooking());
                            $en->appendChild($t);
                        }

                        $tmpI18n->appendChild($en);
                    }

                    $tmpBookingLink->appendChild($tmpI18n);
                }

                if (count($performance->getVorschaubig()) >= 1) {
                    $tmpMedia = $xml->createElement('media');

                    $tmp_img = $performance->getVorschaubig();

                    foreach ($tmp_img as $k => $val) {
                        $origFile = $val->getOriginalResource();
                        $url = $origFile->getOriginalFile()->getPublicUrl();
                        $desc = $origFile->getDescription();
                        $desc_arr = explode('$', $desc);
                        $title = $origFile->getTitle();

                        $tmp = $xml->createElement('EventImage');

                        $tmp22 = $xml->createElement('pooledMedium');
                        $tmp2 = $xml->createElement('PooledEventMedium');

                        $tmpAttr = $xml->createAttribute('url');
                        $tmpAttr->value = $this->request->getBaseUri() . $url;
                        $tmp2->appendChild($tmpAttr);

                        $tmp3 = $xml->createElement('imageType');
                        $tmp4 = $xml->createElement('ImageType');
                        $tmpAttr = $xml->createAttribute('id');
                        $tmpAttr->value = 1;

                        $tmp4->appendChild($tmpAttr);
                        $tmp3->appendChild($tmp4);

                        if ($title) {
                            $tmp5 = $xml->createElement('title');
                            $tmp6 = $xml->createElement('I18n');
                            $tmp7 = $xml->createElement('de');

                            if ('xmltojson' == $style) {
                                $tmp7->nodeValue = $title;
                            } else {
                                $t = $xml->createCDATASection($title);
                                $tmp7->appendChild($t);
                            }

                            $tmp6->appendChild($tmp7);
                            $tmp5->appendChild($tmp6);
                            $tmp2->appendChild($tmp5);
                        }

                        if ($desc_arr[4]) {
                            $tmp8 = $xml->createElement('description');
                            $tmp9 = $xml->createElement('I18n');
                            $tmp10 = $xml->createElement('de');

                            if ('xmltojson' == $style) {
                                $tmp10->nodeValue = $desc_arr[4];
                            } else {
                                $t = $xml->createCDATASection($desc_arr[4]);
                                $tmp10->appendChild($t);
                            }

                            $tmp9->appendChild($tmp10);
                            $tmp8->appendChild($tmp9);
                            $tmp2->appendChild($tmp8);
                        }

                        if ($desc_arr[0]) {
                            $tmp11 = $xml->createElement('copyright');
                            $tmp12 = $xml->createElement('I18n');
                            $tmp13 = $xml->createElement('de');

                            if ('xmltojson' == $style) {
                                $tmp13->nodeValue = $desc_arr[0];
                            } else {
                                $t = $xml->createCDATASection($desc_arr[0]);
                                $tmp13->appendChild($t);
                            }

                            $tmp12->appendChild($tmp13);
                            $tmp11->appendChild($tmp12);
                            $tmp2->appendChild($tmp11);
                        }

                        $tmp22->appendChild($tmp2);
                        $tmp->appendChild($tmp22);
                        $tmp->appendChild($tmp3);

                        $tmpMedia->appendChild($tmp);
                    }
                }

                if (count($performance->getPdf()) >= 1) {
                    if (null == $tmpMedia) $tmpMedia = $xml->createElement('media');

                    $tmp_pdfs = $performance->getPdf();

                    foreach ($tmp_pdfs as $k => $val) {
                        $origFile = $val->getOriginalResource();
                        $url = $origFile->getOriginalFile()->getPublicUrl();
                        $desc = $origFile->getDescription();
                        $desc_arr = explode('$', $desc);
                        $title = $origFile->getTitle();

                        if ($title) $title = 'Pressetext: ' . $title;
                        else $title = 'Pressetext';

                        $tmp = $xml->createElement('EventImage');
                        $tmp22 = $xml->createElement('pooledMedium');
                        $tmp2 = $xml->createElement('PooledEventMedium');

                        $tmpAttr = $xml->createAttribute('url');
                        $tmpAttr->value = $this->request->getBaseUri() . $url;
                        $tmp2->appendChild($tmpAttr);

                        $tmp3 = $xml->createElement('imageType');
                        $tmp4 = $xml->createElement('ImageType');
                        $tmpAttr = $xml->createAttribute('id');
                        $tmpAttr->value = 1;

                        $tmp4->appendChild($tmpAttr);
                        $tmp3->appendChild($tmp4);

                        if ($title) {
                            $tmp5 = $xml->createElement('title');
                            $tmp6 = $xml->createElement('I18n');
                            $tmp7 = $xml->createElement('de');

                            if ('xmltojson' == $style) {
                                $tmp7->nodeValue = $title;
                            } else {
                                $t = $xml->createCDATASection($title);
                                $tmp7->appendChild($t);
                            }

                            $tmp6->appendChild($tmp7);
                            $tmp5->appendChild($tmp6);
                            $tmp2->appendChild($tmp5);
                        }

                        if ($desc_arr[4]) {
                            $tmp8 = $xml->createElement('description');
                            $tmp9 = $xml->createElement('I18n');
                            $tmp10 = $xml->createElement('de');

                            if ('xmltojson' == $style) {
                                $tmp10->nodeValue = $desc_arr[4];
                            } else {
                                $t = $xml->createCDATASection($desc_arr[4]);
                                $tmp10->appendChild($t);
                            }

                            $tmp9->appendChild($tmp10);
                            $tmp8->appendChild($tmp9);
                            $tmp2->appendChild($tmp8);
                        }

                        if ($desc_arr[0]) {
                            $tmp11 = $xml->createElement('copyright');
                            $tmp12 = $xml->createElement('I18n');
                            $tmp13 = $xml->createElement('de');

                            if ('xmltojson' == $style) {
                                $tmp13->nodeValue = $desc_arr[0];
                            } else {
                                $t = $xml->createCDATASection($desc_arr[0]);
                                $tmp13->appendChild($t);
                            }

                            $tmp12->appendChild($tmp13);
                            $tmp11->appendChild($tmp12);
                            $tmp2->appendChild($tmp11);
                        }

                        $tmp22->appendChild($tmp2);
                        $tmp->appendChild($tmp22);
                        $tmp->appendChild($tmp3);

                        $tmpMedia->appendChild($tmp);
                    }
                }

                $allTypes = '';
                if ($performance->getMappedkat()) {
                    $allTypes = (array) json_decode($performance->getMappedkat());
                }

                if ($performance->getTypxtree()) {
                    $typ = json_decode($performance->getTypxtree());

                    if ('' != $allTypes && is_array($typ) && !empty($typ)) {
                        $tmpCategories = $xml->createElement('categories');

                        $tmp_cat_delete = true;

                        foreach ($typ as $key => $value) {
                            $tmp_arr = explode('$', $value);

                            if (array_key_exists($tmp_arr[0], $allTypes) && $allTypes[$tmp_arr[0]]->mrh) {
                                $tmp = $xml->createElement('Category');

                                $tmpAttr = $xml->createAttribute('id');
                                $tmpAttr->value = $allTypes[$tmp_arr[0]]->mrh;
                                $tmp->appendChild($tmpAttr);

                                $tmpAttr = $xml->createElement('id');
                                $tmpAttr->nodeValue = $allTypes[$tmp_arr[0]]->mrh;
                                $tmp->appendChild($tmpAttr);

                                $tmpName = $xml->createElement('Name');
                                $tmpName->nodeValue = $tmp_arr[1];
                                $tmp->appendChild($tmpName);

                                $tmpCategories->appendChild($tmp);

                                $tmp_cat_delete = false;
                            }
                        }

                        if ($tmp_cat_delete) {
                            $tmpCategories = null;
                        }
                    }
                }

                if ($performance->getArtxtree()) {
                    $art = json_decode($performance->getArtxtree());

                    if ('' != $allTypes && is_array($art) && !empty($art)) {
                        $tmpCriteria = $xml->createElement('criteria');

                        $tmp_art_delete = true;

                        foreach ($art as $key => $value) {
                            $tmp_arr = explode('$', $value);

                            if (array_key_exists($tmp_arr[0], $allTypes) && $allTypes[$tmp_arr[0]]->mrh) {
                                $tmp = $xml->createElement('Criterion');

                                $tmpAttr = $xml->createAttribute('id');
                                $tmpAttr->value = $allTypes[$tmp_arr[0]]->mrh;
                                $tmp->appendChild($tmpAttr);

                                $tmpAttr = $xml->createElement('id');
                                $tmpAttr->nodeValue = $allTypes[$tmp_arr[0]]->mrh;
                                $tmp->appendChild($tmpAttr);

                                $tmpName = $xml->createElement('Name');
                                $tmpName->nodeValue = $tmp_arr[1];
                                $tmp->appendChild($tmpName);

                                $tmpCriteria->appendChild($tmp);

                                $tmp_art_delete = false;
                            }

                            if ('Kostenfrei' == $tmp_arr[1]) {
                                $tmpPricing = $xml->createElement('pricing');

                                $tmpPricing2 = $xml->createElement('freeOfCharge');
                                $tmpPricing2->nodeValue = 'true';
                                $tmpPricing->appendChild($tmpPricing2);
                            }
                        }

                        if ($tmp_art_delete) {
                            $tmpCriteria = null;
                        }
                    }
                }

                $spielort_tmp_arr = [];
                $spzeit_tmp_arr = [];
                $spzeit_ev_canceled = [];
                $sp_count = count($spielzeiten);

                $canceled_count = 0;
                if ($sp_count > 0) {
                    $tmpEventDates = $xml->createElement('eventDates');

                    foreach ($spielzeiten as $sp_key => $sp_val) {
                        $loc = null;
                        if (count($sp_val->getSpielort()) > 0) {
                            $loc = $sp_val->getSpielort()[0];
                        } else if (count($performance->getSpielort()) > 0) {
                            $loc = $performance->getSpielort()[0];
                        }

                        $tmp = $xml->createElement('SpecificEventDate');

                        $tmp2 = $xml->createElement('date');
                        $tmp2->nodeValue = $sp_val->getDatum()->format('Y-m-d');
                        $tmp->appendChild($tmp2);

                        $tmp2 = $xml->createElement('startTime');
                        if ($sp_val->getUhrzeit()) {
                            $tmp2->nodeValue = date('H:i:s', $sp_val->getUhrzeit());
                        }
                        $tmp->appendChild($tmp2);

                        $tmp2 = $xml->createElement('duration');
                        if ($sp_val->getDauer()) {
                            $tmp2->nodeValue = $sp_val->getDauer();
                        }
                        $tmp->appendChild($tmp2);

                        if ($sp_val->getStatus() && 'EventCancelled' == $sp_val->getStatus()) {
                            $tmp3 = $xml->createElement('cancelled');
                            $tmp3->nodeValue = 'true';
                            $tmp->appendChild($tmp3);
                            $canceled_count++;

                            if ($loc) {
                                $spzeit_ev_canceled[$loc->getUid()][$sp_val->getUid()] = true;
                            }
                        }

                        $tmpEventDates->appendChild($tmp);

                        if ($loc) {
                            $spielort_tmp_arr[] = $loc;
                            
                            $spzeit_tmp_arr[$loc->getUid()][$sp_val->getUid()] = $tmp->cloneNode(true);
                        }
                    }

                    if ($sp_count == $canceled_count) {
                        $tmpCancelled = $xml->createElement('cancelled');
                        $tmpCancelled->nodeValue = 'true';
                    }
                }

                if ($performance->getName()) {
                    $tmpTitle = $xml->createElement('title');
                    $tmpI18n = $xml->createElement('I18n');
                    $de = $xml->createElement('de');

                    $name = $performance->getName();
                    if ($sp_count == $canceled_count && $canceled_count > 0) {
                        $name = 'Abgesagt: ' . $name;
                    }

                    if ('xmltojson' == $style) {
                        $de->nodeValue = $name;
                    } else {
                        $t = $xml->createCDATASection($name);
                        $de->appendChild($t);
                    }

                    $tmpI18n->appendChild($de);

                    if ($translated && $performance_en->getName()) {
                        $en = $xml->createElement('en');

                        $name = $performance_en->getName();
                        if ($sp_count == $canceled_count && $canceled_count > 0) {
                            $name = 'Cancelled: ' . $name;
                        }

                        if ('xmltojson' == $style) {
                            $en->nodeValue = $name;
                        } else {
                            $t = $xml->createCDATASection($name);
                            $en->appendChild($t);
                        }

                        $tmpI18n->appendChild($en);
                    }

                    $tmpTitle->appendChild($tmpI18n);
                }

                $keys = [];
                $loc_xml_arr = [];
                if (!empty($spielort_tmp_arr) && count($spielort_tmp_arr) > 0) {
                    $tmpLocation = $xml->createElement('location');
                    $tmp8 = null;

                    foreach ($spielort_tmp_arr as $sp_k => $sp_v) {
                        $sp_id = $sp_v->getUid();
                        $sp_mrhid = $sp_v->getMrhid();
                        // if (!in_array($sp_id, $keys) && $sp_mrhid) {
                        if (!in_array($sp_id, $keys)) {
                            $keys[] = $sp_id;

                            $tmp8 = $xml->createElement('AddressPoi');

                            if ($sp_mrhid) {
                                $tmpAttr = $xml->createAttribute('id');
                                $tmpAttr->value = $sp_mrhid;
                                $tmp8->appendChild($tmpAttr);

                                $tmpAttr = $xml->createElement('id');
                                $tmpAttr->nodeValue = $sp_mrhid;
                                $tmp8->appendChild($tmpAttr);
                            }

                            $tmp9 = $xml->createElement('languages');
                            $tmp10 = $xml->createElement('Language');
                            $tmpAttr = $xml->createAttribute('id');
                            $tmpAttr->value = 1;
                            $tmp10->appendChild($tmpAttr);

                            $tmp9->appendChild($tmp10);
                            $tmp8->appendChild($tmp9);

                            $tmp9 = $xml->createElement('types');

                            $tmp10 = $xml->createElement('AddressPoiType');
                            $tmpAttr = $xml->createAttribute('id');
                            $tmpAttr->value = 3;
                            $tmp10->appendChild($tmpAttr);
                            $tmp9->appendChild($tmp10);

                            $tmp8->appendChild($tmp9);

                            $tmp9 = $xml->createElement('title');
                            $tmp10 = $xml->createElement('I18n');
                            $tmp11 = $xml->createElement('de');

                            if ('xmltojson' == $style) {
                                $tmp11->nodeValue = $sp_v->getName();
                            } else {
                                $t = $xml->createCDATASection($sp_v->getName());
                                $tmp11->appendChild($t);
                            }

                            $tmp10->appendChild($tmp11);
                            $tmp9->appendChild($tmp10);

                            $tmp8->appendChild($tmp9);

                            $tmp4 = $xml->createElement('contact1');

                            $tmp6 = $xml->createElement('address');

                            $tmp7 = $xml->createElement('street');
                            $tmp7->nodeValue = $sp_v->getStrasse();
                            $tmp6->appendChild($tmp7);

                            $tmp7 = $xml->createElement('zipcode');
                            $tmp7->nodeValue = $sp_v->getPlz();
                            $tmp6->appendChild($tmp7);

                            $tmp7 = $xml->createElement('city');
                            $tmp7->nodeValue = $sp_v->getOrt();
                            $tmp6->appendChild($tmp7);

                            $tmp7 = $xml->createElement('country');
                            $tmp7->nodeValue = 'Deutschland';
                            $tmp6->appendChild($tmp7);

                            if ($sp_v->getWebsite()) {
                                $tmp51 = $xml->createElement('homepage');
                                $tmp52 = $xml->createElement('I18n');
                                $tmp53 = $xml->createElement('de');

                                if ('xmltojson' == $style) {
                                    $tmp53->nodeValue = $sp_v->getWebsite();
                                } else {
                                    $t = $xml->createCDATASection($sp_v->getWebsite());
                                    $tmp53->appendChild($t);
                                }

                                $tmp52->appendChild($tmp53);
                                $tmp51->appendChild($tmp52);

                                $tmp6->appendChild($tmp51);
                            }

                            $tmp4->appendChild($tmp6);
                            $tmp8->appendChild($tmp4);

                            $tmp4 = $xml->createElement('geoInfo');
                            $tmp5 = $xml->createElement('GeoInfo');
                            $tmp6 = $xml->createElement('coordinates');
                            $tmp7 = $xml->createElement('latitude');
                            $tmp7->nodeValue = $sp_v->getLat();
                            $tmp9 = $xml->createElement('longitude');
                            $tmp9->nodeValue = $sp_v->getLon();

                            $tmp6->appendChild($tmp7);
                            $tmp6->appendChild($tmp9);
                            $tmp5->appendChild($tmp6);

                            $tmp7 = $xml->createElement('street');
                            $tmp7->nodeValue = $sp_v->getStrasse();
                            $tmp5->appendChild($tmp7);

                            $tmp7 = $xml->createElement('zipcode');
                            $tmp7->nodeValue = $sp_v->getPlz();
                            $tmp5->appendChild($tmp7);

                            $tmp7 = $xml->createElement('city');
                            $tmp7->nodeValue = $sp_v->getOrt();
                            $tmp5->appendChild($tmp7);

                            $tmp7 = $xml->createElement('country');
                            $tmp7->nodeValue = 'Deutschland';
                            $tmp5->appendChild($tmp7);

                            $tmp4->appendChild($tmp5);

                            $tmp8->appendChild($tmp4);

                            $loc_xml_arr[$sp_id] = $tmp8->cloneNode(true);

                            $tmpLocation->appendChild($tmp8);
                        }
                    }
                }

                if (count($keys) > 1) {
                    /* Für jede Spielzeit eine Veranstaltung erstellen und füllen */
                    foreach ($spzeit_tmp_arr as $sp_k => $sp_v) {
                        $event_xml = null;
                        $event_xml = $xml->createElement('Event');

                        $eventsAttr = null;
                        $eventsAttr = $xml->createAttribute('id');
                        $eventsAttr->value = $this->maskId($sp_k) + 100000;
                        $event_xml->appendChild($eventsAttr);

                        $event_xml->appendChild($tmpLanguages->cloneNode(true));
                        if ($tmpTitle) {
                            if (!empty($spzeit_ev_canceled) && $spzeit_ev_canceled[$sp_k] && !empty($spzeit_ev_canceled[$sp_k])) {
                                $tmpCancelled = null;

                                $tmpKeys = array_keys($spzeit_ev_canceled[$sp_k]);

                                $tmpKeys2 = array_keys($sp_v);

                                $tmpCncl = [];

                                foreach ($tmpKeys as $tmpKk => $tmpKv) {
                                    if (array_key_exists($tmpKv, $tmpKeys2)) {
                                        $tmpCncl[] = true;
                                    }
                                }

                                if (count($tmpCncl) == count($tmpKeys)) {
                                    $tmpCancelled = $xml->createElement('cancelled');
                                    $tmpCancelled->nodeValue = 'true';

                                    if ($performance->getName()) {
                                        $tmpTitle2 = null;
                                        $tmpTitle2 = $xml->createElement('title');
                                        $tmpI18n = $xml->createElement('I18n');
                                        $de = $xml->createElement('de');

                                        $name = 'Abgesagt: ' . $performance->getName();

                                        if ('xmltojson' == $style) {
                                            $de->nodeValue = $name;
                                        } else {
                                            $t = $xml->createCDATASection($name);
                                            $de->appendChild($t);
                                        }

                                        $tmpI18n->appendChild($de);

                                        if ($translated && $performance_en->getName()) {
                                            $en = $xml->createElement('en');

                                            $name = 'Cancelled: ' . $performance_en->getName();

                                            if ('xmltojson' == $style) {
                                                $en->nodeValue = $name;
                                            } else {
                                                $t = $xml->createCDATASection($name);
                                                $en->appendChild($t);
                                            }

                                            $tmpI18n->appendChild($en);
                                        }

                                        $tmpTitle2->appendChild($tmpI18n);
                                        $event_xml->appendChild($tmpTitle2);
                                    }
                                }
                            } else {
                                $event_xml->appendChild($tmpTitle->cloneNode(true));
                            }
                        }
                        if ($tmpShortDescription) {
                            $event_xml->appendChild($tmpShortDescription->cloneNode(true));
                        }
                        if ($tmpLink) {
                            $event_xml->appendChild($tmpLink->cloneNode(true));
                        }
                        if ($tmpBookingLink) {
                            $event_xml->appendChild($tmpBookingLink->cloneNode(true));
                        }
                        if ($tmpCategories) {
                            $event_xml->appendChild($tmpCategories->cloneNode(true));
                        }
                        if ($tmpCriteria) {
                            $event_xml->appendChild($tmpCriteria->cloneNode(true));
                        }
                        if ($tmpPricing) {
                            $event_xml->appendChild($tmpPricing->cloneNode(true));
                        }
                        
                        $tmpEventDates = null;
                        $tmpEventDates = $xml->createElement('eventDates');
                        foreach ($sp_v as $sp_vk => $sp_vv) {
                            $tmpEventDates->appendChild($sp_vv);
                        }
                        $event_xml->appendChild($tmpEventDates);

                        if ($tmpMedia) {
                            $event_xml->appendChild($tmpMedia->cloneNode(true));
                        }
                        $event_xml->appendChild($tmpCreationTime->cloneNode(true));
                        $event_xml->appendChild($tmpLastChangeTime->cloneNode(true));
                        if ($tmpCancelled) {
                            $event_xml->appendChild($tmpCancelled->cloneNode(true));
                        }

                        $tmpLocation = null;
                        $tmpLocation = $xml->createElement('location');
                        $tmpLocation->appendChild($loc_xml_arr[$sp_k]);
                        $event_xml->appendChild($tmpLocation);

                        if ($tmpClient) {
                            $event_xml->appendChild($tmpClient->cloneNode(true));
                        }

                        if (($tmpLocation || $spl_url) && $tmpShortDescription && $tmpTitle) {
                            $eventsParent->appendChild($event_xml);
                        }

                    }
                } else {
                    // Veranstaltung Elemente Reihenfolge
                    $event_xml->appendChild($tmpLanguages);
                    if ($tmpTitle) {
                        $event_xml->appendChild($tmpTitle);
                    }
                    if ($tmpShortDescription) {
                        $event_xml->appendChild($tmpShortDescription);
                    }
                    if ($tmpLink) {
                        $event_xml->appendChild($tmpLink);
                    }
                    if ($tmpBookingLink) {
                        $event_xml->appendChild($tmpBookingLink);
                    }
                    if ($tmpCategories) {
                        $event_xml->appendChild($tmpCategories);
                    }
                    if ($tmpCriteria) {
                        $event_xml->appendChild($tmpCriteria);
                    }
                    if ($tmpPricing) {
                        $event_xml->appendChild($tmpPricing);
                    }
                    if ($tmpEventDates) {
                        $event_xml->appendChild($tmpEventDates);
                    }
                    if ($tmpMedia) {
                        $event_xml->appendChild($tmpMedia);
                    }
                    $event_xml->appendChild($tmpCreationTime);
                    $event_xml->appendChild($tmpLastChangeTime);
                    if ($tmpCancelled) {
                        $event_xml->appendChild($tmpCancelled);
                    }
                    if ($tmpLocation) {
                        $event_xml->appendChild($tmpLocation);
                    }
                    if ($tmpClient) {
                        $event_xml->appendChild($tmpClient);
                    }

                    if (($tmpLocation || $spl_url) && $tmpShortDescription && $tmpTitle) {
                        $eventsParent->appendChild($event_xml);
                    }
                }

            }

            $xml->appendChild($eventsParent);
        }

        return $xml;
    }

    public function getRDF($url = null)
    {
        if (null != $url) {
            $ch = curl_init();
            $headers = [
                'Accept: application/rdf+xml',
                'Content-type: application/rdf+xml; charset=UTF-8',
            ];
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            $response = curl_exec($ch);
            return $response;
        }
    }

    /**
     *  Erzeugen eines XML Strings aus einem String
     */
    public function makeXMLContent($string = null)
    {
        if (null != $string) {
            $xmlcontent = simplexml_load_string($string);
            if (is_object($xmlcontent)) {
                $namespaces = $xmlcontent->getNamespaces(true);
                if (!empty($namespaces)) {
                    foreach ($namespaces as $key => $value) {
                        $xmlcontent->registerXPathNamespace($key, $value);
                    }
                }
            }
            return $xmlcontent;
        }
    }

    public function getXtree($entry, $cumulate = [])
    {
        $return = [];
        $kategories = $this->getRDF($entry);
        $kategories = $this->makeXMLContent($kategories);
        $all_kategories = $kategories->xpath('//xe:subordinateSimple/@rdf:resource');
        if (!empty($all_kategories)) {
            if (!empty($cumulate)) {
                $all_elements = $cumulate;
            } else {
                $all_elements = [];
            }
            foreach ($all_kategories as $value) {
                if (!empty($value)) {
                    $uri = $value[0]->__toString();
                    $element = $this->getRDF($uri);
                    $element = $this->makeXMLContent($element);
                    $idnode = $element->xpath('//skos:Concept/@rdf:about');
                    $label = $element->xpath('//skos:prefLabel[@xml:lang="de"]/text()');
                    $child_elements = $element->xpath('//xe:subordinateSimple/@rdf:resource');
                    $order = 1000000;
                    $orderNode = $element->xpath('//xe:sortOrder/text()');
                    if (!empty($orderNode)) {
                        $order = $orderNode[0]->__toString();
                    }
                    $mappedkat = [];
                    $mapping_kat = $element->xpath('//skos:exactMatch/@rdf:resource');
                    if (!empty($mapping_kat)) {
                        $mapping_id = $mapping_kat[0]->__toString();
                        if (strpos($mapping_id, 'www.soziokultur-sh.de/kalender/kategorie')) {
                            $tmp_array = explode('/', $mapping_id);
                            $mappedkat['lag'] = ucfirst($tmp_array[count($tmp_array) - 1]);
                        }
                        if (strpos($mapping_id, 'mrh.events')) {
                            $tmp_array = explode('/', $mapping_id);
                            $mappedkat['mrh'] = ucfirst($tmp_array[count($tmp_array) - 1]);
                        }
                    }

                    if (!empty($label)) {
                        $str_label = $label[0]->__toString();
                        preg_match('/\((.*?)\)/', $str_label, $str_match);
                        // selectiert alles in den klammern (text)
                        $str_label = trim(preg_replace('/\((.*?)\)/', '', $str_label));

                        if (is_array($str_match) && !empty($str_match)) {
                            $all_elements[$uri] = ['label' => $str_label, 'value' => $uri . '$' . $str_label, 'thema' => $str_match[1], 'order' => $order, 'mapping' => $mappedkat];
                        } else {
                            $all_elements[$uri] = ['label' => $str_label, 'value' => $uri . '$' . $str_label, 'order' => $order, 'mapping' => $mappedkat];
                        }
                    }
                }
            }
            $return = $all_elements;
        }
        if (!empty($return)) {
            usort($return, function ($a, $b) {
                return $a['order'] - $b['order'];
            });
        }
        return $return;
    }

    /*
     * Mask Uid
     *
     */
    public function maskId($num)
    {
        if (null != $num) {
            $num = $num + 126678;
        }

        return $num;
    }

    /*
     * DeMask Uid
     *
     */
    public function deMaskId($num)
    {
        if (null != $num) {
            $num = $num - 126678;
        }

        return $num;
    }
}
