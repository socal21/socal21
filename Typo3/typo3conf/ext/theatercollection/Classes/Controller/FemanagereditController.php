<?php
namespace JO\Theatercollection\Controller;

use In2code\Femanager\Controller\EditController as FE_EditController;
use JO\Theatercollection\Domain\Model\Veranstalter;
use JO\Theatercollection\Domain\Repository\VeranstalterRepository;

class FemanagereditController extends FE_EditController
{
    /**
     * veranstalterRepository
     *
     * @var VeranstalterRepository
     */
    protected $veranstalterRepository = null;

    /**
     * @param VeranstalterRepository $veranstalterRepository
     */
    public function injectVeranstalterRepository(VeranstalterRepository $veranstalterRepository)
    {
        $this->veranstalterRepository = $veranstalterRepository;
    }

    public function getJSON($url = null)
    {
        if (null != $url) {
            $ch = curl_init();
            $headers = [
                'Accept: application/json',
                'Content-type: application/json',
            ];
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            $response = curl_exec($ch);
            return $response;
        }
    }

    /**
     * action update
     *
     * @param JO\Theatercollection\Domain\Model\User $user
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\ServersideValidator", param="user")
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\PasswordValidator", param="user")
     */
    public function updateAction($user): void
    {
        if ($this->request->hasArgument('veranstalter')) {
            $user->setVeranstalter(filter_var($this->request->getArgument('veranstalter'), FILTER_SANITIZE_NUMBER_INT));
        }

        if ($this->request->hasArgument('veranstalterxtree')) {
            $user->setVeranstalterxtree(filter_var($this->request->getArgument('veranstalterxtree'), FILTER_SANITIZE_STRING));
        }
        parent::updateAction($user);
    }

    public function editAction()
    {
        $veranstalter = $this->veranstalterRepository->findAll();
        $this->view->assign('veranstalter', $veranstalter);

        $uri = 'http://xtree-actor-api.digicult-verbund.de/getRepositoryList?portalURI=http://digicult.vocnet.org/portal/p0314&count=100';
        $veranstalter = json_decode($this->getJSON($uri));
        $ver_arr = [];

        if (isset($veranstalter->Actor)) {
            foreach ($veranstalter->Actor as $value) {
                if (isset($value->name)) {
                    $ver_arr[$value->id] = $value->name;
                }
            }
        }

        $this->view->assign('veranstalterxtree', $ver_arr);

        parent::editAction();
    }
}
