<?php
namespace JO\Theatercollection\Controller;

/***
 *
 * This file is part of the "Theater Collection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 *
 *  (c) 2020 Alexander Miller <info@justorange.org>, JUSTORANGE
 *
 ***/

use JO\Theatercollection\Domain\Model\Lock;
use JO\Theatercollection\Domain\Model\Performance;
use JO\Theatercollection\Domain\Model\Spielort;
use JO\Theatercollection\Domain\Model\Spielzeit;
use JO\Theatercollection\Domain\Model\Veranstalter;
use JO\Theatercollection\Domain\Model\Veranstaltungsart;
use JO\Theatercollection\Domain\Model\Veranstaltungstype;
use JO\Theatercollection\Domain\Repository\FileReferenceRepository;
use JO\Theatercollection\Domain\Repository\LockRepository;
use JO\Theatercollection\Domain\Repository\PerformanceRepository;
use JO\Theatercollection\Domain\Repository\SpielortRepository;
use JO\Theatercollection\Domain\Repository\SpielzeitRepository;
use JO\Theatercollection\Domain\Repository\TheaterRepository;
use JO\Theatercollection\Domain\Repository\VeranstalterRepository;
use JO\Theatercollection\Domain\Repository\VeranstaltungsartRepository;
use JO\Theatercollection\Domain\Repository\VeranstaltungstypeRepository;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\File\BasicFileUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * EditorController
 */
class EditorController extends ActionController
{
    /**
     * fileReferenceRepository
     *
     * @var FileReferenceRepository
     */
    protected $fileReferenceRepository = null;

    /**
     * spielortRepository
     *
     * @var SpielortRepository
     */
    protected $spielortRepository = null;

    /**
     * veranstalterRepository
     *
     * @var VeranstalterRepository
     */
    protected $veranstalterRepository = null;

    /**
     * theaterRepository
     *
     * @var TheaterRepository
     */
    protected $theaterRepository = null;

    /**
     * performanceRepository
     *
     * @var PerformanceRepository
     */
    protected $performanceRepository = null;

    /**
     * spielzeitRepository
     *
     * @var SpielzeitRepository
     */
    protected $spielzeitRepository = null;

    /**
     * veranstaltungsartRepository
     *
     * @var VeranstaltungsartRepository
     */
    protected $veranstaltungsartRepository = null;

    /**
     * veranstaltungstypeRepository
     *
     * @var VeranstaltungstypeRepository
     */
    protected $veranstaltungstypeRepository = null;

    /**
     * veranstalterRepository
     *
     * @var VeranstalterRepository
     */
    protected $lockRepository = null;

    /**
     * Logger
     *
     * @var Logger
     */
    protected $logger = null;

    /**
     * AbstractController constructor.
     * @param FileReferenceRepository $fileReferenceRepository
     * @param LockRepository $lockRepository
     * @param SpielortRepository $spielortRepository
     * @param VeranstalterRepository $veranstalterRepository
     * @param PerformanceRepository $performanceRepository
     * @param VeranstaltungsartRepository $veranstaltungsartRepository
     * @param VeranstaltungstypeRepository $veranstaltungstypeRepository
     * @param TheaterRepository $theaterRepository
     * @param SpielzeitRepository $spielzeitRepository
     */
    public function __construct(
        FileReferenceRepository $fileReferenceRepository,
        LockRepository $lockRepository,
        SpielortRepository $spielortRepository,
        VeranstalterRepository $veranstalterRepository,
        PerformanceRepository $performanceRepository,
        VeranstaltungsartRepository $veranstaltungsartRepository,
        VeranstaltungstypeRepository $veranstaltungstypeRepository,
        TheaterRepository $theaterRepository,
        SpielzeitRepository $spielzeitRepository)
    {
        $this->fileReferenceRepository = $fileReferenceRepository;
        $this->lockRepository = $lockRepository;
        $this->spielortRepository = $spielortRepository;
        $this->veranstalterRepository = $veranstalterRepository;
        $this->performanceRepository = $performanceRepository;
        $this->veranstaltungsartRepository = $veranstaltungsartRepository;
        $this->veranstaltungstypeRepository = $veranstaltungstypeRepository;
        $this->theaterRepository = $theaterRepository;
        $this->spielzeitRepository = $spielzeitRepository;
    }

    public function managePermissions(){
        $permission = array(
            'saveEvents' => 0
        );
        $user = $GLOBALS['TSFE']->fe_user;
        if ($user) {
            if (isset($user->user['usergroup'])) {
                $groups = explode(',', $user->user['usergroup']);
                if(!empty($groups) && isset($this->settings['allowedFunctionsPerGroup']['saveEvents']) && is_array($this->settings['allowedFunctionsPerGroup']['saveEvents'])){
                    $result = array_intersect($this->settings['allowedFunctionsPerGroup']['saveEvents'], $groups);
                    if($result && !empty($result)) $permission['saveEvents'] = 1;
                }
            }
        }
        return $permission;
    }

    public function initializeView(ViewInterface $view)
    {
        $this->logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);
        $permission = $this->managePermissions();
        $this->view->assign('permission', $permission);
    }

    protected $kulturKat = [
        0 => ['label' => 'Bühne', 'value' => 'Bühne'],
        1 => ['label' => 'Draußen', 'value' => 'Draußen'],
        2 => ['label' => 'Film', 'value' => 'Film'],
        3 => ['label' => 'Literatur', 'value' => 'Literatur'],
        4 => ['label' => 'Museum', 'value' => 'Museum'],
        5 => ['label' => 'Musik', 'value' => 'Musik'],
        6 => ['label' => 'Wissen&Natur', 'value' => 'Wissen&Natur'],
    ];

    /**
     * Get Image File Path
     *
     * @param $name
     * @return string
     */
    public function getFileObject($name)
    {
        $cObj = $this->configurationManager->getContentObject();
        $uid = (int) $cObj->data['_LOCALIZED_UID'] ? (int) $cObj->data['_LOCALIZED_UID'] : (int) $cObj->data['uid'];
        $castName = $name;
        $fileRepository = GeneralUtility::makeInstance(FileRepository::class);
        $fileObjects = $fileRepository->findByRelation('tt_content', $castName, $uid);
        return $fileObjects;
    }

    /*
     * Mask Uid
     *
     */
    public function maskId($num)
    {
        if (null != $num) {
            $num = $num + 126678;
        }

        return $num;
    }

    /**
     * starting Action
     * shows the main menu
     *
     * @return void
     */
    public function buildAction()
    {
        if ($this->request->hasArgument('saved')) {
            $this->view->assign('saved', 'saved');
        }

        $this->lockCheck();

        if (0 == $GLOBALS['TSFE']->fe_user->user['tx_femanager_terms']) {
            $uri = $this->uriBuilder->reset()->setTargetPageUid(13)->build();
            $this->redirectToURI($uri);
        }

        $anleitung = $this->getFileObject('anleitung');
        if (!empty($anleitung)) {
            $this->view->assign('anleitung', $anleitung[0]);
        }

        $this->groupPermission();
    }

    /*
     * Check the object lock table
     * clear entry if current user
     * clear entry if difference between last update and current time is bigger than 5 min
     *
     */
    public function lockCheck()
    {
        $username = $GLOBALS['TSFE']->fe_user->user['username'];
        $obj_locks = $this->lockRepository->findAll();
        if (count($obj_locks) > 0 && $username) {
            $persistenceManager = $this->objectManager->get(PersistenceManager::class);

            foreach ($obj_locks as $obj_lock) {
                if ($obj_lock->getUser() == $username) {
                    $this->lockRepository->remove($obj_lock);
                    continue;
                }

                $lasttime = new \DateTime($obj_lock->getLastupdate());
                $curtime = new \DateTime();
                $interval = date_diff($lasttime, $curtime);
                $div_min = $interval->format('%i');

                if ($div_min >= 5) {
                    $this->lockRepository->remove($obj_lock);
                }
            }

            $persistenceManager->persistAll();
        }
    }

    /*
     * check usergroup permission
     * pass on username and permission to html
     *
     */
    public function groupPermission()
    {
        $user = $GLOBALS['TSFE']->fe_user;
        $hideEditOrganizer = true;
        if ($user) {
            // hier noch die ID vom User rein
            $usergroup = [0];

            if (isset($user->user['usergroup'])) {
                $usergroup = explode(',', $user->user['usergroup']);
                if (isset($this->settings['editors']['edit_veranstalter']['settings']['allowedGroup'])) {
                    if (in_array($this->settings['editors']['edit_veranstalter']['settings']['allowedGroup'], $usergroup)) {
                        $hideEditOrganizer = false;
                    }
                }
            }
            $this->view->assign('username', $user->user['username']);
        }
        $this->view->assign('hideEditOrganizer', $hideEditOrganizer);
    }

    public function mapData($object = null, $datatype = null)
    {
        if (null != $object && null != $datatype) {
            $args = [];
            switch ($datatype) {
                case 'performance':
                    $args['hidden'] = $object->getHidden();
                    $args['uid'] = $object->getUid();
                    $args['maskedUid'] = $this->maskId($object->getUid());
                    $args['event-name'] = $object->getName();
                    $args['event-desc'] = $object->getBeschreibung();
                    $args['event-desc-long'] = $object->getLangbeschreibung();
                    $args['editoranmerkung'] = $object->getEditoranmerkung();
                    $args['infos'] = $object->getInfo();
                    $args['eventurl'] = $object->getInfouri();
                    $args['booking'] = $object->getBooking();
                    $args['bookingstart'] = $object->getBookingstart();
                    $args['age'] = $object->getAltersfreigabe();
                    $args['maxattendee'] = $object->getMaxattendee();
                    $args['maxattendeephy'] = $object->getMaxattendeephy();
                    $args['maxattendeevirt'] = $object->getMaxattendeevirt();

                    if ($object->getTyp()) {
                        $args['veranstaltungsTyp'] = $object->getTyp();
                        $args['veranstaltungsTypOutput'] = [];

                        foreach ($args['veranstaltungsTyp'] as $key => $value) {
                            $args['veranstaltungsTypOutput'][] = $value->getUid();
                        }
                    }

                    if ($object->getArt()) {
                        $args['veranstaltungsArt'] = $object->getArt();
                        $args['veranstaltungsArtOutput'] = [];

                        foreach ($args['veranstaltungsArt'] as $key => $value) {
                            $args['veranstaltungsArtOutput'][] = $value->getUid();
                        }
                    }

                    if ($object->getSpielort() && count($object->getSpielort()) > 0) {
                        $args['select-spielort'] = [(string) $object->getSpielort()[0]->getUid()];

                        $args['select-spielortOutput'] = [(string) $object->getSpielort()[0]->getName()];
                    }

                    if ($object->getSpielorturl()) {
                        $args['spielorturl'] = $object->getSpielorturl();
                    }

                    if ($object->getTypxtree()) {
                        $args['veranstaltungsTypeXtree'] = json_decode($object->getTypxtree());
                        $args['veranstaltungsTypeXtreeOutput'] = [];

                        foreach ($args['veranstaltungsTypeXtree'] as $key => $value) {
                            $args['veranstaltungsTypeXtreeOutput'][] = explode('$', $value)[1];
                        }
                    }
                    if ($object->getArtxtree()) {
                        $args['veranstaltungsArtXtree'] = json_decode($object->getArtxtree());
                        $args['veranstaltungsArtXtreeOutput'] = [];

                        foreach ($args['veranstaltungsArtXtree'] as $key => $value) {
                            $args['veranstaltungsArtXtreeOutput'][] = explode('$', $value)[1];
                        }
                    }
                    if ($object->getSozartxtree()) {
                        $args['sozArt'] = json_decode($object->getSozartxtree());
                        $args['sozArtOutput'] = [];

                        foreach ($args['sozArt'] as $key => $value) {
                            $args['sozArtOutput'][] = explode('$', $value)[1];
                        }
                    }
                    if ($object->getSuchkat()) {
                        $args['suchkat'] = json_decode($object->getSuchkat());
                        $args['suchkatOutput'] = [];

                        foreach ($args['suchkat'] as $key => $value) {
                            $args['suchkatOutput'][] = explode('$', $value)[1];
                        }
                    }

                    if ($object->getVeranstalter()) {
                        $args['veranstalter'] = $object->getVeranstalter();
                        $args['veranstalterOutput'] = [];

                        foreach ($args['veranstalter'] as $key => $value) {
                            $args['veranstalterOutput'][] = $value->getUid();
                        }
                    }

                    $args['veranstaltungsArtXtreePrimery'] = $object->getArtprimary();
                    $args['veranstaltungsTypeXtreePrimery'] = $object->getTypprimary();

                    $args['veranstalter'] = $object->getVeranstalter();

                    $args['veranstaltungsArtPrimery'] = $object->getArtprimary();
                    $args['veranstaltungsTypePrimery'] = $object->getTypprimary();

                    if ($object->getImgdefault()) {
                        $args['defaultImg'] = json_decode($object->getImgdefault());
                    }

                    if (count($object->getVorschaubig()) >= 1) {
                        $tmp_img = $object->getVorschaubig()->toArray();

                        foreach ($tmp_img as $key => $value) {
                            $descArr = explode('$', $value->getOriginalResource()->getDescription());
                            $args['foto'][$key]['obj'] = $value;
                            $args['foto'][$key]['urheber'] = $descArr[0];
                            $args['foto'][$key]['rechte'] = $descArr[1];
                            $args['foto'][$key]['lizenz'] = $descArr[2];
                            $args['foto'][$key]['tag'] = $descArr[3];
                            $args['foto'][$key]['desc'] = $descArr[4];
                            $args['foto'][$key]['quelle'] = $descArr[5];

                            $args['foto'][$key]['title'] = $value->getOriginalResource()->getTitle();
                        }
                    }

                    if (count($object->getPdf()) >= 1) {
                        $tmp_img = $object->getPdf()->toArray();

                        foreach ($tmp_img as $key => $value) {
                            $descArr = explode('$', $value->getOriginalResource()->getDescription());
                            $args['pdf'][$key]['obj'] = $value;
                            $args['pdf'][$key]['tag'] = $descArr[3];
                            $args['pdf'][$key]['desc'] = $descArr[4];

                            $args['pdf'][$key]['title'] = $value->getOriginalResource()->getTitle();
                        }
                    }
                    break;
                case 'spielzeiten':
                    $args['date'] = $object->getDatum()->format('d.m.Y');
                    $args['date_out'] = $object->getDatum()->format('Y-m-d');
                    if ($object->getUhrzeit() && $object->getUhrzeit() != 0) {
                        $args['start-time'] = date('H:i', $object->getUhrzeit());
                    }
                    if ($object->getBis() && $object->getBis() != 0) {
                        $args['end-time'] = date('H:i', $object->getBis());
                    }
                    if ($object->getEinlass() && $object->getEinlass() != 0) {
                        $args['einlass'] = date('H:i', $object->getEinlass());
                    }
                    if ($object->getSpielort() && count($object->getSpielort()) > 0) {
                        $args['ort'] = $object->getSpielort()[0];
                    }
                    if ($object->getDauer()) {
                        $args['dauer'] = $object->getDauer();
                    }
                    if ($object->getSpielorturl()) {
                        $args['spielorturl'] = $object->getSpielorturl();
                    }

                    if ($object->getStatus()) {
                        $args['status'] = $object->getStatus();

                        switch ($object->getStatus()) {
                            case 'EventScheduled':
                                break;
                            case 'EventRescheduled':
                                if ($object->getNewdatum()) {
                                    $args['rescheduled'] = $object->getNewdatum()->format('Y-m-d');
                                }
                                break;
                            case 'EventPostponed':
                                if ($object->getNewdatum()) {
                                    $args['postponed'] = $object->getNewdatum()->format('Y-m-d');
                                }
                                break;
                            case 'EventCancelled':
                                if ($object->getAusfallinfo()) {
                                    $args['cancelreason'] = $object->getAusfallinfo();
                                }
                                break;
                        }
                    }

                    $args['orig'] = $object;
                    break;
                case 'ort':
                    $args['uid'] = $object->getUid();
                    $args['maskedUid'] = $this->maskId($object->getUid());
                    $args['digicultid'] = $object->getDigicultid();
                    $args['editoranmerkung'] = $object->getEditoranmerkung();
                    $args['mrhid'] = $object->getMrhid();
                    $args['name'] = $object->getName();
                    $args['raum'] = $object->getRaum();
                    $args['website'] = $object->getWebsite();
                    $args['legalname'] = $object->getLegalname();
                    $args['alternativname'] = $object->getAlternativname();
                    $args['ort'] = $object->getOrt();
                    $args['plz'] = $object->getPlz();
                    $args['lat'] = $object->getLat();
                    $args['lon'] = $object->getLon();
                    $args['strasse'] = $object->getStrasse();
                    $args['geonames'] = $object->getGeonames();
                    $args['ansprechperson'] = $object->getAnsprechperson();
                    $args['email'] = $object->getEmail();
                    $args['telefon'] = $object->getTelefon();
                    $args['mobil'] = $object->getMobil();
                    $args['kultur'] = explode(',', $object->getKultur());
                    $args['opnv'] = $object->getOpnv();

                    if (count($object->getVorschausmall()) >= 1) {
                        $tmp_img = $object->getVorschausmall()->toArray();

                        foreach ($tmp_img as $key => $value) {
                            $descArr = explode('$', $value->getOriginalResource()->getDescription());
                            $args['foto'][$key]['obj'] = $value;
                            $args['foto'][$key]['urheber'] = $descArr[0];
                            $args['foto'][$key]['rechte'] = $descArr[1];
                            $args['foto'][$key]['lizenz'] = $descArr[2];
                            $args['foto'][$key]['tag'] = $descArr[3];
                            $args['foto'][$key]['desc'] = $descArr[4];
                            $args['foto'][$key]['quelle'] = $descArr[5];

                            $args['foto'][$key]['title'] = $value->getOriginalResource()->getTitle();
                        }
                    }

                    break;
                case 'veranstalter':
                    $args['uid'] = $object->getUid();

                    $args['digicultid'] = $object->getDigicultid();
                    $args['name'] = $object->getName();
                    $args['ort'] = $object->getOrt();
                    $args['plz'] = $object->getPlz();
                    $args['strasse'] = $object->getStrasse();
                    $args['telefon'] = $object->getTelefon();
                    $args['mobil'] = $object->getMobil();
                    $args['fax'] = $object->getFax();
                    $args['email'] = $object->getEmail();
                    $args['website'] = $object->getWebsite();
                    $args['ansprechperson'] = $object->getAnsprechperson();

                    if (count($object->getVorschausmall()) >= 1) {
                        $tmp_img = $object->getVorschausmall()->toArray();

                        foreach ($tmp_img as $key => $value) {
                            $descArr = explode('$', $value->getOriginalResource()->getDescription());
                            $args['foto'][$key]['obj'] = $value;
                            $args['foto'][$key]['urheber'] = $descArr[0];
                            $args['foto'][$key]['rechte'] = $descArr[1];
                            $args['foto'][$key]['lizenz'] = $descArr[2];
                            $args['foto'][$key]['tag'] = $descArr[3];
                            $args['foto'][$key]['desc'] = $descArr[4];
                            $args['foto'][$key]['quelle'] = $descArr[5];

                            $args['foto'][$key]['title'] = $value->getOriginalResource()->getTitle();
                        }
                    }
                    break;
            }
            return $args;
        }
    }

    public function getRDF($url = null)
    {
        if (null != $url) {
            $ch = curl_init();
            $headers = [
                'Accept: application/rdf+xml',
                'Content-type: application/rdf+xml; charset=UTF-8',
            ];
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            $response = curl_exec($ch);
            return $response;
        }
    }

    /**
     *  Erzeugen eines XML Strings aus einem String
     */
    public function makeXMLContent($string = null)
    {
        if (null != $string) {
            $xmlcontent = simplexml_load_string($string);

            if (is_object($xmlcontent)) {
                $namespaces = $xmlcontent->getNamespaces(true);
                if (!empty($namespaces)) {
                    foreach ($namespaces as $key => $value) {
                        $xmlcontent->registerXPathNamespace($key, $value);
                    }
                }
            } else {
                return false;
            }

            return $xmlcontent;
        }
    }

    /*
     * XTree Objekte Laden und in die benötigte Form umgestalten
     *
     */
    public function getXtree($entry, $cumulate = [])
    {
        $return = [];
        $kategories = $this->getRDF($entry);
        $kategories = $this->makeXMLContent($kategories);
        $all_kategories = $kategories->xpath('//xe:subordinateSimple/@rdf:resource');
        if (!empty($all_kategories)) {
            if (!empty($cumulate)) {
                $all_elements = $cumulate;
            } else {
                $all_elements = [];
            }

            $parent_imgdefault = [];
            $parent_image = $kategories->xpath('//foaf:Image/@rdf:about');
            if (!empty($parent_image)) {
                $parent_imgdefault['src'] = $parent_image[0]->__toString();
                $parent_img_title = $kategories->xpath('//foaf:Image/dcterms:title[@xml:lang="de"]/text()');
                if (!empty($parent_img_title)) {
                    $parent_imgdefault['title'] = $parent_img_title[0]->__toString();
                }
            }

            foreach ($all_kategories as $value) {
                if (!empty($value)) {
                    $uri = $value[0]->__toString();
                    $element = $this->getRDF($uri);
                    $element = $this->makeXMLContent($element);
                    $idnode = $element->xpath('//skos:Concept/@rdf:about');
                    $label = $element->xpath('//skos:prefLabel[@xml:lang="de"]/text()');
                    $child_elements = $element->xpath('//xe:subordinateSimple/@rdf:resource');
                    $order = 1000000;
                    $orderNode = $element->xpath('//xe:sortOrder/text()');
                    if (!empty($orderNode)) {
                        $order = $orderNode[0]->__toString();
                    }
                    $mappedkat = [];
                    $mapping_kat = $element->xpath('//skos:exactMatch/@rdf:resource');
                    if (!empty($mapping_kat)) {
                        $mapping_id = $mapping_kat[0]->__toString();
                        if (strpos($mapping_id, 'www.soziokultur-sh.de/kalender/kategorie')) {
                            $tmp_array = explode('/', $mapping_id);
                            $mappedkat['lag'] = ucfirst($tmp_array[count($tmp_array) - 1]);
                        }
                        if (strpos($mapping_id, 'mrh.events')) {
                            $tmp_array = explode('/', $mapping_id);
                            $mappedkat['mrh'] = ucfirst($tmp_array[count($tmp_array) - 1]);
                        }
                    }
                    // Image Default
                    $imgdefault = [];
                    $image = $element->xpath('//foaf:Image/@rdf:about');
                    if (!empty($image)) {
                        $imgdefault['src'] = $image[0]->__toString();
                        $img_title = $element->xpath('//foaf:Image/dcterms:title[@xml:lang="de"]/text()');
                        if (!empty($img_title)) {
                            $imgdefault['title'] = $img_title[0]->__toString();
                        }
                    } else if (!empty($parent_imgdefault)) {
                        $imgdefault = $parent_imgdefault;
                    }

                    if (!empty($label)) {
                        $str_label = $label[0]->__toString();
                        preg_match('/\((.*?)\)/', $str_label, $str_match);
                        // selectiert alles in den klammern (text)
                        $str_label = trim(preg_replace('/\((.*?)\)/', '', $str_label));

                        if (is_array($str_match) && !empty($str_match)) {
                            $all_elements[$uri] = ['label' => $str_label, 'value' => $uri . '$' . $str_label, 'thema' => $str_match[1], 'order' => $order, 'mapping' => $mappedkat, 'image' => $imgdefault];
                        } else {
                            $all_elements[$uri] = ['label' => $str_label, 'value' => $uri . '$' . $str_label, 'order' => $order, 'mapping' => $mappedkat, 'image' => $imgdefault];
                        }
                    }
                    if (!empty($child_elements)) {
                        $all_elements[$uri]['items'] = $this->getXtree($uri, $cumulate);
                    }
                }
            }
            $return = $all_elements;
        }
        if (!empty($return)) {
            usort($return, function ($a, $b) {
                return $a['order'] - $b['order'];
            });
        }
        return $return;
    }

    /**
     * action loadform
     * Formular zusammenbauen
     *
     * @return void
     */
    public function loadformAction()
    {
        if ($this->request->hasArgument('target')) {
            $target = filter_var($this->request->getArgument('target'), FILTER_SANITIZE_STRING);
            $user = $GLOBALS['TSFE']->fe_user;
            $v_xtree = $user->user['veranstalterxtree'];

            if ($v_xtree) {
            } else {
                $v_uid = $user->user['veranstalter'];

                $veranstalterName = '';
                if ($v_uid) {
                    $veranstalter = $this->veranstalterRepository->findByUid(intval($v_uid));
                    $veranstalterName = $veranstalter->getName();
                }
            }

            if ($this->settings['editors'][$target]) {
                $uid = null;
                $formdata = [];
                // Performance
                if ($this->request->hasArgument('ep')) {
                    $uid = intval($this->request->getArgument('ep'));
                    $demand['by_ids'] = [$uid];
                    $demand['show_hidden'] = true;
                    $editableperformance = $this->performanceRepository->searchByJODemand($demand)[0];
                    $formdata = $this->mapData($editableperformance, 'performance');
                    // Spielzeiten
                    if ($v_xtree) {
                        $demand['by_veranstalterxtree_ids'] = [$v_xtree];
                    } else {
                        if ($this->settings['select_veranstalter']) {
                            $demand['by_veranstalter_ids'] = intval($formdata['veranstalter']);
                        } else {
                            $demand['by_veranstalter_ids'] = intval($user->user['veranstalter']);
                        }
                    }

                    $demand['by_performance_ids'] = [$uid];
                    $spielzeiten = $this->spielzeitRepository->searchMine($demand);
                    if (!empty($spielzeiten)) {
                        $formdataSpielzeit = [];
                        foreach ($spielzeiten as $value) {
                            $formdataSpielzeit[$value->getDatum()->getTimestamp()] = $this->mapData($value, 'spielzeiten');
                        }
                        // Erste Daten ausgeben
                        if (!empty($formdataSpielzeit)) {
                            $firstelement = reset($formdataSpielzeit);
                            $formdata['start-date'] = $firstelement;

                            if (count($formdataSpielzeit) > 1) {
                                $lastelement = end($formdataSpielzeit);
                                $formdata['end-date'] = $lastelement;
                            }
                        }
                        $formdata['timeline'] = $formdataSpielzeit;
                    }

                    $performance_en = $this->performanceRepository->searchByLang($uid);
                    if (count($performance_en) > 0) {
                        $formdata['en'] = $this->mapData($performance_en[0], 'performance');
                    }

                    $this->view->assign('prefill', $formdata);
                }

                if ($this->request->hasArgument('eTyp')) {
                    $uid = intval($this->request->getArgument('eTyp'));

                    $item = $this->veranstaltungstypeRepository->findByUid($uid);

                    $formdata = [];
                    $formdata['uid'] = $item->getUid();
                    $formdata['name'] = $item->getName();
                    $formdata['mrhid'] = $item->getMrhid();

                    $this->view->assign('prefill', $formdata);
                }

                if ($this->request->hasArgument('eArt')) {
                    $uid = intval($this->request->getArgument('eArt'));

                    $item = $this->veranstaltungsartRepository->findByUid($uid);

                    $formdata = [];
                    $formdata['uid'] = $item->getUid();
                    $formdata['name'] = $item->getName();
                    $formdata['mrhid'] = $item->getMrhid();

                    $this->view->assign('prefill', $formdata);
                }

                if ($this->request->hasArgument('eVer')) {
                    $uid = intval($this->request->getArgument('eVer'));

                    $item = $this->veranstalterRepository->findByUid($uid);

                    $formdata = $this->mapData($item, 'veranstalter');

                    $this->view->assign('prefill', $formdata);
                }

                if ($this->request->hasArgument('eOrt')) {
                    $uid = intval($this->request->getArgument('eOrt'));

                    $item = $this->spielortRepository->findByUid($uid);

                    $formdata = $this->mapData($item, 'ort');

                    $this->view->assign('prefill', $formdata);
                }

                $editorBuild = $this->settings['editors'][$target];

                foreach ($editorBuild['pages'] as $key => $value) {
                    foreach ($value['items'] as $k => $val) {
                        if ('select' == $val['type'] && $val['loadOption'] && !is_array($val['loadOption'])) {
                            switch ($val['loadOption']) {
                                case 'spielorte':
                                    $spielorte = $this->spielortRepository->findAll();
                                    $spielorteArr = [];
                                    $spielorteArrGrp = [];

                                    foreach ($spielorte as $sp_val) {
                                        $spielorteArr[$sp_val->getUid()] = $sp_val->getName();

                                        if ($sp_val->getOrt()) {
                                            $spielorteArrGrp[$sp_val->getOrt()][$sp_val->getName()] = ['label' => $sp_val->getName(), 'value' => $sp_val->getUid()];
                                            ksort($spielorteArrGrp[$sp_val->getOrt()]);
                                        } else {
                                            $spielorteArrGrp['zzz_last'][$sp_val->getName()] = ['label' => $sp_val->getName(), 'value' => $sp_val->getUid()];
                                            ksort($spielorteArrGrp['zzz_last']);
                                        }
                                    }

                                    ksort($spielorteArrGrp);

                                    $editorBuild['pages'][$key]['items'][$k]['loadOption'] = $spielorteArr;
                                    $editorBuild['pages'][$key]['items'][$k]['loadOptionGrp'] = $spielorteArrGrp;
                                    break;
                                case 'veranstaltungsArtXtree':
                                    $kat = 'http://cit.vocnet.org/rdf/c00178';
                                    $kategories = $this->getXtree($kat);
                                    if ($formdata['veranstaltungsArtXtree'] && is_array($formdata['veranstaltungsArtXtree'])) {
                                        foreach ($kategories as $kat_k => $kat_val) {
                                            if (in_array($kat_k['value'], $formdata['veranstaltungsArtXtree'])) {
                                                $primary_arr[] = $kat_val;
                                            }
                                        }
                                        $editorBuild['pages'][$key]['items'][$k]['loadOptionPrimary'] = $primary_arr;
                                    }

                                    $editorBuild['pages'][$key]['items'][$k]['loadOption'] = $kategories;
                                    break;
                                case 'veranstaltungsTypeXtree':
                                    $kat = 'http://cit.vocnet.org/rdf/c00177';
                                    $kategories = $this->getXtree($kat);
                                    if ($formdata['veranstaltungsTypeXtree'] && is_array($formdata['veranstaltungsTypeXtree'])) {
                                        $primary_arr = [];

                                        foreach ($kategories as $kat_k => $kat_val) {
                                            if (in_array($kat_val['value'], $formdata['veranstaltungsTypeXtree'])) {
                                                $primary_arr[] = $kat_val;
                                            }
                                        }

                                        $editorBuild['pages'][$key]['items'][$k]['loadOptionPrimary'] = $primary_arr;
                                    }

                                    $kategoriesGrp = [];

                                    foreach ($kategories as $kat_k => $kat_val) {
                                        if ($kat_val['items']) {
                                            foreach ($kat_val['items'] as $tmp_k => $tmp_v) {
                                                $kategoriesGrp[$kat_val['label']][] = $tmp_v;
                                            }
                                        } else {
                                            $kategoriesGrp[$kat_val['label']] = $kat_val;
                                            $kategoriesGrp[$kat_val['label']]['type'] = 'solo';
                                        }
                                    }

                                    $editorBuild['pages'][$key]['items'][$k]['loadOption'] = $kategories;
                                    $editorBuild['pages'][$key]['items'][$k]['loadOptionGrp'] = $kategoriesGrp;
                                    break;
                                case 'veranstaltungsArt':
                                    $verArt = $this->veranstaltungsartRepository->findAll();
                                    $verArtArr = [];

                                    foreach ($verArt as $verArt_val) {
                                        $verArtArr[] = ['label' => $verArt_val->getName(), 'value' => $verArt_val->getUid()];
                                    }

                                    $editorBuild['pages'][$key]['items'][$k]['loadOption'] = $verArtArr;
                                    break;
                                case 'veranstaltungsType':
                                    $verType = $this->veranstaltungstypeRepository->findAll();
                                    $verTypeArr = [];

                                    foreach ($verType as $verType_val) {
                                        $verTypeArr[] = ['label' => $verType_val->getName(), 'value' => $verType_val->getUid()];
                                    }

                                    $editorBuild['pages'][$key]['items'][$k]['loadOption'] = $verTypeArr;
                                    break;
                                case 'veranstalter':
                                    $veranstalter = $this->veranstalterRepository->findAll();
                                    $veranstalterArr = [];

                                    foreach ($veranstalter as $veranstalter_val) {
                                        $veranstalterArr[] = ['label' => $veranstalter_val->getName(), 'value' => $veranstalter_val->getUid()];
                                    }

                                    $editorBuild['pages'][$key]['items'][$k]['loadOption'] = $veranstalterArr;
                                    break;
                                case 'kulturKat':
                                    $verTypeArr = [];
                                    $editorBuild['pages'][$key]['items'][$k]['loadOption'] = $this->kulturKat;
                                    break;
                                case 'suchkat':
                                    $kat = 'http://cit.vocnet.org/rdf/c00187';
                                    $suchkat = $this->getXtree($kat);

                                    $editorBuild['pages'][$key]['items'][$k]['loadOption'] = $suchkat;
                                    break;
                                case 'sozKat':
                                    $kat = 'http://cit.vocnet.org/rdf/c00167';
                                    $kategories = $this->getXtree($kat);

                                    $editorBuild['pages'][$key]['items'][$k]['loadOption'] = $kategories;
                                    break;
                            }
                        }

                        // @all irgendwie es anders machen, ist einzigartige abfrage
                        if ('input' == $val['type'] && $val['loadValue'] && !is_array($val['loadValue'])) {
                            $editorBuild['pages'][$key]['items'][$k]['value'] = $veranstalterName;
                        }
                    }
                }

                if ('new_event' == $target || 'edit_performance' == $target) {
                    $spl = $this->spielortRepository->findAll();

                    $this->view->assign('spielortModal', $spl);
                }

                $this->view->assignMultiple([
                    'formular' => $editorBuild,
                    'target' => $target,
                ]);
            } else {
                switch ($target) {
                    case 'edit_event':
                        if ($v_xtree) {
                            $demand['by_veranstalterxtree_ids'] = [$v_xtree];
                        } else {
                            $demand['by_veranstalter_ids'] = [intval($user->user['veranstalter'])];
                        }

                        $spielzeiten = $this->spielzeitRepository->searchMine($demand);

                        $this->view->assignMultiple([
                            'spielzeiten' => $spielzeiten,
                            'target' => $target,
                        ]);

                        break;

                    case 'edit_event_performance':
                        if (!$this->settings['alle_veranstalter_anzeigen']) {
                            if ($v_xtree) {
                                $demand['by_veranstalterxtree_ids'] = [$v_xtree];
                            } else {
                                $demand['by_veranstalter_ids'] = [intval($user->user['veranstalter'])];
                            }
                        }
                        $demand['show_hidden'] = true;
                        $performances = $this->performanceRepository->searchByJODemand($demand);

                        $out_sp = [];
                        foreach ($performances as $key => $value) {
                            $demand = [];
                            $demand['by_performance_ids'] = [$value->getUid()];
                            $demand['show_hidden'] = true;
                            $demand['start_date'] = new \Datetime('yesterday');
                            $spielzeiten = $this->spielzeitRepository->searchMine($demand);

                            $ausfall = true;
                            if (count($spielzeiten) > 0) {
                                foreach ($spielzeiten as $k => $v) {
                                    if ('EventCancelled' != $v->getStatus()) {
                                        $ausfall = false;
                                        break;
                                    }
                                }
                            }

                            $out_sp[] = ['sp' => $spielzeiten->getFirst(), 'obj' => $value, 'ausfall' => $ausfall, 'maskedUid' => $this->maskId($value->getUid())];
                        }

                        if (count($performances) > 0) {
                            $id_list = [];

                            foreach ($performances as $k => $v) {
                                $id_list[] = $v->getUid();
                            }

                            // prüfen ob jmd an dem objekt arbeitet
                            $obj_locks = $this->lockRepository->searchIt(null, null, 'edit_performance', $id_list);

                            if (count($obj_locks) > 0) {
                                $lock_list = [];

                                foreach ($obj_locks as $k => $v) {
                                    $lock_list[$v->getObjid()]['user'] = $v->getUser();
                                    $lock_list[$v->getObjid()]['time'] = (new \DateTime($v->getStarttime()))->format('H:i d.m.Y');
                                }

                                $this->view->assign('lock_list', $lock_list);
                            }
                        }

                        $this->view->assignMultiple([
                            'performances' => $out_sp,
                            'target' => $target,
                        ]);
                        break;

                    case 'list_ver_type':
                        $type = $this->veranstaltungstypeRepository->findAll();

                        $this->view->assignMultiple([
                            'ver_type' => $type,
                            'target' => $target,
                        ]);
                        break;

                    case 'list_ver_art':
                        $arten = $this->veranstaltungsartRepository->findAll();

                        $this->view->assignMultiple([
                            'ver_art' => $arten,
                            'target' => $target,
                        ]);

                        break;

                    case 'list_location':
                        $spl = $this->spielortRepository->findAll();

                        if (count($spl) > 0) {
                            $id_list = [];

                            foreach ($spl as $k => $v) {
                                $id_list[] = $v->getUid();
                            }

                            // prüfen ob jmd an dem objekt arbeitet
                            $obj_locks = $this->lockRepository->searchIt(null, null, 'edit_location', $id_list);

                            if (count($obj_locks) > 0) {
                                $lock_list = [];

                                foreach ($obj_locks as $k => $v) {
                                    $lock_list[$v->getObjid()]['user'] = $v->getUser();
                                    $lock_list[$v->getObjid()]['time'] = (new \DateTime($v->getStarttime()))->format('H:i d.m.Y');
                                }

                                $this->view->assign('lock_list', $lock_list);
                            }
                        }

                        $this->view->assignMultiple([
                            'obj' => $spl,
                            'target' => $target,
                        ]);

                        break;

                    case 'list_veranstalter':
                        $ver = $this->veranstalterRepository->findAll();

                        $this->view->assignMultiple([
                            'veranstalter' => $ver,
                            'target' => $target,
                        ]);

                        break;

                    case 'kontakt_form':
                        $this->view->assign('target', $target);

                        break;

                    default:
                        // code...
                        break;
                }
            }
        }
    }

    /**
     * action formsubmit
     * Speicher Formular bearbeiten
     *
     * @return void
     */
    public function formsubmitAction()
    {
        if (!$this->request->hasArgument('form-name')) {
            return false;
        }

        $args = [];

        foreach ($this->request->getArguments() as $key => $value) {
            if (is_array($value)) {
                $args[$key] = filter_var_array($value, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
            } else {
                $args[$key] = filter_var(trim($value), FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
            }
        }

        switch ($args['form-name']) {
            case 'new_event':
            case 'edit_performance':
                $this->new_event($args);
                break;
            case 'new_ver_type':
                $this->new_ver_type($args);
                break;
            case 'new_ver_art':
                $this->new_ver_art($args);
                break;
            case 'new_location':
                $this->new_location($args);
                break;
            case 'new_veranstalter':
                $this->new_veranstalter($args);
                break;
        }
    }

    public function new_event($args)
    {
        $user = $GLOBALS['TSFE']->fe_user;

        $lang_en = $args['event-name-en'] ? true : false;

        $persistenceManager = $this->objectManager->get(PersistenceManager::class);

        $savetype = intval($args['savetype']);
        $savetype = 3 == $savetype ? 1 : $savetype;

        // @all Theater ID verlagern
        $theater = $this->theaterRepository->findByUid(1);
        $do = null;
        $uid = null;
        if ($this->request->hasArgument('do')) {
            $do = filter_var($this->request->getArgument('do'), FILTER_SANITIZE_STRING);
            switch ($do) {
                case 'new':
                    $performance = new Performance;
                    if ($lang_en) {
                        $performance_en = new Performance;
                    }
                    break;
                case 'edit':
                    if ($this->request->hasArgument('currentUid')) {
                        $uid = intval($this->request->getArgument('currentUid'));
                        $demand['by_ids'] = [$uid];
                        $demand['show_hidden'] = true;
                        $performance = $this->performanceRepository->searchByJODemand($demand)[0];

                        if ($lang_en) {
                            $performance_en = $this->performanceRepository->searchByLang($uid)[0];
                            if (count($performance_en) == 0) {
                                $performance_en = new Performance;
                            }
                        }
                    } else {
                        exit('no data selected');
                    }
                    break;
            }
        }

        $v_uid = [1];
        if (is_array($args['veranstalter'])) {
            $v_uid = $args['veranstalter'];
        } else {
            $v_uid = [$user->user['veranstalter']];
        }

        $v_xtree = '';
        if ($user->user['veranstalterxtree']) {
            $v_xtree = $user->user['veranstalterxtree'];
            $v_uid = null;
        }

        $versts = $performance->getVeranstalter();
        if (count($versts) > 0) {
            foreach ($versts as $verst) {
                $performance->removeVeranstalter($verst);
            }
        }

        if (null != $v_uid) {
            $veranstalter = [];
            foreach ($v_uid as $v) {
                $tmp_v = $this->veranstalterRepository->findByUid(intval($v));
                $performance->addVeranstalter($tmp_v);
                $veranstalter[] = $tmp_v;
            }
        }

        if ('' != $v_xtree) {
            $performance->setVeranstalterxtree($v_xtree);
        } else {
            $performance->setVeranstalterxtree('');
        }

        if ($args['event-name']) {
            $performance->setName($args['event-name']);
        } else {
            $performance->setName('');
        }

        if ($args['event-desc']) {
            $performance->setBeschreibung($args['event-desc']);
        } else {
            $performance->setBeschreibung('');
        }

        if ($args['editoranmerkung']) {
            $performance->setEditoranmerkung($args['editoranmerkung']);
        } else {
            $performance->setEditoranmerkung('');
        }

        if ($args['infos']) {
            $performance->setInfo($args['infos']);
        } else {
            $performance->setInfo('');
        }

        if ($args['eventurl']) {
            $performance->setInfouri($args['eventurl']);
        } else {
            $performance->setInfouri('');
        }

        if ($args['booking']) {
            $performance->setBooking($args['booking']);
        } else {
            $performance->setBooking('');
        }

        if ($args['bookingstart']) {
            $performance->setBookingstart($args['bookingstart']);
        } else {
            $performance->setBookingstart('');
        }

        if ($args['age']) {
            $performance->setAltersfreigabe($args['age']);
        } else {
            $performance->setAltersfreigabe('');
        }

        if ($args['maxattendee']) {
            $performance->setMaxattendee($args['maxattendee']);
        } else {
            $performance->setMaxattendee('');
        }

        if ($args['maxattendeephy']) {
            $performance->setMaxattendeephy($args['maxattendeephy']);
        } else {
            $performance->setMaxattendeephy('');
        }

        if ($args['maxattendeevirt']) {
            $performance->setMaxattendeevirt($args['maxattendeevirt']);
        } else {
            $performance->setMaxattendeevirt('');
        }

        if ($lang_en) {
            if ('' != $v_xtree) {
                $performance_en->setVeranstalterxtree($v_xtree);
            } else {
                $performance_en->setVeranstalterxtree('');
            }

            if ($args['event-name-en']) {
                $performance_en->setName($args['event-name-en']);
            } else {
                $performance_en->setName('');
            }

            if ($args['event-desc-en']) {
                $performance_en->setBeschreibung($args['event-desc-en']);
            } else {
                $performance_en->setBeschreibung('');
            }

            if ($args['editoranmerkung-en']) {
                $performance_en->setEditoranmerkung($args['editoranmerkung-en']);
            } else {
                $performance_en->setEditoranmerkung('');
            }

            if ($args['infos-en']) {
                $performance_en->setInfo($args['infos-en']);
            } else {
                $performance_en->setInfo('');
            }

            if ($args['eventurl-en']) {
                $performance_en->setInfouri($args['eventurl-en']);
            } else {
                $performance_en->setInfouri('');
            }

            if ($args['booking-en']) {
                $performance_en->setBooking($args['booking-en']);
            } else {
                $performance_en->setBooking('');
            }

            if ($args['bookingstart-en']) {
                $performance_en->setBookingstart($args['bookingstart-en']);
            } else {
                $performance_en->setBookingstart('');
            }

            if ($args['age-en']) {
                $performance_en->setAltersfreigabe($args['age-en']);
            } else {
                $performance_en->setAltersfreigabe('');
            }

            if ($args['maxattendee-en']) {
                $performance_en->setMaxattendee($args['maxattendee-en']);
            } else {
                $performance_en->setMaxattendee('');
            }

            if ($args['maxattendeephy-en']) {
                $performance_en->setMaxattendeephy($args['maxattendeephy-en']);
            } else {
                $performance_en->setMaxattendeephy('');
            }

            if ($args['maxattendeevirt-en']) {
                $performance_en->setMaxattendeevirt($args['maxattendeevirt-en']);
            } else {
                $performance_en->setMaxattendeevirt('');
            }
        }

        if ($args['veranstaltungsArt']) {
            $perfArts = $performance->getArt();
            if (!empty($perfArts)) {
                foreach ($perfArts as $perfArt) {
                    $performance->removeArt($perfArt);
                }
            }
            foreach ($args['veranstaltungsArt'] as $a) {
                $perfArt = $this->veranstaltungsartRepository->findByUid(filter_var($a, FILTER_VALIDATE_INT));
                if ($perfArt) {
                    $performance->addArt($perfArt);
                }
            }

            if ($args['veranstaltungsArtPrimery']) {
                $performance->setArtprimary($args['veranstaltungsArtPrimery']);
            }
        }

        $mappedkat = [];
        if ($args['veranstaltungsArtXtree']) {
            $art_arr = array_filter($args['veranstaltungsArtXtree']);
            $art_out = '';

            if (!empty($art_arr)) {
                $art_out = json_encode($art_arr);
                $art_arr_en = [];

                foreach ($art_arr as $sk) {
                    $subitem = explode('$', $sk);
                    if (!empty($subitem)) {
                        $vocitem = $this->getRDF($subitem[0]);
                        $vocitem = $this->makeXMLContent($vocitem);

                        $label_en = $vocitem->xpath('//skos:prefLabel[@xml:lang="en"]/text()');
                        if (!empty($label_en)) {
                            $sk_en .= '$' . $label_en[0]->__toString();
                            $art_arr_en[] = $sk_en;
                        }

                        $mapping_kat = $vocitem->xpath('//skos:mappingRelation/@rdf:resource');
                        if (!empty($mapping_kat)) {
                            $mapping_id = $mapping_kat[0]->__toString();
                            if (strpos($mapping_id, 'mrh.events')) {
                                $tmp_array = explode('/', $mapping_id);

                                $itm = ucfirst($tmp_array[count($tmp_array) - 1]);
                                if (is_numeric($itm)) {
                                    $mappedkat[$subitem[0]]['mrh'] = intval($itm);
                                }
                            }
                        }

                        $mapping_kat = $vocitem->xpath('//skos:closeMatch/@rdf:resource');
                        if (!empty($mapping_kat)) {
                            $mapping_id = $mapping_kat[0]->__toString();
                            if (strpos($mapping_id, 'mrh.events')) {
                                $tmp_array = explode('/', $mapping_id);

                                $itm = ucfirst($tmp_array[count($tmp_array) - 1]);
                                if (is_numeric($itm)) {
                                    $mappedkat[$subitem[0]]['mrh'] = $itm;
                                }
                            }
                        }

                        $mapping_kat = $vocitem->xpath('//skos:exactMatch/@rdf:resource');
                        if (!empty($mapping_kat)) {
                            $mapping_id = $mapping_kat[0]->__toString();
                            if (strpos($mapping_id, 'mrh.events')) {
                                $tmp_array = explode('/', $mapping_id);

                                $itm = ucfirst($tmp_array[count($tmp_array) - 1]);
                                if (is_numeric($itm)) {
                                    $mappedkat[$subitem[0]]['mrh'] = $itm;
                                }
                            }
                        }
                    }
                }
            }

            $performance->setArtxtree($art_out);

            if ($args['veranstaltungsArtXtreePrimery']) {
                $performance->setArtprimary($args['veranstaltungsArtXtreePrimery']);
            }

            if ($lang_en && !empty($art_arr_en)) {
                $performance_en->setArtxtree($art_arr_en);
            }
        }

        if ($args['veranstaltungsTyp']) {
            $perfTypes = $performance->getTyp();
            if (!empty($perfTypes)) {
                foreach ($perfTypes as $perfType) {
                    $performance->removeTyp($perfType);
                }
            }
            foreach ($args['veranstaltungsTyp'] as $a) {
                $perfType = $this->veranstaltungstypeRepository->findByUid(filter_var($a, FILTER_VALIDATE_INT));
                if ($perfType) {
                    $performance->addTyp($perfType);
                }
            }

            if ($args['veranstaltungsTypePrimery']) {
                $performance->setTypprimary($args['veranstaltungsTypePrimery']);
            }
        }

        if ($args['veranstaltungsTypeXtree']) {
            $typ_arr = array_filter($args['veranstaltungsTypeXtree']);
            $typ_arr_en = [];

            $typ_prm_en = '';
            if ($args['veranstaltungsTypeXtreePrimery']) {
                $typ_prm_en = $args['veranstaltungsTypeXtreePrimery'];
            }

            // Suchkategorien holen
            if (!empty($typ_arr)) {
                // Dynamisch machen
                $refkat = 'http://cit.vocnet.org/c00187';
                // Event Kategorie
                $eventskat = 'http://cit.vocnet.org/c00261';

                $searchkat = [];
                $eventkat = [];
                foreach ($typ_arr as $sk_key => $sk) {
                    $sk_en = $sk;
                    $subitem = explode('$', $sk);
                    if (!empty($subitem)) {
                        $vocitem = $this->getRDF($subitem[0]);
                        $vocitem = $this->makeXMLContent($vocitem);
                        // translate 
                        $label_en = $vocitem->xpath('//skos:prefLabel[@xml:lang="en"]/text()');
                        if (!empty($label_en)) {
                            $sk_en .= '$' . $label_en[0]->__toString();

                            if ($typ_prm_en != '' && $typ_prm_en == $sk) {
                                $typ_prm_en .= '$' . $label_en[0]->__toString();
                            }
                        }
                        // Check for mappings
                        $mapping_searchkat = $vocitem->xpath('//skos:related/@rdf:resource');
                        if (!empty($mapping_searchkat)) {
                            $related_id = $mapping_searchkat[0]->__toString();
                            $related_object = $this->getRDF($related_id);
                            $related_object = $this->makeXMLContent($related_object);
                            $parent_object = $related_object->xpath('//vocnet:isMemberOfConceptGroup/@rdf:resource');
                            if (!empty($parent_object)) {
                                $identified_parent = $parent_object[0]->__toString();
                                if ($identified_parent == $refkat) {
                                    $idnode = $related_object->xpath('//skos:Concept/@rdf:about');
                                    $label = $related_object->xpath('//skos:prefLabel[@xml:lang="de"]/text()');
                                    $label_en = $related_object->xpath('//skos:prefLabel[@xml:lang="en"]/text()');
                                    $tmp_text = '';
                                    if (!empty($idnode) && !empty($label)) {
                                        $tmp_text = $idnode[0]->__toString() . '$' . $label[0]->__toString();
                                    }

                                    if (!empty($idnode) && !empty($label_en)) {
                                        $tmp_text .= '$' . $label_en[0]->__toString();
                                    }

                                    if ($tmp_text != '') {
                                        $searchkat[] = $tmp_text;   
                                    }
                                }
                            }
                        }

                        $mapping_kat = $vocitem->xpath('//skos:broadMatch/@rdf:resource');
                        if (!empty($mapping_kat)) {
                            $mapping_id = $mapping_kat[0]->__toString();
                            if (strpos($mapping_id, 'www.soziokultur-sh.de/kalender/kategorie')) {
                                $tmp_array = explode('/', $mapping_id);
                                $mappedkat[$subitem[0]]['arr'][] = $mapping_id . '$' . ucfirst($tmp_array[count($tmp_array) - 1]);
                            } else {
                                $mappedkat[$subitem[0]]['arr'][] = $subitem;
                            }
                        }

                        $mapping_kat = $vocitem->xpath('//skos:mappingRelation/@rdf:resource');
                        if (!empty($mapping_kat)) {
                            $mapping_id = $mapping_kat[0]->__toString();
                            if (strpos($mapping_id, 'mrh.events')) {
                                $tmp_array = explode('/', $mapping_id);
                                $mappedkat[$subitem[0]]['mrh'] = ucfirst($tmp_array[count($tmp_array) - 1]);
                            }
                        }

                        $mapping_kat = $vocitem->xpath('//skos:closeMatch/@rdf:resource');
                        if (!empty($mapping_kat)) {
                            $mapping_id = $mapping_kat[0]->__toString();
                            if (strpos($mapping_id, 'mrh.events')) {
                                $tmp_array = explode('/', $mapping_id);
                                $mappedkat[$subitem[0]]['mrh'] = ucfirst($tmp_array[count($tmp_array) - 1]);
                            }
                        }

                        $mapping_kat = $vocitem->xpath('//skos:exactMatch/@rdf:resource');
                        if (!empty($mapping_kat)) {
                            $mapping_id = $mapping_kat[0]->__toString();
                            if (strpos($mapping_id, 'mrh.events')) {
                                $tmp_array = explode('/', $mapping_id);
                                $mappedkat[$subitem[0]]['mrh'] = ucfirst($tmp_array[count($tmp_array) - 1]);
                            }
                        }

                        $all_kategories = $vocitem->xpath('//xe:superordinateSimple/@rdf:resource');
                        if (!empty($all_kategories)) {
                            foreach ($all_kategories as $kat_val) {
                                if (!empty($kat_val)) {
                                    $kat_uri = $kat_val[0]->__toString();
                                    $kat_el = $this->getRDF($kat_uri);
                                    $kat_el = $this->makeXMLContent($kat_el);
                                    $kat_node = $kat_el->xpath('//iso-thes:superGroup/@rdf:resource');

                                    if (!empty($kat_node) && 'http://cit.vocnet.org/c00261' == $kat_node[0]->__toString()) {
                                        $kat_name = $kat_el->xpath('//skos:prefLabel/text()');
                                        if (!empty($kat_name)) {
                                            $eventkat[] = $kat_name[0]->__toString();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $typ_arr_en[] = $sk_en;
                }

                if (!empty($searchkat)) {
                    $args['suchkat'] = $searchkat;
                }

                if (!empty($eventkat)) {
                    $args['eventkat'] = array_unique($eventkat);
                }
            }

            if (!empty($mappedkat)) {
                $args['mappedkat'] = $mappedkat;
            }

            $typ_out = '';

            if (!empty($typ_arr)) {
                $typ_out = json_encode($typ_arr);
            }

            $performance->setTypxtree($typ_out);

            if ($args['veranstaltungsTypeXtreePrimery']) {
                $performance->setTypprimary($args['veranstaltungsTypeXtreePrimery']);
            }

            if ($lang_en) {
                if ($typ_prm_en != '') {
                    $performance_en->setTypprimary($typ_prm_en);
                }

                if (!empty($typ_arr_en)) {
                    $typ_out_en = json_encode($typ_arr_en);
                    
                    $performance_en->setTypxtree($typ_out_en);

                    if ($args['veranstaltungsTypeXtreePrimery']) {
                        $performance_en->setTypprimary($args['veranstaltungsTypeXtreePrimery']);
                    }
                }
            }
        }

        $veranstalterjson = '';
        if ('' != $v_xtree) {
            $uri = 'http://xtree-actor-api.digicult-verbund.de/getRepositoryItem?id=' . $v_xtree;

            $veranstalter_js = json_decode($this->getRDF($uri));
            $veranstalter_js = $veranstalter_js->Actor;

            if (!empty($veranstalter_js)) {
                $uri_self = $veranstalter_js->links->self;

                if ($uri_self) {
                    $veranstalter_js = json_decode($this->getRDF($uri_self))->Actor;

                    $veranstalterjson = json_encode($veranstalter_js, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                } else {
                    $veranstalterjson = json_encode($veranstalter_js, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                }
            }

            $performance->setVeranstalterxtreejson($veranstalterjson);

            if ($lang_en) {
                $performance_en->setVeranstalterxtreejson($veranstalterjson);
            }
        } else {
            $performance->setVeranstalterxtreejson('');
            if ($lang_en) {
                $performance_en->setVeranstalterxtreejson('');
            }
        }

        if ($args['eventkat'] && is_array($args['eventkat'])) {
            $performance->setEventtyp(implode('$', $args['eventkat']));
        } else {
            $performance->setEventtyp('');
        }

        if ($args['sozArt']) {
            $soz_arr = array_filter($args['sozArt']);
            $soz_out = '';

            if (!empty($soz_arr)) {
                $soz_out = json_encode($soz_arr);
            }

            $performance->setSozartxtree($soz_out);
        } else {
            $performance->setSozartxtree('');
        }

        if ($args['mappedkat']) {
            $such_out = json_encode($args['mappedkat']);
            $performance->setMappedkat($such_out);
        } else {
            $performance->setMappedkat([]);
        }

        if ($args['suchkat']) {
            $such_arr = array_values(array_filter(array_unique($args['suchkat'])));
            $such_out = '';
            if (!empty($such_arr)) {
                $such_out = json_encode($such_arr);
            }
            $performance->setSuchkat($such_out);
            if ($lang_en) {
                $performance_en->setSuchkat($such_out);
            }
        } else {
            $performance->setSuchkat([]);
            if ($lang_en) {
                $performance_en->setSuchkat([]);
            }
        }

        $performance->setHidden($savetype);

        $df_arr = [];
        if ($args['defaultImg'] && is_array($args['defaultImg'])) {
            foreach ($args['defaultImg'] as $d_k => $d_v) {
                if ($d_v['url'] && '' != $d_v['url'] && $d_v['save'] && '1' == $d_v['save']) {
                    $df_arr[] = ['url' => $d_v['url'], 'title' => $d_v['title']];
                }
            }
        }

        if (!empty($df_arr)) {
            $performance->setImgdefault(json_encode($df_arr));
        } else {
            $performance->setImgdefault('');
        }

        if ($lang_en) {
            if ($args['eventkat'] && is_array($args['eventkat'])) {
                $performance_en->setEventtyp(implode('$', $args['eventkat']));
            } else {
                $performance_en->setEventtyp('');
            }

            if ($args['sozArt']) {
                $soz_arr = array_filter($args['sozArt']);
                $soz_out = '';

                if (!empty($soz_arr)) {
                    $soz_out = json_encode($soz_arr);
                }

                $performance_en->setSozartxtree($soz_out);
            } else {
                $performance_en->setSozartxtree([]);
            }

            if ($args['mappedkat']) {
                $such_out = json_encode($args['mappedkat']);
                $performance_en->setMappedkat($such_out);
            } else {
                $performance_en->setMappedkat([]);
            }

            if ($args['suchkat']) {
                $such_arr = array_values(array_filter(array_unique($args['suchkat'])));
                $such_out = '';
                if (!empty($such_arr)) {
                    $such_out = json_encode($such_arr);
                }
                $performance_en->setSuchkat($such_out);
            } else {
                $performance_en->setSuchkat([]);
            }

            $performance_en->setHidden($savetype);

            if (!empty($df_arr)) {
                $performance_en->setImgdefault(json_encode($df_arr));
            } else {
                $performance_en->setImgdefault('');
            }
        }

        $this->loadFile('foto', $performance, 'getVorschaubig', 'removeVorschaubig', 'addVorschaubig');
        $this->loadFile('pdf', $performance, 'getPdf', 'removePdf', 'addPdf');

        $all_locations_tmp = $this->spielortRepository->findAll()->toArray();
        $all_locations = [];

        foreach ($all_locations_tmp as $key => $value) {
            $all_locations[$value->getUid()] = $value;
        }

        if ($args['select-spielort'] && is_array($args['select-spielort']) && count($args['select-spielort']) > 0 && $args['select-spielort'][0] != '') {
            $sprOrt = $performance->getSpielort();
            if (!empty($sprOrt)) {
                foreach ($sprOrt as $sprOrt_val) {
                    $performance->removeSpielort($sprOrt_val);
                }
            }

            $performance->addSpielort($all_locations[filter_var($args['select-spielort'][0], FILTER_SANITIZE_NUMBER_INT)]);
        } else {
            $sprOrt = $performance->getSpielort();
            if (!empty($sprOrt)) {
                foreach ($sprOrt as $sprOrt_val) {
                    $performance->removeSpielort($sprOrt_val);
                }
            }
        }

        $spielorturl = '';
        if ($args['spielorturl']) {
            $spielorturl = $args['spielorturl'];
            $performance->setSpielorturl($spielorturl);
        }

        if ('edit' == $do && null != $uid) {
            $this->performanceRepository->update($performance);

            if ($lang_en) {
                $performance_en->setSysLanguageUid(1);
                $performance_en->setL10nParent($performance->getUid());
                $this->performanceRepository->add($performance_en);
            }

            if ('' != $v_xtree) {
                $demand['by_veranstalterxtree_ids'] = [$v_xtree];
            } elseif (null != $v_uid) {
                $demand['by_veranstalter_ids'] = $v_uid;
            }
            $demand['by_performance_ids'] = [$uid];
            $spielzeiten = $this->spielzeitRepository->searchMine($demand);
            // Spielzeiten löschen
            foreach ($spielzeiten as $s) {
                $this->spielzeitRepository->remove($s);
            }

            $append = 0 == $savetype ? 'Veröffentlicht' : 'nicht Veröffentlicht';

            $this->logger->notice('Benutzer "' . $user->user['username'] . '" bearbeitete Event "' . $performance->getName() . '", ID: ' . $performance->getUid() . '.' . ' Status: ' . $append);
        } else {
            $this->performanceRepository->add($performance);
            $persistenceManager->persistAll();

            if ($lang_en) {
                $performance_en->setSysLanguageUid(1);
                $performance_en->setL10nParent($performance->getUid());
                $this->performanceRepository->add($performance_en);
            }

            $this->logger->notice('Benutzer "' . $user->user['username'] . '" erstellte Event "' . $performance->getName() . '".' . ' Status: ' . $append);
        }
        $persistenceManager->persistAll();

        $spielort = '';
        if ($args['select-spielort'] && is_array($args['select-spielort']) && '' != $args['select-spielort'][0]) {
            $spielort = $this->spielortRepository->findByUid($args['select-spielort'][0]);
        }

        // @all lösche alle spielzeiten die gleiche veranstalter und performance haben --> vllt anders lösen?
        if ($theater->getSpielzeit()) {
            $oldSp = $theater->getSpielzeit();
            foreach ($oldSp as $sp_item) {
                $tmpPerf = $sp_item->getPerformance();
                if (count($tmpPerf) > 0 && $tmpPerf[0]->getUid() == $performance->getUid()) {
                    $theater->removeSpielzeit($sp_item);
                }
            }
        }

        if ($args['timeline']) {
            foreach ($args['timeline'] as $key => $value) {
                if ('' == $value['date']) {
                    continue;
                }

                if ($value['check'] && '1' == $value['check']) {
                    $spielzeit = new Spielzeit;

                    $spielzeit->setDatum(new \DateTime($value['date'] . ' 02:00:00'));

                    if ($value['start-time'] && '' != $value['start-time']) {
                        $spielzeit->setUhrzeit(strtotime($value['start-time']));
                    }

                    if ($value['end-time'] && '' != $value['end-time']) {
                        $spielzeit->setBis(strtotime($value['end-time']));
                    }

                    if ($value['einlass'] && '' != $value['einlass']) {
                        $spielzeit->setEinlass(strtotime($value['einlass']));
                    }

                    if ($value['dauer'] && '' != $value['dauer']) {
                        $spielzeit->setDauer($value['dauer']);
                    }

                    if ($value['status'] && '' != $value['status']) {
                        $spielzeit->setStatus($value['status']);

                        switch ($value['status']) {
                            case 'EventScheduled':
                                break;
                            case 'EventRescheduled':
                                if ($value['rescheduled'] && '' != $value['rescheduled']) {
                                    $spielzeit->setNewdatum(new \DateTime($value['rescheduled']));
                                }
                                break;
                            case 'EventPostponed':
                                if ($value['postponed'] && '' != $value['postponed']) {
                                    $spielzeit->setNewdatum(new \DateTime($value['postponed']));
                                }
                                break;
                            case 'EventCancelled':
                                if ($value['cancelreason'] && '' != $value['cancelreason']) {
                                    $spielzeit->setAusfallinfo($value['cancelreason']);
                                }
                                break;
                        }
                    }

                    if (null != $v_uid) {
                        foreach ($veranstalter as $v_v) {
                            $spielzeit->addVeranstalter($v_v);
                        }
                    }

                    if ('' != $v_xtree) {
                        $spielzeit->setVeranstalterxtree($v_xtree);

                        if ('' != $veranstalterjson) {
                            $spielzeit->setVeranstalterxtreejson($veranstalterjson);
                        }
                    }

                    $spielzeit->addPerformance($performance);

                    $spielzeit->setHidden($savetype);

                    if ($value['ort'] && '' != $value['ort']) {
                        $spielzeit->addSpielort($all_locations[filter_var($value['ort'], FILTER_SANITIZE_NUMBER_INT)]);
                    }

                    if ('' != $spielorturl) {
                        $spielzeit->setSpielorturl($spielorturl);
                    }

                    $this->performanceRepository->add($performance);
                    $persistenceManager->persistAll();

                    $theater->addSpielzeit($spielzeit);
                }
            }
        }

        $this->theaterRepository->update($theater);
        $persistenceManager->persistAll();

        $savetype = intval($args['savetype']);

        if (3 == $savetype) {
            $this->view->assign('id', $performance->getUid());
            $this->view->assign('target', 'edit_performance');
        } else {
            $uri = $this->uriBuilder->setCreateAbsoluteUri(true)->uriFor('build', ['saved' => 'saved'], 'Editor', $this->extensionName, 'editor');
            $this->view->assign('redirect', $uri);
        }
    }

    public function new_ver_type($args)
    {
        if ($args['name']) {
            $user = $GLOBALS['TSFE']->fe_user;

            $persistenceManager = $this->objectManager->get(PersistenceManager::class);

            $var_typ = null;
            $do = null;
            $uid = null;
            if ($this->request->hasArgument('do')) {
                $do = filter_var($this->request->getArgument('do'), FILTER_SANITIZE_STRING);
                switch ($do) {
                    case 'new':
                        $var_typ = new Veranstaltungstype;
                        break;
                    case 'edit':
                        if ($this->request->hasArgument('currentUid')) {
                            $uid = intval($this->request->getArgument('currentUid'));
                            $var_typ = $this->veranstaltungstypeRepository->findByUid($uid);
                        } else {
                            exit('no data selected');
                        }
                        break;
                }
            }

            $var_typ->setName($args['name']);

            if ($args['mrhid']) {
                $var_typ->setMrhid($args['mrhid']);
            } else {
                $var_typ->setMrhid('');
            }

            if ('edit' == $do && null != $uid) {
                $this->veranstaltungstypeRepository->update($var_typ);
            } else {
                $this->veranstaltungstypeRepository->add($var_typ);
            }

            $persistenceManager->persistAll();
        }

        $uri = $this->uriBuilder->setCreateAbsoluteUri(true)->uriFor('build', [], 'Editor', $this->extensionName, 'editor');

        $this->view->assign('redirect', $uri);
    }

    public function new_ver_art($args)
    {
        if ($args['name']) {
            $persistenceManager = $this->objectManager->get(PersistenceManager::class);

            $var_art = null;
            $do = null;
            $uid = null;
            if ($this->request->hasArgument('do')) {
                $do = filter_var($this->request->getArgument('do'), FILTER_SANITIZE_STRING);
                switch ($do) {
                    case 'new':
                        $var_art = new Veranstaltungsart;
                        break;
                    case 'edit':
                        if ($this->request->hasArgument('currentUid')) {
                            $uid = intval($this->request->getArgument('currentUid'));
                            $var_art = $this->veranstaltungsartRepository->findByUid($uid);
                        } else {
                            exit('no data selected');
                        }
                        break;
                }
            }

            $var_art->setName($args['name']);

            if ($args['mrhid']) {
                $var_art->setMrhid($args['mrhid']);
            } else {
                $var_art->setMrhid('');
            }

            if ('edit' == $do && null != $uid) {
                $this->veranstaltungsartRepository->update($var_art);
            } else {
                $this->veranstaltungsartRepository->add($var_art);
            }

            $persistenceManager->persistAll();
        }

        $uri = $this->uriBuilder->setCreateAbsoluteUri(true)->uriFor('build', [], 'Editor', $this->extensionName, 'editor');

        $this->view->assign('redirect', $uri);
    }

    // Geonames API Key ändern
    public function getGeonamesHierarchie($loacationurl = null)
    {
        $hierarchy = [];
        if (null != $loacationurl) {
            $hierarchy = json_decode(file_get_contents("http://api.geonames.org/hierarchyJSON?geonameId=" . $loacationurl . "&username=" . $this->settings['geoname_usr'] . "&lang=de"));
            $hierarchy = array_reverse($hierarchy->geonames);
        }
        return $hierarchy;
    }

    public function getGeonamesAdminUnitLabel($hierarchy = null, $label = 'toponymName', $admincode = "current")
    {
        $return = null;
        if (null != $hierarchy && !empty($hierarchy)) {
            foreach ($hierarchy as $units) {
                if ('current' == $admincode) {
                    $return = $units->$label;
                    break;
                } else {
                    if ($units->fcode == $admincode) {
                        $return = $units->$label;
                        break;
                    }
                }
            }
        }
        return $return;
    }

    public function new_location($args)
    {
        $persistenceManager = $this->objectManager->get(PersistenceManager::class);

        $spielort = null;
        $do = null;
        $uid = null;
        if ($this->request->hasArgument('do')) {
            $do = filter_var($this->request->getArgument('do'), FILTER_SANITIZE_STRING);
            switch ($do) {
                case 'new':
                    $spielort = new Spielort;
                    break;
                case 'edit':
                    if ($this->request->hasArgument('currentUid')) {
                        $uid = intval($this->request->getArgument('currentUid'));
                        $spielort = $this->spielortRepository->findByUid($uid);
                    } else {
                        exit('no data selected');
                    }
                    break;
            }
        }

        if ($args['digicultid']) {
            $spielort->setDigicultid($args['digicultid']);
        } else {
            $spielort->setDigicultid('');
        }

        if ($args['mrhid']) {
            $spielort->setMrhid($args['mrhid']);
        } else {
            $spielort->setMrhid('');
        }

        if ($args['name']) {
            $spielort->setName($args['name']);
        } else {
            $spielort->setName('');
        }

        if ($args['editoranmerkung']) {
            $spielort->setEditoranmerkung($args['editoranmerkung']);
        } else {
            $spielort->setEditoranmerkung('');
        }

        if ($args['raum']) {
            $spielort->setRaum($args['raum']);
        } else {
            $spielort->setRaum('');
        }

        if ($args['website']) {
            $spielort->setWebsite($args['website']);
        } else {
            $spielort->setWebsite('');
        }

        if ($args['name'] && $args['raum']) {
            $spielort->setNamefull($args['name'] . '$' . $args['raum']);
        } else {
            $spielort->setNamefull('');
        }

        if ($args['legalname']) {
            $spielort->setLegalname($args['legalname']);
        } else {
            $spielort->setLegalname('');
        }

        if ($args['alternativname']) {
            $spielort->setAlternativname($args['alternativname']);
        } else {
            $spielort->setAlternativname('');
        }

        if ($args['ort']) {
            $spielort->setOrt($args['ort']);
        } else {
            $spielort->setOrt('');
        }

        if ($args['plz']) {
            $spielort->setPlz($args['plz']);
        } else {
            $spielort->setPlz('');
        }

        if ($args['lat']) {
            $spielort->setLat(str_replace(',', '.', $args['lat']));
        } else {
            $spielort->setLat(null);
        }

        if ($args['lon']) {
            $spielort->setLon(str_replace(',', '.', $args['lon']));
        } else {
            $spielort->setLon(null);
        }

        if ($args['kultur']) {
            $spielort->setKultur(is_array($args['kultur']) ? array_filter($args['kultur']) : $args['kultur']);
        } else {
            $spielort->setKultur('');
        }

        if ($args['strasse']) {
            $spielort->setStrasse($args['strasse']);
        } else {
            $spielort->setStrasse('');
        }

        if ($args['opnv']) {
            $spielort->setOpnv($args['opnv']);
        } else {
            $spielort->setOpnv('');
        }

        if ($args['geonames']) {
            $spielort->setGeonames($args['geonames']);
            $geonames_array = explode("/", $args['geonames']);
            if (!empty($geonames_array)) {
                foreach ($geonames_array as $v) {
                    if (is_int(intval($v)) && intval($v) != 0) {
                        $geonamesid = intval($v);
                        break;
                    }
                }
                $rdf_geonames_hierarchie_api = $this->getGeonamesHierarchie($geonamesid);
                $landkreis = $this->getGeonamesAdminUnitLabel($rdf_geonames_hierarchie_api, 'toponymName', "ADM3");
                $bundesland = $this->getGeonamesAdminUnitLabel($rdf_geonames_hierarchie_api, 'toponymName', "ADM1");
                if ($landkreis) {
                    $spielort->setLandkreis($landkreis);
                }

                if ($bundesland) {
                    $spielort->setBundesland($bundesland);
                }
            }
        } else {
            $spielort->setGeonames('');
        }

        if ($args['ansprechperson']) {
            $spielort->setAnsprechperson($args['ansprechperson']);
        } else {
            $spielort->setAnsprechperson('');
        }

        if ($args['email']) {
            $spielort->setEmail($args['email']);
        } else {
            $spielort->setEmail('');
        }

        if ($args['telefon']) {
            $spielort->setTelefon($args['telefon']);
        } else {
            $spielort->setTelefon('');
        }

        if ($args['mobil']) {
            $spielort->setMobil($args['mobil']);
        } else {
            $spielort->setMobil('');
        }

        $this->loadFile('foto', $spielort, 'getVorschausmall', 'removeVorschausmall', 'addVorschausmall');

        if ('edit' == $do && null != $uid) {
            $this->spielortRepository->update($spielort);

            $this->logger->notice('Benutzer "' . $GLOBALS['TSFE']->fe_user->user['username'] . '" bearbeitete Ort "' . $spielort->getName() . '", ID: ' . $spielort->getUid() . '.');
        } else {
            $this->spielortRepository->add($spielort);

            $this->logger->notice('Benutzer "' . $GLOBALS['TSFE']->fe_user->user['username'] . '" erstellte Ort "' . $spielort->getName() . '".');
        }

        $persistenceManager->persistAll();

        $uri = $this->uriBuilder->setCreateAbsoluteUri(true)->uriFor('build', [], 'Editor', $this->extensionName, 'editor');

        $this->view->assign('redirect', $uri);
    }

    public function new_veranstalter($args)
    {
        $persistenceManager = $this->objectManager->get(PersistenceManager::class);

        $veranstalter = null;
        $do = null;
        $uid = null;
        if ($this->request->hasArgument('do')) {
            $do = filter_var($this->request->getArgument('do'), FILTER_SANITIZE_STRING);
            switch ($do) {
                case 'new':
                    $veranstalter = new Veranstalter;
                    break;
                case 'edit':
                    if ($this->request->hasArgument('currentUid')) {
                        $uid = intval($this->request->getArgument('currentUid'));
                        $veranstalter = $this->veranstalterRepository->findByUid($uid);
                    } else {
                        exit('no data selected');
                    }
                    break;
            }
        }

        if ($args['digicultid']) {
            $veranstalter->setDigicultid($args['digicultid']);
        } else {
            $veranstalter->setDigicultid('');
        }

        if ($args['name']) {
            $veranstalter->setName($args['name']);
        } else {
            $veranstalter->setName('');
        }

        if ($args['ort']) {
            $veranstalter->setOrt($args['ort']);
        } else {
            $veranstalter->setOrt('');
        }

        if ($args['plz']) {
            $veranstalter->setPlz($args['plz']);
        } else {
            $veranstalter->setPlz('');
        }

        if ($args['strasse']) {
            $veranstalter->setStrasse($args['strasse']);
        } else {
            $veranstalter->setStrasse('');
        }

        if ($args['ansprechperson']) {
            $veranstalter->setAnsprechperson($args['ansprechperson']);
        } else {
            $veranstalter->setAnsprechperson('');
        }

        if ($args['email']) {
            $veranstalter->setEmail($args['email']);
        } else {
            $veranstalter->setEmail('');
        }

        if ($args['telefon']) {
            $veranstalter->setTelefon($args['telefon']);
        } else {
            $veranstalter->setTelefon('');
        }

        if ($args['mobil']) {
            $veranstalter->setMobil($args['mobil']);
        } else {
            $veranstalter->setMobil('');
        }

        if ($args['fax']) {
            $veranstalter->setFax($args['fax']);
        } else {
            $veranstalter->setFax('');
        }

        if ($args['website']) {
            $veranstalter->setWebsite($args['website']);
        } else {
            $veranstalter->setWebsite('');
        }

        $this->loadFile('foto', $veranstalter, 'getVorschausmall', 'removeVorschausmall', 'addVorschausmall');

        if ('edit' == $do && null != $uid) {
            $this->veranstalterRepository->update($veranstalter);

            $this->logger->notice('Benutzer "' . $GLOBALS['TSFE']->fe_user->user['username'] . '" bearbeitete Veranstalter "' . $veranstalter->getName() . '", ID: ' . $veranstalter->getUid() . '.');
        } else {
            $this->veranstalterRepository->add($veranstalter);

            $this->logger->notice('Benutzer "' . $GLOBALS['TSFE']->fe_user->user['username'] . '" erstellte Veranstalter "' . $veranstalter->getName() . '".');
        }

        $persistenceManager->persistAll();

        $uri = $this->uriBuilder->setCreateAbsoluteUri(true)->uriFor('build', [], 'Editor', $this->extensionName, 'editor');

        $this->view->assign('redirect', $uri);
    }

    public function loadFile($file_name, &$obj, $getItems, $removeItems, $addItems)
    {
        if ($this->request->hasArgument($file_name)) {
            $persistenceManager = $this->objectManager->get(PersistenceManager::class);

            $image_arr = $this->request->getArgument($file_name);

            if ($image_arr && is_array($image_arr) && !empty($image_arr)) {
                $oldObj = call_user_func([$obj, $getItems]);

                // löschen aller alten Bilder die nicht in bilter array vorkommen
                foreach ($oldObj as $k_img) {
                    $img_uid = $k_img->getUid();

                    $found = false;
                    foreach ($image_arr as $img_k => $img_v) {
                        $image = $img_v;

                        if ($image['oldimg'] && $img_uid == $image['oldimg']) {
                            $found = true;
                        }
                    }

                    if (!$found) {
                        call_user_func([$obj, $removeItems], $k_img);
                    }
                }

                foreach ($image_arr as $img_k => $img_v) {
                    $image = $img_v;
                    if ($image[$file_name] && is_array($image[$file_name]) && !empty($image[$file_name]) && !empty($image[$file_name]['tmp_name']) && '' != $image[$file_name]['tmp_name']) {
                        if ($image['oldimg']) {
                            //remove old img
                            foreach ($oldObj as $k_img) {
                                $img_uid = $k_img->getUid();
                                if ($img_uid == $image['oldimg']) {
                                    call_user_func([$obj, $removeItems], $k_img);
                                    break;
                                }
                            }
                        }

                        $foto = $image[$file_name];

                        $img_name = $foto['name'];
                        $img_path = $foto['tmp_name'];

                        $resourceFactory = ResourceFactory::getInstance();
                        // foto
                        $basicFileFunctions = $this->objectManager->get(BasicFileUtility::class);

                        $folder = $resourceFactory->getFolderObjectFromCombinedIdentifier('fileadmin/user_upload/form_images');
                        $storageRepository = $this->objectManager->get('TYPO3\\CMS\\Core\\Resource\\StorageRepository');

                        $storage = $storageRepository->findByUid('1');

                        $uniqueFileName = $basicFileFunctions->getUniqueName($img_name, GeneralUtility::getFileAbsFileName('fileadmin/user_upload/form_images'));
                        $uniqueFileName = end(explode('/', $uniqueFileName));
                        $movedNewFile = $storage->addFile($img_path, $folder, $uniqueFileName);

                        $persistenceManager->persistAll();

                        $fileReference = $resourceFactory->createFileReferenceObject(
                            [
                                'uid_local' => $movedNewFile->getUid(),
                                'uid_foreign' => uniqid('NEW_'),
                                'uid' => uniqid('NEW_'),
                                'crop' => null,
                            ]
                        );

                        $fileRef = $this->objectManager->get('JO\Theatercollection\Domain\Model\FileReference');

                        $fileRef->setOriginalResource($fileReference);

                        if ($image['title']) {
                            $fileRef->setTitle($image['title']);
                        }

                        $desc = '';
                        if ($image['urheber']) {
                            $desc .= $image['urheber'];
                        }

                        $desc .= '$';
                        if ($image['rechte']) {
                            $desc .= $image['rechte'];
                        }

                        $desc .= '$';
                        if ($image['lizenz']) {
                            $desc .= $image['lizenz'];
                        }

                        $desc .= '$';
                        if ($image['tag']) {
                            $desc .= $image['tag'];
                        }

                        $desc .= '$';
                        if ($image['desc']) {
                            $desc .= $image['desc'];
                        }

                        $desc .= '$';
                        if ($image['quelle']) {
                            $desc .= $image['quelle'];
                        }

                        $fileRef->setDescription($desc);

                        call_user_func([$obj, $addItems], $fileRef);
                    } else if ($image['oldimg']) {
                        $imgObj = call_user_func([$obj, $getItems]);

                        foreach ($imgObj as $img_ok => $img_ov) {
                            $img_uid = $img_ov->getUid();
                            if ($img_uid == $image['oldimg']) {
                                $desc = '';
                                if ($image['urheber']) {
                                    $desc .= $image['urheber'];
                                }

                                $desc .= '$';
                                if ($image['rechte']) {
                                    $desc .= $image['rechte'];
                                }

                                $desc .= '$';
                                if ($image['lizenz']) {
                                    $desc .= $image['lizenz'];
                                }

                                $desc .= '$';
                                if ($image['tag']) {
                                    $desc .= $image['tag'];
                                }

                                $desc .= '$';
                                if ($image['desc']) {
                                    $desc .= $image['desc'];
                                }

                                $desc .= '$';
                                if ($image['quelle']) {
                                    $desc .= $image['quelle'];
                                }

                                $fRef = $this->fileReferenceRepository->findByUid($img_uid);
                                $fRef->setDescription($desc);

                                if ($image['title']) {
                                    $fRef->setTitle($image['title']);
                                }

                                $this->fileReferenceRepository->update($fRef);
                                $persistenceManager->persistAll();
                            }
                        }
                    }
                }
            }
        }

        return $obj;
    }

    public function deleteAction()
    {
        if ($this->request->hasArgument('target')) {
            $target = filter_var($this->request->getArgument('target'), FILTER_SANITIZE_STRING);
            $id = filter_var($this->request->getArgument('id'), FILTER_VALIDATE_INT);

            switch ($target) {
                case 'delete_ver_art':
                    $this->delete_ver_art($id);
                    break;
                case 'delete_ver_type':
                    $this->delete_ver_type($id);
                    break;
                case 'delete_performance':
                    $this->delete_performance($id);
                    break;
                case 'delete_location':
                    $this->delete_location($id);
                    break;
            }
        }
    }

    public function delete_performance($id)
    {
        if ($id) {
            try {
                $persistenceManager = $this->objectManager->get(PersistenceManager::class);
                $demand['by_ids'] = [$id];
                $demand['show_hidden'] = true;
                $event = $this->performanceRepository->searchByJODemand($demand)[0];
                
                $demand['by_performance_ids'] = [$id];
                $demand['start_date'] = new \Datetime('yesterday');
                $spielzeiten = $this->spielzeitRepository->searchMine($demand);

                if ($this->request->hasArgument('cancelled')) {
                    $reason = '';
                    if ($this->request->hasArgument('reason')) {
                        $reason = filter_var($this->request->getArgument('reason'), FILTER_SANITIZE_STRING);
                    }

                    if (count($spielzeiten)) {
                        foreach ($spielzeiten as $value) {
                            $value->setStatus('EventCancelled');
                            if ('' != $reason) $value->setAusfallinfo($reason);
                            $this->spielzeitRepository->update($value);
                        }

                        $persistenceManager->persistAll();
                    }

                    $output['message'] = 'Die Zeiten wurden erfolgreich auf "Cancelled" gestellt.';
                    $this->view->assign('output', $output);
                } else {
                    if (count($spielzeiten)) {
                        $sp_arr = [];
                        foreach ($spielzeiten as $value) {
                            $sp_arr[] = $value->getUid();
                        }

                        $theater = $this->theaterRepository->findByUid(1);
                        $oldSp = $theater->getSpielzeit();
                        foreach ($oldSp as $sp_item) {
                            $oldUid = $sp_item->getUid();
                            if (in_array($oldUid, $sp_arr)) {
                                $theater->removeSpielzeit($sp_item);
                            }
                        }
                        $this->theaterRepository->update($theater);

                        foreach ($spielzeiten as $value) {
                            $this->spielzeitRepository->remove($value);
                        }
                    }

                    $this->logger->notice('Benutzer "' . $GLOBALS['TSFE']->fe_user->user['username'] . '" löschte Event "' . $event->getName() . '". ID: ' . $event->getUid() . '.');

                    $this->performanceRepository->remove($event);

                    $persistenceManager->persistAll();

                    $output['message'] = 'Es wurde erfolgreich gelöscht.';
                    $this->view->assign('output', $output);
                }
            } catch (\Throwable $e) {
                $this->view->assign('error', $e->getMessage());
            }
        }
    }

    public function delete_ver_art($id)
    {
        if ($id) {
            try {
                $persistenceManager = $this->objectManager->get(PersistenceManager::class);
                $var_art = $this->veranstaltungsartRepository->findByUid($id);

                $demand['by_veranstaltungsart_ids'] = [$id];
                $demand['show_hidden'] = true;
                $items = $this->performanceRepository->searchByJODemand($demand);

                if (count($items)) {
                    foreach ($items as $key => $value) {
                        $value->removeArt($var_art);
                        $this->performanceRepository->update($value);
                    }
                }

                $this->veranstaltungsartRepository->remove($var_art);
                $persistenceManager->persistAll();
                $output['message'] = 'Es wurde erfolgreich gelöscht.';
                $this->view->assign('output', $output);
            } catch (\Throwable $e) {
                $this->view->assign('error', $e->getMessage());
            }
        }
    }

    public function delete_ver_type($id)
    {
        if ($id) {
            try {
                $persistenceManager = $this->objectManager->get(PersistenceManager::class);
                $var_type = $this->veranstaltungstypeRepository->findByUid($id);

                $demand['by_veranstaltungstype_ids'] = [$id];
                $demand['show_hidden'] = true;
                $items = $this->performanceRepository->searchByJODemand($demand);

                if (count($items)) {
                    foreach ($items as $key => $value) {
                        $value->removeTyp($var_type);
                        $this->performanceRepository->update($value);
                    }
                }

                $this->veranstaltungstypeRepository->remove($var_type);
                $persistenceManager->persistAll();
                $output['message'] = 'Es wurde erfolgreich gelöscht.';
                $this->view->assign('output', $output);
            } catch (\Throwable $e) {
                $this->view->assign('error', $e->getMessage());
            }
        }
    }

    public function delete_location($id)
    {
        if ($id) {
            try {
                $persistenceManager = $this->objectManager->get(PersistenceManager::class);
                $spl = $this->spielortRepository->findByUid($id);

                $demand['by_spielort_ids'] = [$id];
                $items = $this->spielzeitRepository->searchMine($demand);

                if (count($items)) {
                    foreach ($items as $key => $value) {
                        $value->removeSpielort($spl);
                        $this->spielzeitRepository->update($value);
                    }
                }

                $this->logger->notice('Benutzer "' . $GLOBALS['TSFE']->fe_user->user['username'] . '" löschte Ort "' . $spl->getName() . '". ID: ' . $spl->getUid() . '.');

                $this->spielortRepository->remove($spl);
                $persistenceManager->persistAll();
                $output['message'] = 'Es wurde erfolgreich gelöscht.';
                $this->view->assign('output', $output);
            } catch (\Throwable $e) {
                $this->view->assign('error', $e->getMessage());
            }
        }
    }

    public function searchAction()
    {
        if ($this->request->hasArgument('target')) {
            $target = filter_var($this->request->getArgument('target'), FILTER_SANITIZE_STRING);
            $id = filter_var($this->request->getArgument('id'), FILTER_VALIDATE_INT);

            $output = [];

            switch ($target) {
                case 'search_ver_art':
                    $demand['by_veranstaltungsart_ids'] = [$id];
                    $demand['show_hidden'] = true;
                    $item = $this->performanceRepository->searchByJODemand($demand);
                    $num = count($item);

                    if ($num) {
                        $output['message'] = 'Es wurden ' . count($item) . ' Objekte gefunden, in welchem diese Art benutzt wird. Bitte beachten Sie, dass diese Objekte durch das Löschen beeinflusst werden.';

                        foreach ($item as $key => $value) {
                            $tmp['uid'] = $value->getUid();
                            $tmp['name'] = $value->getName();
                            $output['objects'][] = $tmp;
                        }
                    } else {
                        $output['message'] = 'Es wurde kein Objekt gefunden, in welchem diese Art benutzt wird. Es kann ohne Probleme gelöscht werden.';
                    }

                    break;
                case 'search_ver_type':
                    $demand['by_veranstaltungstype_ids'] = [$id];
                    $demand['show_hidden'] = true;
                    $item = $this->performanceRepository->searchByJODemand($demand);
                    $num = count($item);

                    if ($num) {
                        $output['message'] = 'Es wurden ' . count($item) . ' Objekte gefunden. Bitte beachten Sie, dass diese Obejekte durch das Löschen beeinflusst werden.';

                        foreach ($item as $key => $value) {
                            $tmp['uid'] = $value->getUid();
                            $tmp['name'] = $value->getName();
                            $output['objects'][] = $tmp;
                        }
                    } else {
                        $output['message'] = 'Es wurde kein Objekt gefunden, in welchem diese Art benutzt wird. Es kann ohne Probleme gelöscht werden.';
                    }

                    break;
                case 'search_location':
                    $demand['by_spielort_ids'] = [$id];
                    $item = $this->spielzeitRepository->searchMine($demand);
                    $num = count($item);

                    if ($num) {
                        $output['message'] = 'Es wurden ' . count($item) . ' Objekte gefunden. Bitte beachten Sie, dass diese Obejekte durch das Löschen beeinflusst werden.';

                        foreach ($item as $key => $value) {
                            $tmp['uid'] = $value->getUid();
                            $tmp['name'] = $value->getDatum()->format('d.m.Y');
                            $output['objects'][] = $tmp;
                        }
                    } else {
                        $output['message'] = 'Es wurde kein Objekt gefunden, in welchem diese Art benutzt wird. Es kann ohne Probleme gelöscht werden.';
                    }

                    break;
                case 'search_performance':
                    $demand = [];
                    $demand['by_ids'] = [$id];
                    $demand['show_hidden'] = true;
                    $event = $this->performanceRepository->searchByJODemand($demand);
                    
                    if (count($event)) {
                        $out_str = 'Achtung! Die Veranstaltung und alle zugehörigen Termine werden gelöscht.';
                        $event = $event[0];

                        if ($event->getHidden()) {
                            $out_str = 'Achtung! Alle Termine sind für das Löschen vorbereitet. Die Veranstaltung mit allen Terminen wird gelöscht. Sind Sie sicher, dass dies ausgeführt werden soll?';
                        } else {
                            $demand = [];
                            $demand['show_hidden'] = true;
                            $demand['by_performance_ids'] = [$id];
                            $demand['start_date'] = new \Datetime('yesterday');
                            $spielzeiten = $this->spielzeitRepository->searchMine($demand);
                            
                            if (!empty($spielzeiten)) {
                                $ausfall = true;
                                foreach ($spielzeiten as $k => $v) {
                                    if ('EventCancelled' != $v->getStatus()) {
                                        $ausfall = false;
                                        break;
                                    }
                                }

                                if (!$ausfall) {
                                    $out_str = 'Achtung! Es gibt ein oder mehrere Termine, welche noch nicht Abgeschlossen sind. Diese werden zunächst auf "Cancelled" gesetzt und zum Löschen vorbereitet.';
                                } else {
                                    $out_str = 'Achtung! Alle Termine sind für das Löschen vorbereitet. Die Veranstaltung mit allen Terminen wird gelöscht. Sind Sie sicher, dass dies ausgeführt werden soll?';
                                }
                            }
                        }
                        
                        $output['message'] = $out_str;
                    } else {
                        $output['message'] = 'Etwas ging schief. Kann die Veranstaltung nicht finden. Bitte aktualisieren Sie die Seite und versuchen es erneut.';
                    }

                    break;
            }

            if (!empty($output)) {
                $this->view->assign('output', $output);
            }
        }
    }

    public function copyAction()
    {
        if ($this->request->hasArgument('target')) {
            $target = filter_var($this->request->getArgument('target'), FILTER_SANITIZE_STRING);
            $id = filter_var($this->request->getArgument('id'), FILTER_VALIDATE_INT);

            switch ($target) {
                case 'copy_event':
                    $this->copy_event($id);
                    break;
                case 'copy_location':
                    $this->copy_location($id);
                    break;
            }
        }
    }

    public function copy_location($id)
    {
        if ($id) {
            try {
                $persistenceManager = $this->objectManager->get(PersistenceManager::class);
                $spl = $this->spielortRepository->findByUid($id);

                $newspl = new Spielort;

                // copy all properties and unset UID
                $properties = $spl->_getProperties();
                unset($properties['uid']);

                foreach ($properties as $key => $value) {
                    $newspl->_setProperty($key, $value);
                }

                $newspl->setName($newspl->getName() . ' (Kopie)');

                $this->spielortRepository->add($newspl);
                $persistenceManager->persistAll();

                $this->logger->notice('Benutzer "' . $GLOBALS['TSFE']->fe_user->user['username'] . '" duplizierte Ort "' . $spl->getName() . '". ID: ' . $spl->getUid() . '. Neuer Ort ID: ' . $newspl->getUid() . '.');

                $uri = $this->uriBuilder->setCreateAbsoluteUri(true)->setArguments(['type' => '2343'])->uriFor('loadform', ['target' => 'edit_location', 'eOrt' => $newspl->getUid()], 'Editor', $this->extensionName, 'editor');
                $output['message'] = $uri;
                $this->view->assign('output', $output);
            } catch (\Throwable $e) {
                $this->view->assign('error', $e->getMessage());
            }
        }
    }

    public function copy_event($id)
    {
        if ($id) {
            try {
                $persistenceManager = $this->objectManager->get(PersistenceManager::class);

                $demand['by_ids'] = [$id];
                $demand['show_hidden'] = true;
                $event = $this->performanceRepository->searchByJODemand($demand)[0];

                $newevent = new Performance;

                $properties = $event->_getProperties();
                unset($properties['uid']);
                unset($properties['vorschausmall']);
                unset($properties['vorschaubig']);
                unset($properties['spielort']);
                unset($properties['pdf']);
                $properties['hidden'] = true;

                foreach ($properties as $key => $value) {
                    $newevent->_setProperty($key, $value);
                }

                $newevent->setName($newevent->getName() . ' (Kopie)');

                if ($event->getSpielort() && count($event->getSpielort()) > 0) {
                    foreach ($event->getSpielort() as $key => $value) {
                        $newevent->addSpielort($value);
                    }
                }

                $this->performanceRepository->add($newevent);
                $persistenceManager->persistAll();

                $this->logger->notice('Benutzer "' . $GLOBALS['TSFE']->fe_user->user['username'] . '" duplizierte Event "' . $event->getName() . '". ID: ' . $event->getUid() . '. Neuer Event ID: ' . $newevent->getUid() . '.');

                $uri = $this->uriBuilder->setCreateAbsoluteUri(true)->setArguments(['type' => '2343'])->uriFor('loadform', ['target' => 'edit_performance', 'ep' => $newevent->getUid(), 'es' => $newevent->getUid()], 'Editor', $this->extensionName, 'editor');

                $output['message'] = $uri;
                $this->view->assign('output', $output);
            } catch (\Throwable $e) {
                $this->view->assign('error', $e->getMessage());
            }
        }
    }

    public function kontaktAction()
    {
        $user = $GLOBALS['TSFE']->fe_user->user;

        if ($user && $this->request->hasArgument('betreff') && $this->request->hasArgument('beschreibung')) {
            $betreff = filter_var($this->request->getArgument('betreff'), FILTER_SANITIZE_STRING);
            $beschreibung = filter_var($this->request->getArgument('beschreibung'), FILTER_SANITIZE_STRING);

            $mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
            $mail->from(new \Symfony\Component\Mime\Address($this->settings['kontakt']['from_email'], $this->settings['kontakt']['from_user']));
            $mail->to(
                new \Symfony\Component\Mime\Address($this->settings['kontakt']['to_email'], $this->settings['kontakt']['to_user'])
            );
            $mail->subject('Nachricht an Redaktion: ' . $betreff);
            $mail->html(nl2br($beschreibung) . '<br/><br/> Von ' . $user['username']);
            $sended = $mail->send();

            if ($sended) {
                $output['success'] = 'Ihre Anfrage wurde abgesendet.';
            } else {
                $output['error'] = 'Fehler beim absenden der E-Mail.';
            }
        } else {
            $output['error'] = 'Leider fehlen einige Felder. Bitte Prüfen Sie die Eingaben.';
        }

        $this->view->assign('output', $output);
    }

    /*
     *  Update Object Lock Time or Create new one
     *
     */
    public function lockupdateAction()
    {
        if ($this->request->hasArgument('uid') && $this->request->hasArgument('target')) {
            $user = $GLOBALS['TSFE']->fe_user->user;

            if ($user) {
                $persistenceManager = $this->objectManager->get(PersistenceManager::class);

                $uid = filter_var($this->request->getArgument('uid'), FILTER_SANITIZE_STRING);
                $target = filter_var($this->request->getArgument('target'), FILTER_SANITIZE_STRING);

                $obj_locks = $this->lockRepository->searchIt($uid, $user['username'], $target);
                if (count($obj_locks) > 0) {
                    foreach ($obj_locks as $obj_lock) {
                        $obj_lock->setLastupdate((new \DateTime())->format('Y-m-d H:i'));
                        $this->lockRepository->update($obj_lock);
                    }
                } else {
                    $lock = new Lock;
                    $lock->setObjid($uid);
                    $lock->setObjidtyp($target);
                    $lock->setUser($user['username']);
                    $lock->setStarttime((new \DateTime())->format('Y-m-d H:i'));
                    $lock->setLastupdate((new \DateTime())->format('Y-m-d H:i'));
                    $this->lockRepository->add($lock);
                }

                $persistenceManager->persistAll();
            }
        }
    }
}
