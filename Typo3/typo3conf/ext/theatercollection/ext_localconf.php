<?php
defined('TYPO3_MODE') || die();

/**
 *  JO Theater
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'JO.Theatercollection',
    'theater',
    ['Theater' => 'api'],
    ['Theater' => 'api']
);

/**
 *  JO Editor
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'JO.Theatercollection',
    'editor',
    ['Editor' => 'build, loadform, formsubmit, delete, search, copy, kontakt, lockupdate'],
    ['Editor' => 'build, loadform, formsubmit, delete, search, copy, kontakt, lockupdate']
);

/**
 *  JO Editor
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'JO.Theatercollection',
    'portal',
    ['Customer' => 'read'],
    ['Customer' => 'read']
);

/**
 *  JO User
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'JO.Theatercollection',
    'user',
    ['User' => 'edit, termupdate'],
    ['User' => 'edit, termupdate']
);


$extbaseObjectContainer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class);
$extbaseObjectContainer->registerImplementation(
    \In2code\Femanager\Controller\NewController::class,
    \JO\Theatercollection\Controller\FemanagernewController::class
);
$extbaseObjectContainer->registerImplementation(
    \In2code\Femanager\Controller\EditController::class,
    \JO\Theatercollection\Controller\FemanagereditController::class
);
$extbaseObjectContainer->registerImplementation(
    \In2code\Femanager\Controller\UserController::class,
    \JO\Theatercollection\Controller\FemanageruserController::class
);
$extbaseObjectContainer->registerImplementation(
    \In2code\Femanager\Controller\InvitationController::class,
    \JO\Theatercollection\Controller\FemanagerinvitationController::class
);

/*
$GLOBALS['TYPO3_CONF_VARS']['LOG']['Tx']['Theatercollection']['writerConfiguration'] = [
    \TYPO3\CMS\Core\Log\LogLevel::INFO => [
        \TYPO3\CMS\Core\Log\Writer\SyslogWriter::class => [
            'logFile' => \TYPO3\CMS\Core\Core\Environment::getVarPath() . '/log/joTest.log'
        ]
    ],
    \TYPO3\CMS\Core\Log\LogLevel::WARNING => [
        \TYPO3\CMS\Core\Log\Writer\SyslogWriter::class => [
            'logFile' => \TYPO3\CMS\Core\Core\Environment::getVarPath() . '/log/joTest.log'
        ]
    ],
];
*/


/*
$GLOBALS['TYPO3_CONF_VARS']['LOG']['JO']['Theatercollection']['Controller']['EditorController']['writerConfiguration'] = [
    \TYPO3\CMS\Core\Log\LogLevel::ERROR => [
        \TYPO3\CMS\Core\Log\Writer\FileWriter::class => [
            'logFile' => \TYPO3\CMS\Core\Core\Environment::getVarPath() . '/log/editor_user.log'
        ]
    ],
    \TYPO3\CMS\Core\Log\LogLevel::INFO => [
        \TYPO3\CMS\Core\Log\Writer\FileWriter::class => [
            'logFile' => \TYPO3\CMS\Core\Core\Environment::getVarPath() . '/log/editor_user.log'
        ]
    ]
];

$GLOBALS['TYPO3_CONF_VARS']['LOG']['JO']['Theatercollection']['Controller']['TheaterController']['writerConfiguration'] = [
    \TYPO3\CMS\Core\Log\LogLevel::ERROR => [
        \TYPO3\CMS\Core\Log\Writer\FileWriter::class => [
            'logFile' => \TYPO3\CMS\Core\Core\Environment::getVarPath() . '/log/api_output.log'
        ]
    ],
    \TYPO3\CMS\Core\Log\LogLevel::INFO => [
        \TYPO3\CMS\Core\Log\Writer\FileWriter::class => [
            'logFile' => \TYPO3\CMS\Core\Core\Environment::getVarPath() . '/log/api_output.log'
        ]
    ]
];
*/


$GLOBALS['TYPO3_CONF_VARS']['LOG']['JO']['Theatercollection']['Controller']['writerConfiguration'] = [
    \TYPO3\CMS\Core\Log\LogLevel::INFO => [
        \TYPO3\CMS\Core\Log\Writer\DatabaseWriter::class => [
            'logTable' => 'tx_theatercollection_log'
        ],
    ]
];