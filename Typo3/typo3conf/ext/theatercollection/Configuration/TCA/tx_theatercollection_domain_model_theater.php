<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'name,telefon,mobil,fax,email,ansprechperson,beschreibung,langbeschreibung',
        'iconfile' => 'EXT:theatercollection/Resources/Public/Icons/tx_theatercollection_domain_model_theater.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, telefon, mobil, fax, email, ansprechperson, beschreibung, langbeschreibung, spielzeit, urls, metas, vorschausmall, vorschaubig',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, telefon, mobil, fax, email, ansprechperson, beschreibung, langbeschreibung, spielzeit, urls, metas, vorschausmall, vorschaubig, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_theatercollection_domain_model_theater',
                'foreign_table_where' => 'AND {#tx_theatercollection_domain_model_theater}.{#pid}=###CURRENT_PID### AND {#tx_theatercollection_domain_model_theater}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
            ]
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'telefon' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.telefon',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'mobil' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.mobil',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'fax' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.fax',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'ansprechperson' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.ansprechperson',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'beschreibung' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.beschreibung',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => true
            ]
        ],
        'langbeschreibung' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.langbeschreibung',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => true
            ]
        ],
        'spielzeit' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.spielzeit',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theatercollection_domain_model_spielzeit',
                'foreign_field' => 'theater',
                'maxitems' => 9999,
                'appearance' => [
                    'collapseAll' => 1,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
        'urls' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.urls',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theatercollection_domain_model_url',
                'foreign_field' => 'parentid',
                'foreign_table_field' => 'parenttable',
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 1,
                ],
            ],
        ],
        'metas' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_theater.metas',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theatercollection_domain_model_data',
                'foreign_field' => 'parentid',
                'foreign_table_field' => 'parenttable',
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 1,
                ],
            ],
        ],
        'vorschausmall' => [
            'label' => 'Vorschau klein',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('vorschausmall', [
                'maxitems' => 1,
            ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
        ],
        'vorschaubig' => [
            'label' => 'Vorschau groß',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('vorschaubig', [
                'maxitems' => 1,
            ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
        ],
    ],
];
