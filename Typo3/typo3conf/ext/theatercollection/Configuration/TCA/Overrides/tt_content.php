<?php
defined('TYPO3_MODE') || die();

use TYPO3\CMS\Extbase\Utility\ExtensionUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func(function() {
    /**
     *  JO Theater
     */
    ExtensionUtility::registerPlugin(
        'JO.Theatercollection',
        'theater',
        'JO - Theatercollection'
    );
    $TCA['tt_content']['types']['list']['subtypes_addlist']['theatercollection_theater'] = 'pi_flexform';
    ExtensionManagementUtility::addPiFlexFormValue(
        'theatercollection_theater',
        'FILE:EXT:theatercollection/Configuration/FlexForms/theater.xml'
    );

    /**
     *  JO Editor
     */
    ExtensionUtility::registerPlugin(
        'JO.Theatercollection',
        'editor',
        'JO - Editor'
    );
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['theatercollection_editor'] = 'pi_flexform';
    ExtensionManagementUtility::addPiFlexFormValue(
        'theatercollection_editor',
        'FILE:EXT:theatercollection/Configuration/FlexForms/editor.xml'
    );

     /**
     *  JO Customer
     */
    ExtensionUtility::registerPlugin(
        'JO.Theatercollection',
        'portal',
        'JO - Portal'
    );
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['theatercollection_portal'] = 'pi_flexform';
    ExtensionManagementUtility::addPiFlexFormValue(
        'theatercollection_portal',
        'FILE:EXT:theatercollection/Configuration/FlexForms/editor.xml'
    );

    /**
     *  JO User
     */
    ExtensionUtility::registerPlugin(
        'JO.Theatercollection',
        'user',
        'JO - User'
    );
    $TCA['tt_content']['types']['list']['subtypes_addlist']['theatercollection_user'] = 'pi_flexform';
    ExtensionManagementUtility::addPiFlexFormValue(
        'theatercollection_user',
        'FILE:EXT:theatercollection/Configuration/FlexForms/user.xml'
    );

});
