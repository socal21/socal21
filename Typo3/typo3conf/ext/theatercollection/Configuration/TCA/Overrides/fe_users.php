<?php
defined('TYPO3') or die();

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

// Add some fields to fe_users table to show TCA fields definitions
ExtensionManagementUtility::addTCAcolumns('fe_users',
   [
   'veranstalter' => [
         'exclude' => true,
         'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.veranstalter',
         'config' => [
             'type' => 'select',
             'renderType' => 'selectMultipleSideBySide',
             'foreign_table' => 'tx_theatercollection_domain_model_veranstalter',
             'size' => 10,
             'autoSizeMax' => 30,
             'maxitems' => 1,
             'multiple' => 0,
             'fieldControl' => [
                 'editPopup' => [
                     'disabled' => false,
                 ],
                 'addRecord' => [
                     'disabled' => false,
                 ],
                 'listModule' => [
                     'disabled' => true,
                 ],
             ],
         ],
     ],
     'veranstalterxtree' => [
        'exclude' => true,
        'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.veranstalterxtree',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingleBox',
            'selectedListStyle' => 'width: 1000px;',
            'itemListStyle' => 'width: 1000px;',
            "size" => 10,
            "minitems" => 0,
            "maxitems" => 1,
            "itemsProcFunc" => "JO\Theatercollection\Controller\TheaterController->getVeranstalterXtree",
            "itemsProcConfig" => [
                "format" => "tca"
            ],
        ],
     ],
   ]
);
ExtensionManagementUtility::addToAllTCAtypes(
   'fe_users',
   'veranstalter,veranstalterxtree'
);