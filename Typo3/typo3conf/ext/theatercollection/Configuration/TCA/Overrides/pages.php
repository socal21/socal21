<?php
defined('TYPO3') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'theatercollection',
    'Configuration/TSconfig/page.tsconfig',
    'Theater Page Config'
);
