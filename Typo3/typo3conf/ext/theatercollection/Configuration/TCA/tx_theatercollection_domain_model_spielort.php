<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'digicultid,name,raum,legalname,alternativname,ort,plz,strasse,opnv,ansprechperson,email,telefon,mobil,mrhid,geonames,editoranmerkung',
        'iconfile' => 'EXT:theatercollection/Resources/Public/Icons/tx_theatercollection_domain_model_spielort.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, digicultid, name, raum, namefull, legalname, alternativname, ort, plz, strasse, opnv, lat, lon, ansprechperson, email, telefon, mobil, urls, metas, vorschausmall, vorschaubig, mrhid, geonames, website, editoranmerkung',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, digicultid, name, raum, namefull, legalname, alternativname, ort, plz, kultur, strasse, opnv, lat, lon, ansprechperson, email, telefon, mobil, urls, metas, vorschausmall, vorschaubig, mrhid, geonames, website, editoranmerkung, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_theatercollection_domain_model_spielort',
                'foreign_table_where' => 'AND {#tx_theatercollection_domain_model_spielort}.{#pid}=###CURRENT_PID### AND {#tx_theatercollection_domain_model_spielort}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
            ]
        ],
        'crdate' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'kultur' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_performance.kultur',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'digicultid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.digicultid',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'raum' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.raum',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'namefull' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.namefull',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'legalname' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.legalname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'alternativname' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.alternativname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'ort' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.ort',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'plz' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.plz',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'strasse' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.strasse',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'landkreis' => [
            'exclude' => true,
            'label' => 'Landkreis',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'bundesland' => [
            'exclude' => true,
            'label' => 'Bundesland',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'opnv' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.opnv',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'lat' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.lat',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'lon' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.lon',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'ansprechperson' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.ansprechperson',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'telefon' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.telefon',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'mobil' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.mobil',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'website' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.website',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'urls' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.urls',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theatercollection_domain_model_url',
                'foreign_field' => 'parentid',
                'foreign_table_field' => 'parenttable',
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 1,
                ],
            ],
        ],
        'metas' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.metas',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theatercollection_domain_model_data',
                'foreign_field' => 'parentid',
                'foreign_table_field' => 'parenttable',
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 1,
                ],
            ],
        ],
        'vorschausmall' => [
            'label' => 'Vorschau klein',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('vorschausmall', [
                'maxitems' => 1,
            ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
        ],
        'vorschaubig' => [
            'label' => 'Vorschau groß',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('vorschaubig', [
                'maxitems' => 1,
            ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
        ],
        'mrhid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.mrhid',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'geonames' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.geonames',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'editoranmerkung' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielort.editoranmerkung',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => true
            ]
        ],
    ],
];
