<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'digicultid, name,ort,plz,strasse,telefon,mobil,fax,email,ansprechperson,vorbestellungtel,vorbestellungemail',
        'iconfile' => 'EXT:theatercollection/Resources/Public/Icons/tx_theatercollection_domain_model_veranstalter.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, digicultid, name, ort, plz, strasse, telefon, mobil, fax, email, ansprechperson, vorbestellungtel, vorbestellungemail, website, urls, metas, vorschausmall, vorschaubig',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, digicultid, name, ort, plz, strasse, telefon, mobil, fax, email, ansprechperson, vorbestellungtel, vorbestellungemail, website, urls, metas, vorschausmall, vorschaubig, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_theatercollection_domain_model_veranstalter',
                'foreign_table_where' => 'AND {#tx_theatercollection_domain_model_veranstalter}.{#pid}=###CURRENT_PID### AND {#tx_theatercollection_domain_model_veranstalter}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
            ]
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'digicultid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.digicultid',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'ort' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.ort',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'plz' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.plz',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'strasse' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.strasse',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'telefon' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.telefon',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'mobil' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.mobil',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'fax' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.fax',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'ansprechperson' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.ansprechperson',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'vorbestellungtel' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.vorbestellungtel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'vorbestellungemail' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.vorbestellungemail',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'website' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.website',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'editoranmerkung' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.editoranmerkung',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => true
            ]
        ],
        'urls' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.urls',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theatercollection_domain_model_url',
                'foreign_field' => 'parentid',
                'foreign_table_field' => 'parenttable',
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 1,
                ],
            ],
        ],
        'metas' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_veranstalter.metas',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theatercollection_domain_model_data',
                'foreign_field' => 'parentid',
                'foreign_table_field' => 'parenttable',
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 1,
                ],
            ],
        ],
        'vorschausmall' => [
            'label' => 'Vorschau klein',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('vorschausmall', [
                'maxitems' => 1,
            ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
        ],
        'vorschaubig' => [
            'label' => 'Vorschau groß',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('vorschaubig', [
                'maxitems' => 1,
            ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
        ],
    
    ],
];
