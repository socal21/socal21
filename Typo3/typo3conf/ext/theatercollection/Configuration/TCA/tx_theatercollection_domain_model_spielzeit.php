<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit',
        'label' => 'datum',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'default_sortby' => 'ORDER BY datum ASC',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => '',
        'iconfile' => 'EXT:theatercollection/Resources/Public/Icons/tx_theatercollection_domain_model_spielzeit.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, datum, uhrzeit, bis, spielort, spielorturl, veranstalter, veranstalterxtree, veranstalterxtreejson, performance, ausfall, ausfallinfo, newdatum, urls, metas, vorschausmall, vorschaubig',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, booking, status, datum, uhrzeit, dauer, einlass, bis, spielort, spielorturl, veranstalter, veranstalterxtree, veranstalterxtreejson, performance, ausfall, ausfallinfo, newdatumm, urls, metas, vorschausmall, vorschaubig, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_theatercollection_domain_model_spielzeit',
                'foreign_table_where' => 'AND {#tx_theatercollection_domain_model_spielzeit}.{#pid}=###CURRENT_PID### AND {#tx_theatercollection_domain_model_spielzeit}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
            ]
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'booking' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.booking',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'status' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.status',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['EventScheduled', 'EventScheduled'],
                    ['EventRescheduled', 'EventRescheduled'],
                    ['EventCancelled', 'EventCancelled'],
                    ['EventPostponed', 'EventPostponed'],
                ],
            ],
        ],
        'datum' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.datum',
            'config' => [
                'dbType' => 'date',
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date',
                'default' => null,
            ],
        ],
        'uhrzeit' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.uhrzeit',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 4,
                'eval' => 'time',
                'default' => time()
            ]
        ],
        'einlass' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.einlass',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 4,
                'eval' => 'time',
                'default' => time()
            ]
        ],
        'bis' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.bis',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 4,
                'eval' => 'time',
                'default' => time()
            ]
        ],
        'dauer' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.dauer',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'spielort' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.spielort',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_theatercollection_domain_model_spielort',
                'default' => 0,
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 10,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
        ],
        'spielorturl' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.spielorturl',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'veranstalter' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.veranstalter',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_theatercollection_domain_model_veranstalter',
                'MM' => 'tx_theatercollection_spielzeit_veranstalter_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
        ],
        'veranstalterxtree' => [
            'label' => 'Veranstalter Xtree ID',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'veranstalterxtreejson' => [
            'label' => 'Veranstalter Xtree JSON',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ],
        ],
        'performance' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.performance',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_theatercollection_domain_model_performance'
            ],
        ],
        'ausfall' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.ausfall',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => ''
                    ]
                ],
            ],
        ],
        'ausfallinfo' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.ausfallinfo',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'newdatum' => [
            'exclude' => true,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.newdatum',
            'config' => [
                'dbType' => 'date',
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date',
                'default' => null,
            ],
        ],
        'urls' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.urls',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theatercollection_domain_model_url',
                'foreign_field' => 'parentid',
                'foreign_table_field' => 'parenttable',
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 1,
                ],
            ],
        ],
        'metas' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:theatercollection/Resources/Private/Language/locallang_db.xlf:tx_theatercollection_domain_model_spielzeit.metas',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_theatercollection_domain_model_data',
                'foreign_field' => 'parentid',
                'foreign_table_field' => 'parenttable',
                'maxitems' => 99,
                'appearance' => [
                    'collapseAll' => 1,
                ],
            ],
        ],
        'vorschausmall' => [
            'label' => 'Vorschau klein',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('vorschausmall', [
                'maxitems' => 1,
            ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
        ],
        'vorschaubig' => [
            'label' => 'Vorschau groß',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('vorschaubig', [
                'maxitems' => 1,
            ], $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
        ],
        'theater' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];
