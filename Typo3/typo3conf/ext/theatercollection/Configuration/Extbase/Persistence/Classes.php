<?php
declare (strict_types = 1);

return [
    \JO\Theatercollection\Domain\Model\FileReference::class => [
        'tableName' => 'sys_file_reference',
        'properties' => [
            'uid_local' => [
                'fieldName' => 'originalFileIdentifier',
            ],
        ],
    ],
    \In2code\Femanager\Domain\Model\User::class => [
        'subclasses' => [
            \JO\Theatercollection\Domain\Model\User::class
        ]
    ],
    \JO\Theatercollection\Domain\Model\User::class => [
        'tableName' => 'fe_users',
        'recordType' => 0,
    ]
];
