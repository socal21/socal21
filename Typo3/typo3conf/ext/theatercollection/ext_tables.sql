#
# Table structure for table 'tx_theatercollection_domain_model_performance'
#
CREATE TABLE tx_theatercollection_domain_model_performance (

	name varchar(255) DEFAULT '' NOT NULL,
	beschreibung text,
	langbeschreibung text,
	editoranmerkung text,
	info text,
	autor varchar(255) DEFAULT '' NOT NULL,
	infouri varchar(255) DEFAULT '' NOT NULL,
	saisonstart date DEFAULT NULL,
	saisonend date DEFAULT NULL,
	altersfreigabe varchar(255) DEFAULT '' NOT NULL,
	art varchar(255) DEFAULT '' NOT NULL,
	artxtree text NOT NULL,
	artprimary varchar(255) DEFAULT '' NOT NULL,
	typ varchar(255) DEFAULT '' NOT NULL,
	typxtree text NOT NULL,
	typprimary varchar(255) DEFAULT '' NOT NULL,
	eventtyp text NOT NULL,
	sozartxtree text NOT NULL,
	suchkat text NOT NULL,
	mappedkat text NOT NULL,
	booking varchar(255) DEFAULT '' NOT NULL,
	bookingstart varchar(255) DEFAULT '' NOT NULL,
	veranstalter int(11) unsigned DEFAULT '0' NOT NULL,
	veranstalterxtree varchar(255) DEFAULT '' NOT NULL,
	veranstalterxtreejson text NOT NULL,
	ausfallenvon date DEFAULT NULL,
	ausfallenbis date DEFAULT NULL,
	ausfallinfo text NOT NULL,
	maxattendee varchar(255) DEFAULT '' NOT NULL,
	maxattendeephy varchar(255) DEFAULT '' NOT NULL,
	maxattendeevirt varchar(255) DEFAULT '' NOT NULL,
	vorschausmall int(11) unsigned DEFAULT '0',
	vorschaubig int(11) unsigned DEFAULT '0',
	imgdefault text NOT NULL,
	pdf int(11) unsigned DEFAULT '0',
	urls int(11) DEFAULT '0',
	metas int(11) DEFAULT '0',
	spielort text NOT NULL,
	spielorturl varchar(255) DEFAULT '' NOT NULL
);

#
# Table structure for table 'tx_theatercollection_domain_model_spielort'
#
CREATE TABLE tx_theatercollection_domain_model_spielort (

	digicultid varchar(255) DEFAULT '' NOT NULL,
	name varchar(255) DEFAULT '' NOT NULL,
	editoranmerkung text,
	raum varchar(255) DEFAULT '' NOT NULL,
	namefull varchar(255) DEFAULT '' NOT NULL,
	legalname varchar(255) DEFAULT '' NOT NULL,
	alternativname varchar(255) DEFAULT '' NOT NULL,
	ort varchar(255) DEFAULT '' NOT NULL,
	plz varchar(5) DEFAULT '' NOT NULL,
	strasse varchar(255) DEFAULT '' NOT NULL,
	opnv text NOT NULL,
	ansprechperson varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	kultur varchar(255) DEFAULT '' NOT NULL,
	website varchar(255) DEFAULT '' NOT NULL,
	lon float,
	lat float,
	telefon varchar(255) DEFAULT '' NOT NULL,
	landkreis varchar(255) DEFAULT '' NOT NULL,
	bundesland varchar(255) DEFAULT '' NOT NULL,
	mobil varchar(255) DEFAULT '' NOT NULL,
	vorschausmall int(11) unsigned DEFAULT '0',
	vorschaubig int(11) unsigned DEFAULT '0',
	urls int(11) DEFAULT '0',
	metas int(11) DEFAULT '0',
	mrhid varchar(255) DEFAULT '' NOT NULL,
	geonames varchar(255) DEFAULT '' NOT NULL
);

#
# Table structure for table 'tx_theatercollection_domain_model_spielzeit'
#
CREATE TABLE tx_theatercollection_domain_model_spielzeit (

	theater int(11) unsigned DEFAULT '0' NOT NULL,

	datum date DEFAULT NULL,
	uhrzeit int(11) DEFAULT '0' NOT NULL,
	bis int(11) DEFAULT '0' NOT NULL,
	einlass int(11) DEFAULT '0' NOT NULL,
	dauer varchar(255) DEFAULT '' NOT NULL,
	spielort text NOT NULL,
	spielorturl varchar(255) DEFAULT '' NOT NULL,
	veranstalter int(11) unsigned DEFAULT '0' NOT NULL,
	veranstalterxtree varchar(255) DEFAULT '' NOT NULL,
	veranstalterxtreejson text NOT NULL,
	performance text NOT NULL,
	booking varchar(255) DEFAULT '' NOT NULL,
	status varchar(255) DEFAULT '' NOT NULL,
	newdatum date DEFAULT NULL,
	ausfall tinyint(4) unsigned DEFAULT '0' NOT NULL,
	ausfallinfo text NOT NULL,
	vorschausmall int(11) unsigned DEFAULT '0',
	vorschaubig int(11) unsigned DEFAULT '0',
	urls int(11) DEFAULT '0',
	metas int(11) DEFAULT '0'
);

#
# Table structure for table 'tx_theatercollection_domain_model_veranstalter'
#
CREATE TABLE tx_theatercollection_domain_model_veranstalter (

	digicultid varchar(255) DEFAULT '' NOT NULL,
	name varchar(255) DEFAULT '' NOT NULL,
	ort varchar(255) DEFAULT '' NOT NULL,
	plz varchar(5) DEFAULT '' NOT NULL,
	strasse varchar(255) DEFAULT '' NOT NULL,
	telefon varchar(255) DEFAULT '' NOT NULL,
	mobil varchar(255) DEFAULT '' NOT NULL,
	fax varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	website varchar(255) DEFAULT '' NOT NULL,
	ansprechperson varchar(255) DEFAULT '' NOT NULL,
	vorbestellungtel varchar(255) DEFAULT '' NOT NULL,
	vorbestellungemail varchar(255) DEFAULT '' NOT NULL,
	vorschausmall int(11) unsigned DEFAULT '0',
	vorschaubig int(11) unsigned DEFAULT '0',
	urls int(11) DEFAULT '0',
	metas int(11) DEFAULT '0'
);

#
# Table structure for table 'tx_theatercollection_domain_model_theater'
#
CREATE TABLE tx_theatercollection_domain_model_theater (

	name varchar(255) DEFAULT '' NOT NULL,
	telefon varchar(255) DEFAULT '' NOT NULL,
	mobil varchar(255) DEFAULT '' NOT NULL,
	fax varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	ansprechperson varchar(255) DEFAULT '' NOT NULL,
	beschreibung text,
	langbeschreibung text,
	spielzeit int(11) unsigned DEFAULT '0' NOT NULL,
	vorschausmall int(11) unsigned DEFAULT '0',
	vorschaubig int(11) unsigned DEFAULT '0',
	urls int(11) DEFAULT '0',
	metas int(11) DEFAULT '0'
);

#
# Table structure for table 'tx_theatercollection_domain_model_url'
#
CREATE TABLE tx_theatercollection_domain_model_url (

	fieldid varchar(255) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	url text NOT NULL,

	parentid int(11) DEFAULT '0' NOT NULL,
	parenttable varchar(255) DEFAULT '' NOT NULL
);

#
# Table structure for table 'tx_theatercollection_domain_model_data'
#
CREATE TABLE tx_theatercollection_domain_model_data (

	fieldid varchar(255) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	value text NOT NULL,

	parentid int(11) DEFAULT '0' NOT NULL,
	parenttable varchar(255) DEFAULT '' NOT NULL
);


#
# Table structure for table 'tx_theatercollection_spielzeit_veranstalter_mm'
#
CREATE TABLE tx_theatercollection_spielzeit_veranstalter_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_theatercollection_performance_veranstalter_mm'
#
CREATE TABLE tx_theatercollection_performance_veranstalter_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_theatercollection_domain_model_veranstaltungsart'
#
CREATE TABLE tx_theatercollection_domain_model_veranstaltungsart (
	name varchar(255) DEFAULT '' NOT NULL,
	mrhid varchar(255) DEFAULT '' NOT NULL
);

#
# Table structure for table 'tx_theatercollection_domain_model_veranstaltungsart'
#
CREATE TABLE tx_theatercollection_domain_model_veranstaltungstype (
	name varchar(255) DEFAULT '' NOT NULL,
	jsonld varchar(255) DEFAULT '' NOT NULL,
	mrhid varchar(255) DEFAULT '' NOT NULL
);

#
# Table structure for table 'fe_users' expand
#
CREATE TABLE fe_users (
    veranstalter int(11) DEFAULT '0' NOT NULL,
    veranstalterxtree varchar(255) DEFAULT '' NOT NULL
);

#
# Table structure for table 'tx_theatercollection_log'
#
CREATE TABLE tx_theatercollection_log (
    request_id varchar(13) DEFAULT '' NOT NULL,
    time_micro double(16,4) NOT NULL default '0.0000',
    component varchar(255) DEFAULT '' NOT NULL,
    level tinyint(1) unsigned DEFAULT '0' NOT NULL,
    message text,
    data text,

    KEY request (request_id)
);

#
# Table structure for table 'tx_theatercollection_domain_model_lock'
#
CREATE TABLE tx_theatercollection_domain_model_lock (
    objid varchar(255) DEFAULT '' NOT NULL,
    objidtyp varchar(255) DEFAULT '' NOT NULL,
    user varchar(255) DEFAULT '' NOT NULL,
    starttime varchar(255) DEFAULT '' NOT NULL,
    lastupdate varchar(255) DEFAULT '' NOT NULL
);