<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Theater Collection',
    'description' => 'Aufführungsstücke sammeln und per API ausgeben.',
    'category' => 'plugin',
    'author' => 'Alexander Miller',
    'author_email' => 'info@justorange.org',
    'state' => 'alpha',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-10.9.99',
            'femanager' => '6.3.0-6.9.99',
            'logs' => '3.2.0-3.9.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'JO\\Theatercollection\\' => 'Classes',
        ],
    ],
];
