plugin.tx_theatercollection.settings {
	editors {
		new_event {
			settings {
				form-name = new_event
				do = new
				label = Veranstaltung erstellen oder ändern
				// hier können weitere Savebuttons konfiguriert werden - initial wird ein Savebutton angezeigt
				save {
					savebetween = 1
					savenotpublish = 1
				}
			}

			pages {
				page1 {
					label = Basisdaten
					icon = icon-1
					items {
						uid {
							type = input
							label = Lokale ID der Veranstaltung
							disable = disabled
							placeholder = ID der Veranstaltung (wird vom System vergeben)
							popover = Something.
						}
						veranstalter {
							type = input
							label = Veranstalter:in *
							placeholder = Name der Veranstalter:in - nicht änderbar
							popover = Something.
							#container-id = testveranstalter
							#container-class = testveranstalter
							disable = disabled
							loadValue = veranstalter
						}

						event-name {
							type = input
							label = Titel der Veranstaltung *
							placeholder = Titel der Veranstaltung
							popover = Something.
							maxlength = 150
						}

						event-desc {
							type = textarea
							label = Beschreibung der Veranstaltung *(muss pflichtfeld)
							popover = Something.
						}
						/*
						event-desc-long {
							type = textarea
							label = Beschreibung der Veranstaltung (Langbeschreibung)
							popover = Something.
						}
						
						infos {
							type = textarea
							label = Zusätzliche Informationen(Kooperationen, Verweise auf andere Veranstaltungen, etc.)
							popover = Something.
						}
						*/
						eventurl {
							type = textarea
							label = Info-Website(s) zur Veranstaltung
							placeholder = https://www.veranstaltung-info.de
							popover = Something to write
						}

						booking {
							type = input
							label = URL zum Ticketvorverkauf
							popover = Something.
						}
						infos {
							type = textarea
							label = Weitere Informationen zu Ticketpreisen etc.
							popover = Something.
						}

					}
				}

				page2 {
					icon = icon-2
					label = Verortung
					items {
						select-spielort {
							type = select
							label = Veranstaltungsort
							popover = Something.
							container-id = sp-select
							defaultoption = Kein Ort ausgewählt
							loadOption = spielorte
						}

						spielorturl {
							type = input
							label = URL für Livestream
							popover = Something.
						}

						/*
						fixed-text2 {
							type = fixed-text
							text = Ihre Veranstaltung findet an mehreren Orten statt? Wählen Sie entsprechend einen weiteren Ort aus:
						}

						copy-field {
							type = copy-field
							btn-text = Einen weiteren Ort hinzufügen

							target = #sp-select
						}
						*/
						/*
						fixed-text3 {
							type = fixed-text
							text = Der Veranstaltungsort ist nicht in dem Dropdown zu finden? <br /> Legen Sie ihn einfach an. Sie können die Veranstaltung zwischenspeichern und später vervollständigen:
						}
						load-form {
							type = load-form

							btn-text = Veranstaltungsort hinzufügen
							btn-icon = blob

							form-name = new_location
						}
						*/
					}
				}

				page3 {
					icon = icon-3
					label = Klassifikation
					items {
						fixed-text1 {
							type = fixed-text
							text = Bitte klassifizieren Sie Ihre Veranstaltung. Insofern die Veranstaltung mehreren Kategorien zugewiesen werden soll oder mehrere Merkmale enthält, halten Sie bitte die Command/Strg-Taste gedrückt, während Sie Ihre Auswahl treffen.
						}

						veranstaltungsTypeXtree {
							type = select
							label = Kategorie
							popover = Something.
							special = multiselect
							container-id = vt-select

							#defaultoption = Keine Kategorie ausgewählt

							loadOption = veranstaltungsTypeXtree

							/* activates primary select + set its label*/
							primary = bevorzugte
						}
						/*
						fixed-text2 {
							type = fixed-text
							text = Ihre Veranstaltung ist mehreren Typen zuordenbar? Bitte wählen Sie weitere Typen aus:
						}

						copy-field1 {
							type = copy-field
							btn-text = Einen weiteren Typ hinzufügen

							target = #vt-select
						}
						*/
						veranstaltungsArtXtree {
							type = select
							label = Merkmale
							popover = Something.
							special = multiselect
							container-id = va-select

							#defaultoption = Kein Merkmal ausgewählt

							loadOption = veranstaltungsArtXtree
						}

						sozArt {
							type = select
							label = Soziokulturelle Arbeitsfelder
							popover = Something.
							special = multiselect
							container-id = vs-select

							#defaultoption = Kein Merkmal ausgewählt

							loadOption = sozKat
						}

						

						/*
						fixed-text3 {
							type = fixed-text
							text = Ihre Veranstaltung ist mehreren Arten zuordenbar? Bitte wählen Sie weitere Arten aus:
						}

						copy-field2 {
							type = copy-field
							btn-text = Eine weitere Art hinzufügen

							target = #va-select
						}
						*/
					}
				}

				page4 {
					label = Terminierung
					icon = icon-4
					items {
						fixed-text1 {
							type = fixed-text
							text = Bitte terminieren Sie Ihre Veranstaltung
						}

						start-date {
							type = datepicker
							label = Startdatum
							popover = Something.

							time = true
							time-label = Uhrzeit
							time-start-label = Beginn:
							time-start-placeholder = 10:00
							time-end-label = Ende:
							time-end-placeholder = 12:00
							time-einlass-label = Einlass:
							time-einlass-placeholder = 10:00
							time-dauer-label = Dauer:
							time-dauer-placeholder = 1h 20min
							container-class = datepicker_1
						}

						fixed-text2 {
							type = fixed-text
							text = <strong>Die Veranstaltung geht über mehrere Tage?</strong>
						}

						fixed-text3 {
							type = fixed-text
							text = Definieren Sie ein Enddatum und die Öffnungszeiten während dieser Tage. Insofern Sie die Tage nicht mit Öffnungszeiten <br/> versehen, werden die Öffnungszeiten des ersten Tages vererbt. Sie können einzelne Tage auch ausblenden:
						}

						end-date {
							type = datepicker
							label = Enddatum
							popover = Something.

							container-class = datepicker_2

							js-event = daterange
						}
						timeline {
							type = timeline
							label = Tagesübersicht
							popover = Something.
							
							status = hidden master_datepicker_2

							js-event = daterange
							js-event-start = .datepicker_1
							js-event-end = .datepicker_2
						}
					}
				}

				page5 {
					label = Abbildungen
					icon = icon-5
					items {
						fixed-text1 {
							type = fixed-text
							text = <strong>Möchten Sie ein Bild hinterlegen?</strong>
						}

						fixed-text2 {
							type = fixed-text
							text = Bitte laden Sie nur Abbildungen hoch, bei denen Sie über alle notwendigen Rechte verfügen und die > 1000 Pixel sind!
						}

						multiple {
							type = multiple

							container-id = foto-multiple

							items {
								foto {
									type = fileupload
									label = Abbildung
									btn-text = Bild auswählen
									popover = Something.
								}

								rechte {
									type = checkbox
									label = Ich bestätige, dass das Bild frei von Rechten Dritter ist
									btn-text = Bild auswählen

									asFor = foto
								}

								urheber {
									type = input
									label = Urheber:in
									placeholder = Name der Urheber:in der Abbildung
									popover = Something.

									asFor = foto
								}

							}
						}

						/*
						fixed-text3 {
							type = fixed-text
							text = Sie möchten weitere Bilder hochladen?
						}

						copy-field1 {
							type = copy-field
							btn-text = Ein weiteres Bild hinzufügen

							target = #foto-multiple
						}
						*/
					}
				}

				page6 {
					icon = icon-6
					label = Zusammenfassung
					items {
						fixed-text1 {
							type = fixed-text
							text = Bitte prüfen Sie Ihre Eingaben und speichern, ändern oder ergänzen Sie die Daten
						}

						zusammenfassung {
							type = zusammenfassung
						}
						
						button-group {
							type = button-group
						}

					}
				}
/*
				page7 {
					icon = icon-7
					items {

					}
				}

				page8 {
					icon = icon-8
					items {

					}
				}
				*/
			}
		}
	}
}

plugin.tx_theatercollection.settings.editors.edit_performance < plugin.tx_theatercollection.settings.editors.new_event

plugin.tx_theatercollection.settings.editors.edit_performance {
	settings {
		form-name = edit_performance
		do = edit
	}
}


plugin.tx_theatercollection.settings.editors.new_ver_type {
	settings {
		form-name = new_ver_type
		do = new
		label = Kategorie erstellen oder bearbeiten
	}
	pages {
		page1 {
			items {
				name {
					type = input
					label = Name der Kategorie
					popover = Something.
					maxlength = 150
				}

				button-group {
					type = button-group
				}
			}
		}
	}
}

plugin.tx_theatercollection.settings.editors.edit_ver_type < plugin.tx_theatercollection.settings.editors.new_ver_type

plugin.tx_theatercollection.settings.editors.edit_ver_type {
	settings {
		form-name = new_ver_type
		do = edit
	}
}

plugin.tx_theatercollection.settings.editors.new_ver_art {
	settings {
		form-name = new_ver_art
		do = new
		label = Merkmal erstellen oder bearbeiten
	}

	pages {
		page1 {
			items {
				name {
					type = input
					label = Name des Merkmals
					popover = Something.
					maxlength = 150
				}

				button-group {
					type = button-group
				}
			}
		}
	}
}

plugin.tx_theatercollection.settings.editors.edit_ver_art < plugin.tx_theatercollection.settings.editors.new_ver_art

plugin.tx_theatercollection.settings.editors.edit_ver_art {
	settings {
		form-name = new_ver_art
		do = edit
	}
}

plugin.tx_theatercollection.settings.editors.new_location {
	settings {
		form-name = new_location
		do = new
		label = Veranstaltungsort erstellen oder bearbeiten
	}

	pages {
		page1 {
			icon = icon-1
			items {
				digicultid {
					type = input
					label = Digicultid
					popover = Digicultid falls vorhanden.
				}

				name {
					type = input
					label = Name des Ortes
					popover = Something.
					maxlength = 150
				}

				legalname {
					type = input
					label = Offizieller Name
					popover = Something.
					maxlength = 150
				}

				alternativname {
					type = input
					label = Alternativnet Name
					popover = Something.
					maxlength = 150
				}

				ort {
					type = input
					label = Ort
					popover = Something.
				}

				plz {
					type = input
					label = PLZ
					popover = Something.
				}

				strasse {
					type = input
					label = Strasse
					popover = Something.
				}
				lat {
					type = input
					label = Geodaten(Latitude)
					popover = Something.
					placeholder = 50.125432232
				}
				lon {
					type = input
					label = Geodaten(Longitude)
					popover = Something.
					placeholder = 10.125432232
				}
				geonames {
					type = input
					label = Geonames
					popover = Something.
				}
				kultur {
					type = select
					label = Kategorie (Kultursphäre)
					popover = Something.
					special = multiselect
					container-id = sok-select

					defaultoption = Keine Kategorie ausgewählt

					loadOption = kulturKat
				}
			}
		}
		page2 {
			icon = icon-3
			items {
				ansprechperson {
					type = input
					label = Ansprechperson
					popover = Something.
				}
				email {
					type = input
					label = E-Mail
					popover = Something.
				}
				telefon {
					type = input
					label = Telefon
					popover = Something.
				}
				mobil {
					type = input
					label = Mobil
					popover = Something.
				}
			}
		}
		page3 {
			icon = icon-5
			items {
				vorschausmall {
					type = multiple

					container-id = foto-vorschausmall

					items {
						foto {
							type = fileupload
							label = Vorschau klein
							btn-text = Bild auswählen
							popover = Something.
						}

						rechte {
							type = checkbox
							label = Ich bestätige, dass das Bild frei von Rechten Dritter ist
							btn-text = Bild auswählen

							asFor = foto
						}

						urheber {
							type = input
							label = Urheber
							placeholder = Name des Fotografen/Urheber der Abbildung
							popover = Something.

							asFor = foto
						}

					}
				}

				/*
				vorschaubig {
					type = multiple

					container-id = foto-vorschaubig

					items {
						foto {
							type = fileupload
							label = Vorschau groß
							btn-text = Bild auswählen
							popover = Something.
						}

						rechte {
							type = checkbox
							label = Ich bestätige, dass das Bild frei von Rechten Dritter ist
							btn-text = Bild auswählen

							asFor = foto
						}

						urheber {
							type = input
							label = Urheber
							placeholder = Name des Fotografen/Urheber der Abbildung
							popover = Something.

							asFor = foto
						}

					}
				}
				*/
			}
		}
		/*
		page4 {
			icon = icon-4
			items {
				urls {
					type = input
					label = URLs
					popover = Something.
				}
				meta {
					type = input
					label = Meta
					popover = Something.
				}
			}
		}
		*/
		page4 {
			icon = icon-6
			label = Zusammenfassung
			items {
				fixed-text1 {
					type = fixed-text
					text = Bitte prüfen Sie Ihre Eingaben und speichern, ändern oder ergänzen Sie die Daten
				}

				zusammenfassung {
					type = zusammenfassung
				}
				
				button-group {
					type = button-group
				}
			}
		}
	}
}

plugin.tx_theatercollection.settings.editors.edit_location < plugin.tx_theatercollection.settings.editors.new_location

plugin.tx_theatercollection.settings.editors.edit_location {
	settings {
		form-name = new_location
		do = edit
	}
}


plugin.tx_theatercollection.settings.editors.new_veranstalter {
	settings {
		form-name = new_veranstalter
		do = new
		label = Veranstalter erstellen oder bearbeiten
	}

	pages {
		page1 {
			items {
				name {
					type = input
					label = Name des Merkmals
					popover = Something.
					maxlength = 150
				}

				button-group {
					type = button-group
				}
			}
		}
	}
}

plugin.tx_theatercollection.settings.editors.edit_veranstalter < plugin.tx_theatercollection.settings.editors.new_veranstalter

plugin.tx_theatercollection.settings.editors.edit_veranstalter {
	settings {
		form-name = new_veranstalter
		do = edit
	}
}