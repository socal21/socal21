﻿<?xml version="1.0" encoding="UTF-8"?>

<!-- Change to annotation/documentation instead of comments -->

<xs:annotation>
	<xs:appinfo xmlns:dc="http://purl.org/dc/elements/1.1/" docstore.mik.ua/orelly/xml/schema/ch14_02.htm>
		<dc:creator>
		Thilo Pfennig <t.pfennig@soziokultur-sh.de>, 2020-2022
		</dc:creator>
		<dc:publisher>
			Landesarbeitsgemeinschaft Soziokultur Schleswig-Holstein e.V.,
			https://www.soziokultur-sh.de
			Heiligendammer Str.15, 24106 Kiel/Germany
		</dc:publisher>
		<dc:date>2022-02-11</dc:date>
		<dc:title>(Unofficial) XML Schema for MRH VADB XML</dc:title>
		<dc:description>
			This XML Schema enables the export of event data to the
			"Metropolregion Hamburg"/HHT. For more see https://mrh.events
		</dc:description>
		<dc:rights>
			This work is CC0 http://creativecommons.org/publicdomain/zero/1.0/
			To the extent possible under law, Landesarbeitsgemeinschaft
			Soziokultur Schleswig-Holstein e.V.  has waived all copyright and
			related or neighboring rights to SozioKulturKalender
			(https://api.soziokultur-sh.de/)
			This work is published from: Deutschland/Germany
		<dc:rights>
	</xs:appinfo>
	<xs:documentation xml:lang="de">
		Diese Schema-Datei dient zum Validieren von Exporten zur
		Metropolregion Hamburg Veranstaltungsdatenbank. Sie enthält einige
		Erweiterungen für weitere Nutzer:innen, die aber nicht zulasten der
		Kompatibilität gehen. Die Dokumentation findet sich auch innerhalb
		der Kommentare.

		Dieses Schema und die Dokumentation sind während der Programmierung der
		Schnittstellen für den SozioKulturKalender 2021 entstanden.  Weitere
		Informationen, die aktualisiert werden finden sich unterhalb der URL:
		https://socal21.gitlab.io/socal21-docs/api/mrh/

		Zu XML Schema: https://de.wikipedia.org/wiki/XML_Schema

		## Verwendete Syntax:

		* Die Typendefinition entsprechen immer dem Elementnamen plus angehängtem Type
		* zur einfacheren Suche!  <xs:element name="" type=""> "name" ist der Name des
		* Elements. "type" verweisen auf Definitionen zu dem Element <complexType>
		* https://www.data2type.de/xml-xslt-xslfo/xml-schema/element-referenz/xs-complextype-globale-definit
		* maxOccurs = Maximale Häufigkeit des Auftretens: "unbounded" = unbegrenzt
		* minOccurs = Minimale Häufigkeit. Wenn nicht vorhanden, wird "1" (ein mal)
		* angenommen!



		## Version: 2022-2

		* Die erste veröffentlichte Version des XML Schemas  und ist als Ergänzung der
		* Dokumentation der API gedacht

	-<xs:documentation>
	</xs:annotation>




<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" attributeFormDefault="unqualified" elementFormDefault="qualified">


<!-- <events> als Root Element für alle Veranstaltungen! -->
<!-- die Definition des Elements erfolgt unter <xs:complexType name="eventsType"> -->
	<xs:element name="events" type="eventsType"/>

<!-- <events> Elements. Hier wird das <events> Element genau definiert -->
	<xs:complexType name="eventsType">
		<xs:sequence>
			<xs:element name="Event" type="EventType"  maxOccurs="unbounded"/>
			<!-- es kann beliebig viele Events unterhalb von <events> geben! Aber außer dem Element <Element> ist kein Anderes erlaubt. -->
		</xs:sequence>
	</xs:complexType>

<!-- Beschreibung des Elements <Event> -->
<xs:complexType name ="EventType">
        <xs:sequence>
		<xs:element name="id" type="xs:positiveInteger" use="optional"/>
                <xs:element name="languages" type="languagesType"  />
                <xs:element name="title" type="titleType" />
                <xs:element name="shortDescription" type="shortDescriptionType" minOccurs="1"/> <!-- Es gibt keine lingDescription ( mehr) -->
                <xs:element name="link" type="linkType" minOccurs="0"  />
                <xs:element name="bookingLink" type="linkType" minOccurs="0"  />
                <xs:element name="categories" type="categoriesType"  />
                <xs:element name="criteria" type="criteriaType" minOccurs="0" />
                <xs:element name="pricing" type="pricingType" minOccurs="0"  />
                <xs:element name="eventDates" type="eventDatesType"  />
                <xs:element name="media" type="mediaTypeEvents" minOccurs="0"/>
		<xs:element name="cancelled" type="xs:boolean" minOccurs="0"/>
                <xs:element name="creationTime" type="xs:string" minOccurs="0" />
                <xs:element name="lastChangeTime" type="xs:string" minOccurs="0" />
                <xs:element name="location" type="locationType" />
                <xs:element name="contributor" type="contributorType" minOccurs="0" maxOccurs="unbounded" />
<!-- wird nicht benötigt               <xs:element name="targetGroups" type="targetGroupsType" minOccurs="0" nillable="true"/> -->
                <xs:element name="advanceBookingStartDate" minOccurs="0"  />
                <xs:element name="advanceBookingStartTime" minOccurs="0"  />
		<xs:element name="series" type="seriesType" minOccurs="0"   />
                <xs:element name="autoTranslated" minOccurs="0"  />
        </xs:sequence>
        <xs:attribute name="id" type="xs:positiveInteger" use="required"/>
</xs:complexType>



<!-- <categories> Element -->
<xs:complexType name="categoriesType">
<!-- Dieses Element enthält ein weiteres Element <Category>, aber kann mehrere enthalten -->
	<xs:sequence>
		<xs:element name="Category" maxOccurs="unbounded" type="CategoryType"/>
	</xs:sequence>
</xs:complexType>

<!-- Definition von <Category>. Enthält immer eine ID und kann Text enthalten -->
 <xs:complexType name="CategoryType">
	<xs:sequence>
		<xs:element name="Name" type="xs:string"/>
<!-- Zusätzlich ID (gleiche wie im Attribut) auch als Element: -->
		<xs:element name="id" type="xs:positiveInteger" use="optional"/>
	</xs:sequence>
<!-- Die ID der Kategorie, Quelle: MRHID -->
        <xs:attribute name="id" type="xs:string"/>
 </xs:complexType>


<xs:complexType name="I18nType" >
        <xs:sequence>
<!-- Es muss mindestens das Element <de> für einen deutschen Text geben -->
		<xs:element name="de" type="xs:string"/>
		<xs:element name="en" type="xs:positiveInteger"  minOccurs="0"/>
	</xs:sequence>
	<xs:attribute name="id" type="xs:positiveInteger" />
</xs:complexType>

        <!-- Criterias Elements -->
<xs:complexType name="criteriaType" >
        <xs:sequence>
		<xs:element name="Criterion" minOccurs="0" maxOccurs="unbounded"  type="CriterionType"/>
        </xs:sequence>
</xs:complexType>

<!-- "Merkmale" Es ist auch Content erlaubt -->
<xs:complexType name="CriterionType">
	<xs:sequence>
<!-- Diese Regel bewirkt, dass auch Content als String angegeben werden darf! -->
		<xs:element name="Name" type="xs:string"/>
<!-- Zusätzlich ID (gleiche wie im Attribut)  auch als Element: -->
		<xs:element name="id" type="xs:positiveInteger" use="optional"/>
	</xs:sequence>
<!-- Die ID der Kategorie, Quelle: MRHID -->
	<xs:attribute name="id" type="xs:positiveInteger" use="required"/>
</xs:complexType>


<!-- Languages Type Elements -->

<xs:complexType name="languagesType">
        <xs:sequence>
<xs:element name="Language" type="LanguageType"  maxOccurs="unbounded"/>
        </xs:sequence>
      <!-- valid language values are: de = 1, en = 2 -->
</xs:complexType>

<xs:complexType name="LanguageType">
	<xs:attribute name="id" type="xs:positiveInteger" default="1"/>
</xs:complexType>

<xs:simpleType name="NameTypeLang">
	<xs:restriction base="xs:language">
		<xs:enumeration value="de"/>
		<xs:enumeration value="en"/>
	</xs:restriction>
</xs:simpleType>


<xs:complexType name="titleType">
        <xs:all>
		<xs:element name="I18n" type="I18nType"/>
        </xs:all>
</xs:complexType>


<xs:complexType name="descriptionType">
        <xs:all>
		<xs:element name="I18n" type="I18nType" minOccurs="0"/>
        </xs:all>
</xs:complexType>

<xs:complexType name="barrierFreeInformationType">
        <xs:sequence>
                <xs:element name="I18n" type="I18nType" minOccurs="0" />
        </xs:sequence>
</xs:complexType>

<xs:complexType name="contact1Type">
        <xs:sequence>
                <xs:element name="contactName" type="xs:string" minOccurs="0"  />
                <xs:element name="salutation" type="xs:string" minOccurs="0" />
                <xs:element name="firstname" type="xs:string" minOccurs="0" />
                <xs:element name="lastname" type="xs:string" minOccurs="0" />
<!-- Auch mindestens Adresse angeben, wenn auch nicht vollständig: -->
                <xs:element name="address" type="addressType"  />
        </xs:sequence>
</xs:complexType>

<!-- Adressdefinitionen: -->

<xs:complexType name="addressType">
	<xs:sequence>
		<xs:element name="street" type="xs:string"/>
		<xs:element name="streetNo" type="xs:string" minOccurs="0"/>
		<xs:element name="zipcode" type="xs:string" minOccurs="0"/>
<!-- <city> Stadtname ist Pflicht: -->
		<xs:element name="city" type="xs:string"/>
		<xs:element name="country" type="xs:string" minOccurs="0" />
<!-- Aktuell von uns nicht exportiert:
		<xs:element name="phone1" type="xs:string" minOccurs="0" />
		<xs:element name="phone2" type="xs:string" minOccurs="0" />
		<xs:element name="fax" type="xs:string" minOccurs="0" />
		<xs:element name="email" type="xs:string" minOccurs="0" />
-->
		<xs:element name="homepage" type="homepageType" minOccurs="0" />
		<xs:element name="homepageLinkText" type="xs:anyType" minOccurs="0" />
	</xs:sequence>
</xs:complexType>


<!-- some simpleType location elements are used in different contexts: -->



<xs:complexType name="homepageType">
	<xs:sequence>
                <xs:element name="I18n" type="I18nType" />
	</xs:sequence>
</xs:complexType>

<xs:complexType name="geoInfoType">
        <xs:sequence>
                <xs:element name="GeoInfo" type="GeoInfoType"/>
        </xs:sequence>
</xs:complexType>

<xs:complexType name="GeoInfoType">
<xs:sequence>
                <xs:element name="coordinates" type="coordinatesType" />
                <xs:element name="street" type="xs:string" />
                <xs:element name="streetNo" type="xs:string" minOccurs="0"/>
                <xs:element name="zipcode" type="xs:string" minOccurs="0"/>
                <xs:element name="city" type="xs:string" />
                <xs:element name="country" type="xs:string" minOccurs="0" />
</xs:sequence>
</xs:complexType>

<xs:complexType name="coordinatesType">
	<xs:sequence>
                <xs:element name="latitude" type="xs:float" />
                <xs:element name="longitude" type="xs:float" />
	</xs:sequence>
</xs:complexType>



<!-- End of Pois elements -->


<xs:complexType name="pricingType">
        <xs:sequence>
                <xs:element name="fromPrice" type="xs:anyType" minOccurs="0"/>
                <xs:element name="toPrice" type="xs:anyType" minOccurs="0"/>
                <xs:element name="absolutePrice" nillable="true" type="xs:anyType" minOccurs="0"/>
                <xs:element name="freeOfCharge" type="xs:boolean" minOccurs="0"/>
                <xs:element name="priceDescription"  nillable="true" type="xs:anyType" minOccurs="0"/>
                <xs:element name="priceUnit"  nillable="true" type="xs:anyType" minOccurs="0"/>
        </xs:sequence>
        <xs:attribute name="type" type="xs:string"/>
</xs:complexType>


<!-- short description element -->

<xs:complexType name="shortDescriptionType">
        <xs:sequence>
                <xs:element name="I18n" type="I18nType"  />
        </xs:sequence>
</xs:complexType>


<!-- Adressen -->

<xs:complexType name="AddressPoiType">
	<xs:sequence>
                <xs:element name="languages" type="languagesType"  />
                <xs:element name="types" type="typesType"  />
                <xs:element name="title" type="titleType" />
                <xs:element name="contact1" type="contact1Type" />
                <xs:element name="geoInfo" type="geoInfoType" />
	</xs:sequence>
<!-- Momentan ist die ID des Ortes optional, da es auch Orte ohne ID gibt. TODO -->
        <xs:attribute name="id" type="xs:positiveInteger" use="optional"/>
</xs:complexType>


<xs:complexType name="locationType">
        <xs:sequence>
<!-- QUICKFIX: mehrere Adressen sind möglich, wahrscheinlich aber nicht zulässig -->
                <xs:element name="AddressPoi" type="AddressPoiType" maxOccurs="unbounded" />
	</xs:sequence>
</xs:complexType>



<xs:complexType name="typesType">
        <xs:sequence>
                <xs:element name="AddressPoiType" type="AddressPoiTypeType" maxOccurs="2"  />
	</xs:sequence>
</xs:complexType>


<xs:complexType name="AddressPoiTypeType">
	<!-- valid IDs are: 2 = contributor, 3 = eventlocation
Wir setzen nur "3", weil wir nur eventlocations ausspielen.
 -->
        <xs:attribute name="id" type="xs:positiveInteger" use="required" fixed="3"/>
</xs:complexType>


<xs:complexType name="linkType">
        <xs:sequence>
                <xs:element name="I18n" type="I18nType" minOccurs="0"/>
        </xs:sequence>
</xs:complexType>

<xs:complexType name="contributorType">
        <xs:sequence>
                <xs:element name="AddressPoi" type="AddressPoiType" />
        </xs:sequence>
        <xs:attribute name="multiple" type="xs:boolean"/>
</xs:complexType>

<xs:complexType name="mediaTypeEvents">
        <xs:sequence>
                <xs:element name="EventImage" type="EventImageType" minOccurs="0" maxOccurs="unbounded"/>
                <xs:element name="EventVideo" type="EventVideoType" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="multiple" type="xs:boolean"/>
</xs:complexType>

<!-- event image elements -->
<xs:complexType name="EventImageType">
        <xs:sequence>
                <xs:element name="pooledMedium" type="pooledMediumType" minOccurs="0" />
               <xs:element name="imageType" type="imageTypeType"  />
	</xs:sequence>
</xs:complexType>


<xs:complexType name="pooledMediumType">
	<xs:sequence>
                <xs:element name="PooledEventMedium" type="PooledEventMediumType"  />
	</xs:sequence>

</xs:complexType>

<xs:complexType name="PooledEventMediumType">
	<!-- valid file extensions: jpeg, jpg, gif, png -->
	<xs:sequence>
		<xs:element name="title" type="titleType" minOccurs="0" />
		<xs:element name="description" type="descriptionType" minOccurs="0" />
                <xs:element name="copyright" type="copyrightType" minOccurs="0" />
	</xs:sequence>
        <xs:attribute name="url" type="xs:anyURI" use="required"/>
</xs:complexType>




<xs:complexType name="EventVideoType">
        <xs:sequence>
                <xs:element name="link" type="linkType" minOccurs="0" />
                <xs:element name="title" type="titleType"  />
                <xs:element name="width" type="xs:anyType" nillable="true" minOccurs="0" />
                <xs:element name="height" type="xs:anyType" nillable="true" minOccurs="0" />
                <xs:element name="description" type="descriptionType" minOccurs="0" />
                <xs:element name="copyright" type="copyrightType" minOccurs="0" />
                <xs:element name="deeplink" type="xs:anyURI" minOccurs="0" />
        </xs:sequence>
        <xs:attribute name="id" type="xs:positiveInteger" use="required"/>
</xs:complexType>

        <!-- attention there is imageType an ImageType - two distinct elements -->
<xs:complexType name="imageTypeType">
        <xs:sequence>
                <xs:element name="ImageType" type="ImageTypeType"  />
        </xs:sequence>
</xs:complexType>


<xs:complexType name="ImageTypeType">
        <xs:attribute name="id" type="xs:positiveInteger" use="required"/>
</xs:complexType>

<!-- copyright declaration for images is a must have -->
<xs:complexType name="copyrightType">
        <xs:sequence>
                <xs:element name="I18n" type="I18nType"  />
        </xs:sequence>
</xs:complexType>

<!-- just guessing: -->
<xs:complexType name="seriesType">
        <xs:sequence>
                <xs:element name="EventSeries" type="EventSeriesType" minOccurs="0" />
        </xs:sequence>
</xs:complexType>

<xs:complexType name="EventSeriesType">
        <xs:sequence>
                <xs:element name="I18n" type="I18nType" />
        </xs:sequence>
        <xs:attribute name="id" type="xs:positiveInteger" use="required"/>
</xs:complexType>

<xs:complexType name="eventDatesType">
        <xs:sequence>
                <xs:element name="SpecificEventDate" type="SpecificEventDateType"  maxOccurs="unbounded"/>
        </xs:sequence>
        <xs:attribute name="multiple" type="xs:boolean"/>
</xs:complexType>

<xs:complexType name="SpecificEventDateType">
        <xs:sequence>
                <xs:element name="date" type="xs:date" />
                <xs:element name="startTime" type="xs:time" />
                        <!-- type should be xs:duration but some instances are empty: -->
                <xs:element name="duration" nillable="true" type="xs:anyType" minOccurs="0"/>
		<xs:element name="cancelled" type="xs:boolean" minOccurs="0"/>
                <xs:element name="bookingLink" nillable="true" type="xs:anyURI" minOccurs="0"/>
        </xs:sequence>
</xs:complexType>




</xs:schema>
